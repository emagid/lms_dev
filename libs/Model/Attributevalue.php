<?php 

namespace Model; 

class Attributevalue extends \Emagid\Core\Model {

	static $tablename = 'attributevalue'; 
	
	public static $fields =  [
		'frontend_label1', 
		'attribute_code1',
		'attribute_id',
		'value_id',
		'value'
	];
	
}