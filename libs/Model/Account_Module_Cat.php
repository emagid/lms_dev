<?php

namespace Model;

use Emagid\Core\Model;

class Account_Module_Cat extends Model{
    static $tablename = 'account_module_cats';
    static $fields = [
        'account_id',
        'module_id',
        'ucat_id'
    ];

    public static function getAssignedCats($account_id, $module_id){
        $amys = [];
        foreach (self::getList(['where'=>"account_id = $account_id AND module_id = $module_id"]) as $amy){
            $amys[$amy->ucat_id] = true;
        }
        return $amys;
    }
}