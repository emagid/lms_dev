<?php
namespace Model;

use Carbon\Carbon;

class Answers extends \Emagid\Core\Model
{

    public static $tablename = "answers";

    public static $fields = [
        'question_id',
        'title',
        'number',
        'correct'
    ];

}
