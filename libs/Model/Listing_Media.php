<?php

namespace Model;

class Listing_Media extends \Emagid\Core\Model {

	static $tablename = "listing_media";
	public static $fields = ['listing_id', 'media_id'];

	public function getMediaPath()
	{
		$media = Media::getItem($this->media_id);
		if($media){
			return $media->path();
		} else {
			return null;
		}
	}

	public function getMedia()
	{
		return Media::getItem($this->media_id);
	}
}
