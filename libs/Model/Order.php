<?php
namespace Model;

class Order extends \Emagid\Core\Model {
	static $tablename = "public.order";
    public static $status = ['New', 'Paid', 'Declined', 'Shipped', 'Complete', 'Delivered', 'Canceled', 'Discontinued', 'Returned', 'Processed'];

    //public static $status = ['New', 'Paid', 'Payment Declined', 'Shipped', 'Complete', 'Pending Paypal', 'Canceled', 'Imported as Processing', 'Returned'];

	public static $fields  = [
		'insert_time',
        'viewed'=>['type'=>'boolean'],
        // billing info
        'bill_first_name'=>['required'=>true, 'name'=>'Bill - First Name'],
        'bill_last_name'=>['required'=>true, 'name'=>'Bill - Last Name'],
        'bill_address'=>['required'=>true, 'name'=>'Bill - Address'],
        'bill_address2',
        'bill_city'=>['required'=>true, 'name'=>'Bill - City'],
        'bill_state'=>['name'=>'Bill - State'],
        'bill_country'=>['required'=>true, 'name'=>'Bill - Country'],
        'bill_zip'=>['required'=>true, 'type'=>'numeric', 'name'=>'Bill - Zip'],
        //shipping info
        'ship_first_name'=>['required'=>true, 'name'=>'Shipping - First Name'],
        'ship_last_name'=>['required'=>true, 'name'=>'Shipping - Last Name'],
        'ship_address'=>['required'=>true, 'name'=>'Shipping - Address'],
        'ship_address2',
        'ship_city'=>['required'=>true, 'name'=>'Shipping - City'],
        'ship_state'=>['name'=>'Shipping - State'],
        'ship_country'=>['required'=>true, 'name'=>'Shipping - Country'],
        'ship_zip'=>['required'=>true, 'type'=>'numeric', 'name'=>'Shipping - Zip'],
        //card info
        'cc_number'=>['type'=>'numeric', 'name'=>'Credit Card Number'],
        'cc_expiration_month'=>['type'=>'numeric', 'name'=>'Credit Card Expiration Month'],
        'cc_expiration_year'=>['type'=>'numeric', 'name'=>'Credit Card Expiration Year'],
        'cc_ccv'=>['type'=>'numeric', 'name'=>'Credit Card CCV'],
        // price info
        'payment_method'=>['required'=>true, 'type'=>'numeric'], //1 - Credit Card, 2 - Bank Wire
        'shipping_method'=>['required'=>true, 'name'=>'Shipping Method'],
        'shipping_cost'=>['numeric'],
        'subtotal'=>['numeric'],
        'total'=>['numeric'],
        'tax',
        'tax_rate',
        //shipping and tracking info
        'status',
        'tracking_number',
        'user_id',
        'coupon_code',
        'coupon_type',
        'coupon_amount',
        'gift_card',
        'email'=>['type'=>'email'],
        'phone'=>['required'=>true],
        'note',
        'comment',
        'fraud_status'
	];

	public function beforeValidate(){
		if ($this->payment_method == 1){
			self::$fields['cc_number']['required'] = true;
			self::$fields['cc_expiration_month']['required'] = true;
			self::$fields['cc_expiration_year']['required'] = true;
		}
		if ($this->bill_country == 'United States'){
			self::$fields['bill_state']['required'] = true;
		}
		if ($this->ship_country == 'United States'){
			self::$fields['ship_state']['required'] = true;
		}
	}

	public static function getDashBoardData($year, $month){
		$data = new \stdClass();

		$sql = " FROM ".self::$tablename." WHERE ";
		$sql .= " EXTRACT(MONTH FROM insert_time)::INTEGER = ".$month;
		$sql .= " AND EXTRACT(YEAR FROM insert_time)::INTEGER = ".$year;
		$sql .= " AND status = '".self::$status[4]."'";

		$data->monthly_sales = 0;
		$monthly_sales = self::execute("SELECT SUM(total) AS s ".$sql);
		if (count($monthly_sales) > 0){
			$data->monthly_sales = $monthly_sales[0]['s'];
		}

		$data->gross_margin = 0;

		$data->monthly_orders = 0;
		$monthly_orders = self::execute("SELECT COUNT(id) AS c ".$sql);
		if (count($monthly_orders) > 0){
			$data->monthly_orders = $monthly_orders[0]['c'];
		}

		return $data;
	}
	
        public static function search($keywords, $limit = 20){
        $sql = "select id,ship_first_name, ship_last_name, bill_first_name,bill_last_name from public.order where active = 1 and (";
        if (is_numeric($keywords)){
            $sql .= "id = ".$keywords." or ";
        }
        $sql .= " lower(email) like '%".strtolower(urldecode($keywords))."%' or lower(ship_first_name) like '%".strtolower(urldecode($keywords))."%' or lower(ship_last_name) like '%".strtolower(urldecode($keywords))."%' or lower(bill_last_name) like '%".strtolower(urldecode($keywords))."%' or lower(bill_first_name) like '%".strtolower(urldecode($keywords))."%') limit ".$limit;
         
        return self::getList(['sql' => $sql]);
    }
}










































