<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/7/16
 * Time: 11:38 AM
 */

namespace Model;

class MediaType extends \Emagid\Core\Model {

    static $tablename = "media_type";
    public static $fields = ['name'];

}
