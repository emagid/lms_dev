<?php
namespace Model;

use Carbon\Carbon;

class Certificates extends \Emagid\Core\Model
{

    public static $tablename = "certificates";

    public static $fields = [
        'user_id',
        'course_id',
        'issued_at',
        'downloaded_at',
        'last_download_ip',
        'logo',
        'signature_name',
        'signature_title',
        'signature_image',
        'access_token',
        'course_title',
        'user_full_name',
        'awarded_by',
    ];
}
