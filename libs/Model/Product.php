<?php
namespace Model;

class Product extends \Emagid\Core\Model
{

    public static $tablename = "product";

    public static $fields = [
        'brand',
        'name' => ['required' => true],
        'price' => ['type' => 'numeric', 'required' => true],
        'description',
        'gender', //[1=>"Unisex", 2=>"Men", 3=>"Women"]
        'msrp',
        'quantity',
        'slug' => ['required' => true, 'unique' => true],
        'tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'featured_image',
        'length',
        'width',
        'height',
        'condition',
        'type',
        'color',
        'size',
        'availability', // TODO st-dev: replaced day_delivery with availability
        'age_group',
        'sub_brand',
        'alias',
        'options'
    ];

    static $relationships = [
        [
            'name' => 'product_category',
            'class_name' => '\Model\Product_Category',
            'local' => 'id',
            'remote' => 'product_id',
            'remote_related' => 'category_id',
            'relationship_type' => 'many'
        ]
    ];

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id, name, featured_image, mpn,mpn2,mpn3, price, msrp, slug from product where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "id = " . $keywords . " or ";
        }
        $sql .= " lower(name) like '%" . strtolower(urldecode($keywords)) . "%' or lower(mpn) like '%" . strtolower(urldecode($keywords)) . "%'
        or lower(mpn2) like '%" . strtolower(urldecode($keywords)) . "%'
        or lower(mpn3) like '%" . strtolower(urldecode($keywords)) . "%') order BY mpn ASC limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function getPriceSum($productsStr)
    {
        $total = 0;
        foreach (explode(',', $productsStr) as $productId) {
            $product = self::getItem($productId);
            $total += $product->price;
        }
        return $total;
    }

    public function getAvailability(){
        switch($this->availability){
            case 0:
                return 'Inactive';break;
            case 1:
                return 'In Stock';break;
            case 2:
                return 'Active';break;
        }
        return null;
    }

    public function getSubBrands(){
        return Brand::getList(['where'=>"parent_id = $this->brand"]);
    }

    public static function generateToken()
    {
        return md5(uniqid(rand(), true));
    }

    public function featuredImage(){
        $product = self::getItem($this->id);
        if($product->featured_image != ''){
            return $product->featured_image;
        } elseif($product = Product_Image::getItem(null,['where'=>"product_id = {$this->id} and image != ''"])) {
            return $product->image;
        } else {
            return '';
        }
    }
}


































