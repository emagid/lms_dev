<?php

namespace Model;

use Emagid\Core\Model;

class File extends Model{
    static $tablename = 'files';
    static $fields = [
    	'name',
        'file',
        'default_file',
    ];

}