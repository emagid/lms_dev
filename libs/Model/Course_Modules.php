<?php

namespace Model;

use Emagid\Core\Model;

class Course_Modules extends Model{
    static $tablename = 'course_modules';
    static $fields = [
        'course_id',
        'title',
        'status',
        'number',
        'keyword',
        'short_description',
        'large_description',
        'course_image',
        'video_background_image',
        'video_url',
        'video_duration_in_seconds',
        'suggested_year_level'
    ];

    public function getQuestions(){
        return Question::getList(['where'=>"course_module_id = $this->id"]);
    }

    public function getEnrolledStudentCount($account_id = 0){
        $accountCheck = '';
        if($account_id){
            $accountCheck = 'AND u.account_id = '.$account_id;
        }
        $sql = "SELECT count(*) as count FROM user_course_modules ucm, users u, course_modules cm WHERE ucm.user_id = u.id $accountCheck AND u.active = 1 AND ucm.active = 1 AND cm.id = ucm.course_module_id AND cm.id = $this->id";
        global $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults($sql);
        return $results[0]['count'];
    }

    public function get_course(){
        return Course::getItem($this->course_id);
    }

    public function getVideoDuration($seconds){
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;
        return "$minutes:$seconds";
    }

    public function video_duration_in_milliseconds(){
        return $this->video_duration_in_seconds * 1000;
    }

    public function questions_count(){
        
        return \Model\Question::getCount(['where'=>"course_module_id = ".$this->id]);
    }
}