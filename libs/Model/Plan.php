<?php

namespace Model;

use Emagid\Core\Model;

class Plan extends Model{
    static $tablename = 'plan';
    static $fields = [
        'name',
        'student_limit'=>['type'=>'numeric'],
        'icon',
        'features',
        'display',
        'account_category_id',
    ];

    public function getTiers(){
        return Plan_Tiers::getList(['where'=>"plan_id = {$this->id}",'orderBy'=>"min_users ASC, max_users ASC"]);
    }

    public function getCourses(){
        return Course::getList(['sql'=>"SELECT * FROM courses WHERE active = 1 AND id IN (SELECT course_id FROM plan_courses WHERE active = 1 AND plan_id = $this->id)"]);
    }

    public function getFeatures(){
        return explode('|',$this->features);
    }

    public static function search($keywords, $limit = 20) {
        $sql = "select * from plan where active = 1 and (";
        $sql .= "lower(name) like '%". strtolower(urldecode($keywords)). "%') limit " . $limit;
        return self::getList(['sql'=> $sql]);
    }
}