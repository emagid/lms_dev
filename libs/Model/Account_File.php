<?php

namespace Model;

use Emagid\Core\Model;

class Account_File extends Model{
    static $tablename = 'account_files';
    static $fields = [
    	'file_id',
        'account_id',
    ];

}