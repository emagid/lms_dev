<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/18/16
 * Time: 4:40 PM
 */

namespace Model;

class Location extends \Emagid\Core\Model
{
    static $tablename = "location";
    public static $fields = [
        'name',
        'admin_id',
        'own_type', // 0 managed, 1 owned
        'submit',
        'type'
    ];

    public static $MAP = [
        'rsb-cleo' => 'cleo south beach',
        'cleo south beach' => 'cleo south beach',
        'bazaar south beach' => 'bazaar south beach',
        'sls sb - bazaar' => 'bazaar south beach',
        'cleo- 3rd st' => 'cleo- 3rd st',
        'bar centro south beach' => 'bar centro south beach',
        'hyde beach kitchen and cocktail' => 'hyde beach kitchen + cocktail',
        'hyde beach kitchen + cocktails' => 'hyde beach kitchen + cocktail',
        'hyde beach kitchen + cocktail' => 'hyde beach kitchen + cocktail',
        'hyde k+c hallendale' => 'hyde beach kitchen + cocktail',
        'hyde miami aaa arena' => 'hyde miami aaa arena',
        'hyde miami arena' => 'hyde miami aaa arena',
        'hyde south beach' => 'hyde south beach',
        'katsuya south beach' => 'katsuya south beach',
        'raleigh hotel f&b' => 'raleigh miami rms',
        'raleigh miami rms' => 'raleigh miami rms',
        'raleigh restaurant' => 'raleigh miami rms',
        'sls sb hotel' => 'sls sb hotel',
        'sls south beach' => 'sls sb hotel',
        'the raleigh hotel miami' => 'the raleigh hotel miami',
        'the raleigh hotel miama' => 'the raleigh hotel miami',
        'the redbury sb hotel' => 'the redbury sb hotel',
        'the redbury south beach' => 'the redbury sb hotel',
        'bazaar beverly hills' => 'bazaar beverly hills',
        'sls bh - bazaar' => 'bazaar beverly hills',
        'create' => 'create',
        'greystone manor' => 'greystone manor',
        'sls bkl - m schwartz dining' => 'sls bkl - m schwartz dining',
        'sls bazaar mar' => 'sls bazaar mar',

        //Update Greystone = Nightingale Plaza
        'nightingale plaza' => 'greystone manor',

        'nightingale' => 'greystone manor',

        'hyde staples center' => 'hyde staples',
        'hyde staples' => 'hyde staples',
        'hyde kitchen' => 'hyde sunset kitchen + cocktails',
        'hyde sunset kitchen + cocktails' => 'hyde sunset kitchen + cocktails',
        'hyde sunset kitchen + cocktail' => 'hyde sunset kitchen + cocktails',
        'katsuya brentwood' => 'katsuya brentwood',
        'katsuya la live' => 'katsuya downtown',
        'katsuya downtown' => 'katsuya downtown',
        'katsuya glendale' => 'katsuya glendale',
        'katsuya hollywood' => 'katsuya hollywood',
        'sls bh hotel' => 'sls bh hotel',
        'sls beverly hills' => 'sls bh hotel',
        'the abbey' => 'the abbey',
        'cleo' => 'cleo',
        'cleo hollywood' => 'cleo',
        'the colony' => 'the colony',
        'the emerson theatre' => 'the emerson theatre',
        'the library' => 'the library',
        'sls bh - tres' => 'tres beverly hills',
        'tres beverly hills' => 'tres beverly hills',
        'the redbury hw hotel' => 'the redbury hw hotel',
        'the redbury hollywood' => 'the redbury hw hotel',
        'sayers club' => 'the sayers club',
        'the sayers club' => 'the sayers club',
        'double barrel' => 'double barrel',
        'hyde bellagio' => 'hyde bellagio',
        'the redbury ny' => 'the redbury ny',
        'hyde t-mobile arena' => 'hyde t-mobile arena',
        'doheny room' => 'doheny room',
//        'sls bh - ird' => 'tres beverly hills',
        'vetro' => 'doheny room',
        'little hyde' => 'little hyde',
        'hyde sunset' => 'little hyde',
        'cleo downtown la' => 'cleo downtown la',

        // start umami venue
        'anaheim' => 'anaheim',
        'arts district' => 'arts district',
        'broadway' => 'broadway',
        'brookfield place' => 'brookfield place',
        'burbank' => 'burbank',
        'costa mesa' => 'costa mesa',
        'greenwich village ny' => 'greenwich village ny',
        'grove' => 'grove',
        'hollywood' => 'hollywood',
        'los feliz' => 'los feliz',
        'marina dist sf' => 'marina dist sf',
        'oakland' => 'oakland',
        'palo alto' => 'palo alto',
        'pasadena' => 'pasadena',
        'santa monica' => 'santa monica',
        'soma' => 'soma',
        'studio city' => 'studio city',
        'thousand oaks' => 'thousand oaks',
        'west loop' => 'west loop',
        'wicker park' => 'wicker park',
        'williamsburg' => 'williamsburg',
        'hermosa' => 'hermosa',
        'hudson hotel' => 'hudson hotel',
    ];

    static $PRINT = [
        'sls sb hotel' => 'SLS South Beach',
        'sls bh hotel' => 'SLS Beverly Hill',
        'sls bazaar mar' => 'SLS Bazaar Mar',
        'the redbury hw hotel' => 'The Redbury Hollywood',
        'the redbury ny' => 'The Redbury New York',
        'the redbury sb hotel' => 'The Redbury South Beach',
        'hyde miami aaa arena' => 'Hyde Miami AAA Arena',
        'hyde staples' => 'Hyde Staples Center',
        'cleo' => 'Cleo Hollywood',
        'greystone manor' => 'Nightingale Plaza',
        'cleo downtown la' => 'Cleo LA Live',
        'sls bkl - m schwartz dining' => 'Fi’Lia',
        'cleo- 3rd st' => 'Cleo- 3rd St'
    ];

    static $UMAMI_LOCATIONS = [
        'anaheim',
        'arts district',
        'broadway',
        'brookfield place',
        'burbank',
        'costa mesa',
        'greenwich village ny',
        'grove',
        'hollywood',
        'los feliz',
        'marina dist sf',
        'oakland',
        'palo alto',
        'pasadena',
        'santa monica',
        'soma',
        'studio city',
        'thousand oaks',
        'west loop',
        'wicker park',
        'williamsburg',
        'hermosa',
        'hudson hotel',
    ];

    public static function umamiWhereInBuilder()
    {
        $locations = implode("','", self::$UMAMI_LOCATIONS);
        return "('$locations')";
    }

    public function getPrefix()
    {
        if(in_array($this->name, self::$UMAMI_LOCATIONS)){
            return 'UMAMI - ';
        } else {
            return null;
        }
    }

    public static function printName($name)
    {
        if(isset(self::$PRINT[$name]) && self::$PRINT[$name]){
            return ucwords(self::$PRINT[$name]);
        }

        return ucwords($name);
    }

    public static function findLocation($name)
    {
        $name = str_replace('?', '-', $name);
        $name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $name);
        $name = str_replace('amp;', '', $name);
        $name = strtolower($name);
        $name = trim($name);

        $location = Location::getItem(null, ['where' => "name = '$name'"]);
        if (isset(self::$MAP[$name])) {
            return self::$MAP[$name];
        } elseif($location){
            return $location->name;
        } else {
            return null;
        }
    }

    public function getOriginName()
    {
        $array = explode('-', $this->name);
        if (count($array) == 2) {
            $name = $array[0];
        } else {
            // remove last word
            unset($array[count($array) - 1]);

            // glue with -, because otherwise it won't generate that
            $name = implode('-', $array);
        }

        $name = str_replace('amp;', '', $name);
        return $name;
    }

    public function isOwned()
    {
        return $this->own_type == 1;
    }

    public function isManaged()
    {
        return $this->own_type == 0;
    }

    public static function getType($location)
    {
        $location = self::findLocation($location);
        if(!$location){
            return null;
        }

        $locationModel = self::getItem(null, ['where' => "name = '$location'"]);
        return $locationModel->type;
    }
}