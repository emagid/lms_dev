<?php

namespace Model;

use Emagid\Core\Model;

class Plan_Courses extends Model{
    static $tablename = 'plan_courses';
    static $fields = [
        'plan_id',
        'course_id',
        'modules'
    ];
}