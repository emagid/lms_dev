<?php

namespace Model;

use Emagid\Core\Model;

class Client extends Model{
    static $tablename = 'client';
    static $fields = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'comments',
        'address',
        'city',
        'state',
        'zip',
    ];

    public function fullName(){
        return $this->first_name .' '. $this->last_name;
    }
}