<?php
namespace Model;

class Article extends \Emagid\Core\Model {
 
    static $tablename = "article";

    public static $fields = [
    	'insert_time',
  		'name' => ['required'=>true],
  		'slug' => ['required'=>true, 'unique'=>true],
  		'meta_title', 
		'meta_keywords', 
		'meta_description',
  		'image',
  		'author',
  		'content'
    ];
  
}