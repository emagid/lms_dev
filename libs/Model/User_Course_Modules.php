<?php

namespace Model;

use Emagid\Core\Model;

class User_Course_Modules extends Model{
    static $tablename = 'user_course_modules';
    static $fields = [
        'user_id',
        'course_module_id',
        'video_progress_in_milliseconds',
        'video_watched_count',
        'correct_responses_count',
        'quiz_taken_count',
        'created_at',
        'updated_at',
        'status',
        'video_progress_in_percent',
        'current'
    ];

    public function getVideoProgressInSeconds($milliseconds){
        return ($milliseconds/1000);
    }

    public function track_video_progress($video_progress_in_seconds){
        if(self::should_track_progress() && $video_progress_in_seconds != 0){
            $video_progress_in_milliseconds = $video_progress_in_seconds*1000;
            $course_module = \Model\Course_Modules::getItem($this->course_module_id);
            $video_progress_in_percent = $video_progress_in_milliseconds / $course_module->video_duration_in_milliseconds();
            $updateSql = "UPDATE ".self::$tablename." SET video_progress_in_milliseconds = ".$video_progress_in_milliseconds.", video_progress_in_percent = ".$video_progress_in_percent.", status = 'in_progress' WHERE id = ".$this->id; 
            self::execute($updateSql);
        }
    }

    public function should_track_progress(){
        $bool = $this->status != 'passed' || $this->status != 'finished';
        return $bool;
    }

    public function can_watch_video($rewatch = false){
        $bool = $this->status == 'not_started' || $this->status == 'in_progress' || $this->status == 'passed' || $this->status == 'failed' || $rewatch == true;
        return $bool; 
    }

    public function can_take_quiz(){
        
        return $this->status != 'passed';
    }

    public function unanswered_question(){
        $unanswered = [];
        $course_module_questions = \Model\Questions::getList(['where'=>"course_module_id = ".$this->course_module_id]);

    }

    public function last_question(){

        return unanswered_question() == 0;
    }
}