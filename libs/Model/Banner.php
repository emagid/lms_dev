<?php 

namespace Model;

class Banner extends \Emagid\Core\Model {
	static $tablename = 'banner';

	public static $fields = [
		'title',
		'image',
		'link',
		'is_deal'=>['type'=>'boolean'],
		'featured_id'
	];
}