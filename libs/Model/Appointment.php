<?php

namespace Model;

use Emagid\Core\Model;

class Appointment extends Model{
    static $tablename = 'appointment';
    static $fields = [
        'opportunities_id',
        'lists',
        'date',
        'location',
        'email_notification'
    ];

    function getOpportunity(){
        return Opportunities::getItem($this->opportunities_id);
    }
    function getListingList($format = ', '){
        $listing = json_decode($this->lists,true);
        $list = array_map(function($data){return Listing::getItem($data)->getAddress();},$listing);
        return implode($format,$list);
    }
    function getFormattedDate($format = 'Y-m-d'){
        return date($format,strtotime($this->date));
    }
}