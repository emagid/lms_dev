<?php 

namespace Model;

class Listing extends \Emagid\Core\Model {
	static $tablename = 'listing';

	public static $fields = [
		'address',
		'state',
		'city',
		'zipcode',
		'type',
		'sq',
		'use_for',
		'term',
		'bedroom',
		'bathroom',
		'condo_fee',
		'real_tax',
		'lot_size',
		'description',
		'price_month',
		'price_day',
		'price_week',
		'deposit',
		'building_name',
		'access_type',
		'payout',
		'company',
		'contact_name',
		'contact_phone',
		'company_name',
		'company_phone',
		'listing_id',
		'unit_num',
		'open_house',
		'close_house',
		'last_updated',
		'availability',
		'available_immediately',
		'status',
		'active_listing',
		'furnished',
	];

	CONST COMMERCIAL = 1;
	CONST RESIDENTIAL = 2;

	static $accessType = ['KWDM','Onsite','KIO#'];
	static $payout = ['Open','Co-Broke','House','OP'];

	function getAddress(){
		return $this->address.', '.$this->city.' '.$this->state.' '.$this->zip;
	}

	public function getMedias()
	{
		$data = [];
		foreach(Listing_Media::getList(['where' => "listing_id = {$this->id}"]) as $listingMedia){
			if($media = $listingMedia->getMedia()){
				$data[] = $media;
			}
		}

		return $data;
	}
}