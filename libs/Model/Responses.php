<?php
namespace Model;

use Carbon\Carbon;

class Responses extends \Emagid\Core\Model
{

    public static $tablename = "responses";

    public static $fields = [
        'answer_id',
        'response_set_id',
    ];

}