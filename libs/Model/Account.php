<?php
namespace Model;

class Account extends \Emagid\Core\Model {
    static $tablename = "accounts";
    static $statuses = ['new','pending','approved','active','inactive'];

    public static $fields = [
        'name',
        'subdomain',
        'logo',
        'status',
        'main_color',
        'button_color',
        'contact_name',
        'contact_email',
        'contact_phone',
        'contact_available_hours',
        'contact_additional_comments',
        'certificate_logo',
        'certificate_signature_name',
        'certificate_signature_title',
        'certificate_signature_image',
        'students_count',
        'managers_count',
        'certified_students_count',
        'main_course_id',
        'terms_and_conditions',
        'certificate_awarded_by',
        'videos_enabled',
        'brand_id',
        'max_user_password_length',
        'created_at',
        'plan_id',
        'account_category_id',
    ];

    public static function search($keywords, $limit = 20)
    {
        $sql = "select * from accounts where active = 1 and (";
        
        $sql .= " lower(name) like '%" . strtolower(urldecode($keywords))."%') limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public function getUserCount($type = 'student'){
        return User::getCount(['where'=>"account_id = $this->id AND role = '$type'"]);
    }

    public function getMaxUserCount(){

        $plan = \Model\Plan::getItem(null, ['where'=>"id = $this->plan_id "]);
        if($plan){
            return $plan->student_limit;
        }else{
            return 1;    
        }
    }
    public function getRemainingUserCount(){
        $total_users = \Model\User::getCount(['where'=>"account_id = $this->id"]);
        if($this->getMaxUserCount() >= $total_users){
            return ($this->getMaxUserCount() - $total_users);
        }else{
            return "-";
        }
    }

    public function getCertifiedUserCount(){
       $sql = "SELECT count(*) AS count  FROM certificates c, users u WHERE u.account_id = $this->id AND c.user_id = u.id AND u.active = 1 AND c.active = 1";
       global $emagid;
       $db = $emagid->getDb();
       $results = $db->getResults($sql);
       return $results[0]['count'];
    }

    public function getAccountPlan(){
        if($this->plan_id != ""){
            $plan = Plan::getItem(null,['where'=>"id = $this->plan_id"]);
            return $plan->name;
        }else{
            return "None";
        }
    }

    public function getCourses(){
        $sql = "SELECT c.* FROM courses c, account_courses ac WHERE ac.account_id = $this->id AND c.id = ac.course_id AND c.active = 1 AND ac.active = 1 GROUP BY c.id";
        return Course::getList(['sql'=>$sql]);
    }

    public function getAccountCourseCount(){
        if($this->plan_id != ""){
            $total_acc_courses = \Model\Plan_Courses::getCount(['where'=>"plan_id = $this->plan_id"]);
            return $total_acc_courses;
        }else{
            return 0;
        }
    }

    public function getCourseCount(){
        $sql = "SELECT c.* FROM courses c, account_courses ac WHERE ac.account_id = $this->id AND c.id = ac.course_id AND c.active = 1 AND ac.active = 1 GROUP BY c.id";
        return Course::getCount(['sql'=>$sql]);
    }

    public static function getAccountDashBoardData($year, $month){
        $data = new \stdClass();

        $sql = "FROM ".self::$tablename." WHERE ";
        $sql .= " EXTRACT(MONTH FROM created_at) = ".$month;
        $sql .= " AND EXTRACT(YEAR FROM created_at) = ".$year;

        $data->monthly_accounts_enrolled = 0;
        $monthly_accounts_enrolled = self::execute("SELECT COUNT(id) AS acc ".$sql);
       
        if (count($monthly_accounts_enrolled) > 0){
            $data->monthly_accounts_enrolled = $monthly_accounts_enrolled[0]['acc'];
        }

        return $data;
    }

    public static function getStudentDashBoardData($year, $month){
        $data = new \stdClass();

        $sql = "FROM ".\Model\User::$tablename." WHERE role = 'student' AND ";
        $sql .= " EXTRACT(MONTH FROM created_at) = ".$month;
        $sql .= " AND EXTRACT(YEAR FROM created_at) = ".$year;

        $data->monthly_students_enrolled = 0;
        $monthly_students_enrolled = \Model\User::execute("SELECT COUNT(id) AS s ".$sql);
        
        if (count($monthly_students_enrolled) > 0){
            $data->monthly_students_enrolled = $monthly_students_enrolled[0]['s'];
        }

        return $data;
    }
}