<?php

namespace Model;

use Emagid\Core\Model;

class Course extends Model{
    static $tablename = 'courses';
    static $fields = [
        'title',
        'status',
        'description'
    ];

    public function getModules(){
        return Course_Modules::getList(['where'=>"course_id = $this->id"]);
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "select * from courses where active = 1 and (";
        
        $sql .= " lower(title) like '%" . strtolower(urldecode($keywords))."%') limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public function isCompleted($user){
        // $ucm = \Model\User_Course_Modules::getList(['where'])
        $sql = "SELECT * FROM user_course_modules where user_id = ".$user." AND course_module_id IN (SELECT id from course_modules WHERE course_id = ".$this->id.")";
        $ucm = \Model\User_Course_Modules::getList(['sql'=>$sql]);
        $count = 0;
        foreach ($ucm as $user_module) {
            if($user_module->status == 'passed'){
                $count = $count + 1;
            }
        }

        if(count($ucm) == $count && $count > 0){
            return true;
        } else {
            return false;
        }
    }
}