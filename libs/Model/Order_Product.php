<?php

namespace Model;

class Order_Product extends \Emagid\Core\Model {
    static $tablename = "order_products";
  
    public static $fields = [
  		'order_id',
  		'product_id',
  		'quantity',
  		'unit_price',
		'details'
    ];
	
	 
}