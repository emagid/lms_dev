<?php

namespace Model;

use Emagid\Core\Model;

class Account_Plan_Modules extends Model{
    static $tablename = 'account_plan_modules';
    static $fields = [
        'tier_id',
        'balance',
    ];
}