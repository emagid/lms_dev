<?php

namespace Model;

use Emagid\Core\Model;

class Transaction extends Model{
    static $tablename = 'transaction';
    static $fields = [
        'order_id',
        'authorize_net_data',
        'ref_tran_id'
    ];
}