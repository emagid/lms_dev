<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/18/16
 * Time: 4:41 PM
 */

namespace Model;

use Carbon\Carbon;

class ReportData extends \Emagid\Core\Model {
    static $tablename = "report_data";
    public static $fields = [
        'date',
        'location_id',
        'field_id',
        'value',
        'update_time'
    ];

    public function getFieldMappingData($format = false)
    {
        $field = Field::getItem($this->field_id);
        $value = $this->value;
        return [$field->name => $value];
    }

    /**
     * We do not save any new location or new field, we rather not save than save dirty data,
     * if there is new location or field, please update field and location then upload or import again
     * @param $data ['field' => 'Daily $', 'location' => 'South Beach', 'value' => '123', 'date' => '04/16/2016']
     * @return Boolean
     */
    public static function saveData($data, $option = [])
    {
        /**
         * if field is format as gm_12, ly_12, fc_12, bd_12, nt_12 then we don't convert it
         */
        $isFieldAlreadyConverted = false;

        // we don't convert value if field is nt_12
        $isNote = false;

        if(!isset($data['field']) || !$data['field'] || !isset($data['location']) || !$data['location'] || !isset($data['date']) || !$data['date']){
            return false;
        }
        
        $field = $data['field'];
        $date = $data['date'];
        $location = $data['location'];
        $value = (!isset($data['value']) || !$data['value'])?0:$data['value'];
        $date = new Carbon($date);
        $dateString = $date->toDateString();

        $findLocation = Location::findLocation($location);
        if(!$findLocation){
            return false;
        }

        if(!($locationModel = \Model\Location::getItem(null, ['where' => "name = '$findLocation'"]))){
            $locationModel = new Location();
            $locationModel->name = $findLocation;
            $locationModel->save();
        }

        /**
         * Start GM Name exception, if gn set true, then we only updating gn_note table
         */
        if(isset($option['gn']) && $option['gn']){
            return GmNote::updateGm(['location_id' => $locationModel->id, 'value' => $value]);
        }
        
        $findField = null;

        $field = strtolower(trim($field));
        $fieldType = Field::getFieldType($field);


        $fieldValidArray = explode('_', $field);
        if(in_array($fieldValidArray[0], ['gm', 'ly', 'fc', 'bd', 'nt'])){
            $findField = $field;
            $isFieldAlreadyConverted = true;
            if($fieldValidArray[0] == 'nt'){
                $isNote = true;
            }
        }

        $value = Field::convertValue($value, $isNote);

        /**
         * If field says restaurant, but location shows it's nightlife, we replace restaurant with nightlife
         */
        if($fieldType && $locationModel->type != $fieldType){
            $newField = str_replace($fieldType, $locationModel->type, $field);
            $findField = Field::findField($newField);
        } elseif(!$isFieldAlreadyConverted){
            $findField = Field::findField($field);
        }
        

        /**
         * If lastyear option exists and true, we replace gm to ly
         */
        if(isset($option['lastyear']) && $option['lastyear']){
            $findField = str_replace('gm', 'ly', $findField);
        }

        if(isset($option['budget']) && $option['budget']){
            $findField = str_replace('gm', 'bd', $findField);
        }
        
        if(!$findField){
            return false;
        }

        if(!($fieldModel = \Model\Field::getItem(null, ['where' => "name = '$findField'"]))){
            $fieldModel = new Field();
            $fieldModel->name = $findField;
            $fieldModel->save();
        }

        /**
         * Mark location as submitted status
         */
        if(isset($option['submit']) && $option['submit']){
            $locationModel->submit = 1;
            $locationModel->save();
        }

        $reportModelId = 0;
        if($reportExistingModel = ReportData::getItem(null, ['where' => "field_id = {$fieldModel->id} AND date = '$dateString' AND location_id = {$locationModel->id}"])){
            $reportModelId = $reportExistingModel->id;
        }

        /**
         * Overwrite situation only happen when value is not empty
         */
        if(isset($option['overwrite']) && $option['overwrite'] && $reportModelId && $reportExistingModel && !$value){
            return null;
        }

        $reportModel = new ReportData();
        $reportModel->id = $reportModelId;
        $reportModel->field_id = $fieldModel->id;
        $reportModel->location_id = $locationModel->id;
        $reportModel->date = $date->toDateString();
        $reportModel->value = $value;
        $reportModel->update_time = Carbon::now()->toDateTimeString();
        return $reportModel->save();
    }

    public static function getYtdField()
    {
        return [
            'restaurant' => [
                'gm_172', // restaurant mtd
                'ly_172',
                'bd_172',
            ],
            'nightlife' => [
                'gm_174', // nightlife mtd
                'ly_174',
                'bd_174',
            ],
            'hotel' => [
                'gm_120', // hotel room revenue mtd
                'ly_120',
                'bd_120',
                'gm_106', // hotel occp% mtd
                'ly_106',
                'bd_106',
                'gm_16', // hotel ADR mtd
                'ly_16',
                'bd_16'
            ]
        ];
    }

    public static function MTD($location, $date, $field){
        $end = new Carbon($date);
        $start = $end->copy()->startOfMonth();

        $dateArray = [];
        for($startDate = $start->copy(); $startDate->lte($end); $startDate->addDay()) {
            $tmpDate = $startDate->toDateString();
            $dateArray[] = "'".$tmpDate."'";
        }

        $dateStrings = implode(',', $dateArray);

        global $emagid;
        $db = $emagid->getDb();
        $sql = "select report_data.value as value, location.name as location, report_data.date as date, field.name as field from report_data join location on location.id = report_data.location_id join field on field.id = report_data.field_id where date in ($dateStrings) AND location.name = '$location' AND field.name = '$field'";
        $reportData = $db->getResults($sql);

        $summaryData = [];
        foreach($reportData as $data){
            $summaryData[$data['date']] = $data;
        }


        if($end->daysInMonth == 1){
            return $summaryData[$end->toDateString()]['value'];
        }

        $totalValue = 0;

        for($startDate = $start->copy(); $startDate->lte($end); $startDate->addDay()) {
            $date = $startDate->toDateString();
            if(isset($summaryData[$date])){
                $totalValue += floatval($summaryData[$date]['value']);
            }
        }

        return $totalValue;
    }

    public static function WTD($location, $date, $field){
        $end = new Carbon($date);
        $start = $end->copy()->startOfWeek();

        $dateArray = [];
        for($startDate = $start->copy(); $startDate->lte($end); $startDate->addDay()) {
            $tmpDate = $startDate->toDateString();
            $dateArray[] = "'".$tmpDate."'";
        }

        $dateStrings = implode(',', $dateArray);

        global $emagid;
        $db = $emagid->getDb();
        $sql = "select report_data.value as value, location.name as location, report_data.date as date, field.name as field from report_data join location on location.id = report_data.location_id join field on field.id = report_data.field_id where date in ($dateStrings) AND location.name = '$location' AND field.name = '$field'";
        $reportData = $db->getResults($sql);

        $summaryData = [];
        foreach($reportData as $data){
            $summaryData[$data['date']] = $data;
        }


        if($end->dayOfWeek == Carbon::MONDAY){
            return $summaryData[$end->toDateString()]['value'];
        }

        $totalValue = 0;

        for($startDate = $start->copy(); $startDate->lte($end); $startDate->addDay()) {
            $date = $startDate->toDateString();
            if(isset($summaryData[$date])){
                $totalValue += floatval($summaryData[$date]['value']);
            }
        }

        return $totalValue;
    }

    /**
     * Calculate fields year to date data
     * @param $location
     * @param $date
     */
    public static function YTD($location, $date){
        $locationModel = Location::getItem(null, ['where' => "name = '$location'"]);
        $type = $locationModel->type;
        $accept = self::getYtdField();
        $date = new Carbon($date);
        /**
         * ['2016-01-31', '2016-02-29', '2016-03-31'...]
         */
        $dateArray = [$date->toDateString(), $date->copy()->subYear()->toDateString()];
        $tmpDate = $date->copy();
        for($i = 1; $i <= ($date->month - 1); $i++){
            $tmpDate = $tmpDate->firstOfMonth()->subMonth()->lastOfMonth();
            $dateArray[] = $tmpDate->toDateString();
            $dateArray[] = $tmpDate->copy()->subYear()->toDateString();
        }

        $accept = $accept[$type];

        /**
         * Wrap up data with single quote
         */
        foreach($accept as &$value){
            $value = "'$value'";
        }

        foreach($dateArray as &$value){
            $value = "'$value'";
        }

        $fieldString =  implode(',', $accept);

        $dateString = implode(',', $dateArray);

        $sql = "select report_data.value as value, location.name as location,  field.name as field, report_data.date from report_data join location on location.id = report_data.location_id join field on field.id = report_data.field_id where date in ($dateString) AND location.name = '$location' AND field.name in ($fieldString)";
        global $emagid;
        $db = $emagid->getDb();
        $reportData = $db->execute($sql);

        $result = [];
        $countArray = [];
        foreach($reportData as $data){
            if($type == 'hotel' && in_array($data['field'], ['gm_106', 'ly_106', 'bd_106', 'gm_16', 'ly_16', 'bd_16'])){
                if(!isset($countArray[$data['field']])){
                    $countArray[$data['field']] = 0;
                }

                $countArray[$data['field']] += 1;
            }
            if(!isset($result[$data['field']])){
                $result[$data['field']] = 0;
            }

            /**
             * Calculate sum of each mtd
             */
            $result[$data['field']] += floatval($data['value']);
        }

        foreach($countArray as $filed => $count){
            if(!$count){
                continue;
            }
            $result[$filed] = $result[$filed]/$count;
        }

        return $result;
    }
}