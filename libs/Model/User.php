<?php

namespace Model;

class User extends \Emagid\Core\Model {
    static $tablename = "users";

    static $all_sections = [
        'Accounts'  => [],
        'Users'     => [],
        'Courses'   => [],
        'Plans'     => [],
    ];

    public static $fields  =  [
        'email'=>['required'=>'true'],
        'encrypted_password',
        'first_name'=>['required'=>'true', 'name'=>'First Name'],
        'last_name'=>['required'=>'true', 'name'=>'Last Name'],
        'account_id',
        'category_id',
        'user_category_id',
        'role',
        'reset_password_token',
        'reset_password_sent_at',
        'remember_created_at',
        'sign_in_count',
        'current_sign_in_at',
        'last_sign_in_at',
        'current_sign_in_ip',
        'last_sign_in_ip',
        'created_at',
        'updated_at',
        'invitation_token',
        'invitation_created_at',
        'invitation_sent_at',
        'invitation_accepted_at',
        'invitation_limit',
        'invited_by_id',
        'invited_by_type',
        'invitations_count',
        'status',
        'invitation_status',
        'confirmation_token',
        'confirmed_at',
        'confirmation_sent_at',
        'access_token',
        'current_user_course_module_id',
        'signed_in_as',
        'year_level',
        'permissions',
        'phone',
        'address',
        'city',
        'state',
        'zip',
    ];

    /**
    * concatenates first name and last name to create full name string and returns it
    * @return type: string of full name
    */
    function full_name() {
        return $this->first_name.' '.$this->last_name;
    }

    public function getRoleName()
    {
        return $this->role;
    }

    public function category(){
        if($this->category_id != NULL){
            return Category::getItem($this->category_id);
        } else {
            return null;
        }
    }

    public function getCurrentCourse(){
        return User_Course_Modules::getList(['where'=>"user_id = $this->id AND current = 1"]);//todo Count number of user courses
    }

    public function getCourses(){
       $modules = User_Course_Modules::getList(['where'=>"user_id = $this->id"]);
       foreach ($modules as &$ucm){
           $ucm->module = Course_Modules::getItem($ucm->course_module_id);
       }//todo Count number of user courses
        return $modules;
    }

    public function getSchool(){
        return Account::getItem($this->account_id);
    }


    public static function search($keywords, $limit = 20)
    {
        $sql = "select * from users where role = 'student' and active = 1 and (";
        
        $sql .= " lower(first_name) like '%" . strtolower(urldecode($keywords))."%' or lower(last_name) like '%" . strtolower(urldecode($keywords))."%' or lower(email) like '%". strtolower(urlencode($keywords)). "%') limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function search_managers($keywords, $limit = 20)
    {
        $sql = "select * from users where role = 'manager' and active = 1 and (";
        
        $sql .= " lower(first_name) like '%" . strtolower(urldecode($keywords))."%' or lower(last_name) like '%" . strtolower(urldecode($keywords))."%' or lower(email) like '%". strtolower(urlencode($keywords)). "%') limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    //Searching for students on User side.
    public static function search_account_students($keywords){
        $sql = "select * from users where account_id = ". $this->viewData->user->account_id. " and (";
        $sql .= " lower(first_name) like '%" . strtolower(urlencode($keywords))."%' or lower(last_name) like '%". strtolower(urlencode($keywords)). "%' or lower(email) like '%". strtolower(urlencode($keywords)). "%')";
        return self::getList(['sql'=> $sql]);
    }

    /**
    * Verify login and create the authentication cookie / session 
    */
    public static function login($email , $password){
        $list = User::getList(['where'=>"email = '{$email}' AND role = 'manager'"]);
        if (count($list) > 0) {
            $manager = $list[0];
            $account = Account::getItem($manager->account_id);
            if($account->status != Account::$statuses[2] ){
                $n = new \Notification\ErrorHandler('Account hasn\'t been approved.');
                $_SESSION["notification"] = serialize($n);
                return false;
            }


            //$password = \Emagid\Core\Membership::hash($password, $admin->hash, null );
            if (hash_equals($manager->encrypted_password, crypt($password,$manager->encrypted_password))) {

                $rolesNames = [];
                $rolesNames[] = 'manager';

                \Emagid\Core\Membership::setAuthenticationSession($manager->id,$rolesNames, $manager);

                return true;
            }
            $n = new \Notification\ErrorHandler('Incorrect email or password.');
            $_SESSION["notification"] = serialize($n);
        }
        $n = new \Notification\ErrorHandler('Incorrect password or email.');
        $_SESSION["notification"] = serialize($n);
        $_POST['redirect-url'] = '/account';
        return false;
    }

    public static function student_login($email , $password){
        $list = User::getList(['where'=>"email = '{$email}' AND role = 'student'"]);
        if (count($list) > 0) {
            $student = $list[0];
            $account = Account::getItem($student->account_id);
            if($account->status != Account::$statuses[2] ){
                $n = new \Notification\ErrorHandler('Account hasn\'t been approved.');
                $_SESSION["notification"] = serialize($n);
                return false;
            }


            //$password = \Emagid\Core\Membership::hash($password, $admin->hash, null );
            if (hash_equals($student->encrypted_password, crypt($password,$student->encrypted_password))) {

                $rolesNames = [];
                $rolesNames[] = 'manager';

                \Emagid\Core\Membership::setAuthenticationSession($student->id,$rolesNames, $student);
                $_POST['redirect-url'] = '/student';
                return true;
            }
            $n = new \Notification\ErrorHandler('Incorrect email or password.');
            $_SESSION["notification"] = serialize($n);
        }
        $n = new \Notification\ErrorHandler('Incorrect password or email.');
        $_SESSION["notification"] = serialize($n);
        $_POST['redirect-url'] = '/account';
        return false;
    }

    public static function getAdmins(){
        return self::getList(['where' => 'role = "superadmin"']);
    }

    public static function getManagers($account_id = null){
        $accSql = $account_id?" AND account_id = $account_id":'';
        return self::getList(['where' => "role = 'manager' $accSql"]);
    }

    public static function getStudents($account_id = null){
        $accSql = $account_id?"AND account_id = $account_id":'';
        return self::getList(['where' => "role = 'student' $accSql"]);
    }


}
























