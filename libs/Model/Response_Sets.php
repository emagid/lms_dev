<?php
namespace Model;

use Carbon\Carbon;

class Response_Sets extends \Emagid\Core\Model
{

    public static $tablename = "response_sets";

    public static $fields = [
        'user_course_module_id',
        'question_id',
        'quiz_attempt',
    ];

    public function getResponses(){
      $answers = [];
      foreach (Response::getList(['where'=>"response_set_id = $this->id"]) as $response){
          $answers = Answers::getItem($response->answer_id);
      }
      return $answers;
    }

}