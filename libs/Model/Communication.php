<?php

namespace Model;

use Emagid\Core\Model;

class Communication extends Model{
    static $tablename = 'communication';
    //status: 0 = fail, 1 = sent
    static $fields = [
        'to_','from_','subject','body','time_sent','status','opportunity_id'
    ];

    //returns contact obj
    function getTo(){
        return Contact::getItem($this->to);
    }
    function getFrom(){
        return Admin::getItem($this->from);
    }
    function getFormattedDate($format = 'Y-m-d'){
        return date($format,strtotime($this->time_sent));
    }
}