<?php
namespace Model;

use Carbon\Carbon;

class Account_Courses extends \Emagid\Core\Model
{

    public static $tablename = "account_courses";

    public static $fields = [
        'account_id',
        'course_id',
        'available_from',
    ];

}


