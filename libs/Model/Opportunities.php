<?php

namespace Model;

use Emagid\Core\Model;

class Opportunities extends Model{
    static $tablename = 'opportunities';
    static $fields = [
        'contact_id',
        'building_type',
        'amenities',
        'square_feet',
        'budget',
        'location',
        'moving_date'
    ];
    static $buildingType = [1=>"Commercial",2=>"Residential"];

    function getContact(){
        return $this->contact_id ? Contact::getItem($this->contact_id): null;
    }

    function getFormattedDate($format = 'Y-m-d'){
        return date($format,strtotime($this->moving_date));
    }

    function getAmenitiesList($separator = ','){
        $amenities = json_decode($this->amenities,true);
        $amenityName = array_map(function($am){
            return Amenity::getItem($am)->name;
        },$amenities);
        return implode($separator,$amenityName);
    }

    function getAmenitiesArray(){
        $oppAmenities = json_decode($this->amenities,true);
        $amenities = array_map(function($am){
            return Amenity::getItem($am);
        },$oppAmenities);
        return $amenities;
    }
}