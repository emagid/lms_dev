<?php 

namespace Model; 

class Question extends \Emagid\Core\Model {

	static $tablename = 'questions';

	public static $fields =  [
		'course_module_id',
		'title',
		'number',
	];

	public function getAnswers(){
	    return Answers::getList(['where'=>"question_id = $this->id"]);
	}
}