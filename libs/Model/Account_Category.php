<?php

namespace Model;

use Emagid\Core\Model;

class Account_Category extends Model{
    static $tablename = 'account_category';
    static $fields = [
        'name',
    ];
}