<?php

namespace Model;

use Emagid\Core\Model;

class Log_Status extends Model{
    static $tablename = 'log_status';
    static $fields = [
        'order_id',
        'status',
        'admin_id'
    ];
}