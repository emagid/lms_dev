<?php
namespace Model;

class Amenity_Type extends \Emagid\Core\Model {
 
    static $tablename = "amenity_type";

    public static $fields = [
		'name'
    ];
  
}