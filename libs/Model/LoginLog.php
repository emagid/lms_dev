<?php
namespace Model;

class LoginLog extends \Emagid\Core\Model {
	static $tablename = "login_log";
	public static $fields = [
		'user_id',	
		'time'
	];
}