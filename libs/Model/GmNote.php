<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 7/5/16
 * Time: 5:33 PM
 */

namespace Model;

use Carbon\Carbon;

class GmNote extends \Emagid\Core\Model
{
    static $tablename = "gm_note";
    public static $fields = [
        'location_id',
        'value'
    ];

    public static function updateGm(array $data)
    {
        $locationId = $data['location_id'];
        $value = $data['value'];

        if($gmNote = self::getItem(null, ['where' => "location_id = $locationId"])){
            $gmNote->value = $value;
        } else {
            $gmNote = new GmNote();
            $gmNote->location_id = $locationId;
            $gmNote->value = $value;
        }

        $gmNote->update_time = Carbon::now()->toDateTimeString();

        return $gmNote->save();
    }

    /**
     * @param $locationName
     * @return null|String
     */
    public static function getGm($locationName)
    {
        $locationName = Location::findLocation($locationName);
        if(!$locationName){
            return null;
        }

        $locationModel = Location::getItem(null, ['where' => "name = '$locationName'"]);

        if(!$locationModel){
            return null;
        }

        $item = self::getItem(null, ['where' => "location_id = {$locationModel->id}"]);

        if($item){
            if($item->value && $item->value != '0'){
                return $item->value;
            } else {
                return null;
            }
        }

        return null;
    }
}