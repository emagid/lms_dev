<?php
namespace Model;

class Brand extends \Emagid\Core\Model {
 
    static $tablename = "brand";

    public static $fields = [
  		'name' => ['required'=>true],
    ];  
}