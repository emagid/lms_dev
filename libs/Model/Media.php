<?php

namespace Model;

class Media extends \Emagid\Core\Model {

	static $tablename = "media";
	public static $fields = ['name', 'file', 'type_id'];

	public function getTypeName()
	{
		if(!$this->type_id){
			return 'None';
		}
		$type = MediaType::getItem($this->type_id);
		if($type){
			return $type->name;
		} else {
			return 'None';
		}
	}

	public function path()
	{
		return "/content/uploads/media/{$this->file}";
	}
}
