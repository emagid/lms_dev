<?php

namespace Model;

use Emagid\Core\Model;

class Color extends Model{
    static $tablename = 'color';
    static $fields = [
        'name',
        'slug'
    ];
}