<?php

namespace Model;

use Emagid\Core\Model;

class Mail_Log extends Model{
    static $tablename = 'mail_log';
    static $fields = [
        'order_id',
        'text',
        'from_',
        'to_',
        'subject'
    ];
}