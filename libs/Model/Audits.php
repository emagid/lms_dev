<?php
namespace Model;

use Carbon\Carbon;

class Audits extends \Emagid\Core\Model
{

    public static $tablename = "audits";

    public static $fields = [
        'user_id',
        'created_at',
        'device',
        'action',
        'action_description'
    ];

}



