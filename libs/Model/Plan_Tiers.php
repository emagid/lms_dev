<?php

namespace Model;

use Emagid\Core\Model;

class Plan_Tiers extends Model{
    static $tablename = 'plan_tiers';
    static $fields = [
        'plan_id',
        'min_users',
        'max_users',
        'price',
    ];

    public function getModules(){
        return Course_Modules::getList(['where'=>"course_id = $this->id"]);
    }
}