<?php

namespace Emagid; 


/** 
* A pagination wrapper for \Emagid\Core\Model classes . 
*/ 
class Pagination extends \Emagid\Core\Core{
	
	var $page_size = 10; 

	var $current_page_index = 0;

	var $total_pages = 0; 

	var $total_records = 0; 

	var $first_record = 0 ;

	var $last_record = 0 ;

	var $display_range = PHP_INT_MAX; 

	var $start_page = 0; 

	var $end_page = 0; 

	// var $total_pages = 0; 


	private $class_name ; 

	private $conditions = []; 



	function __construct ($model, array $params = []){
		$this->class_name = $model;

		$this->conditions = array_merge($params, $this->conditions);

		
		$page = isset($params['page']) && $params['page']  ?: (isset($_GET['page'])?$_GET['page']:1); 
        
		$this->current_page_index = ($page?:1)-1 ;


		$this->applyParams($params);
		unset ($this->conditions['limit']);
		unset ($this->conditions['offset']);
	}	



	/** 
	* build the list of items to display in the current page, and display them.
	*/
	public function getList(){
		$class_name = $this->class_name ; 
		$this->parse();
		return $class_name::getList($this->conditions);
	}

	public function pagesArray(){
		$list = range ($this->start_page, $this->end_page); 

		return $list;
	}

	private function buildDisplayRange(){
		// get the mid point .
		$display_range = $this->display_range -1 ; 

		$mid = $display_range / 2 ;

		$this->start_page = max(1, $this->current_page_index+1-$mid);
		$this->end_page   = min($this->start_page + $display_range , $this->total_pages);
	}

	/**
	* parse the configs, and produce the missing variables, including the list .
	*/
	private function parse(){
		$class_name = $this->class_name ; 

		$this->total_records = $class_name::getCount($this->conditions); 
		$this->total_pages   = ceil($this->total_records / $this->page_size);
		$this->first_record  = $this->current_page_index * $this->page_size + 1;
		$this->last_record   = min($this->first_record + $this->page_size-1,$this->total_records);


		$this->conditions['limit']  = $this->page_size; 
		if ($this->current_page_index == 0){
			$this->conditions['offset'] = 0;
		} else {
			$this->conditions['offset'] = $this->current_page_index * $this->page_size;
		}

    
		$this->buildDisplayRange();
	}
}