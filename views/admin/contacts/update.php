<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->contact->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>First Name</label>
                <?php echo $model->form->editorFor("first_name"); ?>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <?php echo $model->form->editorFor("last_name"); ?>
            </div>
            <div class="form-group">
                <label>Phone</label>
                <?php echo $model->form->editorFor("phone"); ?>
            </div>
            <div class="form-group">
                <label>Email</label>
                <?php echo $model->form->editorFor("email"); ?>
            </div>
            <div class="form-group">
                <label>Comments</label>
                <?php echo $model->form->editorFor("comments"); ?>
            </div>
            <div class="form-group">
                <label>Address</label>
                <?php echo $model->form->editorFor("address"); ?>
            </div>
            <div class="form-group">
                <label>City</label>
                <?php echo $model->form->editorFor("city"); ?>
            </div>
            <div class="form-group">
                <label>State</label>
                <?php echo $model->form->editorFor("state"); ?>
            </div>
            <div class="form-group">
                <label>Zip</label>
                <?php echo $model->form->editorFor("zip"); ?>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
  
</form>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
});
</script>