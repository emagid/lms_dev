<?php if(count($model->attributes)>0) { ?>
  <div class="box box-table">
    <table id="data-list" class="table">
      <thead>
        <tr>
        <th width="40%">Code</th>  
          <th width="65%">Name</th>  
          <th width="15%" class="text-center">Values</th>	
         
      </thead>
      <tbody>
       <?php foreach($model->attributes as $obj){ ?>
        <tr class="originalNews">
        <td><a href="<?php echo ADMIN_URL; ?>attributes/update/<?php echo $obj->attribute_id; ?>"><?php echo $obj->attribute_code; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>attributes/update/<?php echo $obj->attribute_id; ?>"><?php echo $obj->frontend_label; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>attributes/update/<?php echo $obj->attribute_id; ?>">
           <i class="icon-eye"></i> 
           </a>
         </td>
       
       </tr>
       <?php } ?>
   	  </tbody>
	</table>
	<div class="box-footer clearfix">
	  <div class='paginationContent'></div>
	</div>
</div>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'attributes';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

