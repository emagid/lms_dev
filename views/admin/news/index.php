<?php if(count($model->articles)>0) { ?>
  <div class="box box-table">
    <table id="data-list" class="table">
      <thead>
        <tr>
          <th width="5%">Image</th>
          <th width="65%">Name</th>  
          <th width="15%" class="text-center">Edit</th>	
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->articles as $obj){ ?>
        <tr class="originalNews">
       	<td>
        <?php 
             $img_path = "";
             if($obj->image != "" && file_exists(UPLOAD_PATH.'articles'.DS.$obj->image)){ 
             	$img_path = UPLOAD_URL . 'articles/' . $obj->image;
        ?>
             	<img src="<?php echo $img_path; ?>" width="50" />
		<?php } ?>
        </td>
         <td><a href="<?php echo ADMIN_URL; ?>news/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>news/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>news/delete/<?php echo $obj->id; ?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   	  </tbody>
	</table>
	<div class="box-footer clearfix">
	  <div class='paginationContent'></div>
	</div>
</div>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'news';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

