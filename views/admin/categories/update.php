<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="test">
        <input type="hidden" name="id" value="<?php echo $model->category->id; ?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>
                    <div class="form-group">
                    	<div class="custom-select">
	                        <label>School</label>
	                        <?php echo $model->form->dropDownListFor("account_id",$model->accounts); ?>
	                    </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="display: none">
				<div class="box">
					<h4>SEO</h4>
					<div class="form-group">
						<label>Slug</label>
						<?php echo $model->form->editorFor('slug'); ?>
					</div>
					<div class="form-group">
						<label>Meta Title</label>
						<?php echo $model->form->editorFor('meta_title'); ?>
					</div>
					<div class="form-group">
						<label>Meta Keywords</label>
						<?php echo $model->form->editorFor('meta_keywords'); ?>
					</div>
					<div class="form-group">
						<label>Meta Description</label>
						<?php echo $model->form->textAreaFor('meta_description'); ?>
					</div>
				</div>
			</div>
        </div>
      </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
	$(document).ready(function() {
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'600');
					img.attr('height','172');
					$("#preview-container").html(img);
				};

				$(input).parent().parent().find('input[type="hidden"]').remove();

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
			$('#previewupload').show();
		});	

		$("input[name='name']").on('keyup',function(e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g,'-');
			$("input[name='slug']").val(val.toLowerCase());
		});
	});
</script>