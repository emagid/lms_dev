<?php
$gender = [1 => 'Unisex', 2 => "Men", 3 => "Women"];
?>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->product->id; ?>"/>
    <input type="hidden" name="redirectTo" value="<?= $_SERVER['HTTP_REFERER'] ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
            <li role="presentation"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li>
            <?php if ($model->product->id > 0) { ?>
            <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
            <?}?>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>DELETE</label>
                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>products/delete/<?php echo $model->product->id; ?>?token_id=<?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                    <i class="icon-cancel-circled"></i>
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Name</label>

                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

                            <div class="form-group">
                                <label>Alias</label>
                                <?php echo $model->form->editorFor("alias"); ?>
                            </div>

                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small>(ideal featured image size is 500 x 300)</small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->product->featured_image != "" && file_exists(UPLOAD_PATH . 'products' . DS . $model->product->featured_image)) {
                                        $img_path = UPLOAD_URL . 'products/' . $model->product->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'products/delete_image/' . $model->product->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->product->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>

    <!--                    <div class="form-group">-->
    <!--                        <a href="#" id="add-to-hottest">Add this product to hottest deals</a>-->
    <!--                    </div>-->
                            <div class="form-group">
                                <label>Summary</label>
                                <?php echo $model->form->textAreaFor("summary"); ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ['class' => 'ckeditor']); ?>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <?php echo $model->form->editorFor("quantity"); ?>
                            </div>
                            <div class="form-group">
                                <label>Color</label>
                                <select name="color[]" class="multiselect" multiple data-placeholder="Colors">
                                    <?php foreach ($model->colors as $c) {
                                        $selected = in_array($c->id,json_decode($model->product->color)) ? "selected" : "";
                                        ?>
                                        <option
                                            value="<?php echo $c->id; ?>"<?php echo $selected; ?>><?php echo $c->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Sizes</label>
                                <select name="size[]" class="multiselect" multiple data-placeholder="Sizes">
                                    <?php for($i = 5; $i < 15; $i += .5) {
                                        $selected = in_array($i,json_decode($model->product->size)) ? "selected" : ""?>
                                        <option value="<?php echo $i; ?>"<?=$selected?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Availability</label>
                                <select name="availability" class="form-control">
                                    <?php
                                    $selected_stock = ($model->product->availability);
                                    ?>
                                    <option value="2" <?php if ($selected_stock == 2) {
                                        echo "selected";
                                    } ?>>Active
                                    </option>
                                    <option value="1" <?php if ($selected_stock == 1) {
                                        echo "selected";
                                    } ?>> In stock
                                    </option>
                                    <option value="0" <?php if ($selected_stock == 0) {
                                        echo "selected";
                                    } ?>>Inactive
                                    </option>

                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Prices</h4>

                            <div class="form-group">
                                <label>MSRP</label>
                                <?php echo $model->form->editorFor("msrp"); ?>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label>MSRP Enabled?</label>
                                <?php echo $model->form->checkBoxFor("msrp_enabled", 1); ?>
                            </div>
                            <div class="form-group">
                                <label>Sell price</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Dimensions</h4>

                            <div class="form-group">
                                <label>Length</label>
                                <?php echo $model->form->textAreaFor('length'); ?>
                            </div>
                            <div class="form-group">
                                <label>Width</label>
                                <?php echo $model->form->textAreaFor('width'); ?>
                            </div>
                            <div class="form-group">
                                <label>Height</label>
                                <?php echo $model->form->textAreaFor('height'); ?>
                            </div>
                        </div>
                    </div>

                    <?php if (count($model->product_questions) > 0) { ?>
                        <div class="col-md-12">
                            <div class="box">
                                <h4>Questions</h4>
                                <table id="data-list" class="table">
                                    <thead>
                                    <tr>
                                        <th width="30%">Date</th>
                                        <th width="60%">Subject</th>
                                        <th width="10%">View</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($model->product_questions as $obj) { ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?php echo $obj->insert_time; ?></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?= $obj->subject ?></a>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="seo-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <?php echo $model->form->editorFor("tags"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="categories-tab">
                <select name="product_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
                    <?php foreach ($model->categories as $cat) { ?>
                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php if ($model->product->id > 0) { ?>
                <div role="tabpanel" class="tab-pane" id="images-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <div class="dropzone" id="dropzoneForm"
                                     action="<?php echo ADMIN_URL . 'products/upload_images/' . $model->product->id; ?>">

                                </div>
                                <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                                <br/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <table id="image-container" class="table table-sortable-container">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>File Name</th>
                                        <th>Display Order</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach (\Model\Product_Image::getList(['where' => 'product_id=' . $model->product->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {

                                        if ($pimg->exists_image()) {
                                            ?>
                                            <tr data-image_id="<?php echo $pimg->id; ?>">
                                                <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100"
                                                         height="100"/></td>
                                                <td><?php echo $pimg->image; ?></td>
                                                <td class="display-order-td"><?php echo $pimg->display_order; ?></td>
                                                <td class="text-center">
                                                    <a class="btn-actions delete-product-image"
                                                       href="<?php echo ADMIN_URL; ?>products/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                        <i class="icon-cancel-circled"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <button type="submit" class="btn btn-save" style="display:none">Save</button>
    <div id="target" class="btn btn-save">Save</div>
</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var categories = <?php echo json_encode($model->product_categories); ?>;
    var colors = <?php echo ($model->product->color?:'0'); ?>;
    var sizes = <?php echo ($model->product->size?:0); ?>;
//    var collections = <?php //echo json_encode($model->product_collections); ?>//;
//    var materials = <?php //echo json_encode($model->product_materials); ?>//;
    //console.log(categories);
    var site_url =<?php echo json_encode(ADMIN_URL.'products/'); ?>;
    $(document).ready(function () {
        //var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#add-to-hottest').on('click', function () {
            var product_id = "<?=$model->product->id?>";
            $.post('/admin/hottest_deal/addHottestDeal', {product_id: product_id}, function (data) {
                window.location.replace("<?=ADMIN_URL?>hottest_deal/update/" + data.replace(/\"/g, ""));
            });
        });

        $("#target").click(function () {
            var price = $("input[name='price']").val();
            var name = $("input[name='name']").val();
            var slug = $("input[name='slug']").val();
            var id = <?=$model->product->id?>;
            var errors = new Array();
            if ($.trim(price) > 0) {
                $("input[name='price']").css({
                    "border-color": ""
                });
            } else {
                $("input[name='price']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect price");
            }
            if ($.trim(name) == "") {
                $("input[name='name']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect name");
            } else {
                $("input[name='name']").css({
                    "border-color": ""
                });
            }
            if ($.trim(slug) == "") {
                $("input[name='slug']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect slug");
            } else {

                $.ajax({
                    async: false,
                    url: '/admin/products/check_slug',
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    data: {
                        slug: slug,
                        id: id
                    },
                    success: function (data) {

                        if (data == 1) {
                            errors.push("Slug must be uniq!");

                            $("input[name='slug']").css({
                                "border-color": "red"
                            });

                        } else {
                            $("input[name='slug']").css({
                                "border-color": ""
                            });
                        }
                    }
                });
            }
            var text = "";
            for (i = 0; i < errors.length; i++) {

                text += "<li>" + errors[i] + "</li>";
            }
            if (errors.length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                });
                $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
                errors = [];
            } else {
                $(".form").submit();
            }
        });

        $("input.image").change(function () {
            readURL(this);
        });


        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='product_category[]']").val(categories);
        $("select[name='color[]']").val(colors);
        $("select[name='size[]']").val(sizes);
//        $("select[name='product_collection[]']").val(collections);
//        $("select[name='product_material[]']").val(materials);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-product-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            }
        });

    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'products/upload_images/'.$model->product->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>
