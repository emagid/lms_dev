<div class='bg-danger'>
    <?php if(isset($model->errors) && count($model->errors)>0) {
        echo implode("<br>",$model->errors);
    } ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="id" value="<?php echo isset($model->media_type)?$model->media_type->id:null;?>" />
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>

            </div>
        </div>
        <div class="col-md-24">
            <button type="submit" class="btn-save">Save</button>
        </div>
    </div>
</form>

<?= footer(); ?>

<script type='text/javascript'>
    $(document).ready(function() {

    });

</script>