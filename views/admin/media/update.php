<div class='bg-danger'>
    <?php if(isset($model->errors) && count($model->errors)>0) {
        echo implode("<br>",$model->errors);
    } ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="id" value="<?php echo $model->media->id;?>" />
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>File</label>
                    <input type="text" readonly name="file" value="<?=$model->media->file?>">
                </div>
                <div class="form-group">
                    <label>Type</label>
                    <select name="type_id">
                        <option value="">--SELECT--</option>
                        <?php foreach(\Model\MediaType::getList() as $type){ ?>
                        <option value="<?=$type->id?>" <?=($model->media->type_id == $type->id)?"selected":null?>><?=$type->name?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-24">
            <button type="submit" class="btn-save">Save</button>
        </div>
    </div>
</form>

<?= footer(); ?>

<script type='text/javascript'>
    $(document).ready(function() {
        $("#list-multi").multiselect({
            enableFiltering: true,
            filterBehavior: 'both'
        });
    });

</script>