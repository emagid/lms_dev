<div role="tabpanel" class="tab-pane" id="images-tab">
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <div class="dropzone" id="dropzoneForm"
                     action="<?php echo ADMIN_URL . 'media/upload'; ?>">

                </div>
            </div>
        </div>
    </div>
</div>
<?php if (count($model->media) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">File</th>
                <th width="20%">Type</th>
                <th width="10%">Create Date</th>
                <th width="15%" class="text-center">Download</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->media as $obj) { ?>
                <tr>
                    <td><?php echo $obj->name; ?></td>
                    <td><?php echo $obj->file; ?></td>
                    <td><?php echo $obj->getTypeName(); ?></td>
                    <?php $date = new \Carbon\Carbon($obj->insert_time);?>
                    <td><?= $date->format('m/d/Y g:iA') ?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="/content/uploads/media/<?=$obj->file?>">
                            <i class="icon-print"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>media/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>media/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/bs/jq-2.2.0,dt-1.10.11/datatables.min.css"/>-->

<!--<script type="text/javascript" src="https://cdn.datatables.net/t/bs/jq-2.2.0,dt-1.10.11/datatables.min.js"></script>-->
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'media';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

