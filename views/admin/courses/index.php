<style type="text/css">
    .select-selected:after {
        top: 40px;
    }
</style>

<div class="row filter_holder">
    <form id="filters">
        <div>
            <div class="custom-select">
                Filter by Account:
                <select name="account">
                    <option disabled selected>Select an account</option>
                    <? foreach($model->accounts AS $account) { ?>
                        <option value="<?=$account->id?>" <?= isset($_GET['account']) && $_GET['account'] == $account->id?'selected':'' ?>>
                            <?=$account->name?>
                        </option>
                    <? } ?>
                </select>
            </div>
        </div>

        <div style='margin-top: 15px;'>
            <div class="custom-select">
                Filter by Category:
                <select name="category">
                    <option disabled selected>Select category</option>
                    <? foreach($model->categories AS $category) { ?>
                        <option value="<?=$category->id?>" <?= isset($_GET['category']) && $_GET['category'] == $category->id?'selected':'' ?>>
                            <?=$category->name?>
                        </option>
                    <? } ?>
                </select>
            </div>
        </div>

        <a href="<?= ADMIN_URL?>courses" class="btn_space btn btn-primary" >Reset Filters</a>
    </form>

    <form>
        <div>
            <label>Search:</label> 
            <input id="search" class="form-control" type="text" name="search" placeholder="Search by Name"/>
        </div> 
    </form>
</div>

<?php if(count($model->courses)>0): ?>
    <div class="box box-table">
        <table id="data-list" class="table courses_table" style="word-wrap: break-word;table-layout: fixed;">
            <thead>
            <tr>
                <th width="30%">Title</th>
                <th width="10%">Number Of Modules</th>
                <th width="40%">Descriptions</th>
                <th width="15%" >Edit</th>
                <th width="15%" >Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($model->courses as $obj): ?>
                <tr class="originalProducts">
                    <td><a href="/admin/courses/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a></td>
                    <td><a href="/admin/courses/update/<?php echo $obj->id; ?>"><?php echo  count($obj->getModules()); ?></a></td>
                    <td><a href="/admin/courses/update/<?php echo $obj->id; ?>"><?php echo $obj->description; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="/admin/courses/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="/admin/courses/delete/<?php echo $obj->id; ?>" onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'courses';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

    $('#filters :input').change(function(e){
        $(this).parents('form').submit();
    });

    function jsonEscape(str)  {
        return str.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    }
    $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>courses/search";
        var keywords = $(this).val();
        if (keywords.length > 2) {
            $.get(url, {keywords: keywords}, function(data) {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').hide();
                $('.originalProducts').hide();

                var list = JSON.parse(jsonEscape(data));

                for (key in list) {
                    var tr = $('<tr />');
                    
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>courses/update/' + list[key].id).html(list[key].title));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>courses/update/' + list[key].id).html(list[key].modules));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>courses/update/' + list[key].id).html(list[key].description));

                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>courses/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>courses/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.originalProducts').remove();
            $('.paginationContent').show();
            $('.originalProducts').show();
        }
    }).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
</script>

