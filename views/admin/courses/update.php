<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
	<input type="hidden" name="id" value="<?php echo $model->course->id;?>" />
	<input type="hidden" name="redirectTo" value="/admin/courses/update/<?php echo $model->course->id;?>" />

	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<div class="form-group">
					<label>Name</label>
					<?php echo $model->form->editorFor("title"); ?>
				</div>
				<div class="form-group">
					<label>Description</label>
                    <?php echo $model->form->textAreaFor("description",["class"=>"ckeditor"]); ?>
				</div>
			</div>
            <h2 style='margin-top: 60px; margin-bottom: 20px;'>Modules <a class="btn btn-info" onclick="add_module()">Add Module+</a></h2>
            <div role="tabpanel">
                <ul class="nav nav-tabs" id="module_tabs" role="tablist">
                    <? $module_tabs = ''; ?>
                    <? foreach ($model->modules as $id => $module){ ?>
                        <li id="mtm_<?=$module->id?>" role="presentation" ><a href="#mt_<?=$module->id?>" aria-controls="general" role="tab" data-toggle="tab"><span class="module_title mt_<?=$module->id?>"><?=$module->title?></span></a></li>
                        <? ob_start(); ?>
                            <div role="tabpanel" class="tab-pane" id="mt_<?=$module->id?>">
                                <div class="box">
                                    <h2><span class="module_title mt_<?=$module->id?>"><?=$module->title?></span> &nbsp;&nbsp;&nbsp; <a class="btn btn-danger" onclick="delete_module(<?=$module->id?>)">Remove</a></h2>
                                    <p>Enrolled Students: <?=$module->getEnrolledStudentCount()?></p>
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input class="update_title" data-titles=".mt_<?=$id?>" name="module[<?=$id?>][title]" value="<?=$module->title?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input name="module[<?=$id?>][keyword]" value="<?=$module->keyword?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Short Desc</label>
                                        <input name="module[<?=$id?>][short_description]" value="<?=$module->short_description?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Long Desc</label>
                                        <textarea name="module[<?=$id?>][long_description]"> <?=$module->long_description?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Video Url</label>
                                        <input name="module[<?=$id?>][video_url]" value="<?=$module->video_url?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Duration</label>
                                        <input name="module[<?=$id?>][video_duration_in_seconds]" value="<?php echo gmdate("i:s", $module->video_duration_in_seconds)?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Video Background Image</label>
                                        <p>
                                            <small>(190px x 120px)</small>
                                        </p>
                                        <p><input type="file" name="video_background_image" class='image'/></p>

                                        <div style="display:inline-block">
                                            <?php
                                            $img_path = "";
                                            if($module->video_background_image != "") {
                                                $img_path = UPLOAD_URL .$module->video_background_image; ?>
                                                <div class="well well-sm pull-left">
                                                    <img src="<?php echo $img_path; ?>" width="100"/>
                                                    <br/>
                                                    <a href="<?= ADMIN_URL . 'course_module/delete_image/' . $module->id; ?>?video_background_image=1"
                                                       onclick="return confirm('Are you sure?');"
                                                       class="btn btn-default btn-xs">Delete</a>
                                                    <input type="hidden" name="video_background_image"
                                                           value="<?= $module->video_background_image ?>"/>
                                                </div>
                                            <?}?>
                                            <div class='preview-container'></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <h2>Question-Answers <a class="btn btn-info" href="/admin/courses/addQuestion/<?=$module->id?>">Add Questions+</a></h2>
                                    <table id="data-list" class="table questions_table" style="word-wrap: break-word;table-layout: fixed;">
                                        <thead>
                                            <tr>
                                                <th width="30%">Question</th>
                                                <th width="10%"> Question Number</th>
                                                <th width="40%">Correct Answer</th>
                                                <th width="15%" class="text-center">Edit</th>
                                                <th width="15%" class="text-center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $questions = \Model\Question::getList(['where'=>"course_module_id = $module->id"]); 
                                        foreach($questions as $q) { 
                                            $answer = \Model\Answers::getItem(null,['where'=>"question_id = ".$q->id . " and correct = 1"]);?>
                                            <tr id="question<?=$q->id?>">
                                                <td><a href="/admin/questions/update/<?php echo $q->id ?>"><?php echo $q->title; ?></a></td>
                                                <td><a href="/admin/questions/update/<?php echo $q->id ?>"><?php echo  $q->number; ?></a></td>
                                                <td><a href="/admin/questions/update/<?php echo $q->id ?>"><?php echo $answer->title; ?></a></td>
                                                <td class="text-center">
                                                    <a class="btn-actions" href="/admin/questions/update/<?php echo $q->id ?>">
                                                        <i class="icon-pencil"></i>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn-actions del_question" data-question="<?= $q->id?>" href="">
                                                        <i class="icon-cancel-circled"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <? $module_tabs .= ob_get_clean(); ?>
                    <? } ?>
                </ul>
                <div class="tab-content" id="modules_info">
                    <?=$module_tabs?>
                </div>
            </div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>
<link rel="stylesheet" type="text/css" href="/content/frontend/css/jquery.tagsinput.css">
<script src="/content/frontend/js/jquery.tagsinput.js"></script>
<script type='text/javascript'>
	$(document).ready(function() {

		$("#email-tags").tagsInput({
			'defaultText':'add an email, use comma to separate'
		});

		$("select.multiselect").multiselect();

		$(document).on('input','.update_title',function(e){
		    var titles = $(this).data('titles');
		    $('span'+titles).text(this.value);
        });
        $($('#module_tabs li a')[0]).click()

        $(".del_question").click( function(e) {
            if(window.confirm('Are You Sure You want to delete this question?')){;
                e.preventDefault();
                $("#question"+$(this).data('question')).remove();
                $.post('/admin/courses/delete_question',{id: $(this).data('question')}, function(response){
                    console.log(response);
                });
            }
        })

	});

</script>
<script type="text/javascript">
    function delete_module(module_id){
        if(confirm('Are you sure you want to remove this module?')){
            $.post('/admin/courses/remove_module',{id:module_id},function(data){
                if(data.status){
                    $('#mt_'+module_id).remove();
                    $('#mtm_'+module_id).remove();
                    $($('#module_tabs li a')[0]).click();
                }
            });
        }
    }
    function add_module(){
        var module_info = '<div role="tabpanel" class="tab-pane" id="mt_%%">\n' +
            '<div class="box">\n' +
            '<h2><span class="module_title mt_%%">New Module</span> &nbsp;&nbsp;&nbsp; <a class="btn btn-danger" onclick="delete_module(%%)">Remove</a></h2>\n' +
            '<div class="form-group">\n' +
            '<label>Title</label>\n' +
            '<input class="update_title" data-titles=".mt_%%" name="module[%%][title]" value="New Module"/>\n' +
            '</div>\n' +
            '<div class="form-group">\n' +
            '<label>Keyword</label>\n' +
            '<input name="module[%%][keyword]" value=""/>\n' +
            '</div>\n' +
            '<div class="form-group">\n' +
            '<label>Short Desc</label>\n' +
            '<input name="module[%%][short_description]" value=""/>\n' +
            '</div>\n' +
            '<div class="form-group">\n' +
            '<label>Long Desc</label>\n' +
            '<textarea name="module[%%][long_description]"> </textarea>\n' +
            '</div>\n' +
            '<div class="form-group">\n' +
            '<label>Video Url</label>\n' +
            '<input name="module[%%][video_url]" value=""/>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>';
        var bookmark_info = '<li id="mtm_%%" role="presentation" ><a href="#mt_%%" aria-controls="general" role="tab" data-toggle="tab"><span class="module_title mt_%%">New Module</span></a></li>';
        $.post('/admin/courses/add_module',{id:<?=$model->course->id?>},function(data){
            if(data.status){
                var mod_id = data.module_id;
                module_info = module_info.replace(/%%/g,mod_id);
                bookmark_info = bookmark_info.replace(/%%/g,mod_id);
                $('#module_tabs').prepend(bookmark_info).click();
                $($('#mtm_'+mod_id+' a')[0]).trigger('click');
                $('#modules_info').prepend(module_info);
                $('tabpanel').removeClass('active');
                $('#mt_'+mod_id).addClass('active');
            }
        });

    }
</script>