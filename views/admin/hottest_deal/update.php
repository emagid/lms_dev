<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->hottest_deal->id;?>" />
  <div class="row">
    <div class="col-md-12">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Product</label>
                <input disabled="disabled" type="text" value="<?=$model->hottest_deal->getProduct()->name?>"/>
            </div>
            <div class="form-group">
                <label>Title</label>
<!--                --><?php //echo $model->form->editorFor("title"); ?>
                <input name="title" type="text" value="<?=$model->hottest_deal->title?>"/>
            </div>
            <div class="form-group">
                <label>Subtitle</label>
<!--                --><?php //echo $model->form->editorFor("subtitle"); ?>
                <input name="subtitle" type="text" value="<?=$model->hottest_deal->subtitle?>"/>
            </div>
            <div class="form-group">
                <label>Discount by percentage</label>
<!--                --><?php //echo $model->form->editorFor("discount"); ?>
                <input name="discount" type="text" value="<?=$model->hottest_deal->discount?>"/>
                <div id="discount-price">Current value: $<?=$model->hottest_deal->getProduct()->price?></div>
            </div>
            <div class="form-group">
                <label>Start Date</label>
                <input name="start_date" type="text" class="datetimepicker" value="<?=$model->hottest_deal->start_date?>">
            </div>
            <div class="form-group">
                <label>End Date</label>
                <input name="end_date" type="text" class="datetimepicker" value="<?=$model->hottest_deal->end_date?>">
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status">
                    <option value="0" <?if($model->hottest_deal->status == false){echo 'selected';}?>>Inactive</option>
                    <option value="1" <?if($model->hottest_deal->status == true){echo 'selected';}?>>Active</option>
                </select>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php footer();?>

<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS?>jquery.datetimepicker.css"/ >
<script src="<?=ADMIN_JS?>plugins/jquery.datetimepicker.full.min.js"></script>

<script>
$(document).ready(function(){
    $('input[name=discount]').on('input',function(){
        console.log('fired');
        if($.isNumeric($(this).val())){
            $('#discount-price').html('Discount: $'+((1-($(this).val()/100))*<?=$model->hottest_deal->getProduct()->price?>).toFixed(2));
        } else {
            $('#discount-price').html('Original: $'+<?=$model->hottest_deal->getProduct()->price?>);
        }
    });

    $('.datetimepicker').datetimepicker();
})
</script>
 