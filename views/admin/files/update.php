<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="acive"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li> 
      <? if($model->file->id){ ?>
        <li role="presentation"><a href="#accounts-tab" aria-controls="general" role="tab" data-toggle="tab">Accounts</a></li>
      <? } ?>
       
    </ul>

    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general-tab">
        <input name="id" type="hidden" value="<?php echo $model->file->id;?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />

          <div class="row">
            <div class="col-md-24">
                <div class="box">
                  <h4>Add files that managers can access and download.</h4>
                    <div class="form-group">
                      <label>File Name</label>
                      <?php echo $model->form->textBoxFor('name'); ?>
                    </div>
                    <div class="form-group">
                      <label>File</label>
                      <p><input type="file" name="file" class='image'/></p>

                      <div style="display:inline-block">
                          <?php
                          $img_path = "";
                          if($model->file->file != "") {
                              $img_path = UPLOAD_URL .$model->file->file; ?>
                              <div class="well well-sm pull-left">
                                  <img src="<?php echo $img_path; ?>" width="100"/>
                                  <br/>
                                  <a href="<?= ADMIN_URL . 'file/delete_image/' . $model->file->id; ?>?file=1"
                                     onclick="return confirm('Are you sure?');"
                                     class="btn btn-default btn-xs">Delete</a>
                                  <input type="hidden" name="file"
                                         value="<?= $model->file->file ?>"/>
                              </div>
                          <?}?>
                          <div class='preview-container'></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Is Accessible ? (check the box if it is downloadable for Managers)</label>
                      <?php echo $model->form->checkBoxFor('default_file',1); ?>
                    </div> 
                </div>
            </div>
            <!-- <div class="col-md-12">
                <div class="box">
                  <h4>Assign this file to Account</h4>
                  <label>Account</label>
                  <select name="account" class="form-control">
                    <option value="" selected disabled>None</option>
                    <?php foreach ($model->accounts as $account) {
                        $select = ($account->id) ? " selected='selected'" : "";
                        ?>
                        <option value="<?php echo $account->id; ?>" <?php echo $select; ?> ><?php echo $account->name; ?></option>
                    <?php } ?>
                  </select>
                </div>
            </div> -->
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="accounts-tab">
          <div class="row">
            <div class="col-md-24">
                <div class="box">
                  <h4>Accounts assigned this file</h4>
                  <table class="table">
                    <thead>
                      <tr>
                        <th colspan="2">Account</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <? $file_id = $model->file->id; 
                    $sql = "SELECT * from accounts where id IN (SELECT distinct account_id from account_files where file_id = $file_id);" ?>
                    <tbody id="managers-list">
                      <?php foreach(\Model\Account::getList(['sql'=>$sql]) as $obj){ ?>
                        <tr class="originalProducts">
                          <td colspan="2"><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></td>
                          <td>
                            <a class="btn-actions" href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>">
                              <i class="icon-pencil"></i> 
                            </a>
                          </td>
                          <td>
                            <a class="btn-actions" href="<?php echo ADMIN_URL; ?>accounts/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                              <i class="icon-cancel-circled"></i> 
                            </a>
                          </td>
                        </tr>
                      <? } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
