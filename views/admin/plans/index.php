<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>

<div class="row filter_holder">
  <form>
    <div>
        <label>Search:</label> 
        <input id="search" class="form-control" type="text" name="search" placeholder="Search by Name"/>
    </div> 
  </form>
</div>

<?php
 if(count($model->plans)>0): ?>
 <?php //dd($model)?>
  <div class="box box-table">
    <table id="data-list" class="table plans_table">
      <thead>
        <tr>
          <th width="15%">Plan Name</th>
          <th width="15%">Max Students</th>
          <th width="15%">Tiers</th>
          <th width="15%">Courses</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody class="sort">
       <?php foreach($model->plans as $obj){ ?>
        <tr class="originalProducts">
         <td>
            <a href="<?php echo ADMIN_URL; ?>plans/update/<?= $obj->id ?>"><? $school = \Model\Account::getItem($obj->account_id); ?>
                <? if ( strtoupper($obj->name) == 'GOLD' ) { ?>
                    <img style='height: 30px; margin-right: 10px;' src="/content/frontend/img/gold.png">
                <? } else if ( strtoupper($obj->name) == 'SILVER' ) { ?>
                    <img style='height: 30px; margin-right: 10px;' src="/content/frontend/img/silver.png">
                <? } else if ( strtoupper($obj->name) == 'BRONZE' ) {?>
                    <img style='height: 30px; margin-right: 10px;' src="/content/frontend/img/bronze.png">
                <? } ?>
            </a>
            <a href="<?php echo ADMIN_URL; ?>plans/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>plans/update/<?= $obj->id ?>"><?php echo $obj->student_limit; ?></a></td>
         <td>
             <? foreach($obj->getTiers() as $tier) { ?>
                <a href="<?php echo ADMIN_URL; ?>plans/update/<?= $obj->id ?>"><? echo "$tier->min_users - $tier->max_users: $".number_format($tier->price,2) ?></a><br />
             <? } ?>
         </td>
         <td>
             <? $courses = $obj->getCourses(); ?>
             <a href="<?php echo ADMIN_URL; ?>plans/update/<?= $obj->id ?>">
             <? foreach($courses as $i => $course) { ?>
             <?=$course->title?>
                 <? if($i < count($courses) - 1) echo "<br/>" ?>
                 <? if(count($courses) > 5 && $i == 4) { echo "..."; break; } ?>
             <? } ?>
             </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>plans/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i>
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>plans/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i>
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'categories';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

    $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>plans/search";
        var keywords = $(this).val();
        if (keywords.length > 2) {
            $.get(url, {keywords: keywords}, function(data) {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').hide();
                $('.originalProducts').hide();

                var list = JSON.parse(data);

                for (key in list) {
                    var tr = $('<tr />');
                    var img = $('<img />').prop('src', "https://sm2elearning.com/uploads/account/logo/" + list[key].school_id + "/" + list[key].school_logo);
                    
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>plans/update/' + list[key].id).html(list[key].name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>plans/update/' + list[key].id).html(list[key].student_limit));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>plans/update/' + list[key].id).html(list[key].tier));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>plans/update/' + list[key].id).html(list[key].courses));

                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>plans/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>plans/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.originalProducts').remove();
            $('.paginationContent').show();
            $('.originalProducts').show();
        }
    }).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
</script>
<script>
    $(document).ready(function(){
        var adjustment;
        $('#sortables').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden">.</td></tr>',
            onDrop: function ($item, container, _super) {
//                var $clonedItem = $('<tr/>').css({height: 0});
//                $item.before($clonedItem);
//                $clonedItem.animate({'height': $item.height()});
//
//                $item.animate($clonedItem.position(), function  () {
//                    $clonedItem.detach();
//                    _super($item, container);
//                });
//                if ($(event.target).hasClass('delete-product-image')) {
//                    $(event.target).trigger('click');
//                }
//
//                var ids = [];
//                var tr_containers = $("#image-container > tbody > tr");
//                tr_containers.each(function (i, e) {
//                    ids.push($(e).data("image_id"));
//
//                });
//                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
//                    sort_number_display();
//                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
    })
</script>