<style type="text/css">
    .form-group .custom-select .select-selected:after {
        top: 18px;
        right: 13px;
    }

    .select-selected.select-arrow-active:after {
        border-color: #000033 transparent #fff transparent;
    }
</style>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
        <li role="presentation"><a href="#tiers-tab" aria-controls="general" role="tab" data-toggle="tab">Tiers</a></li>
        <li role="presentation"><a href="#courses-tab" aria-controls="general" role="tab" data-toggle="tab">Courses</a></li>
        <li role="presentation"><a href="#categories-tab" aria-controls="general" role="tab" data-toggle="tab">Categories</a></li>
    </ul>
    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="test">
            <input type="hidden" name="id" value="<?php echo $model->plan->id; ?>" />
            <input name="token" type="hidden" value="<?php echo get_token();?>" />
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Name</label>
                            <?php echo $model->form->editorFor("name"); ?>
                        </div>
                        <div class="form-group">
                            <label>Student Limit</label>
                            <?php echo $model->form->editorFor("student_limit"); ?>
                        </div>
                        <div class="form-group">
                            <label>Display on plans page?</label>
                            <?php echo $model->form->checkBoxFor("display",1); ?>
                        </div>
                        <div class="form-group">
                            <label>Features <super>( Separate with '|' )</super></label>
                            <textarea name="features" ><?=$model->plan->features?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="courses-tab">
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Courses</h4>
                        <div class="form-group">
                            <label>Courses available on this plan</label>
                            <select multiple class="multiselect" name="courses[]">
                                <? foreach (\Model\Course::getList() as $course){ ?>
                                    <? $selected = in_array($course->id,$model->courses)?'selected':'' ?>
                                    <option <?=$selected?> value="<?=$course->id?>"><?=$course->title?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tiers-tab">
            <div class="row">
                <div class="col-md-24">
                    <div class='box filter_holder'>
                        <a class="btn" id="add_tier">Add a tier</a>
                    </div>
                    <div class="box box-table" style="min-height:500px">
                        <h4>Tiers</h4>
                        <table id="tier-container" class="table">
                            <thead>
                            <tr>
                                <th width="10%">Monthly Price per user</th>
                                <th width="15%">Minimum Users</th>
                                <th width="16%">Maximum Users</th>
                                <th width="16%">Remove</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach(\Model\Plan_Tiers::getList(['where'=>"plan_id = {$model->plan->id}"]) AS $tier) { ?>
                                <tr>
                                    <td>
                                        <input type="number" name="tiers[<?=$tier->id?>][price]" value="<?=$tier->price?>" />
                                    </td>
                                    <td>
                                        <input type="number" name="tiers[<?=$tier->id?>][min_users]" value="<?=$tier->min_users?>" />
                                    </td>
                                    <td>
                                        <input type="number" name="tiers[<?=$tier->id?>][max_users]" value="<?=$tier->max_users?>" />
                                    </td>
                                    <td>
                                        <a class="remove_tier btn">Remove Tier</a>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="categories-tab">
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Categories</h4>
                        <div class="form-group">
                                <label>Account Categories</label>
                            <div class="custom-select">
                                <select name="category_id" class="form-control">
                                    <?php foreach ($model->acategories as $acategory) {
                                        $select = ($acategory->id) ? " selected='selected'" : "";
                                        ?>
                                        <option value="<?php echo $acategory->id; ?>" <?php echo $select; ?> ><?php echo $acategory->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    var new_tiers = 0;
	$(document).ready(function() {
		var courses = <?= json_encode($model->courses)?>;

		$('#add_tier').click(function () {
            var container = $('#tier-container tbody');
            var html = '' +
                '<tr>' +
                    '<td>\n' +
                    '<input type="number" name="tiers[0]['+new_tiers+'][price]" />' +
                    '</td>\n' +
                    '<td>\n' +
                    '<input type="number" name="tiers[0]['+new_tiers+'][min_users]" />' +
                    '</td>\n' +
                    '<td>\n' +
                    '<input type="number" name="tiers[0]['+new_tiers+'][max_users]" />' +
                    '</td>\n' +
                    '<td>\n' +
                        '<a class="remove_tier btn">Remove Tier</a>\n' +
                    '</td>\n' +
                '</tr>'
            new_tiers++;
            container.append($(html));
        });

		$(document).on('click','.remove_tier',function () {
            $(this).parents('tr').remove();
        });

	    function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'600');
					img.attr('height','172');
					$("#preview-container").html(img);
				};

				$(input).parent().parent().find('input[type="hidden"]').remove();

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
			$('#previewupload').show();
		});	

		$("input[name='name']").on('keyup',function(e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g,'-');
			$("input[name='slug']").val(val.toLowerCase());
		});

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });


        $('select[name="courses[]"]').val(courses);
        $('select.multiselect').multiselect('rebuild');
	});
</script>