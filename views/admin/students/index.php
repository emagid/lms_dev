<style type="text/css">
  .flex_forms {
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;
  }

  .flex_forms .custom-select {
    margin: 15px 10px;
  }

  .select-selected:after {
    top: 40px;
  }

  .export {
    float: right;
  }
</style>

<div class="row filter_holder">
  <form id="filters">
    <div class='flex_forms'>


        <div class="custom-select">
          Filter by Account:
          <select name="school">
              <option disabled selected>Select an Account</option>
              <? foreach($model->accounts AS $account) { ?>
                  <option value="<?=$account->id?>" <?= isset($_GET['school']) && $_GET['school'] == $account->id?'selected':'' ?>>
                      <?=$account->name?>
                  </option>
              <? } ?>
          </select>
        </div>

        <div class="custom-select">
          Filter by Category:
          <select name="category">
              <option disabled selected>Select a Category</option>
              <? foreach($model->categories AS $category) { ?>
                  <option value="<?=$category->id?>" <?= isset($_GET['category']) && $_GET['category'] == $category->id?'selected':'' ?>>
                      <?=$category->name?>
                  </option>
              <? } ?>
          </select>
        </div>

        <div class="custom-select">
          Filter by Course:
          <select name="course">
              <option disabled selected>Select a Course</option>
              <? foreach($model->courses AS $course) { ?>
                  <option value="<?=$course->id?>" <?= isset($_GET['course']) && $_GET['course'] == $course->id?'selected':'' ?>>
                      <?=$course->title?>
                  </option>
              <? } ?>
          </select>
        </div>

        <div class="custom-select">
          Filter by Topic:
          <select name="module">
              <option disabled selected>Select a Topic</option>
              <? foreach($model->modules AS $module) { ?>
                  <option value="<?=$module->id?>" <?= isset($_GET['module']) && $_GET['module'] == $module->id?'selected':'' ?>>
                      <?=$module->title?>
                  </option>
              <? } ?>
          </select>
        </div>

        
    </div>

    <a class="export btn btn-warning">Export CSV</a>

    <a href="<?= ADMIN_URL?>students" class="btn btn-primary" >Reset Filters</a>
    
  </form>
</div>

<div class="row filter_holder">
  <div style='margin: 15px 10px;'>
    <div class="custom-select">
        Show on page:
        <select class="how_many" name="how_many" style="cursor:pointer">

            <option <? if (isset($_GET['how_many'])) {
                if ($_GET['how_many'] == 10) {
                    echo "selected";
                }
            } ?> selected value="10">10
            </option>
            <option <? if (isset($_GET['how_many'])) {
                if ($_GET['how_many'] == 50) {
                    echo "selected";
                }
            } ?> value="50">50
            </option>
            <option <? if (isset($_GET['how_many'])) {
                if ($_GET['how_many'] == 100) {
                    echo "selected";
                }
            } ?> value="100">100
            </option>
            <option <? if (isset($_GET['how_many'])) {
                if ($_GET['how_many'] == 500) {
                    echo "selected";
                }
            } ?> value="500">500
            </option>
            <option <? if (isset($_GET['how_many'])) {
                if ($_GET['how_many'] == 1000) {
                    echo "selected";
                }
            } ?> value="1000">1000
            </option>
        </select>
    </div>
  </div>
  <form>
    <div>
      Search:
      <input style='margin-top: 5px;' id="search" class="form-control" type="text" name="search" placeholder="Search by Name or Email"/>
    </div> 
  </form>
</div>

<?php
    $model->students = $model->users;
if(count($model->students)>0) { ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="20%">Name</th>
          <th width="20%">Email</th>
          <th width="20%">School</th>
          <th width="20%">Category</th>
          <th width="10%">Status</th>
          <th width="20%">Last Login</th>
          <th width="20%">Number of Enrolled Courses</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
        <?php foreach($model->students as $obj){ 
          $student_activity = \Model\Audits::getItem(null,['where'=>"user_id = $obj->id AND action = 'user_login'"]);
          if($student_activity){
            $last_login = $student_activity->created_at;
          }else{
            $last_login = ' - ';
          }
        ?>
        <tr class="originalProducts">
         <td><a href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>"><?php echo $obj->full_name(); ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>"><?php echo $obj->email?></a></td>
         <td>
             <a href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>">
             <? $school = \Model\Account::getItem($obj->account_id); ?>
                 <img src="https://sm2elearning.com/uploads/account/logo/<?=$school?$school->id:''?>/<?=$school?$school->logo:''?>">
             </a>
         </td>
         <td>
             <a href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>">
             <? $category = \Model\Category::getItem($obj->category_id); 
              if($category){
                if($category->name != ''){ 
                  echo $category->name;
                }else{ 
                  echo "None";
                }
              }else{
                echo "None";
              }
              ?>
             </a>
         </td>
         <td><a href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>"><?php echo $obj->status; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>"><?php echo $last_login; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>"><?php echo count($obj->getCourses())?></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>students/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>students/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'students';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;

  $('#filters :input').change(function(e){
     $(this).parents('form').submit();
  });


  $('.export').on('click', function(){    
    var url = "<?php echo ADMIN_URL; ?>students/exportCSV";
    $.post(url, $('#filters').serialize(), function(data){
      console.log(data);
    })
  });


  $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>students/search";
        var keywords = $(this).val();
        if (keywords.length > 2) {
            $.get(url, {keywords: keywords}, function(data) {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').hide();
                $('.originalProducts').hide();

                var list = JSON.parse(data);

                for (key in list) {
                    var tr = $('<tr />');
                    var img = $('<img />').prop('src', "https://sm2elearning.com/uploads/account/logo/" + list[key].school_id + "/" + list[key].school_logo);
                    
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>students/update/' + list[key].id).html(list[key].name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>students/update/' + list[key].id).html(list[key].email));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>students/update/' + list[key].id).html(img));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>students/update/' + list[key].id).html(list[key].enrolled_courses));

                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>students/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>students/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.originalProducts').remove();
            $('.paginationContent').show();
            $('.originalProducts').show();
        }
    }).keydown(function(event){
      if(event.keyCode == 13) {
          event.preventDefault();
          return false;
      }
    });
</script>

