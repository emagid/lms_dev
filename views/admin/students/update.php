<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li>
            <li role="presentation" ><a href="#courses-tab" aria-controls="courses"  role="tab" data-toggle="tab">Course Status</a></li>
            <li role="presentation" ><a href="#activity-tab" aria-controls="activity"  role="tab" data-toggle="tab">Activity Log</a></li>
            <li role="presentation" ><a href="#certificates-tab" aria-controls="certificates"  role="tab" data-toggle="tab">Certificates</a></li>
            <li role="presentation" ><a href="#history-tab" aria-controls="history"  role="tab" data-toggle="tab">Course History</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->editorFor("first_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->editorFor("last_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <?php echo $model->form->editorFor("phone"); ?>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <?php echo $model->form->editorFor("email"); ?>
                            </div>
                            <div class="form-group">
                                <div class="custom-select">
                                    <label>Category</label>
                                    <?php echo $model->form->dropDownListFor("category_id",$model->categories); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Comments</label>
                                <?php echo $model->form->editorFor("comments"); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->editorFor("address"); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->editorFor("city"); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?php echo $model->form->editorFor("state"); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip</label>
                                <?php echo $model->form->editorFor("zip"); ?>
                            </div>
                            <div class="form-group">
                                <label>Signin Count</label>
                                <?php echo $model->form->textBoxFor("sign_in_count",['readonly'=>true]); ?>
                            </div>
                            <div class="form-group">
                                <label>Password (leave blank to keep current password)</label>
                                <input type="password" name="password" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="courses-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Course Title</th>
                                        <th>Module Title</th>
                                        <th>Video Duration</th>
                                        <th colspan =2 >Video Progress</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? foreach ($model->enrolled_courses as $ucm){
                                        $course = $ucm->module->get_course();
                                        ?>
                                    <tr>
                                        <td><?=$course->title?></td>
                                        <td><?=$ucm->module->title?></td>
                                        <td><?=gmdate("i:s", $ucm->module->video_duration_in_seconds);?></td>
                                        <?
                                            $input = $ucm->video_progress_in_milliseconds;

                                            $uSec = $input % 1000;
                                            $input = floor($input / 1000);

                                            $seconds = $input % 60;
                                            $input = floor($input / 60);

                                            $minutes = $input % 60;
                                            $input = floor($input / 60);
                                        ?>
                                        <td><?=str_pad($minutes,2,'0',STR_PAD_LEFT).':'.str_pad($seconds,2,'0',STR_PAD_LEFT)?> </td><td> %<?=$ucm->video_progress_in_percent*100?></td>
                                    </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="activity-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <table class="table" id="activity-list">
                                <thead>
                                <tr>
                                    <th style="display:none;">ID</th>
                                    <th>Time</th>
                                    <th>Device</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach (\Model\Audits::getList(['where'=>"user_id = {$model->user->id}",'orderBy'=>"id DESC"]) as $audit){ ?>
                                    <tr>
                                        <td style="display:none;"><?=$audit->id?></td>
                                        <td><?=$audit->created_at?></td>
                                        <td><?=$audit->device ?> </td>
                                        <td><?=$audit->action_description?></td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="certificates-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <table class="table" id="certificate-list">
                                <thead>
                                <tr>
                                    <th>Course Title</th>
                                    <th>Earned Date</th>
                                    <th>Link</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach (\Model\Certificates::getList(['where'=>"user_id = {$model->user->id}",'orderBy'=>"id DESC"]) as $certificate){ ?>
                                    <? $course = \Model\Course::getItem($certificate->course_id)?>
                                    <tr>
                                        <td><?=$course->title ?> </td>
                                        <td><?=$certificate->issued_at?></td>
                                        <td><?=SITE_URL."certificate/".$certificate->access_token?></td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="history-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Course History</h4>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Select Course</label>
                                    <select id="view_course">
                                        <? foreach (\Model\Course::getList(['where'=>"id IN(SELECT cm.course_id FROM course_modules cm, user_course_modules ucm WHERE cm.id = ucm.course_module_id AND ucm.user_id = {$model->user->id}) "]) as $course) { ?>
                                        <option value="<?=$course->id?>"><?=$course->title?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Select Module</label>
                                    <select id="view_module">
                                        <? foreach($model->enrolled_courses as $ucm){ ?>
                                            <? if(\Model\Response_Sets::getCount(['where'=>"user_course_module_id = {$ucm->id}"]) == 0) { continue; }?>
                                            <option value="<?=$ucm->id?>" data-course_id="<?=$ucm->module->course_id?>" ><?=$ucm->module->title?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Select Attempt</label>
                                    <select id="view_attempt">
                                        <?  $attemptsSql = "SELECT distinct rs.quiz_attempt AS quiz_attempt, rs.user_course_module_id as ucm_id FROM response_sets rs, user_course_modules ucm WHERE rs.user_course_module_id = ucm.id AND ucm.user_id = {$model->user->id}";
                                            global $emagid;
                                            $db = $emagid->getDb();
                                            $results = $db->getResults($attemptsSql);
                                            foreach ($results AS $result) {
                                        ?>
                                            <option value="<?=$result['quiz_attempt']?>" data-ucm_id="<?=$result['ucm_id']?>"><?=ordinal($result['quiz_attempt'])?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                            <table class="table" id="activity-list">
                                <thead>
                                <tr>
                                    <th>Question</th>
                                    <th>Selected Answer</th>
                                    <th>Correct Answer</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?
                                        $attemptsSql = "SELECT rs.quiz_attempt AS quiz_attempt, q.title AS question, ga.title AS given_answer , ca.title AS correct_answer, ga.correct as correct, ucm.id AS ucm_id
                                                        FROM user_course_modules ucm, questions q, answers ca, 
                                                          answers ga, response_sets rs, responses r, course_modules cm
                                                        WHERE ucm.user_id = {$model->user->id} AND rs.user_course_module_id = ucm.id AND 
                                                              cm.id = ucm.course_module_id AND rs.question_id = q.id AND r.response_set_id = rs.id AND 
                                                              ga.id = r.answer_id AND ca.question_id = q.id AND ca.correct = 1 AND
                                                              r.active = 1 AND ucm.active = 1 AND cm.active = 1 AND rs.active = 1 AND q.active = 1 AND 
                                                              ga.active = 1 AND ca.active = 1 
                                                        ORDER BY ucm.id ASC, rs.quiz_attempt ASC, q.number ASC";
                                        global $emagid;
                                        $db = $emagid->getDb();
                                        $results = $db->getResults($attemptsSql);
                                        foreach ($results AS $result) {
                                    ?>
                                        <tr class="answer <?=$result['correct']?'correct_answer':''?>" data-attempt_id="<?=$result['quiz_attempt']?>" data-ucm_id="<?=$result['ucm_id']?>">
                                            <td><?=$result['question']?></td>
                                            <td <?=$result['correct']?'colspan="2"':''?>><?=$result['given_answer']?></td>
                                            <? if($result['correct'] != 1) { ?>
                                                <td><?=$result['correct_answer']?></td>
                                            <? } ?>
                                        </tr>
                                    <? } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2">Total Correct Answers</th>
                                        <th id="correct_count"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <input type="hidden" name="id" value="<?php echo $model->user->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <button type="submit" class="btn btn-save">Save</button>
  
</form>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
    $('#activity-list').DataTable({
        "bPaginate": true,
        "bInfo": false,
        "bSort": false
    });



    $("#view_attempt").change(function(){
        $('tr.answer').hide().removeClass('visible').filter('[data-attempt_id='+this.value+'][data-ucm_id='+$('#view_module').val()+']').show().addClass('visible');
        $('#correct_count').text($('tr.answer.correct_answer:visible').length+'/'+$('tr.answer:visible').length);
    });
    $("#view_module").change(function(){
        $('#view_attempt').find('option').hide().removeClass('visible').filter('[data-ucm_id='+this.value+']').show().addClass('visible');
        var attempt = $('#view_attempt option.visible').first().val();
        $('#view_attempt').val(attempt).change();
    });
    $("#view_course").change(function(){
        $('#view_module').find('option').hide().removeClass('visible').filter('[data-course_id='+this.value+']').show().addClass('visible');
        var module = $('#view_module option.visible').first().val();
        $("#view_module").val(module).change();
    }).change();
});
</script>