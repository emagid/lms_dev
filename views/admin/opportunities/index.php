<?php if(count($model->opportunities)>0) { ?>
    <!-- Modal -->
    <div class="modal fade" id="calendar-modal" tabindex="-1" role="dialog" aria-labelledby="Calendar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add To Calendar</h4>
                </div>
                <div class="modal-body">
                    <h6>Please fill up the form, and click update button at bottom before add to calendar</h6>
                    <div class="row content-row" style="margin-bottom: 10px;" id="fmail">
                        <div class="col-12 col-lg-12 col-sm-12">

                            <div class="form-inline" style="margin-bottom:10px; margin-top:10px;">
                                <div class="form-group">
                                    <label for="f_date_start">Date start</label>
                                    <input type="text" id="f_date_start" value="" placeholder="0000-00-00 00:00:00">
                                </div>
                                <div class="form-group">
                                    <label for="f_date_end">Date end</label>
                                    <input type="text" id="f_date_end" value="" placeholder="0000-00-00 00:00:00">
                                </div>
                            </div>

                            <div class="form-inline" style="margin-bottom:10px; margin-top:10px;">
                                <div class="form-group">
                                    <label for="f_timezone" class="">Timezone</label><br>
                                    <select id="f_timezone" class="form-control">
                                        <option value="America/New_York">(UTC-0500) America/New_York</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="f_location">Location</label>
                                    <input type="text" id="f_location" value="" placeholder="Location">
                                </div>
                            </div>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="f_organizer">Organizer</label>
                                    <input type="text" id="f_organizer" value="" placeholder="Organizer name">
                                </div>
                                <div class="form-group">
                                    <label for="f_organizer_email">Email</label>
                                    <input type="email" id="f_organizer_email" value="" placeholder="Organizer email">
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 col-sm-12">
                            <div class="form-horizontal" style="padding:15px;">
                                <div class="form-group">
                                    <label for="f_title">Event title</label>
                                    <input id="f_title" class="form-control" value="" placeholder="Title" style="max-width:400px;" maxlength="70">
                                </div>
                                <div class="form-group">
                                    <label for="f_description">Event description</label>
                                    <textarea id="f_description" class="form-control" placeholder="Description" rows="3" style="max-width: 530px;">Description here...</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="update-calendar">Update</button>
                      <span class="addtocalendar atc-style-blue" data-on-button-click="validateForm">
                          <var class="atc_event">
                              <var class="atc_date_start"></var>
                              <var class="atc_date_end"></var>
                              <var class="atc_timezone">America/New_York</var>
                              <var class="atc_title"></var>
                              <var class="atc_description"></var>
                              <var class="atc_location"></var>
                              <var class="atc_organizer"></var>
                              <var class="atc_organizer_email"></var>
                          </var>
                      </span>
                </div>
            </div>
        </div>
    </div>
  <div class="box box-table" id="calendar-modal">
      <div id="event-detail-panel" style="display: none">


      </div>
      <div id="email-detail-panel" style="display: none">


      </div>
    <table class="table">
      <thead>
        <tr>
          <th width="20%">Applicant</th>
          <th width="20%">Building Type</th>
          <th width="20%">Budget</th>
          <th width="20%">Location</th>
          <th width="20%">Amenities</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	
          <th width="15%" class="text-center">Calendar</th>
          <th width="15%" class="text-center">Mail Applicant</th>
          <th width="15%" class="text-center">Transfer</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->opportunities as $obj){ ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>opportunities/update/<?php echo $obj->id; ?>"><?php echo $obj->getContact()->fullName(); ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>opportunities/update/<?php echo $obj->id; ?>"><?php echo \Model\Opportunities::$buildingType[$obj->building_type]?></td>
         <td><a href="<?php echo ADMIN_URL; ?>opportunities/update/<?php echo $obj->id; ?>">$<?php echo $obj->budget?></td>
         <td><a href="<?php echo ADMIN_URL; ?>opportunities/update/<?php echo $obj->id; ?>"><?php echo ucwords($obj->location)?></td>
         <td><a href="<?php echo ADMIN_URL; ?>opportunities/update/<?php echo $obj->id; ?>"><?php echo $obj->getAmenitiesList('<br>')?></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>opportunities/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>opportunities/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
        <!--Modal Trigger-->
         <td class="text-center">
           <a class="btn-actions calendar-button" href="#"  data-toggle="modal" data-target="#calendar-modal">
             <i class="icon-calendar"></i>
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions open-email-modal" href="#" data-toggle="modal" data-target="#email-modal" data-opportunityid="<?=$obj->id?>" data-id="<?=$obj->getContact()->id?>" data-email="<?=$obj->getContact()->email?>" data-name="<?=$obj->getContact()->fullName()?>">
             <i class="icon-mail"></i>
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="#" data-toggle="modal" data-target="#transfer-modal">
             <i class="icon-arrows-ccw"></i>
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<div id="email-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Email">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Write an Email</h4>
                </div>
                <input id="to_id" hidden>
                <input id="from_id" value="<?=$model->logged_admin->id?>" hidden>
                <input id="opportunity_id" hidden>
                <div class="form-inline" style="margin-bottom:10px; margin-top:10px;">
                    <div class="form-group" style="width: 45%;">
                        <label>To</label>
                        <input type="text" id="to"><br>
                    </div>
                    <div class="form-group" style="width: 45%;">
                        <label>Email</label>
                        <input type="text" id="to_email"><br>
                    </div>
                </div>
                <div class="form-inline" style="margin-bottom:10px; margin-top:10px;">
                    <div class="form-group" style="width: 45%;">
                        <label>From</label>
                        <input type="text" id="from" value="<?=$model->logged_admin->full_name()?>"><br>
                    </div>
                    <div class="form-group" style="width: 45%;">
                        <label>Email</label>
                        <input type="text" id="from_email" value="<?=$model->logged_admin->email?>"><br>
                    </div>
                </div>
                <label>Subject</label>
                <input type="text" id="subject"><br>
                <label>Body</label>
                <textarea id="body"></textarea>
                <br>
                <button class="btn btn-primary" value="Send" id="send-email">Submit</button>
            </div>
        </div>
    </div>
</div>

    <div id="transfer-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Email">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Transfer To</h4>
                    </div>
                    <div class="modal-body">
                        <h5>Please select admin you want to transfer this lead to</h5>
                        <select>
                            <option value="">--SELECT--</option>
                            <?php $admins = \Model\Admin::getList();?>
                            <?php foreach($admins as $admin){ ?>
                                <option value="<?=$admin->id?>"><?=$admin->full_name()?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" value="Send" id="send-email" data-dismiss="modal">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'opportunities';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
    var logged_admin_email = '<?=$model->logged_admin->email?>';
    var logged_admin_name = '<?=$model->logged_admin->full_name()?>';
    $(".calendar-button").on('click', function(){
        $("#event-detail-panel").toggle();
    $("#f_date_start").datetimepicker({
        format: 'Y-m-d h:i'
    });
    $("#f_date_end").datetimepicker({
        format: 'Y-m-d h:i'
    });

    });
    $('.open-email-modal').on('click',function(){
        $("#email-detail-panel").toggle();
        $('#to').val($(this).data('name'));
        $('#to_id').val($(this).data('id'));
        $('#to_email').val($(this).data('email'));
        $('#opportunity_id').val($(this).data('opportunityid'));
    });
    $('#send-email').on('click',function(){
        //to and from are contact_id and admin_id respectively
        var data = {
            to : $('#to_id').val(),
            to_email : $('#to_email').val(),
            from : $('#from_id').val(),
            from_email : $('#from_email').val(),
            subject : $('#subject').val(),
            body : $('#body').val(),
            opportunity_id : $('#opportunity_id').val()
        };
        $.post('/admin/opportunities/sendEmail',data,function(data){
            var jData = $.parseJSON(data);
            console.log(data,jData);
            alert(jData.message);
            $("#email-modal").modal('hide');
        })
    });
    $("#update-calendar").on('click', function(){
        addtocalendar.load();
        $(".addtocalendar").fadeOut().delay(500).fadeIn();
    });

    function validateForm(mouseEvent){
        
    }
</script>

