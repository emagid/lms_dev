<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->opportunities->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-tab" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
            <li role="presentation"><a href="#appointment-tab" aria-controls="appointment" role="tab" data-toggle="tab">Appointments</a></li>
            <li role="presentation"><a href="#com-log-tab" aria-controls="com-log" role="tab" data-toggle="tab">Communications</a></li>
        </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="profile-tab">
          <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>Profile</h4>
            <div class="form-group">
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="first_name" value="<?=isset($model->contact)?$model->contact->first_name:''?>" required>
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="<?=isset($model->contact)?$model->contact->last_name:''?>" required>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" value="<?=isset($model->contact)?$model->contact->phone:''?>" required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" value="<?=isset($model->contact)?$model->contact->email:''?>" required>
                </div>
                <div class="form-group">
                    <label>Comments</label>
                    <input type="text" name="comments" value="<?=isset($model->contact)?$model->contact->comments:''?>" required>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" name="address" value="<?=isset($model->contact)?$model->contact->address:''?>" required>
                </div>
                <div class="form-group">
                    <label>City</label>
                    <input type="text" name="city" value="<?=isset($model->contact)?$model->contact->city:''?>" required>
                </div>
                <div class="form-group">
                    <label>State</label>
                    <input type="text" name="state" value="<?=isset($model->contact)?$model->contact->state:''?>" required>
                </div>
                <div class="form-group">
                    <label>Zip</label>
                    <input type="text" name="zip" value="<?=isset($model->contact)?$model->contact->zip:''?>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="building_type">Building Type</label>
                <select class="form-control" name="building_type" id="building_type">
                    <?foreach(\Model\Opportunities::$buildingType as $key=>$value){?>
                        <option value="<?=$key?>"><?=$value?></option>
                    <?}?>
                </select>
            </div>
            <div class="form-group">
                <label>Budget</label>
                <input type="text" name="budget" value="<?=$model->opportunities->budget?:0?>" required>
            </div>
            <div class="form-group">
                <label>Location</label>
                <input type="text" name="location" value="<?=$model->opportunities->location?:''?>" required>
            </div>
            <div class="form-group">
                <label for="amenities">Amenities</label>
                <select class="form-control" name="amenities[]" id="amenities" multiple required>
                    <?$oppAmenities = $model->opportunities->getAmenitiesArray();
                    $amenitiesId = array_map(function($am){return $am->id;},$oppAmenities);
                    foreach($model->amenities as $amenity){?>
                        <option value="<?=$amenity->id?>" <?=count($amenitiesId)>0 && in_array($amenity->id,$amenitiesId)?'selected':''?>><?=$amenity->name?></option>
                    <?}?>
                </select>
            </div>
            <div class="form-group">
                <label>Moving Date</label>
<!--                --><?php //echo $model->form->editorFor("moving_date"); ?>
<!--                <input name="moving_date" type="text" value="--><?//=date('Y-m-d',strtotime($model->opportunities->moving_date))?><!--">-->
                <input name="moving_date" type="text" value="<?=$model->opportunities->date?date('Y-m-d h:i',strtotime($model->opportunities->date)):null?>" required>
            </div>
        </div>
    </div>
  </div>
            <button type="submit" class="btn btn-save">Save</button>

        </div>
        <div role="tabpanel" class="tab-pane" id="appointment-tab">
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Appointments</h4>
                        <?if(isset($model->appointments) && count($model->appointments)>0){?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>Listings</td>
                                    <td>Date</td>
                                    <td>Location</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?foreach($model->appointments as $appointment){?>
                                <tr>
                                    <td><a href="<?=ADMIN_URL.'appointments/update/'.$appointment->id?>"><?=$appointment->getListingList('<br>')?></a></td>
                                    <td><a href="<?=ADMIN_URL.'appointments/update/'.$appointment->id?>"><?=$appointment->getFormattedDate('Y-m-d h:i')?></a></td>
                                    <td><a href="<?=ADMIN_URL.'appointments/update/'.$appointment->id?>"><?=$appointment->location?></a></td>
                                </tr>
                                <?}?>
                                </tbody>
                            </table>
                        <?} else {?>
                            <div>There are no appointments.</div>
                        <?}?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="com-log-tab">
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Communications</h4>
                        <?if(isset($model->communications) && count($model->communications)>0){?>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>To</td>
                                    <td>From</td>
                                    <td>Subject</td>
                                    <td>Body</td>
                                    <td>Sent Time</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?foreach($model->communications as $communication){?>
                                    <tr>
                                        <td><?=$communication->getTo()->fullName();?></a></td>
                                        <td><?=$communication->getFrom()->full_name()?></a></td>
                                        <td><?=$communication->subject?></a></td>
                                        <td><?=$communication->body?></a></td>
                                        <td><?=$communication->getFormattedDate('Y-m-d h:i:s')?></a></td>
                                    </tr>
                                <?}?>
                                </tbody>
                            </table>
                        <?} else {?>
                            <div>There has been no communication.</div>
                        <?}?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</form>
		 

<?php footer();?>
<link rel = "stylesheet" type = "text/css" href = "<?=ADMIN_CSS.'jquery.datetimepicker.css'?>">
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });

    $('input[name=moving_date]').datetimepicker({
        timepicker:false,
        format: 'Y-m-d'
    });

    $('#amenities').multiselect({
        enableFiltering: true,
        filterBehavior: 'both'
    });
});
</script>