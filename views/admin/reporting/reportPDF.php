<?php

$dateString = $date->toDateString();
$lastYear = $date->copy()->subYear()->addDay();
$lastYearString = $lastYear->toDateString();
//$lastSevenDays = [];
//for($i = 1; $i <= 7; $i++){
//    $lastSevenDays[] = "'".$date->copy()->subDays($i)->toDateString()."'";
//}
//$lastSevenDaysString = implode(',', $lastSevenDays);
//$sql = "SELECT * FROM report_data WHERE location_id in (SELECT id FROM location WHERE type = '$type') AND (date = '$dateString' OR date = '{$lastYearString}')";
//$reportData = \Model\ReportData::getList(['sql' => $sql]);

$fieldOptions = [
    'restaurant' => ['actual' => 'gm_153', 'actualMonth' => 'gm_172', 'budget' => 'bd_153', 'budgetMonth' => 'bd_172', 'lastyear' => 'ly_153', 'lastyearMonth' => 'ly_172'],
    'nightlife' => ['actual' => 'gm_152', 'actualMonth' => 'gm_174', 'budget' => 'bd_152', 'budgetMonth' => 'bd_174', 'lastyear' => 'ly_152', 'lastyearMonth' => 'ly_174'],
    'hotel' => [
        'room' => ['actual' => 'gm_119', 'actualMonth' => 'gm_120', 'budget' => 'bd_119', 'budgetMonth' => 'bd_120', 'lastyear' => 'ly_119', 'lastyearMonth' => 'ly_120'],
        'occupancy' => ['actual' => 'gm_105', 'actualMonth' => 'gm_106', 'budget' => 'bd_105', 'budgetMonth' => 'bd_106', 'lastyear' => 'ly_105', 'lastyearMonth' => 'ly_106'],
        'ADR' => ['actual' => 'gm_15', 'actualMonth' => 'gm_16', 'budget' => 'bd_15', 'budgetMonth' => 'bd_16', 'lastyear' => 'ly_15', 'lastyearMonth' => 'ly_16']
        ]
];
$umamiLocationsWhereIn = \Model\Location::umamiWhereInBuilder();
global $emagid;
$db = $emagid->getDb();
$sql = "select report_data.value as value, location.name as location,  field.name as field, report_data.date as date, location.submit as submit from report_data join location on location.id = report_data.location_id join field on field.id = report_data.field_id where (date = '$dateString') AND location.type = '$type' AND location.name NOT IN $umamiLocationsWhereIn";
$reportData = $db->getResults($sql);

//$tmpData = [];
/**
 * last year data is from this year
 * budget data is current year
 * note data is from current year
 */
//foreach($reportData as $index => $data){
//    if(strpos($data['field'], 'ly_') !== false && strpos($data['date'], "{$lastYear->year}-") !== false){
//        continue;
//    } elseif(strpos($data['field'], 'bd_') !== false && strpos($data['date'], "{$date->year}-") !== false){
//        continue;
//    } elseif(strpos($data['field'], 'gm_') !== false && strpos($data['date'], "{$date->year}-") !== false){
//        continue;
//    } elseif(strpos($data['field'], 'nt_') !== false && strpos($data['date'], "{$date->year}-") !== false){
//        continue;
//    } else {
//        unset($reportData[$index]);
//    }
//}

//foreach($reportData as $data){
//    if(strpos($data['field'], 'ly_') !== false && strpos($data['date'], '2016-') !== false){
//        continue;
//    } elseif(strpos($data['field'], 'bd_') !== false && strpos($data['date'], '2016-') !== false){
//        continue;
//    } else {
//        $tmpData[] = $data;
//    }
//}

//$reportData = $tmpData;

$formatData = [];
$submitDetails = [];

/**
 * For indicate should we show or hide nightlife venues
 * Rules: if past 7 days, any days they have daily total sales > 0, we show it,
 * ^ we are not doing that now
 */
$nightlifeActiveVenues = [
    'hyde sunset kitchen + cocktails' => 1,
    'hyde staples' => 1,
    'little hyde' => 1,
    'doheny room' => 1,
    'create' => 1,
    'hyde bellagio' => 1,
    'hyde t-mobile arena' => 1,
    'double barrel' => 1,
    'hyde south beach' => 1,
    'hyde miami aaa arena' => 1,
    'greystone manor' => 1,
];

$hotelNoShow = [
    'sls bh hotel' => 1
];

//$activeSql = "select report_data.value as value, location.name as location,  field.name as field from report_data join location on location.id = report_data.location_id join field on field.id = report_data.field_id where date in ($lastSevenDaysString) AND location.type = 'nightlife' AND field.name = 'gm_152'";
//$nightlifeActiveData = $db->getResults($activeSql);
//foreach($nightlifeActiveData as $activeData){
//    if(floatval($activeData['value']) > 0){
//        $nightlifeActiveVenues[$activeData['location']] = 1;
//    }
//}

/**
 * Start helper functions
 */

/**
 * @param $data
 * @param $field
 * @return float|int
 */
if(!function_exists('getField')){
    function getField($data, $field)
    {
        if(!isset($data[$field])){
            return 0;
        }

        return floatval($data[$field]);
    }
}

if(!function_exists('formatField')){
    function formatField($value, $isPercentage = false, $isNA = false)
    {
        $value = floatval($value);
        /**
         * We are not format value when it's N/A
         */
        if($isNA && $value == 0){
            return 'N/A';
        }

        $print = $value?number_format($value, 0):'-';

        if($isPercentage && $print != '-'){
            return $print.'%';
        }

        return trim($print);
    }
}

if(!function_exists('variance')){
    function variance($value1, $value2, $isPercentage = false, $isMinus = false){
        $value1 = floatval($value1);
        $value2 = floatval($value2);
        if($value2 == 0){
            return '-';
        }

        if(!$isPercentage){
            return number_format($value1 - $value2, 0);
        } else {
            if($isMinus){
                return number_format(($value1/$value2)*100, 1).'%';
            } else {
                return number_format((($value1 - $value2)/$value2)*100, 1).'%';
            }

        }
    }
}

if(!function_exists('trimNote')){
    function trimNote($value){
        $value = trim($value);
        $value = preg_replace('/\s+/', ' ', $value);
        $value = preg_replace('/[^A-Za-z0-9\,\. \-\'"$]/', '', $value);
        $data = explode(' ', $value);
        $newData = [];
        foreach($data as $word){
            if(strlen($word) <= 15){
                $newData[] = $word;
            }
        }

        if(count($newData) >= 1){
            $value = implode(' ', $newData);
        }

        return $value;
    }
}

/**
 * Get location detailed data and location submit status
 * ['south beach' => ['gm_171' => '123', 'ly_123'], ...]
 */


foreach($reportData as $data){
    $locationName = $data['location'];
    $filed = [$data['field'] => $data['value']];
    if(!isset($formatData[$locationName])){
        $formatData[$locationName] = [];
    }
    $submitDetails[$locationName] = $data['submit'] == 1?1:0;
    $formatData[$locationName] = array_merge($formatData[$locationName], $filed);
}
/**
 * Sort by location name
 */
ksort($formatData);

$orders = [];

unset($reportData);

/**
 * Merge
 * 1. bar centro south beach with bazaar south beach
 * 2. Bazaar Beverly Hills with Tres Beverly Hills
 */
if(isset($formatData['bazaar south beach'])){
    foreach($formatData['bazaar south beach'] as $key => &$value){
        if(strpos($key, 'nt_') !== false){
            continue;
        }
        $value += isset($formatData['bar centro south beach'][$key])?$formatData['bar centro south beach'][$key]:0;
    }
    unset($formatData['bar centro south beach']);
}

if(isset($formatData['bazaar beverly hills'])){
    foreach($formatData['bazaar beverly hills'] as $key => &$value){
        if(strpos($key, 'nt_') !== false){
            continue;
        }
        $value += isset($formatData['tres beverly hills'][$key])?$formatData['tres beverly hills'][$key]:0;
    }

    unset($formatData['tres beverly hills']);
}





/**
 * Sort by location name
 */
ksort($submitDetails);

/**
 * nightlife or restaurant summary data will looks like this:
 * ['owned' =>
 * ['south beach' => ['actual' => '', 'budget' => '', 'lastyear' => '', 'actualMonth' => '', 'budgetMonth' => '', 'lastyearMonth' => ''],
 * 'managed' => ['actual' => '', 'budget' => '', 'lastyear' => '', 'actualMonth' => '', 'budgetMonth' => '', 'lastyearMonth' => ''],
 * 'ownedActualTotal' => 1000, 'ownedBudgetTotal' => 1200, 'ownedLastyearTotal' => 1000, 'managedActualTotal' => 1000, 'managedBudgetTotal' => 1200, 'managedLastyearTotal' => 1000];
 */
if($type == 'restaurant' || $type == 'nightlife'){
    $ownedData = ['owned' => [], 'managed' => [],
        'ownedActualTotal' => 0, 'ownedBudgetTotal' => 0, 'ownedLastyearTotal' => 0,
        'ownedActualMonthTotal' => 0, 'ownedBudgetMonthTotal' => 0, 'ownedLastyearMonthTotal' => 0,
        'managedActualTotal' => 0, 'managedBudgetTotal' => 0, 'managedLastyearTotal' => 0,
        'managedActualMonthTotal' => 0, 'managedBudgetMonthTotal' => 0, 'managedLastyearMonthTotal' => 0];
    $fieldOption = $fieldOptions[$type];
    foreach($formatData as $locationName => $fieldData){
        /**
         * Exclude nightlife daily total sales empty venue
         */
        if($type == 'nightlife' && !isset($nightlifeActiveVenues[$locationName])){
            continue;
        }
        $location = \Model\Location::getItem(null, ['where' => "name = '$locationName'"]);

        $saleTotal = isset($fieldData[$fieldOption['actual']])?$fieldData[$fieldOption['actual']]: 0;
        $saleBudgetTotal = isset($fieldData[$fieldOption['budget']])?$fieldData[$fieldOption['budget']]: 0;
        $saleLastyearTotal = isset($fieldData[$fieldOption['lastyear']])?$fieldData[$fieldOption['lastyear']]: 0;

        /**
         * Restaurant total sales on 1st page, needs add special events
         */
        if($type == 'restaurant'){
            $saleTotal += isset($fieldData['gm_131'])?$fieldData['gm_131']: 0;
            $saleBudgetTotal += isset($fieldData['bd_131'])?$fieldData['bd_131']: 0;
        }

        $saleMonthTotal = isset($fieldData[$fieldOption['actualMonth']])?$fieldData[$fieldOption['actualMonth']]: 0;

        if($type == 'restaurant'){
            if($locationName == 'bazaar south beach'){
                $saleBudgetMonthTotal = \Model\ReportData::MTD($locationName, $dateString, 'bd_153');
                $saleBudgetMonthTotal += \Model\ReportData::MTD('bar centro south beach', $dateString, 'bd_153');
                $saleBudgetMonthTotal += \Model\ReportData::MTD($locationName, $dateString, 'bd_131');
            } else {
                $saleBudgetMonthTotal = \Model\ReportData::MTD($locationName, $dateString, 'bd_153');
                $saleBudgetMonthTotal += \Model\ReportData::MTD($locationName, $dateString, 'bd_131');
            }
        } else {
            $saleBudgetMonthTotal = \Model\ReportData::MTD($locationName, $dateString, 'bd_152');
        }

        if($type == 'restaurant'){
//            if($locationName == 'bazaar south beach'){
//                $saleLastyearMonthTotal = \Model\ReportData::MTD($locationName, $lastYearString, 'ly_153');
//                $saleLastyearMonthTotal += \Model\ReportData::MTD('bar centro south beach', $lastYearString, 'ly_153');
//                $saleLastyearMonthTotal += isset($locationName['bar centro south beach']['ly_172'])?$locationName['bar centro south beach']['ly_172']: 0;
//            } else {
//                $saleLastyearMonthTotal = isset($fieldData['ly_172'])?$fieldData['ly_172']: 0;
//            }

            /**
             * 2017 piror year is 2016 GM data, so we just pick the data
             */
            $saleLastyearMonthTotal = isset($fieldData['ly_172'])?$fieldData['ly_172']: 0;

        } else {
            $saleLastyearMonthTotal = isset($fieldData['ly_174'])?$fieldData['ly_174']: 0;
//            $saleLastyearMonthTotal = \Model\ReportData::MTD($locationName, $lastYearString, 'ly_174');
        }


        if($location->isOwned()){
            $ownedData['owned'][$locationName] = ['actual' => $saleTotal, 'budget' => $saleBudgetTotal, 'lastyear' => $saleLastyearTotal, 'actualMonth' => $saleMonthTotal, 'budgetMonth' => $saleBudgetMonthTotal, 'lastyearMonth' => $saleLastyearMonthTotal];
        } else {
            $ownedData['managed'][$locationName] = ['actual' => $saleTotal, 'budget' => $saleBudgetTotal, 'lastyear' => $saleLastyearTotal, 'actualMonth' => $saleMonthTotal, 'budgetMonth' => $saleBudgetMonthTotal, 'lastyearMonth' => $saleLastyearMonthTotal];
        }
    }

    // calculate total
    foreach($ownedData['owned'] as $location => $data){
        $orders[] = $location;
        $ownedData['ownedActualTotal'] += $data['actual'];
        $ownedData['ownedBudgetTotal'] += $data['budget'];
        $ownedData['ownedLastyearTotal'] += $data['lastyear'];

        $ownedData['ownedActualMonthTotal'] += $data['actualMonth'];
        $ownedData['ownedBudgetMonthTotal'] += $data['budgetMonth'];
        $ownedData['ownedLastyearMonthTotal'] += $data['lastyearMonth'];
    }

    foreach($ownedData['managed'] as $location => $data){
        $orders[] = $location;
        $ownedData['managedActualTotal'] += $data['actual'];
        $ownedData['managedBudgetTotal'] += $data['budget'];
        $ownedData['managedLastyearTotal'] += $data['lastyear'];

        $ownedData['managedActualMonthTotal'] += $data['actualMonth'];
        $ownedData['managedBudgetMonthTotal'] += $data['budgetMonth'];
        $ownedData['managedLastyearMonthTotal'] += $data['lastyearMonth'];
    }
}

/**
 * Hotel summary data will looks like this
 * ['Room Revenue' => ['south beach' => ['actual' => '', 'budget' => '', 'lastyear' => '', 'actualMonth' => '', 'budgetMonth' => '', 'lastyearMonth' => '']],
 * 'Occupancy %' => ['south beach' => ['actual' => '', 'budget' => '', 'lastyear' => '', 'actualMonth' => '', 'budgetMonth' => '', 'lastyearMonth' => '']],
 * 'ADR' => ['north beach' => ['actual' => '', 'budget' => '', 'lastyear' => '', 'actualMonth' => '', 'budgetMonth' => '', 'lastyearMonth' => '']],
 * 'summary' => ['actual' => 0, 'budget' => 0, 'lastyear' => 0]] // summary only for room revenue
 */
if($type == 'hotel'){
    $hotelSummaryData = ['Room Revenue' => [], 'Occupancy %' => [], 'ADR' => [], 'summary' => ['actual' => 0, 'budget' => 0, 'lastyear' => 0, 'actualMonth' => 0, 'budgetMonth' => 0, 'lastyearMonth' => 0]];
    $fieldOption = $fieldOptions[$type];

    foreach($formatData as $locationName => $fieldData){

        /**
         * For now, we do not show sls bh hotel
         */
        if(isset($hotelNoShow[$locationName])){
            continue;
        }

        // room data
        $roomTotal = isset($fieldData[$fieldOption['room']['actual']])?$fieldData[$fieldOption['room']['actual']]: 0;
        $roomBudgetTotal = isset($fieldData[$fieldOption['room']['budget']])?$fieldData[$fieldOption['room']['budget']]: 0;
        $roomLastyearTotal = isset($fieldData[$fieldOption['room']['lastyear']])?$fieldData[$fieldOption['room']['lastyear']]: 0;

        $roomMonthTotal = isset($fieldData[$fieldOption['room']['actualMonth']])?$fieldData[$fieldOption['room']['actualMonth']]: 0;
        $roomBudgetMonthTotal = isset($fieldData[$fieldOption['room']['budgetMonth']])?$fieldData[$fieldOption['room']['budgetMonth']]: 0;
        $roomLastyearMonthTotal = isset($fieldData[$fieldOption['room']['lastyearMonth']])?$fieldData[$fieldOption['room']['lastyearMonth']]: 0;

        // occupancy data
        $occupancyTotal = isset($fieldData[$fieldOption['occupancy']['actual']])?$fieldData[$fieldOption['occupancy']['actual']]: 0;
        $occupancyBudgetTotal = isset($fieldData[$fieldOption['occupancy']['budget']])?$fieldData[$fieldOption['occupancy']['budget']]: 0;
        $occupancyLastyearTotal = isset($fieldData[$fieldOption['occupancy']['lastyear']])?$fieldData[$fieldOption['occupancy']['lastyear']]: 0;

        $occupancyMonthTotal = isset($fieldData[$fieldOption['occupancy']['actualMonth']])?$fieldData[$fieldOption['occupancy']['actualMonth']]: 0;
        $occupancyBudgetMonthTotal = isset($fieldData[$fieldOption['occupancy']['budgetMonth']])?$fieldData[$fieldOption['occupancy']['budgetMonth']]: 0;
        $occupancyLastyearMonthTotal = isset($fieldData[$fieldOption['occupancy']['lastyearMonth']])?$fieldData[$fieldOption['occupancy']['lastyearMonth']]: 0;

        // ADR data
        $ADRTotal = isset($fieldData[$fieldOption['ADR']['actual']])?$fieldData[$fieldOption['ADR']['actual']]: 0;
        $ADRBudgetTotal = isset($fieldData[$fieldOption['ADR']['budget']])?$fieldData[$fieldOption['ADR']['budget']]: 0;
        $ADRLastyearTotal = isset($fieldData[$fieldOption['ADR']['lastyear']])?$fieldData[$fieldOption['ADR']['lastyear']]: 0;

        $ADRMonthTotal = isset($fieldData[$fieldOption['ADR']['actualMonth']])?$fieldData[$fieldOption['ADR']['actualMonth']]: 0;
        $ADRBudgetMonthTotal = isset($fieldData[$fieldOption['ADR']['budgetMonth']])?$fieldData[$fieldOption['ADR']['budgetMonth']]: 0;
        $ADRLastyearMonthTotal = isset($fieldData[$fieldOption['ADR']['lastyearMonth']])?$fieldData[$fieldOption['ADR']['lastyearMonth']]: 0;


        $hotelSummaryData['Room Revenue'][$locationName] = ['actual' => $roomTotal, 'budget' => $roomBudgetTotal, 'lastyear' => $roomLastyearTotal, 'actualMonth' => $roomMonthTotal, 'budgetMonth' => $roomBudgetMonthTotal, 'lastyearMonth' => $roomLastyearMonthTotal];
        $hotelSummaryData['Occupancy %'][$locationName] = ['actual' => $occupancyTotal, 'budget' => $occupancyBudgetTotal, 'lastyear' => $occupancyLastyearTotal, 'actualMonth' => $occupancyMonthTotal, 'budgetMonth' => $occupancyBudgetMonthTotal, 'lastyearMonth' => $occupancyLastyearMonthTotal];
        $hotelSummaryData['ADR'][$locationName] = ['actual' => $ADRTotal, 'budget' => $ADRBudgetTotal, 'lastyear' => $ADRLastyearTotal, 'actualMonth' => $ADRMonthTotal, 'budgetMonth' => $ADRBudgetMonthTotal, 'lastyearMonth' => $ADRLastyearMonthTotal];
    }

    foreach($hotelSummaryData as $key => $value){
        if($key == 'Room Revenue'){
            foreach($value as $location => $data) {
                $hotelSummaryData['summary']['actual'] += $data['actual'];
                $hotelSummaryData['summary']['budget'] += $data['budget'];
                $hotelSummaryData['summary']['lastyear'] += $data['lastyear'];

                $hotelSummaryData['summary']['actualMonth'] += $data['actualMonth'];
                $hotelSummaryData['summary']['budgetMonth'] += $data['budgetMonth'];
                $hotelSummaryData['summary']['lastyearMonth'] += $data['lastyearMonth'];
            }
        }
    }
}

/**
 * Do validation for total overall revenue, if it is zero or string, send out error email
 */
if(isset($manually) && $manually){

} else {
    if($type == 'hotel'){
        if(!\Service\ReportValidation::totalAmountValidation($type, $hotelSummaryData['summary']['actual'], $dateString)){
            exit();
        }
    }

    if($type == 'restaurant' || $type == 'nightlife'){
        $overallActualTotal = floatval($ownedData['ownedActualTotal']) + floatval($ownedData['managedActualTotal']);
        if(!\Service\ReportValidation::totalAmountValidation($type, $overallActualTotal, $dateString)){
            exit();
        }
    }
}

?>

<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">-->
    <style>
        hr {
            page-break-after: always;
            border: 0;
        }

        @page {
            margin: 2cm;
        }

        body {
            font-family: sans-serif;
            margin: 0.5cm 0;
            text-align: justify;
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: 0.9em;
        }

        #header {
            top: -50px;
            height: 50px;
            border-bottom: 0.1pt solid #aaa;
        }

        #footer {
            bottom: 0;
            border-top: 0.1pt solid #aaa;
        }

        #header table,
        #footer table {
            width: 100%;
            border-collapse: collapse;
            border: none;
        }

        #header td,
        #footer td {
            padding: 0;
            width: 50%;
        }

        table td {
            padding: 5px;
            margin: 0
        }

        .actual {
            background-color: #CAF2DA;
        }

        .underline {
            text-decoration: underline;
        }

        .highlight {
            font-weight: bold;
        }

        .level-1 {
            padding-left: 0px;
        }

        .level-2 {
            padding-left: 20px;
        }

        .level-3 {
            padding-left: 40px;
        }
        tr.overline_tr{
            border-top:2px solid #999999;
        }
        .serif_underline{
            font-family:Georgia,times-new-roman, serif;
            font-size: 18px;
            border-bottom:1px solid #000000;
        }

        .variance {
            color: grey;
            padding-left: 80px;
            font-size: 12px;
        }

        .summary-head {
            font-size: 13px;
            font-weight: bold;
            text-decoration: underline;
        }
        .summary-content {
            font-size: 11px
        }
        .page-number {
            text-align: center;
        }
        h2{
            border-bottom:2px solid #777777;
            padding-top:2px;
            padding-bottom:4px;
        }
        .right-text{
            text-align: right;
        }
        
        .center-text{
            text-align: center;
        }
        table {
            width: 90%;
        }

        tr.border-1 td{
            border-bottom:1px solid #000000;
        }

        tr.border-2 td{
            border-bottom:3px solid #000000;
        }
    </style>
</head>

<body marginwidth="0" marginheight="0">

<div id="header">
    <table>
        <tbody>
        <tr>
            <td><?=ucfirst($type)?> Group Nightly Report<br><?=$date->format('M j, Y')?></td>
            <td style="text-align: right;"><img width="50" src="content/frontend/img/logo.png" style="float:right"></td>
        </tr>
        </tbody>
    </table>
</div>

<div id="footer">
    <div class="page-number">Hotels | Residences | Restaurants | Nightlife | Events & Catering</div>
</div>

<div class="page-header">
    <div style="margin-top: 250px;border-bottom: 5px solid grey;width:100%; font-size: 20px">
        <div style="width: 60%">
            <h1>Nightly Flash Report</h1>
            <h3>SBE <?=ucfirst($type)?> Group</h3>
        </div>
        <div style="width: 40%">
            <img src="content/frontend/img/logo.png" style="top: 215;position: absolute;left: 340;width: 170px;">
        </div>
    </div>
    <div style="font-size: 20px">
        <h3><?=$date->format('M j, Y')?></h3>
    </div>
</div>

<hr/>

<?php if($type == 'restaurant' || $type == 'nightlife'){ ?>
<h2>Daily Sales Summary</h2>
<div class="summary-content">
    <table class="table">
        <tbody>
        <?php foreach(['Owned' => $ownedData['owned'], 'Managed' => $ownedData['managed']] as $key => $summaryData) { $totalKey = strtolower($key);?>
            <tr class="summary-head">
                <td width="40%"></td>
                <td width="20%" class="actual center-text">Actual</td>
                <td width="20%" class="center-text">Budget</td>
                <td width="20%" class="center-text">Prior Year</td>
            </tr>
            <tr class="highlight summary-content">
                <td width="40%" class="level-1"><?=$key?> Venues</td>
                <td width="20%" class="actual"></td>
                <td width="20%"></td>
                <td width="20%"></td>
            </tr>
            <?php foreach($summaryData as $locationName => $totalSale){ ?>
                <tr class="summary-content border-1">
                    <td width="40%" class="level-2"><?=\Model\Location::printName($locationName)?></td>
                    <td width="20%" class="actual right-text"><?=formatField($totalSale['actual'], false, true)?></td>
                    <td width="20%" class="right-text"><?=formatField($totalSale['budget'])?></td>
                    <td width="20%" class="right-text"><?=formatField($totalSale['lastyear'], false, true)?></td>
                </tr>
            <?php } ?>
            <tr class="summary-content highlight border-2">
                <td width="40%" class="level-2">Total <?=$key?> <?=ucfirst($type)?></td>
                <td width="20%" class="actual right-text"><?=formatField($ownedData[$totalKey.'ActualTotal'])?></td>
                <td width="20%" class="right-text"><?=formatField($ownedData[$totalKey.'BudgetTotal'])?></td>
                <td width="20%" class="right-text"><?=formatField($ownedData[$totalKey.'LastyearTotal'])?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance $</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"<?=variance($ownedData[$totalKey.'ActualTotal'], $ownedData[$totalKey.'BudgetTotal'])?>></td>
                <td width="20%" class="right-text"><?=variance($ownedData[$totalKey.'ActualTotal'], $ownedData[$totalKey.'LastyearTotal'])?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance %</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"><?=variance($ownedData[$totalKey.'ActualTotal'], $ownedData[$totalKey.'BudgetTotal'], true)?></td>
                <td width="20%" class="right-text"><?=variance($ownedData[$totalKey.'ActualTotal'], $ownedData[$totalKey.'LastyearTotal'], true)?></td>
            </tr>
        <?php } ?>
            <?php
            $overallActualTotal = floatval($ownedData['ownedActualTotal']) + floatval($ownedData['managedActualTotal']);
            $overallBudgetTotal = floatval($ownedData['ownedBudgetTotal']) + floatval($ownedData['managedBudgetTotal']);
            $overallLastyearTotal = floatval($ownedData['ownedLastyearTotal']) + floatval($ownedData['managedLastyearTotal']);
            ?>
            <tr class="summary-content highlight border-2">
                <td width="40%" class="level-2">Total <?=ucfirst($type)?> Revenue</td>
                <td width="20%" class="actual right-text"><?=formatField($overallActualTotal)?></td>
                <td width="20%" class="right-text"><?=formatField($overallBudgetTotal)?></td>
                <td width="20%" class="right-text"><?=formatField($overallLastyearTotal)?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance $</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"><?=variance($overallActualTotal, $overallBudgetTotal)?></td>
                <td width="20%" class="right-text"><?=variance($overallActualTotal, $overallLastyearTotal)?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance %</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"><?=variance($overallActualTotal, $overallBudgetTotal, true)?></td>
                <td width="20%" class="right-text"><?=variance($overallActualTotal, $overallLastyearTotal, true)?></td>
            </tr>
        </tbody>
    </table>
</div>
<hr/>

<h2>Month Sales Summary</h2>
<div class="row summary-content">
    <table class="table">
        <tbody>
        <?php foreach(['Owned' => $ownedData['owned'], 'Managed' => $ownedData['managed']] as $key => $summaryData) { $totalKey = strtolower($key);?>
            <tr class="summary-head">
                <td width="40%"></td>
                <td width="20%" class="actual center-text">Actual</td>
                <td width="20%" class="center-text">Budget</td>
                <td width="20%" class="center-text">Prior Year</td>
            </tr>
            <tr class="highlight summary-content">
                <td width="40%" class="level-1"><?=$key?> Venues</td>
                <td width="20%" class="actual"></td>
                <td width="20%"></td>
                <td width="20%"></td>
            </tr>
            <?php foreach($summaryData as $locationName => $totalSale){ ?>
                <tr class="summary-content border-1">
                    <td width="40%" class="level-2"><?=\Model\Location::printName($locationName)?></td>
                    <td width="20%" class="actual right-text"><?=formatField($totalSale['actualMonth'], false, true)?></td>
                    <td width="20%" class="right-text"><?=formatField($totalSale['budgetMonth'])?></td>
                    <td width="20%" class="right-text"><?=formatField($totalSale['lastyearMonth'], false, true)?></td>
                </tr>
            <?php } ?>
            <tr class="summary-content highlight border-2">
                <td width="40%" class="level-2">Total <?=$key?> <?=ucfirst($type)?></td>
                <td width="20%" class="actual right-text"><?=formatField($ownedData[$totalKey.'ActualMonthTotal'])?></td>
                <td width="20%" class="right-text"><?=formatField($ownedData[$totalKey.'BudgetMonthTotal'])?></td>
                <td width="20%" class="right-text"><?=formatField($ownedData[$totalKey.'LastyearMonthTotal'])?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance $</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"><?=variance($ownedData[$totalKey.'ActualMonthTotal'], $ownedData[$totalKey.'BudgetMonthTotal'])?></td>
                <td width="20%" class="right-text"><?=variance($ownedData[$totalKey.'ActualMonthTotal'], $ownedData[$totalKey.'LastyearMonthTotal'])?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance %</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"><?=variance($ownedData[$totalKey.'ActualMonthTotal'], $ownedData[$totalKey.'BudgetMonthTotal'], true)?></td>
                <td width="20%" class="right-text"><?=variance($ownedData[$totalKey.'ActualMonthTotal'], $ownedData[$totalKey.'LastyearMonthTotal'], true)?></td>
            </tr>
        <?php } ?>
            <?php
            $overallActualMonthTotal = floatval($ownedData['ownedActualMonthTotal']) + floatval($ownedData['managedActualMonthTotal']);
            $overallBudgetMonthTotal = floatval($ownedData['ownedBudgetMonthTotal']) + floatval($ownedData['managedBudgetMonthTotal']);
            $overallLastyearMonthTotal = floatval($ownedData['ownedLastyearMonthTotal']) + floatval($ownedData['managedLastyearMonthTotal']);
            ?>
            <tr class="summary-content highlight border-2">
                <td width="40%" class="level-2">Total <?=ucfirst($type)?> Revenue</td>
                <td width="20%" class="actual right-text"><?=formatField($overallActualMonthTotal)?></td>
                <td width="20%" class="right-text"><?=formatField($overallBudgetMonthTotal)?></td>
                <td width="20%" class="right-text"><?=formatField($overallLastyearMonthTotal)?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance $</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"><?=variance($overallActualMonthTotal, $overallBudgetMonthTotal)?></td>
                <td width="20%" class="right-text"><?=variance($overallActualMonthTotal, $overallLastyearMonthTotal)?></td>
            </tr>
            <tr class="variance summary-content">
                <td width="40%" class="variance">Actual Variance %</td>
                <td width="20%" class="actual right-text"></td>
                <td width="20%" class="right-text"><?=variance($overallActualMonthTotal, $overallBudgetMonthTotal, true)?></td>
                <td width="20%" class="right-text"><?=variance($overallActualMonthTotal, $overallLastyearMonthTotal, true)?></td>
            </tr>
        </tbody>
    </table>
</div>
<hr/>
<!--<h2>Year Sales Summary</h2>-->
<!--<div class="row summary-content">-->
<!--    <table class="table">-->
<!--        <tbody>-->
<!--        --><?php //$ytdTotal = [
//            'Owned' => [
//                'actual' => 0,
//                'budget' => 0,
//                'lastyear' => 0
//            ],
//            'Managed' => [
//                'actual' => 0,
//                'budget' => 0,
//                'lastyear' => 0
//            ]
//        ];?>
<!--        --><?php //foreach(['Owned' => $ownedData['owned'], 'Managed' => $ownedData['managed']] as $key => $summaryData) { $totalKey = strtolower($key);?>
<!--            <tr class="summary-head">-->
<!--                <td width="40%"></td>-->
<!--                <td width="20%" class="actual center-text">Actual</td>-->
<!--                <td width="20%" class="center-text">Budget</td>-->
<!--                <td width="20%" class="center-text">Prior Year</td>-->
<!--            </tr>-->
<!--            <tr class="highlight summary-content">-->
<!--                <td width="40%" class="level-1">--><?//=$key?><!-- Venues</td>-->
<!--                <td width="20%" class="actual"></td>-->
<!--                <td width="20%"></td>-->
<!--                <td width="20%"></td>-->
<!--            </tr>-->
<!--            --><?php //foreach($summaryData as $locationName => $totalSale){ ?>
<!--                --><?php //$ytdData = \Model\ReportData::YTD($locationName, $dateString); $field = \Model\ReportData::getYtdField()[$type];?>
<!--                --><?php
//                    $ytdTotal[$key]['actual'] += floatval(getField($ytdData, $field[0]));
//                    $ytdTotal[$key]['budget'] += floatval(getField($ytdData, $field[1]));
//                    $ytdTotal[$key]['lastyear'] += floatval(getField($ytdData, $field[2]));
//                ?>
<!--                <tr class="summary-content border-1">-->
<!--                    <td width="40%" class="level-2">--><?//=\Model\Location::printName($locationName)?><!--</td>-->
<!--                    <td width="20%" class="actual right-text">--><?//=formatField(getField($ytdData, $field[0]))?><!--</td>-->
<!--                    <td width="20%" class="right-text">--><?//=formatField(getField($ytdData, $field[1]))?><!--</td>-->
<!--                    <td width="20%" class="right-text">--><?//=formatField(getField($ytdData, $field[2]))?><!--</td>-->
<!--                </tr>-->
<!--            --><?php //} ?>
<!--            <tr class="summary-content highlight border-2">-->
<!--                <td width="40%" class="level-2">Total --><?//=$key?><!-- --><?//=ucfirst($type)?><!--</td>-->
<!--                <td width="20%" class="actual right-text">--><?//=formatField($ytdTotal[$key]['actual'])?><!--</td>-->
<!--                <td width="20%" class="right-text">--><?//=formatField($ytdTotal[$key]['budget'])?><!--</td>-->
<!--                <td width="20%" class="right-text">--><?//=formatField($ytdTotal[$key]['lastyear'])?><!--</td>-->
<!--            </tr>-->
<!--            <tr class="variance summary-content">-->
<!--                <td width="40%" class="variance">Actual Variance $</td>-->
<!--                <td width="20%" class="actual right-text"></td>-->
<!--                <td width="20%" class="right-text">--><?//=variance($ytdTotal[$key]['actual'], $ytdTotal[$key]['budget'])?><!--</td>-->
<!--                <td width="20%" class="right-text">--><?//=variance($ytdTotal[$key]['actual'], $ytdTotal[$key]['lastyear'])?><!--</td>-->
<!--            </tr>-->
<!--            <tr class="variance summary-content">-->
<!--                <td width="40%" class="variance">Actual Variance %</td>-->
<!--                <td width="20%" class="actual right-text"></td>-->
<!--                <td width="20%" class="right-text">--><?//=variance($ytdTotal[$key]['actual'], $ytdTotal[$key]['budget'], true)?><!--</td>-->
<!--                <td width="20%" class="right-text">--><?//=variance($ytdTotal[$key]['actual'], $ytdTotal[$key]['lastyear'], true)?><!--</td>-->
<!--            </tr>-->
<!--        --><?php //} ?>
<!--        --><?php
//        $overallActualMonthTotal = $ytdTotal['Owned']['actual'] + $ytdTotal['Managed']['actual'];
//        $overallBudgetMonthTotal = $ytdTotal['Owned']['budget'] + $ytdTotal['Managed']['budget'];
//        $overallLastyearMonthTotal = $ytdTotal['Owned']['lastyear'] + $ytdTotal['Managed']['lastyear'];
//        ?>
<!--        <tr class="summary-content highlight border-2">-->
<!--            <td width="40%" class="level-2">Total --><?//=ucfirst($type)?><!-- Revenue</td>-->
<!--            <td width="20%" class="actual right-text">--><?//=formatField($overallActualMonthTotal)?><!--</td>-->
<!--            <td width="20%" class="right-text">--><?//=formatField($overallBudgetMonthTotal)?><!--</td>-->
<!--            <td width="20%" class="right-text">--><?//=formatField($overallLastyearMonthTotal)?><!--</td>-->
<!--        </tr>-->
<!--        <tr class="variance summary-content">-->
<!--            <td width="40%" class="variance">Actual Variance $</td>-->
<!--            <td width="20%" class="actual right-text"></td>-->
<!--            <td width="20%" class="right-text">--><?//=variance($overallActualMonthTotal, $overallBudgetMonthTotal)?><!--</td>-->
<!--            <td width="20%" class="right-text">--><?//=variance($overallActualMonthTotal, $overallLastyearMonthTotal)?><!--</td>-->
<!--        </tr>-->
<!--        <tr class="variance summary-content">-->
<!--            <td width="40%" class="variance">Actual Variance %</td>-->
<!--            <td width="20%" class="actual right-text"></td>-->
<!--            <td width="20%" class="right-text">--><?//=variance($overallActualMonthTotal, $overallBudgetMonthTotal, true)?><!--</td>-->
<!--            <td width="20%" class="right-text">--><?//=variance($overallActualMonthTotal, $overallLastyearMonthTotal, true)?><!--</td>-->
<!--        </tr>-->
<!--        </tbody>-->
<!--    </table>-->
<!--</div>-->
<!--<hr/>-->
<?php } elseif($type == 'hotel') { ?>
    <h2>Daily Room Summary</h2>
    <div class="row summary-content">
        <?php foreach($hotelSummaryData as $summaryType => $summaryData) {?>
            <table class="table">
                <tbody>
                <?php if($summaryType !== 'summary'){?>
                    <tr class="summary-head">
                        <td width="40%"><strong><?=$summaryType?></strong></td>
                        <td width="20%" class="actual center-text">Actual</td>
                        <td width="20%" class="center-text">Budget</td>
                        <td width="20%" class="center-text">Prior Year</td>
                    </tr>
                    <?php foreach($summaryData as $locationName => $totalSale){ ?>
                        <tr class="summary-content">
                            <td width="40%" class="level-2"><?=\Model\Location::printName($locationName)?></td>
                            <td width="20%" class="actual right-text"><?=formatField($totalSale['actual'], false, true)?></td>
                            <td width="20%" class="right-text"><?=formatField($totalSale['budget'])?></td>
                            <td width="20%" class="right-text"><?=formatField($totalSale['lastyear'], false, true)?></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <?php if($summaryType == 'Room Revenue'){ ?>
                <tr>
                    <td width="40%" class="level-2"><strong>Total Room Revenue</strong></td>
                    <td width="20%" class="actual right-text"><strong><?=formatField($hotelSummaryData['summary']['actual'])?></strong></td>
                    <td width="20%" class="right-text"><strong><?=formatField($hotelSummaryData['summary']['budget'])?></strong></td>
                    <td width="20%" class="right-text"><strong><?=formatField($hotelSummaryData['summary']['lastyear'])?></strong></td>
                </tr>
                <tr class="variance summary-content">
                    <td width="40%" class="variance">Actual Variance $</td>
                    <td width="20%" class="actual"></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actual'], $hotelSummaryData['summary']['budget'])?></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actual'], $hotelSummaryData['summary']['lastyear'])?></td>
                </tr>
                <tr class="variance summary-content">
                    <td width="40%" class="variance">Actual Variance %</td>
                    <td width="20%" class="actual"></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actual'], $hotelSummaryData['summary']['budget'], true)?></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actual'], $hotelSummaryData['summary']['lastyear'], true)?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
    <hr/>

    <h2>Month Sales Summary</h2>
    <div class="row summary-content">
        <?php foreach($hotelSummaryData as $summaryType => $summaryData) {?>
            <table class="table">
                <tbody>
                <?php if($summaryType !== 'summary'){?>
                    <tr class="summary-head">
                        <td width="40%"><strong><?=$summaryType?></strong></td>
                        <td width="20%" class="actual center-text">Actual</td>
                        <td width="20%" class="center-text">Budget</td>
                        <td width="20%" class="center-text">Prior Year</td>
                    </tr>
                    <?php foreach($summaryData as $locationName => $totalSale){ ?>
                        <tr class="summary-content">
                            <td width="40%" class="level-2"><?=\Model\Location::printName($locationName)?></td>
                            <td width="20%" class="actual right-text"><?=formatField($totalSale['actualMonth'], false, true)?></td>
                            <td width="20%" class="right-text"><?=formatField($totalSale['budgetMonth'])?></td>
                            <td width="20%" class="right-text"><?=formatField($totalSale['lastyearMonth'], false, true)?></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <?php if($summaryType == 'Room Revenue'){ ?>
                <tr>
                    <td width="40%" class="level-2"><strong>Total Room Revenue</strong></td>
                    <td width="20%" class="actual right-text"><strong><?=formatField($hotelSummaryData['summary']['actualMonth'])?></strong></td>
                    <td width="20%" class="right-text"><strong><?=formatField($hotelSummaryData['summary']['budgetMonth'])?></strong></td>
                    <td width="20%" class="right-text"><strong><?=formatField($hotelSummaryData['summary']['lastyearMonth'])?></strong></td>
                </tr>
                <tr class="variance summary-content">
                    <td width="40%" class="variance">Actual Variance $</td>
                    <td width="20%" class="actual"></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actualMonth'], $hotelSummaryData['summary']['budgetMonth'])?></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actualMonth'], $hotelSummaryData['summary']['lastyearMonth'])?></td>
                </tr>
                <tr class="variance summary-content">
                    <td width="40%" class="variance">Actual Variance %</td>
                    <td width="20%" class="actual"></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actualMonth'], $hotelSummaryData['summary']['budgetMonth'], true)?></td>
                    <td width="20%" class="right-text"><?=variance($hotelSummaryData['summary']['actualMonth'], $hotelSummaryData['summary']['lastyearMonth'], true)?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
    <hr/>

<!--    <h2>Year Sales Summary</h2>-->
<!--    <div class="row summary-content">-->
<!--        --><?php //$ytdTotal = [
//            'Room Revenue' => [
//                'actual' => 0,
//                'budget' => 0,
//                'lastyear' => 0
//            ],
//            'Occupancy %' => [
//                'actual' => 0,
//                'budget' => 0,
//                'lastyear' => 0
//            ],
//            'ADR' => [
//                'actual' => 0,
//                'budget' => 0,
//                'lastyear' => 0
//            ]
//        ];?>
<!--        --><?php //foreach($hotelSummaryData as $summaryType => $summaryData) {?>
<!--            <table class="table">-->
<!--                <tbody>-->
<!--                --><?php //if($summaryType !== 'summary'){?>
<!--                    <tr class="summary-head">-->
<!--                        <td width="40%"><strong>--><?//=$summaryType?><!--</strong></td>-->
<!--                        <td width="20%" class="actual center-text">Actual</td>-->
<!--                        <td width="20%" class="center-text">Budget</td>-->
<!--                        <td width="20%" class="center-text">Prior Year</td>-->
<!--                    </tr>-->
<!--                    --><?php //foreach($summaryData as $locationName => $totalSale){ ?>
<!--                        --><?php
//                            $ytdData = \Model\ReportData::YTD($locationName, $dateString); $field = \Model\ReportData::getYtdField()[$type];
//                            if($summaryType == 'Room Revenue'){
//                                $field = ['gm_120', 'bd_120', 'ly_120'];
//                            } elseif($summaryType == 'Occupancy %'){
//                                $field = ['gm_106', 'bd_106', 'ly_106'];
//                            } elseif($summaryType == 'ADR'){
//                                $field = ['gm_16', 'bd_16', 'ly_16'];
//                            }
//                        ?>
<!--                        --><?php
//                        $ytdTotal[$summaryType]['actual'] += floatval(getField($ytdData, $field[0]));
//                        $ytdTotal[$summaryType]['budget'] += floatval(getField($ytdData, $field[1]));
//                        $ytdTotal[$summaryType]['lastyear'] += floatval(getField($ytdData, $field[2]));
//                        ?>
<!--                        <tr class="summary-content">-->
<!--                            <td width="40%" class="level-2">--><?//=\Model\Location::printName($locationName)?><!--</td>-->
<!--                            <td width="20%" class="actual right-text">--><?//=formatField(getField($ytdData, $field[0]))?><!--</td>-->
<!--                            <td width="20%" class="right-text">--><?//=formatField(getField($ytdData, $field[1]))?><!--</td>-->
<!--                            <td width="20%" class="right-text">--><?//=formatField(getField($ytdData, $field[2]))?><!--</td>-->
<!--                        </tr>-->
<!--                    --><?php //} ?>
<!--                --><?php //} ?>
<!--                --><?php //if($summaryType == 'Room Revenue'){ ?>
<!--                    <tr>-->
<!--                        <td width="40%" class="level-2"><strong>Total Room Revenue</strong></td>-->
<!--                        <td width="20%" class="actual right-text"><strong>--><?//=formatField($ytdTotal[$summaryType]['actual'])?><!--</strong></td>-->
<!--                        <td width="20%" class="right-text"><strong>--><?//=formatField($ytdTotal[$summaryType]['budget'])?><!--</strong></td>-->
<!--                        <td width="20%" class="right-text"><strong>--><?//=formatField($ytdTotal[$summaryType]['lastyear'])?><!--</strong></td>-->
<!--                    </tr>-->
<!--                    <tr class="variance summary-content">-->
<!--                        <td width="40%" class="variance">Actual Variance $</td>-->
<!--                        <td width="20%" class="actual"></td>-->
<!--                        <td width="20%" class="right-text">--><?//=variance($ytdTotal[$summaryType]['actual'], $ytdTotal[$summaryType]['budget'])?><!--</td>-->
<!--                        <td width="20%" class="right-text">--><?//=variance($ytdTotal[$summaryType]['actual'], $ytdTotal[$summaryType]['lastyear'])?><!--</td>-->
<!--                    </tr>-->
<!--                    <tr class="variance summary-content">-->
<!--                        <td width="40%" class="variance">Actual Variance %</td>-->
<!--                        <td width="20%" class="actual"></td>-->
<!--                        <td width="20%" class="right-text">--><?//=variance($ytdTotal[$summaryType]['actual'], $ytdTotal[$summaryType]['budget'], true)?><!--</td>-->
<!--                        <td width="20%" class="right-text">--><?//=variance($ytdTotal[$summaryType]['actual'], $ytdTotal[$summaryType]['lastyear'], true)?><!--</td>-->
<!--                    </tr>-->
<!--                --><?php //} ?>
<!--                </tbody>-->
<!--            </table>-->
<!--        --><?php //} ?>
<!--    </div>-->
<!--    <hr/>-->
<?php } ?>
<?php unset($ownedData);unset($hotelSummaryData);?>
<?php if($type == 'restaurant'){ ?>
    <?php foreach($orders as $locationString){
        $fieldData = $formatData[$locationString];?>
        <h2><?=\Model\Location::printName($locationString)?></h2>
        <h3><?=$date->format('M j, Y')?></h3>
        <div class="row">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="40%"><strong class="serif_underline">Revenue</strong></td>
                        <td width="20%" class="serif_underline center-text actual"><strong>Actual</strong></td>
                        <td width="20%" class="serif_underline center-text"><strong>Budget</strong></td>
                        <td width="20%" class="serif_underline center-text"><strong>Prior Year</strong></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Daily Sales</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_153'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_153'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_153'))?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Daily Events Sales</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_131'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_131'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_131'))?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Comp $</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_39'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_39'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_39'))?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Comp %</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_42'), true, true)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'bd_39'), getField($fieldData, 'bd_153'), true, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_42'), true)?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Net Revenue</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_102'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_153') - getField($fieldData, 'bd_39') + getField($fieldData, 'bd_131'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_102'))?></td>
                    </tr>
                    <tr>
                        <td width="40%"><strong class="underline">Labor</strong></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Daily FOH labor %</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_59'), true, true)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'bd_61'), getField($fieldData, 'bd_153') + getField($fieldData, 'bd_131'), true, true)?></td>
<!--                        <td width="20%" class="right-text">--><?//=formatField(getField($fieldData, 'bd_61') / (getField($fieldData, 'bd_153') + getField($fieldData, 'bd_131')), true)?><!--</td>-->
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_59'), true)?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Daily BOH labor %</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_28'), true, true)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'bd_30'), getField($fieldData, 'bd_153') + getField($fieldData, 'bd_131'), true, true)?></td>
<!--                        <td width="20%" class="right-text">--><?//=formatField(getField($fieldData, 'bd_30') / (getField($fieldData, 'bd_153') + getField($fieldData, 'bd_131')), true)?><!--</td>-->
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_28'), true)?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Total Daily Var. Labor %</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_149'), true, true)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'bd_61') + getField($fieldData, 'bd_30'), getField($fieldData, 'bd_153') + getField($fieldData, 'bd_131'), true, true)?></td>
<!--                        <td width="20%" class="right-text">--><?//=formatField(getField($fieldData, 'ly_59') + getField($fieldData, 'ly_28'), true)?><!--</td>-->
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_149'), true)?></td>
                    </tr>
                    <tr>
                        <td width="40%"><strong class="serif_underline">Statistics</strong></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">Covers</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_5'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_5'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_5'))?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2"># Of Reservation</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_104'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_104'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_104'))?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2"># Of Actualized</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_14'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_14'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_14'))?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2"># Of CRM Contracts</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_8'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'bd_8'))?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_8'))?></td>
                    </tr>
                    <tr>
                        <td width="40%"><strong class="serif_underline">Sales Trend</strong></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <?php
                    $restaurantMTD = \Model\ReportData::MTD($locationString, $dateString, 'bd_153');
                    $restaurantMTD += \Model\ReportData::MTD($locationString, $dateString, 'bd_131');

                    $restaurantWTD = \Model\ReportData::WTD($locationString, $dateString, 'bd_153');
                    $restaurantWTD += \Model\ReportData::WTD($locationString, $dateString, 'bd_131');
                    ?>
                    <tr>
                        <td width="40%" class="level-2">MTD Sales</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_172'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField($restaurantMTD)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_172'))?></td>
                    </tr>
                    <tr class="variance">
                        <td width="40%" class="variance">Actual Variance $</td>
                        <td width="20%" class="actual"></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_172'), $restaurantMTD)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_172'), getField($fieldData, 'ly_172'))?></td>
                    </tr>
                    <tr class="variance">
                        <td width="40%" class="variance">Actual Variance %</td>
                        <td width="20%" class="actual"></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_172'), $restaurantMTD, true)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_172'), getField($fieldData, 'ly_172'), true)?></td>
                    </tr>
                    <tr>
                        <td width="40%" class="level-2">WTD Sales</td>
                        <td width="20%" class="right-text actual"><?=formatField(getField($fieldData, 'gm_171'), false, true)?></td>
                        <td width="20%" class="right-text"><?=formatField($restaurantWTD)?></td>
                        <td width="20%" class="right-text"><?=formatField(getField($fieldData, 'ly_171'))?></td>
                    </tr>
                    <tr class="variance">
                        <td width="40%" class="variance">Actual Variance $</td>
                        <td width="20%" class="actual"></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_171'), $restaurantWTD)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_171'), getField($fieldData, 'ly_171'))?></td>
                    </tr>
                    <tr class="variance">
                        <td width="40%" class="variance">Actual Variance %</td>
                        <td width="20%" class="actual"></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_171'), $restaurantWTD, true)?></td>
                        <td width="20%" class="right-text"><?=variance(getField($fieldData, 'gm_171'), getField($fieldData, 'ly_171'), true)?></td>
                    </tr>
                    <tr>
                        <td width="40%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%">GM Name</td>
                        <td colspan="3"><?=\Model\GmNote::getGm($locationString)?></td>
                    </tr>
                    <tr>
                        <td width="40%"><strong class="serif_underline">Notes</strong></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%">General Notes</td>
                        <td colspan="3"><?=isset($fieldData['nt_74'])?trimNote($fieldData['nt_74']):null?></td>
                    </tr>
                    <tr>
                        <td width="40%">Critical Notes</td>
                        <td colspan="3"><?=isset($fieldData['nt_46'])?trimNote($fieldData['nt_46']):null?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr/>
    <?php } ?>
<?php } elseif($type == 'nightlife'){ ?>
    <?php foreach($orders as $locationString){
        $fieldData = $formatData[$locationString];
        /**
         * If daily total sales isn't there, means they don't have submit the form with numbers, we omit this venue from report
         */
        if(!isset($nightlifeActiveVenues[$locationName])){continue;}?>
        <h2><?=\Model\Location::printName($locationString)?></h2>
        <h3><?=$date->format('M j, Y')?></h3>
        <div class="row">
            <table class="table" style="max-width: 300px">
                <tbody>
                    <tr>
                        <td><strong class="serif_underline">Revenue</strong></td>
                        <td class="actual center-text"><strong class="serif_underline">Actual</strong></td>
                        <td class="center-text"><strong class="serif_underline">Budget</strong></td>
                        <td class="center-text"><strong class="serif_underline">Prior Year</strong></td>
                    </tr>
                    <tr>
                        <td class="level-2">Beverage Bar</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_20'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_20'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_20'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Beverage Table</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_21'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_21'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_21'))?></td>
                    </tr>
                    <tr>
                        <td><strong>Total Beverage Sales</strong></td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_176'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_20') + getField($fieldData, 'bd_21'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_176'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Food</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_64'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_64'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_64'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Door</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_50'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_50'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_50'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Tickets</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_139'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_139'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_139'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Entertainment Fee</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_51'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_51'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_51'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Miscellaneous</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_98'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_98'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_98'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Special Events Sales Total</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_130'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_130'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_130'))?></td>
                    </tr>
                    <tr>
                        <td><strong>Total Sales</strong></td>
                        <td class="overline_tr actual right-text"><?=formatField(getField($fieldData, 'gm_152'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_152'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_152'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Comp $</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_38'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_38'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_38'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Comp %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_41'), true, true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'bd_38'), getField($fieldData, 'bd_152'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_41'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-3"><strong>Net Sales</strong></td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_101'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_152') - getField($fieldData, 'bd_38'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_101'))?></td>
                    </tr>
                    <tr>
                        <td><strong>Variable Labor Expenses</strong></td>
                        <td class="actual right-text"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="level-2">FOH %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_58'), true, true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'bd_60'), getField($fieldData, 'bd_152'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_58'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-2">BOH %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_27'), true, true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'bd_29'), getField($fieldData, 'bd_152'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_27'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Total Variable Labor Costs</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_140'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_140'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_140'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Total Variable Labor %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_148'), true, true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'bd_140'), getField($fieldData, 'bd_152'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_148'), true)?></td>
                    </tr>
                    <tr>
                        <td><strong>Nightly Expenses</strong></td>
                        <td class="actual right-text"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="level-2">DJ/Entertainment $</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_48'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_48'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_48'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">DJ/Entertainment %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_49'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_49'), true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_49'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Promoter $</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_178'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_178'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_178'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Promoter %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_112'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_112'), true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_112'), true)?></td>
                    </tr>
                    <tr>
                        <td><strong>Sales Trend</strong></td>
                        <td class="actual right-text"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="level-2">MTD Sales</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_174'), false, true)?></td>
                        <td class="right-text"><?=formatField(\Model\ReportData::MTD($locationString, $dateString, 'bd_152'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_174'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="right-text variance">Actual Variance $</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_174'), \Model\ReportData::MTD($locationString, $dateString, 'bd_152'))?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_174'), getField($fieldData, 'ly_174'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="right-text variance">Actual Variance %</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_174'), \Model\ReportData::MTD($locationString, $dateString, 'bd_152'), true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_174'), getField($fieldData, 'ly_174'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-2">WTD Sales</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_173'), false, true)?></td>
                        <td class="right-text"><?=formatField(\Model\ReportData::WTD($locationString, $dateString, 'bd_152'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_173'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="right-text variance">Actual Variance $</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_173'), \Model\ReportData::WTD($locationString, $dateString, 'bd_152'))?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_173'), getField($fieldData, 'ly_173'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="right-text variance">Actual Variance %</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_173'), \Model\ReportData::WTD($locationString, $dateString, 'bd_152'), true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_173'), getField($fieldData, 'ly_173'), true)?></td>
                    </tr>
                    <tr>
                        <td><strong>Statistics</strong></td>
                        <td class="actual right-text"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="level-2"># of Covers</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_4'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_4'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_4'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2"># of Bottles</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_1'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_1'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_1'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Spend Per Person</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_177'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_177'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_177'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Table Reservations #</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_132'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_132'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_132'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Table Reservations $</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_133'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_133'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_133'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">% of Males</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_10'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_10'), true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_10'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-2">% of Females</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_9'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_9'), true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_9'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-2"># of Emails Collected for CRM</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_7'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_7'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_7'))?></td>
                    </tr>
                    <tr>
                        <td width="40%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%">GM Name</td>
                        <td colspan="3"><?=\Model\GmNote::getGm($locationString)?></td>
                    </tr>
                    <tr>
                        <td><strong class="serif_underline">Notes</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>General Notes</td>
                        <td colspan="3"><?=isset($fieldData['nt_72'])?trimNote($fieldData['nt_72']):null?></td>
                    </tr>
                    <tr>
                        <td>Critical Notes</td>
                        <td colspan="3"><?=isset($fieldData['nt_45'])?trimNote($fieldData['nt_45']):null?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr/>
    <?php } ?>
<?php } elseif($type == 'hotel'){ ?>
        <?php foreach($formatData as $locationString => $fieldData){ if(isset($hotelNoShow[$locationString])){ continue; } ?>
        <h2><?=\Model\Location::printName($locationString)?></h2>
        <h3><?=$date->format('M j, Y')?></h3>
        <div class="row">
            <table class="table">
                <tbody>
                    <tr>
                        <td class="serif_underline"><strong>Daily</strong></td>
                        <td class="actual center-text serif_underline"><strong>Actual</strong></td>
                        <td class="center-text serif_underline"><strong>Budget</strong></td>
                        <td class="center-text serif_underline"><strong>Prior Year</strong></td>
                    </tr>
                    <tr>
                        <td class="level-2">Occupancy %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_105'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_105'), true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_105'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-2">ADR</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_15'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_15'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_15'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">RevPar</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_117'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_117'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_117'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Room Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_119'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_119'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_119'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Telephone Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_134'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_134'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_134'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Parking and Trans Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_110'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_110'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_110'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Rentals and Other Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_115'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_115'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_115'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">MiniBar Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_96'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_96'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_96'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3"><strong>Hotel Revenue Non F&B</strong></td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_150'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_150'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_150'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="variance">Actual Variance $</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_150'), getField($fieldData, 'bd_150'))?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_150'), getField($fieldData, 'ly_150'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="variance">Actual Variance %</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_150'), getField($fieldData, 'bd_150'), true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_150'), getField($fieldData, 'ly_150'), true)?></td>
                    </tr>
                    <tr>
                        <td class="serif_underline"><strong>Month to Date</strong></td>
                        <td class="actual right-text"></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="level-2">Available Rooms</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_19'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_19'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_19'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Occupied Rooms</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_108'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_108'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_108'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">Occupancy %</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_106'), true, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_106'), true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_106'), true)?></td>
                    </tr>
                    <tr>
                        <td class="level-2">ADR</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_16'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_16'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_16'))?></td>
                    </tr>
                    <tr>
                        <td class="level-2">RevPar</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_118'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_118'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_118'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Room Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_120'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_120'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_120'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Telephone Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_135'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_135'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_135'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Parking and Trans Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_111'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_111'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_111'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Rentals and Other Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_116'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_116'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_116'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3">Minibar Revenue</td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_97'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_97'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_97'))?></td>
                    </tr>
                    <tr>
                        <td class="level-3"><strong>Hotel Revenue Non F&B</strong></td>
                        <td class="actual right-text"><?=formatField(getField($fieldData, 'gm_151'), false, true)?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'bd_151'))?></td>
                        <td class="right-text"><?=formatField(getField($fieldData, 'ly_151'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="variance">Actual Variance $</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_151'), getField($fieldData, 'bd_151'))?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_151'), getField($fieldData, 'ly_151'))?></td>
                    </tr>
                    <tr class="variance">
                        <td class="variance">Actual Variance %</td>
                        <td class="actual right-text"></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_151'), getField($fieldData, 'bd_151'), true)?></td>
                        <td class="right-text"><?=variance(getField($fieldData, 'gm_151'), getField($fieldData, 'ly_151'), true)?></td>
                    </tr>
                    <tr>
                        <td width="40%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="40%">GM Name</td>
                        <td colspan="3"><?=\Model\GmNote::getGm($locationString)?></td>
                    </tr>
                    <tr>
                        <td><strong class="serif_underline">Notes</strong></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>General Notes</td>
                        <td colspan="3"><?=isset($fieldData['nt_71'])?trimNote($fieldData['nt_71']):null?></td>
                    </tr>
                    <tr>
                        <td>Critical Notes</td>
                        <td colspan="3"><?=isset($fieldData['nt_44'])?trimNote($fieldData['nt_44']):null?></td>
                    </tr>
                </tbody>
            </table>
        <hr/>
    <?php } ?>
<?php } ?>

<h2><?=ucfirst($type)?></h2>
<table class="table">
    <tbody>
        <tr>
            <td width="280"></td>
            <td width="100"><strong class="serif_underline center-text">Submitted</strong></td>
            <td width="100"><strong class="serif_underline center-text">Not Submitted</strong></td>
        </tr>
        <?php $submitted = 0; $unsubmitted = 0; foreach($submitDetails as $locationName => $submit){ if(\Model\Location::printName($locationName) == 'The Sayers Club'){ continue; } ?>
            <tr>
                <td width="280" style="border-bottom:1px solid #777777;" class="center-text"><?=\Model\Location::printName($locationName)?></td>
                <td width="100" style="border-bottom:1px solid #777777;" class="center-text"><img src="<?php if($submit){ echo 'content/frontend/img/check.png'; $submitted++; }?>" height="14px" width="14px" style="margin:4px;"></td>
                <td width="100" style="border-bottom:1px solid #777777;" class="center-text"><img src="<?php if(!$submit){ echo 'content/frontend/img/delete.png'; $unsubmitted++; }?>" height="14px" width="14px" style="margin:4px;"></td>
            </tr>
        <?php } ?>
        <tr>
            <td width="280" style="border-bottom:1px solid #777777;" class="center-text"><strong>Total</strong></td>
            <td width="100" style="border-bottom:1px solid #777777;" class="center-text"><strong><?=$submitted?></strong></td>
            <td width="100" style="border-bottom:1px solid #777777;" class="center-text"><strong><?=$unsubmitted?></strong></td>
        </tr>
    </tbody>
</table>


<?php if($type == 'nightlife'){ ?>
    <hr>
    <h2>Nightlife Programming</h2>
    <table class="table" border="1" style="font-size: 10px">
        <tbody>
            <tr>
                <td></td>
                <td class="actual">Monday</td>
                <td class="actual">Tuesday</td>
                <td class="actual">Wednesday</td>
                <td class="actual">Thursday</td>
                <td class="actual">Friday</td>
                <td class="actual">Saturday</td>
                <td class="actual">Sunday</td>
            </tr>
            <tr>
                <td class="actual">The Abbey</td>
                <td>Lip Service Mondays(Drag)</td>
                <td>Abbey University (College)</td>
                <td>Girl Bar</td>
                <td>Touch Thursday</td>
                <td>Sinful Fridays</td>
                <td>Confression Saturdays</td>
                <td>Sunday< Funday (Day Party)</td>
            </tr>
            <tr>
                <td class="actual">The Colony</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>XIV Sessions (Season Finale 9/14)</td>
            </tr>
            <tr>
                <td class="actual">The Emerson Theatre</td>
                <td></td>
                <td>#thatTuesday(Urban)</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>XIV Sessions (Season Finale 9/14)</td>
            </tr>
            <tr>
                <td class="actual">Greystone<br>Manor</td>
                <td></td>
                <td></td>
                <td>WNDSDYS(Hip Hop Fashion Hostings)</td>
                <td></td>
                <td>OMG Fridays(Asian)</td>
                <td>Greystone Saturdays</td>
                <td>Greystone Sundays(Urban)</td>
            </tr>
            <tr>
                <td class="actual">Hyde Sunset<br>Kitchen + Cocktails</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Hyde Fridays</td>
                <td>Hyde Satuirdays</td>
                <td></td>
            </tr>
            <tr>
                <td class="actual">Create</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Noize Fridays</td>
                <td>Arcade Saturdays</td>
                <td></td>
            </tr>
            <tr>
                <td class="actual">Hyde AAA</td>
                <td>Based on AAA Games/Events</td>
                <td>Based on AAA Games/Events</td>
                <td>Based on AAA Games/Events</td>
                <td>Based on AAA Games/Events</td>
                <td>Based on AAA Games/Events</td>
                <td>Based on AAA Games/Events</td>
                <td>Based on AAA Games/Events</td>
            </tr>
            <tr>
                <td class="actual">Hyde Bellagio</td>
                <td></td>
                <td>Lost Angels(Industry Night)</td>
                <td></td>
                <td>Live Music Thursdays</td>
                <td>Fridays at Hyde Bellagio</td>
                <td>Saturdays at Hyde Bellagio</td>
                <td></td>
            </tr>
            <tr>
                <td class="actual">Hyde Staples Center</td>
                <td>Based on Staples Games/Events</td>
                <td>Based on Staples Games/Events</td>
                <td>Based on Staples Games/Events</td>
                <td>Based on Staples Games/Events</td>
                <td>Based on Staples Games/Events</td>
                <td>Based on Staples Games/Events</td>
                <td>Based on Staples Games/Events</td>
            </tr>
            <tr>
                <td class="actual">Hyde South Beach</td>
                <td></td>
                <td></td>
                <td></td>
                <td>Your Move Thursdays</td>
                <td>Hyde Fridays</td>
                <td>Swim Club</td>
                <td>Based on Staples Games/Events</td>
            </tr>
        </tbody>
    </table>
<?php } ?>

</body>
</html>
