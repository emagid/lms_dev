<style>
    input {
        width: inherit !important;
        display: inherit !important;
        text-align: right !important;
    }
</style>
<script>
    function handleEnter (field, event) {
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        if (keyCode == 13) {
            var i;
            for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                    break;
            i = (i + 1) % field.form.elements.length;
            field.form.elements[i].focus();
            return false;
        }
        else
            return true;
    }

    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /-?[0-9]*|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    function checkRequiredInput(){
        var rslt = true;
        var textPreview = "";
        var keyVal = "";
        var selectedCount = 0;
        var element;
        // We want to loop through all controls and make sure the user has inputted values for required fields.
        for (var i = 0; i < document.forms[0].elements.length; i++)
        {
            element = document.forms[0].elements[i];
            switch (element.type)
            {
                case 'text':

                    var nm = "nm_" + element.id.substring(3,element.id.length);

                    // If the field is required i.e. gm, ac, and enabled set flag and change color of header
                    if( (element.id.substring(0,2) == "gm" || element.id.substring(0,2) == "ac") && (!element.hidden) && (!element.readOnly) && (!element.disabled) && element.value.length == 0 )
                    {
                        rslt = false;
                        //document.contains(document.getElementById(nm));
                        if(document.getElementById(nm) != null)
                        {
                            document.getElementById(nm).style.color = "red";
                            //textPreview += document.getElementById(nm).innerHTML + " \n";
                        }
                    }
                    else if( (element.id.substring(0,2) == "gm" || element.id.substring(0,2) == "ac") && (!element.hidden) && (!element.readOnly) && (!element.disabled) && element.value.length > 0 )
                    {

                        // If we are only checking GM
                        if(element.id.substring(0,2) == "gm" && element.value.length > 0){
                            if(document.getElementById(nm) != null)
                            {
                                document.getElementById(nm).style.color = "black";
                                //textPreview += document.getElementById(nm).innerHTML + " \n";
                            }
                        }
                        else{
                            // If we are checking both required inputs (GM/Accounting) are filled in before setting to black.
                            var gm = (element.id.substring(0,2) == "gm") ? "ac_" + element.id.substring(3, element.id.length) : "gm_" + element.id.substring(3, element.id.length);
                            if(document.getElementById(gm) != null && document.getElementById(gm).value > 0)
                            {
                                if(document.getElementById(nm) != null)
                                {
                                    document.getElementById(nm).style.color = "black";
                                    //textPreview += document.getElementById(nm).innerHTML + " \n";
                                }
                            }
                        }
                    }
                    break;
            }
        }
        //document.getElementById("textPreview").innerHTML= textPreview;
        return rslt;

    }

    // Add commas to numerical value inputted
    function commaUpdate(input){
        // Check if user inputted data.
        if(input.value != ""){
            var val = input.value.replace(",","");
            val = parseFloat(val).toFixed(2);
            val = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            document.getElementById(input.id).value = val;
        }
    }

    // Add Commas
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //var strVal = x.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //document.getElementById(x.id).value = strVal;
    }

    // Computation Functions
    function computeTotalSales(id) {
        var id65 = (document.getElementById(id + "_65").value) != "" ? parseFloat(document.getElementById(id + "_65").value.replace(",","")): 0;
        var id22 = (document.getElementById(id + "_22").value) != "" ? parseFloat(document.getElementById(id + "_22").value.replace(",","")): 0;
        var id99 = (document.getElementById(id + "_99").value) != "" ? parseFloat(document.getElementById(id + "_99").value.replace(",","")): 0;
        var id39 = (document.getElementById(id + "_39").value) != "" ? parseFloat(document.getElementById(id + "_39").value.replace(",","")): 0;
        var id131 = (document.getElementById(id + "_131") && document.getElementById(id + "_131").value != "") ? parseFloat(document.getElementById(id + "_131").value.replace(",","")): 0;
        /**
         * 2017 budget file has bd_153 data,so we skip below calculation part
         */
        if(id == 'bd'){
            document.getElementById(id + "_102").value = parseFloat(document.getElementById(id + "_153").value) - id39 + id131;
            document.getElementById(id + "_102").value = parseFloat(document.getElementById(id + "_102").value).toFixed(2);
            return;
        }

        document.getElementById(id + "_153").value  = id65 + id22 + id99;
        document.getElementById(id + "_153").value = parseFloat(document.getElementById(id + "_153").value).toFixed(2);

        document.getElementById(id + "_102").value = id65 + id22 + id99 - id39 + id131;
        document.getElementById(id + "_102").value = parseFloat(document.getElementById(id + "_102").value).toFixed(2);
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        if(id65 > 0 && id22 > 0 && id99 > 0 && id153 > 0){
            var food = parseFloat((id65/id153) * 100).toFixed(0);
            var bev = parseFloat((id22/id153) * 100).toFixed(0);
            var misc = parseFloat((id99/id153) * 100).toFixed(0);
            document.getElementById(id + "_69").value = food.toString() + "/" + bev.toString() + "/" + misc.toString();
            //document.getElementById(id + "_69").value = parseFloat(document.getElementById(id + "_69").value).toFixed(2);
        }
        if(id65 > 0 && id22 > 0 && id153 > 0){
            var food = parseFloat((id65/id153) * 100).toFixed(0);
            var bev = parseFloat((id22/id153) * 100).toFixed(0);
            document.getElementById(id + "_67").value = food.toString() + "/" + bev.toString();
            //document.getElementById(id + "_67").value = parseFloat(document.getElementById(id + "_67").value).toFixed(2);
        }
    }

    function computeTotalSalesBudget(id){
        /**
         * 2017 budget file has bd_153 data,so we skip below calculation part
         */
        if(id == 'bd'){
            var id39 = (document.getElementById(id + "_39").value) != "" ? parseFloat(document.getElementById(id + "_39").value.replace(",","")): 0;
            var id131 = (document.getElementById(id + "_131") && document.getElementById(id + "_131").value != "") ? parseFloat(document.getElementById(id + "_131").value.replace(",","")): 0;
            document.getElementById(id + "_102").value = parseFloat(document.getElementById(id + "_153").value) - id39 + id131;
            document.getElementById(id + "_102").value = parseFloat(document.getElementById(id + "_102").value).toFixed(2);
            return;
        } else {
            var id65 = (document.getElementById(id + "_65").value) != "" ? parseFloat(document.getElementById(id + "_65").value.replace(",","")): 0;
            var id22 = (document.getElementById(id + "_22").value) != "" ? parseFloat(document.getElementById(id + "_22").value.replace(",","")): 0;
            var id99 = (document.getElementById(id + "_99").value) != "" ? parseFloat(document.getElementById(id + "_99").value.replace(",","")): 0;
            document.getElementById(id + "_153").value  = id65 + id22 + id99;
            document.getElementById(id + "_153").value = parseFloat(document.getElementById(id + "_153").value).toFixed(2);
        }


        var id63 = (document.getElementById(id + "_63").value) != "" ? parseFloat(document.getElementById(id + "_63").value.replace(",","")): 0;
        var id32 = (document.getElementById(id + "_32").value) != "" ? parseFloat(document.getElementById(id + "_32").value.replace(",","")): 0;
        document.getElementById(id + "_143").value  = id63 + id32;
        document.getElementById(id + "_143").value = parseFloat(document.getElementById(id + "_143").value).toFixed(2);

        var id61 = (document.getElementById(id + "_61").value) != "" ? parseFloat(document.getElementById(id + "_61").value.replace(",","")): 0;
        var id30 = (document.getElementById(id + "_30").value) != "" ? parseFloat(document.getElementById(id + "_30").value.replace(",","")): 0;
        document.getElementById(id + "_141").value  = id61 + id30;
        document.getElementById(id + "_141").value = parseFloat(document.getElementById(id + "_141").value).toFixed(2);

        var id141 = (document.getElementById(id + "_141").value) != "" ? parseFloat(document.getElementById(id + "_141").value.replace(",","")): 0;
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;

        /**
         * Last year 149 don't need calculate
         */
        if(id141 > 0 && id153 > 0){

            document.getElementById(id + "_149").value  = parseFloat((id141/id153) * 100).toFixed(2);


        }

    }

    function computeCompPercentage(id){
        var id102 = (document.getElementById(id + "_102").value) != "" ? parseFloat(document.getElementById(id + "_102").value.replace(",","")): 0;
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        var id39 = (document.getElementById(id + "_39").value) != "" ? parseFloat(document.getElementById(id + "_39").value.replace(",","")): 0;
        if(id153 > 0 && id39 > 0)
        {
            //document.getElementById(id + "_42").value  = Math.round((id39/id153) * 100);
            document.getElementById(id + "_42").value  = (id39/id153) * 100;
            document.getElementById(id + "_42").value = parseFloat(document.getElementById(id + "_42").value).toFixed(2);
            //document.getElementById(id + "_102").value = id102 - id39;
            //document.getElementById(id + "_102").value = parseFloat(document.getElementById(id + "_102").value).toFixed(2);
        }
    }

    function computeCompPercentageBudget(id){
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        var id39 = (document.getElementById(id + "_39").value) != "" ? parseFloat(document.getElementById(id + "_39").value.replace(",","")): 0;
        var id131 = (document.getElementById(id + "_131") && document.getElementById(id + "_131").value != "") ? parseFloat(document.getElementById(id + "_131").value.replace(",","")): 0;
        document.getElementById(id + "_102").value = id153 - id39 + id131;
        document.getElementById(id + "_102").value = parseFloat(document.getElementById(id + "_102").value).toFixed(2);

    }


    function computeSeTotal(id) {
        if(document.getElementById(id + "_131") && document.getElementById(id + "_131").value && id != 'gm'){
            return;
        }
        var id127 = (document.getElementById(id + "_127").value) != "" ? parseFloat(document.getElementById(id + "_127").value.replace(",","")): 0;
        var id125 = (document.getElementById(id + "_125").value) != "" ? parseFloat(document.getElementById(id + "_125").value.replace(",","")): 0;
        var id129 = (document.getElementById(id + "_129").value) != "" ? parseFloat(document.getElementById(id + "_129").value.replace(",","")): 0;
        document.getElementById(id + "_131").value  = id127 + id125 + id129;
        document.getElementById(id + "_131").value = parseFloat(document.getElementById(id + "_131").value).toFixed(2);
    }

    function computeFOHPercentage(id){
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        var id61 = (document.getElementById(id + "_61").value) != "" ? parseFloat(document.getElementById(id + "_61").value.replace(",","")): 0;
        var id131 = (document.getElementById(id + "_131") && document.getElementById(id + "_131").value != "") ? parseFloat(document.getElementById(id + "_131").value.replace(",","")): 0;
        if(id153 > 0 && id61 > 0){
            document.getElementById(id + "_59").value  = (id61/(id153 + id131)) * 100;
            document.getElementById(id + "_59").value = parseFloat(document.getElementById(id + "_59").value).toFixed(2);
        }
    }

    function computeBOHPercentage(id){
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        var id30 = (document.getElementById(id + "_30").value) != "" ? parseFloat(document.getElementById(id + "_30").value.replace(",","")): 0;
        var id131 = (document.getElementById(id + "_131") && document.getElementById(id + "_131").value != "") ? parseFloat(document.getElementById(id + "_131").value.replace(",","")): 0;
        if(id153 > 0 && id30 > 0){
            document.getElementById(id + "_28").value  = (id30/(id153 + id131)) * 100;
            document.getElementById(id + "_28").value = parseFloat(document.getElementById(id + "_28").value).toFixed(2);
        }
    }

    function computeTotalHours(id){
        var id63 = (document.getElementById(id + "_63").value) != "" ? parseFloat(document.getElementById(id + "_63").value.replace(",","")): 0;
        var id32 = (document.getElementById(id + "_32").value) != "" ? parseFloat(document.getElementById(id + "_32").value.replace(",","")): 0;
        document.getElementById(id + "_143").value  = id63 + id32;
        document.getElementById(id + "_143").value = parseFloat(document.getElementById(id + "_143").value).toFixed(2);

    }

    function computeTotalCost(id){
        var id61 = (document.getElementById(id + "_61").value) != "" ? parseFloat(document.getElementById(id + "_61").value.replace(",","")): 0;
        var id30 = (document.getElementById(id + "_30").value) != "" ? parseFloat(document.getElementById(id + "_30").value.replace(",","")): 0;
        document.getElementById(id + "_141").value  = id61 + id30;
        document.getElementById(id + "_141").value = parseFloat(document.getElementById(id + "_141").value).toFixed(2);

    }

    function computeTotalLaborPercentage(id){
        var id141 = (document.getElementById(id + "_141").value) != "" ? parseFloat(document.getElementById(id + "_141").value.replace(",","")): 0;
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        if(id153 > 0 && id141 > 0){
            document.getElementById(id + "_149").value  = (id141/id153) * 100;
            document.getElementById(id + "_149").value = parseFloat(document.getElementById(id + "_149").value).toFixed(2);
        }

    }

    function computeFOHCover(id){
        var id61 = (document.getElementById(id + "_61").value) != "" ? parseFloat(document.getElementById(id + "_61").value.replace(",","")): 0;
        var id5 = (document.getElementById(id + "_5").value) != "" ? parseFloat(document.getElementById(id + "_5").value.replace(",","")): 0;
        if(id61 > 0 && id5 > 0){
            document.getElementById(id + "_55").value  = (id61/id5) * 100;
            document.getElementById(id + "_55").value = parseFloat(document.getElementById(id + "_55").value).toFixed(2);
        }
    }

    function computeBOHCover(id){
        var id30 = (document.getElementById(id + "_30").value) != "" ? parseFloat(document.getElementById(id + "_30").value.replace(",","")): 0;
        var id5 = (document.getElementById(id + "_5").value) != "" ? parseFloat(document.getElementById(id + "_5").value.replace(",","")): 0;
        if(id30 > 0 && id5 > 0){
            document.getElementById(id + "_24").value  = (id30/id5) * 100;
            document.getElementById(id + "_24").value = parseFloat(document.getElementById(id + "_24").value).toFixed(2);
        }
    }

    function computeTotalLaborCover(id){
        var id141 = (document.getElementById(id + "_141").value) != "" ? parseFloat(document.getElementById(id + "_141").value.replace(",","")): 0;
        var id5 = (document.getElementById(id + "_5").value) != "" ? parseFloat(document.getElementById(id + "_5").value.replace(",","")): 0;
        if(id141 > 0 && id5 > 0){
            document.getElementById(id + "_145").value  = (id141/id5) * 100;
            document.getElementById(id + "_145").value = parseFloat(document.getElementById(id + "_145").value).toFixed(2);
        }
    }

    function computeFOHSales(id){
        var id61 = (document.getElementById(id + "_61").value) != "" ? parseFloat(document.getElementById(id + "_61").value.replace(",","")): 0;
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        if(id61 > 0 && id153 > 0){
            document.getElementById(id + "_57").value  = (id61/id153) * 100;
            document.getElementById(id + "_57").value = parseFloat(document.getElementById(id + "_57").value).toFixed(2);
        }
    }

    function computeBOHSales(id){
        var id30 = (document.getElementById(id + "_30").value) != "" ? parseFloat(document.getElementById(id + "_30").value.replace(",","")): 0;
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        if(id30 > 0 && id153 > 0){
            document.getElementById(id + "_26").value  = (id30/id153) * 100;
            document.getElementById(id + "_26").value = parseFloat(document.getElementById(id + "_26").value).toFixed(2);
        }
    }

    function computeTotalLaborSales(id){
        var id141 = (document.getElementById(id + "_141").value) != "" ? parseFloat(document.getElementById(id + "_141").value.replace(",","")): 0;
        var id153 = (document.getElementById(id + "_153").value) != "" ? parseFloat(document.getElementById(id + "_153").value.replace(",","")): 0;
        if(id141 > 0 && id153 > 0){
            document.getElementById(id + "_147").value  = (id141/id153) * 100;
            document.getElementById(id + "_147").value = parseFloat(document.getElementById(id + "_147").value).toFixed(2);
        }

    }


    function submitForm(txt) {
        document.form1.submit();
        alert ("Saving data. Click OK");
        document.location = txt;
    }
    function finalizeData(){
        document.getElementById("dataFinal").value = "done";
        //document.getElementById("userID").value = 1;
        // Check to see if all the required fields have input.
        var rslt = checkRequiredInput();
        if(!rslt){
            alert("Please fill in all required fields highlighted in red.");
            return false;
        }

        return true;
    }
</script>
<h4 class="headers">SBE Reporting System</h4>
<h5 class="headers">Location: <?=ucwords(urldecode($_GET['location']))?></h5>
<div>
    <form method="post" action="/admin/reporting/restaurant" name="form1">
        <input type="hidden" name="location" id="location" value="<?=rawurldecode($_GET['location'])?>">
        <input type="hidden" name="date" id="date" value="<?=$_GET['date']?>">
        <table border="1">
            <tr>
                <td>
                    <table>
                        <tbody>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Total Daily Sales</td>
                            <td style="font-size:14px;">Actual (Draft/GM)</td>
                            <td style="font-size:14px;"><div id="ac_hdr" style="display:none;">Actual (Accounting)</div></td>
                            <td style="font-size:14px;">Budget</td>
                            <td style="font-size:14px;">Forecast</td>
                            <td style="font-size:14px;">Last Year</td>
                        </tr>
                        <tr>
                            <!-- The controls id is a concatenation of dimVersion.fdPageControlIDMap, dimScenario.fdPageControlIDMap, dimAccount.fdPageControlIDMap in order to accommodate our db structure of each control value being a record in tblfctData-->
                            <td><div id="nm_65">Food $ - Daily - Restaurant</div></td>
                            <!--<td><input type="number" name="gm_65" value="" id="gm_65" class="textField" size="6"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');" pattern="[0-9]+([\.|,][0-9]+)?" step="0.01" title="This should be a number with up to 2 decimal places."></td>-->
                            <td><input type="text" name="gm_65" value="" id="gm_65" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeFOHPercentage('gm');computeBOHPercentage('gm');computeTotalLaborPercentage('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_65" value="" id="ac_65" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac');computeBOHPercentage('ac');computeTotalLaborPercentage('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_65" value="" id="bd_65" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('bd'); computeBOHPercentage('bd')"></td>
                            <td><input type="text" name="fc_65" value="" id="fc_65" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_65" value="" id="ly_65" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('ly'); computeBOHPercentage('ly')"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_22">Beverage $ - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_22" value="" id="gm_22" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeFOHPercentage('gm');computeBOHPercentage('gm');computeTotalLaborPercentage('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_22" value="" id="ac_22" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac');computeBOHPercentage('ac');computeTotalLaborPercentage('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_22" value="" id="bd_22" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('bd');computeBOHPercentage('bd')"></td>
                            <td><input type="text" name="fc_22" value="" id="fc_22" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_22" value="" id="ly_22" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('ly');computeBOHPercentage('ly')"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_99">Miscellaneous $ - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_99" value="" id="gm_99" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeFOHPercentage('gm');computeBOHPercentage('gm');computeTotalLaborPercentage('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_99" value="" id="ac_99" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac');computeBOHPercentage('ac');computeTotalLaborPercentage('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_99" value="" id="bd_99" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('bd');computeBOHPercentage('bd')"></td>
                            <td><input type="text" name="fc_99" value="" id="fc_99" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_99" value="" id="ly_99" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('ly');computeBOHPercentage('ly')"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_153">Total Sales $ - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_153" value="" id="gm_153" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_153" value="" id="ac_153" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_153" value="" id="bd_153" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_153" value="" id="fc_153" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_153" value="" id="ly_153" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_39">Comps $ - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_39" value="" id="gm_39" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeFOHPercentage('gm');computeBOHPercentage('gm');computeTotalLaborPercentage('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_39" value="" id="ac_39" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac');computeBOHPercentage('ac');computeTotalLaborPercentage('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_39" value="" id="bd_39" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('bd');computeBOHPercentage('bd')"></td>
                            <td><input type="text" name="fc_39" value="" id="fc_39" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_39" value="" id="ly_39" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('ly');computeBOHPercentage('ly')"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_42">Comps % - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_42" value="" id="gm_42" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <div id="ac_prc" style="display:none;">%  </div><input type="hidden" name="ac_42" value="" id="ac_42" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_42" value="" id="bd_42" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_42" value="" id="fc_42" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_42" value="" id="ly_42" class="textField" size="11" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_102">Net Sales - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_102" value="" id="gm_102" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_102" value="" id="ac_102" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_102" value="" id="bd_102" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_102" value="" id="fc_102" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_102" value="" id="ly_102" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_69">Food/Beverage/Other Mix - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_69" value="" id="gm_69" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_69" value="" id="ac_69" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_69" value="" id="bd_69" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_69" value="" id="fc_69" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_69" value="" id="ly_69" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_171">Total Sales - WTD - Restaurant</div></td>
                            <td><input type="text" name="gm_171" value="" id="gm_171" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_171" value="" id="ac_171" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_171" value="" id="bd_171" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_171" value="" id="fc_171" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_171" value="" id="ly_171" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_172">Total Sales - MTD - Restaurant</div></td>
                            <td><input type="text" name="gm_172" value="" id="gm_172" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_172" value="" id="ac_172" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_172" value="" id="bd_172" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_172" value="" id="fc_172" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_172" value="" id="ly_172" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Special Events</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div id="nm_127">Special Events - Food - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_127" value="" id="gm_127" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_127" value="" id="ac_127" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ac');commaUpdate(this);"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_127" value="" id="bd_127" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_127" value="" id="ly_127" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_125">Special Events - Drink - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_125" value="" id="gm_125" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_125" value="" id="ac_125" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ac');commaUpdate(this);"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_125" value="" id="bd_125" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_125" value="" id="ly_125" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_129">Special Events - Misc. - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_129" value="" id="gm_129" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_129" value="" id="ac_129" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ac');commaUpdate(this);"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_129" value="" id="bd_129" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_129" value="" id="ly_129" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_131">Special Events - Total - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_131" value="" id="gm_131" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_131" value="" id="ac_131" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_131" value="" id="bd_131" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_131" value="" id="ly_131" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Statistics</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div id="nm_5"># of Covers - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_5" value="" id="gm_5" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_5" value="" id="ac_5" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_5" value="" id="bd_5" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_5" value="" id="fc_5" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_5" value="" id="ly_5" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_3"># of Checks - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_3" value="" id="gm_3" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_3" value="" id="ac_3" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_3" value="" id="bd_3" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_3" value="" id="fc_3" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_3" value="" id="ly_3" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_104">Number of Reservations - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_104" value="" id="gm_104" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_104" value="" id="ac_104" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_104" value="" id="bd_104" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_104" value="" id="fc_104" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_104" value="" id="ly_104" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_14">Actualized Reservations - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_14" value="" id="gm_14" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_14" value="" id="ac_14" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_14" value="" id="bd_14" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_14" value="" id="fc_14" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_14" value="" id="ly_14" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_8"># of Emails Collected for CRM - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_8" value="" id="gm_8" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_8" value="" id="ac_8" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_8" value="" id="bd_8" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_8" value="" id="fc_8" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_8" value="" id="ly_8" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_67">Food / Bev Mix - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_67" value="" id="gm_67" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_67" value="" id="ac_67" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_67" value="" id="bd_67" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_67" value="" id="fc_67" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_67" value="" id="ly_67" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_55">FOH $ / Cover - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_55" value="" id="gm_55" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_55" value="" id="ac_55" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_55" value="" id="bd_55" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_55" value="" id="fc_55" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_55" value="" id="ly_55" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_24">BOH $ / Cover - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_24" value="" id="gm_24" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_24" value="" id="ac_24" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_24" value="" id="bd_24" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_24" value="" id="fc_24" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_24" value="" id="ly_24" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_145">Total Labor $ / Cover - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_145" value="" id="gm_145" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_145" value="" id="ac_145" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_145" value="" id="bd_145" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_145" value="" id="fc_145" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_145" value="" id="ly_145" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_57">FOH $ / Sales - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_57" value="" id="gm_57" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_57" value="" id="ac_57" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_57" value="" id="bd_57" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_57" value="" id="fc_57" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_57" value="" id="ly_57" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_26">BOH $ / Sales - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_26" value="" id="gm_26" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_26" value="" id="ac_26" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_26" value="" id="bd_26" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_26" value="" id="fc_26" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_26" value="" id="ly_26" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_147">Total Labor $ / Sales $ - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_147" value="" id="gm_147" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_147" value="" id="ac_147" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_147" value="" id="bd_147" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_147" value="" id="fc_147" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_147" value="" id="ly_147" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Labor</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div id="nm_63">FOH Hours - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_63" value="" id="gm_63" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_63" value="" id="ac_63" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_63" value="" id="bd_63" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_63" value="" id="fc_63" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_63" value="" id="ly_63" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_61">FOH Cost - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_61" value="" id="gm_61" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeFOHPercentage('gm');computeTotalCost('gm');computeTotalLaborPercentage('gm');computeFOHCover('gm');computeTotalLaborCover('gm');computeFOHSales('gm');computeTotalLaborSales('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_61" value="" id="ac_61" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeFOHPercentage('ac');computeTotalCost('ac');computeTotalLaborPercentage('ac');computeFOHCover('ac');computeTotalLaborCover('ac');computeFOHSales('ac');computeTotalLaborSales('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_61" value="" id="bd_61" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('bd')"></td>
                            <td><input type="text" name="fc_61" value="" id="fc_61" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_61" value="" id="ly_61" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeFOHPercentage('ly')"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_59">FOH % - Daily - Restaurant</td>
                            <td>% <input type="text" name="gm_59" value="" id="gm_59" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_59" value="" id="ac_59" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_59" value="" id="bd_59" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_59" value="" id="fc_59" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_59" value="" id="ly_59" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_32">BOH Hours - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_32" value="" id="gm_32" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_32" value="" id="ac_32" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_32" value="" id="bd_32" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_32" value="" id="fc_32" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_32" value="" id="ly_32" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_30">BOH Cost - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_30" value="" id="gm_30" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeBOHPercentage('gm');computeTotalCost('gm');computeTotalLaborPercentage('gm');computeBOHCover('gm');computeTotalLaborCover('gm');computeBOHSales('gm');computeTotalLaborSales('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_30" value="" id="ac_30" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeBOHPercentage('ac');computeTotalCost('ac');computeTotalLaborPercentage('ac');computeBOHCover('ac');computeTotalLaborCover('ac');computeBOHSales('ac');computeTotalLaborSales('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_30" value="" id="bd_30" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeBOHPercentage('bd')"></td>
                            <td><input type="text" name="fc_30" value="" id="fc_30" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_30" value="" id="ly_30" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="computeBOHPercentage('ly')"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_28">BOH % - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_28" value="" id="gm_28" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_28" value="" id="ac_28" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_28" value="" id="bd_28" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_28" value="" id="fc_28" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_28" value="" id="ly_28" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_143">Total Hours - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_143" value="" id="gm_143" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_143" value="" id="ac_143" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_143" value="" id="bd_143" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_143" value="" id="fc_143" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_143" value="" id="ly_143" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_141">Total Cost - Daily - Restaurant</div></td>
                            <td><input type="text" name="gm_141" value="" id="gm_141" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_141" value="" id="ac_141" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_141" value="" id="bd_141" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_141" value="" id="fc_141" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_141" value="" id="ly_141" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_149">Total Labor % - Daily - Restaurant</div></td>
                            <td>% <input type="text" name="gm_149" value="" id="gm_149" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_149" value="" id="ac_149" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_149" value="" id="bd_149" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_149" value="" id="fc_149" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_149" value="" id="ly_149" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <!--<td valign="top" width="200">
                    <span class="headers">Text Message Preview</span><br>
                    <span style="color:#FF0000; font-size:10px">Make sure that the following preview has
                    all the neccesary fields</span><hr>
                    <span id="textPreview"></span>
                    <input type="hidden" id="textMessage" name="theText">
                </td>-->
            </tr>
        </table>
        <table border="1">
            <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Notes</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Weather - Daily - Restaurant</td>
                            <td>Temperature<input type="text"  name="nt_161" value="" id="nt_161" class="textField" size="13"  onKeyPress="return handleEnter(this, event)">
                                <select name="nt_164" id="nt_164" class="textField">
                                    <option value="Cold">Cold</option>
                                    <option value="Mild">Mild</option>
                                    <option value="Hot">Hot</option>
                                </select>
                                <select name="nt_167" id="nt_167" class="textField">
                                    <option value="Clear">Clear</option>
                                    <option value="Cloudy">Cloudy</option>
                                    <option value="Raining/Snowing">Raining/Snowing</option>
                                </select>
                                <select name="nt_170" id="nt_170" class="textField">
                                    <option value="Mild">Mild</option>
                                    <option value="Low">Low</option>
                                    <option value="High">High</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Critical Notes - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_46" value="" id="nt_46" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Incident - Daily - Restaurant</td>
                            <td><input type="checkbox" name="nt_82" id="nt_82" value="1" >Check</td>
                        </tr>
                        <tr>
                            <td>Incident Notes - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_85" value="" id="nt_85" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Security Notes - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_123" value="" id="nt_123" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Celebrity Notes - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_35" value="" id="nt_35" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>VIP Guests - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_156" value="" id="nt_156" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Local Events / Shows - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_90" value="" id="nt_90" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>HR Notes - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_77" value="" id="nt_77" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>GM Names - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="gn_2" value="" id="gn_2" class="textField" size="13"  onKeyPress="return handleEnter(this, event)" disabled="disabled"><?=\Model\GmNote::getGm(urldecode($_GET['location']))?></textarea></td>
                        </tr>
                        <tr>
                            <td>General Notes - Daily - Restaurant</td>
                            <td><textarea rows="3" cols="60"  name="nt_74" value="" id="nt_74" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Manager - Daily - Restaurant</td>
                            <td><input type="text" name="mgr" value="<?=urldecode($_GET['location'])?>" id="mgr" class="textField" size="13" style="background-color:lightgray;" readonly="readonly"></textarea></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Submit Manager Report" class="button" onClick="window.onbeforeunload=null; return finalizeData();"></td>
            </tr>
            </tbody>
        </table>

    </form>
</div>
<?php echo footer(); ?>
<script type="text/javascript">

    $(document).ready(function(){
        //====================================================

        // Loop through php server side code and setup javascript variables that contain budget, last year, forecast data...

        var fieldData = <?=json_encode($model->fields?:[])?>;

        //===================================================
        function loadData() {
            for(var key in fieldData){
                var value = fieldData[key];
                var $field = $("#" + key);

                $field.val(value);
                if(key == 'nt_82' && value == '1'){
                    $field.attr('checked', true);
                }
                $field.trigger('change');
            }
        }

        // Display accounting fields if user has access i.e. admin/Accountant
        function showAccountingColumns(){
            var position = "GM";

            // Check if user should have access
            if(position == "Accountant" || position == "admin"){
                var element;
                // We want to loop through all controls and set to display.
                for (var i = 0; i < document.forms[0].elements.length; i++)
                {
                    element = document.forms[0].elements[i];
                    switch (element.type)
                    {
                        case 'hidden':

                            var prefix = element.id.substring(0,2);

                            // If the field is for accounting i.e. ac then show
                            if(prefix == "ac")
                            {
                                element.type = "text";
                            }
                            break;
                    }
                }
            }
        }


        /***********************************************
         * Disable "Enter" key in Form script- By Nurul Fadilah(nurul@REMOVETHISvolmedia.com)
         * This notice must stay intact for use
         * Visit http://www.dynamicdrive.com/ for full source code
         ***********************************************/

        // Run at front
        loadData();showAccountingColumns();computeTotalSales('ac');computeTotalSales('bd');computeTotalSalesBudget('bd');computeCompPercentageBudget('bd');
        computeBOHPercentage('bd');computeFOHPercentage('bd');computeTotalSales('gm');computeSeTotal('gm');
        computeTotalLaborPercentage('ly');
        /**
         * Do not calculate last year data
         * DO CALCULATE NOW, TODO: turn this shit off
         */
        computeTotalSales('ly');
        computeTotalSalesBudget('ly');
        computeCompPercentage('ly');
        computeFOHPercentage('ly');
        computeBOHPercentage('ly');

    });


</script>
<script src="/content/admin/js/core.js"></script>