<style>
    input {
        width: inherit !important;
        display: inherit !important;
        text-align: right !important;
    }
</style>
<script>
    function handleEnter (field, event) {
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        if (keyCode == 13) {
            var i;
            for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                    break;
            i = (i + 1) % field.form.elements.length;
            field.form.elements[i].focus();
            return false;
        }
        else
            return true;
    }

    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /-?[0-9]*|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    function checkRequiredInput(){
        var rslt = true;
        var textPreview = "";
        var keyVal = "";
        var selectedCount = 0;
        var element;
        // We want to loop through all controls and make sure the user has inputted values for required fields.
        for (var i = 0; i < document.forms[0].elements.length; i++)
        {
            element = document.forms[0].elements[i];
            switch (element.type)
            {
                case 'text':

                    var nm = "nm_" + element.id.substring(3,element.id.length);

                    // If the field is required i.e. gm, ac, and enabled set flag and change color of header
                    if( (element.id.substring(0,2) == "gm" || element.id.substring(0,2) == "ac") && (!element.hidden) && (!element.readOnly) && (!element.disabled) && element.value.length == 0 )
                    {
                        rslt = false;
                        //document.contains(document.getElementById(nm));
                        if(document.getElementById(nm) != null)
                        {
                            document.getElementById(nm).style.color = "red";
                            //textPreview += document.getElementById(nm).innerHTML + " \n";
                        }
                    }
                    else if( (element.id.substring(0,2) == "gm" || element.id.substring(0,2) == "ac") && (!element.hidden) && (!element.readOnly) && (!element.disabled) && element.value.length > 0 )
                    {

                        // If we are only checking GM
                        if(element.id.substring(0,2) == "gm" && element.value.length > 0){
                            if(document.getElementById(nm) != null)
                            {
                                document.getElementById(nm).style.color = "black";
                                //textPreview += document.getElementById(nm).innerHTML + " \n";
                            }
                        }
                        else{
                            // If we are checking both required inputs (GM/Accounting) are filled in before setting to black.
                            var gm = (element.id.substring(0,2) == "gm") ? "ac_" + element.id.substring(3, element.id.length) : "gm_" + element.id.substring(3, element.id.length);
                            if(document.getElementById(gm) != null && document.getElementById(gm).value > 0)
                            {
                                if(document.getElementById(nm) != null)
                                {
                                    document.getElementById(nm).style.color = "black";
                                    //textPreview += document.getElementById(nm).innerHTML + " \n";
                                }
                            }
                        }
                    }
                    break;
            }
        }
        //document.getElementById("textPreview").innerHTML= textPreview;
        return rslt;

    }

    // Add commas to numerical value inputted
    function commaUpdate(input){
        // Check if user inputted data.
        if(input.value != ""){
            var val = input.value.replace(",","");
            val = parseFloat(val).toFixed(2);
            val = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            document.getElementById(input.id).value = val;
        }
    }

    // Add Commas
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //var strVal = x.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //document.getElementById(x.id).value = strVal;
    }

    // Computation Functions
    function computeTotalSales(id) {

        var id119 = (document.getElementById(id + "_119").value) != "" ? parseFloat(document.getElementById(id + "_119").value.replace(",","")): 0;
        //var id52 = (document.getElementById(id + "_52").value) != "" ? parseFloat(document.getElementById(id + "_52").value.replace(",","")): 0;
        //var id86 = (document.getElementById(id + "_86").value) != "" ? parseFloat(document.getElementById(id + "_86").value.replace(",","")): 0;
        //var id94 = (document.getElementById(id + "_94").value) != "" ? parseFloat(document.getElementById(id + "_94").value.replace(",","")): 0;
        var id134 = (document.getElementById(id + "_134").value) != "" ? parseFloat(document.getElementById(id + "_134").value.replace(",","")): 0;
        var id110 = (document.getElementById(id + "_110").value) != "" ? parseFloat(document.getElementById(id + "_110").value.replace(",","")): 0;
        var id115 = (document.getElementById(id + "_115").value) != "" ? parseFloat(document.getElementById(id + "_115").value.replace(",","")): 0;
        var id96 = (document.getElementById(id + "_96").value) != "" ? parseFloat(document.getElementById(id + "_96").value.replace(",","")): 0;

        //document.getElementById(id + "_100").value  = id119 + id134 + id110 + id115 + id96;
        //document.getElementById(id + "_100").value = parseFloat(document.getElementById(id + "_100").value).toFixed(2);
        document.getElementById(id + "_150").value = id119 + id134 + id110 + id115 + id96;
        document.getElementById(id + "_150").value = parseFloat(document.getElementById(id + "_150").value).toFixed(2);

    }

    function computeTotalSalesMTD(id) {

        var id120 = (document.getElementById(id + "_120").value) != "" ? parseFloat(document.getElementById(id + "_120").value.replace(",","")): 0;
        //var id53 = (document.getElementById(id + "_53").value) != "" ? parseFloat(document.getElementById(id + "_53").value.replace(",","")): 0;
        //var id87 = (document.getElementById(id + "_87").value) != "" ? parseFloat(document.getElementById(id + "_87").value.replace(",","")): 0;
        //var id95 = (document.getElementById(id + "_95").value) != "" ? parseFloat(document.getElementById(id + "_95").value.replace(",","")): 0;
        var id135 = (document.getElementById(id + "_135").value) != "" ? parseFloat(document.getElementById(id + "_135").value.replace(",","")): 0;
        var id111 = (document.getElementById(id + "_111").value) != "" ? parseFloat(document.getElementById(id + "_111").value.replace(",","")): 0;
        var id116 = (document.getElementById(id + "_116").value) != "" ? parseFloat(document.getElementById(id + "_116").value.replace(",","")): 0;
        var id97 = (document.getElementById(id + "_97").value) != "" ? parseFloat(document.getElementById(id + "_97").value.replace(",","")): 0;


        document.getElementById(id + "_151").value = id120 + id135 + id111 + id116 + id97;
        document.getElementById(id + "_151").value = parseFloat(document.getElementById(id + "_151").value).toFixed(2);
        //document.getElementById(id + "_103").value = id120 + id53 + id87 + id95 + id135 + id111 + id116 + id97;
        //document.getElementById(id + "_103").value = parseFloat(document.getElementById(id + "_103").value).toFixed(2);
    }

    function computeCompPercentage(id){

        //id100 = (document.getElementById(id + "_100").value) != "" ? parseFloat(document.getElementById(id + "_100").value.replace(",","")): 0;
        //id150 = (document.getElementById(id + "_150").value) != "" ? parseFloat(document.getElementById(id + "_150").value.replace(",","")): 0;
        //id36 = (document.getElementById(id + "_36").value) != "" ? parseFloat(document.getElementById(id + "_36").value.replace(",","")): 0;
        //if(id150 > 0 && id36 > 0)
        //{

        //document.getElementById(id + "_40").value  = (id36/id150) * 100;
        //document.getElementById(id + "_40").value = parseFloat(document.getElementById(id + "_40").value).toFixed(2);
        //document.getElementById(id + "_100").value = id150 - id36;
        //document.getElementById(id + "_100").value = parseFloat(document.getElementById(id + "_100").value).toFixed(2);
        //}

    }
    function computeCompPercentageMTD(id){

        //id103 = (document.getElementById(id + "_103").value) != "" ? parseFloat(document.getElementById(id + "_103").value.replace(",","")): 0;
        //id151 = (document.getElementById(id + "_151").value) != "" ? parseFloat(document.getElementById(id + "_151").value.replace(",","")): 0;
        //id37 = (document.getElementById(id + "_37").value) != "" ? parseFloat(document.getElementById(id + "_37").value.replace(",","")): 0;
        //if(id151 > 0 && id37 > 0)
        //{

        //document.getElementById(id + "_43").value  = (id37/id151) * 100;
        //document.getElementById(id + "_43").value = parseFloat(document.getElementById(id + "_43").value).toFixed(2);
        //document.getElementById(id + "_103").value = id151 - id37;
        //document.getElementById(id + "_103").value = parseFloat(document.getElementById(id + "_103").value).toFixed(2);
        //}

    }
    function computeSeTotal(id) {
        /*
         var id126 = (document.getElementById(id + "_126").value) != "" ? parseFloat(document.getElementById(id + "_126").value.replace(",","")): 0;
         var id124 = (document.getElementById(id + "_124").value) != "" ? parseFloat(document.getElementById(id + "_124").value.replace(",","")): 0;
         var id128 = (document.getElementById(id + "_128").value) != "" ? parseFloat(document.getElementById(id + "_128").value.replace(",","")): 0;
         document.getElementById(id + "_130").value  = id126 + id124 + id128;
         document.getElementById(id + "_130").value = parseFloat(document.getElementById(id + "_130").value).toFixed(2);
         */
    }
    function computeOcc(id){
        var id107 = (document.getElementById(id + "_107").value) != "" ? parseFloat(document.getElementById(id + "_107").value.replace(",","")): 0; //occupied
        var id17 = (document.getElementById(id + "_17").value) != "" ? parseFloat(document.getElementById(id + "_17").value.replace(",","")): 0; //available
        if(id107 > 0 && id17 > 0)
        {
            document.getElementById(id + "_105").value  = (id107/id17) * 100;
            document.getElementById(id + "_105").value = parseFloat(document.getElementById(id + "_105").value).toFixed(2); // occ percent
        }

    }
    function computeAdr(id){
        var id119 = (document.getElementById(id + "_119").value) != "" ? parseFloat(document.getElementById(id + "_119").value.replace(",","").replace(",","")): 0; //room revenue
        var id107 = (document.getElementById(id + "_107").value) != "" ? parseFloat(document.getElementById(id + "_107").value.replace(",","").replace(",","")): 0; //occupied
        if(id107 > 0 && id119 > 0)
        {
            document.getElementById(id + "_15").value  = (id119/id107);
            document.getElementById(id + "_15").value = parseFloat(document.getElementById(id + "_15").value).toFixed(2);
        }

    }
    function computeRevPar(id){
        var id119 = (document.getElementById(id + "_119").value) != "" ? parseFloat(document.getElementById(id + "_119").value.replace(",","").replace(",","")): 0; //room revenue
        var id17 = (document.getElementById(id + "_17").value) != "" ? parseFloat(document.getElementById(id + "_17").value.replace(",","").replace(",","")): 0; //available
        if(id119 > 0 && id17 > 0)
        {
            document.getElementById(id + "_117").value  = (id119/id17);
            document.getElementById(id + "_117").value = parseFloat(document.getElementById(id + "_117").value).toFixed(2);
        }

    }
    function computeOccMtd(id){

        var id108 = (document.getElementById(id + "_108").value) != "" ? parseFloat(document.getElementById(id + "_108").value.replace(",","")): 0; //occupied mtd
        var id19 = (document.getElementById(id + "_19").value) != "" ? parseFloat(document.getElementById(id + "_19").value.replace(",","")): 0; //available mtd
        if(id108 > 0 && id19 > 0)
        {
            document.getElementById(id + "_106").value  = (id108/id19) * 100;
            document.getElementById(id + "_106").value = parseFloat(document.getElementById(id + "_106").value).toFixed(2); // occ percent mtd
        }
    }
    function computeAdrMtd(id){
        var id120 = (document.getElementById(id + "_120").value) != "" ? parseFloat(document.getElementById(id + "_120").value.replace(",","").replace(",","")): 0; //room revenue mtd
        var id108 = (document.getElementById(id + "_108").value) != "" ? parseFloat(document.getElementById(id + "_108").value.replace(",","").replace(",","")): 0; //occupied mtd
        if(id120 > 0 && id108 > 0)
        {
            document.getElementById(id + "_16").value  = (id120/id108);
            document.getElementById(id + "_16").value = parseFloat(document.getElementById(id + "_16").value).toFixed(2); //adr mtd
        }

    }
    function computeRevParMtd(id){
        var id120 = (document.getElementById(id + "_120").value) != "" ? parseFloat(document.getElementById(id + "_120").value.replace(",","").replace(",","")): 0; //room revenue mtd
        var id19 = (document.getElementById(id + "_19").value) != "" ? parseFloat(document.getElementById(id + "_19").value.replace(",","").replace(",","")): 0; //available mtd
        if(id120 > 0 && id19 > 0)
        {
            document.getElementById(id + "_118").value  = (id120/id19);
            document.getElementById(id + "_118").value = parseFloat(document.getElementById(id + "_118").value).toFixed(2); //revpar mtd
        }
    }


    function computeBudgetTotalRevenue(id) {

        var id119 = (document.getElementById(id + "_119").value) != "" ? parseFloat(document.getElementById(id + "_119").value.replace(",","").replace(",","")): 0;
        var id134 = (document.getElementById(id + "_134").value) != "" ? parseFloat(document.getElementById(id + "_134").value.replace(",","").replace(",","")): 0;
        var id110 = (document.getElementById(id + "_110").value) != "" ? parseFloat(document.getElementById(id + "_110").value.replace(",","").replace(",","")): 0;
        var id115 = (document.getElementById(id + "_115").value) != "" ? parseFloat(document.getElementById(id + "_115").value.replace(",","").replace(",","")): 0;
        var id96 = (document.getElementById(id + "_96").value) != "" ? parseFloat(document.getElementById(id + "_96").value.replace(",","").replace(",","")): 0;


        document.getElementById(id + "_150").value = id119 + id134 + id110 + id115 + id96;
        document.getElementById(id + "_150").value = parseFloat(document.getElementById(id + "_150").value).toFixed(2);
    }


    function computeBudgetTotalRevenueMTD(id) {

        var id120 = (document.getElementById(id + "_120").value) != "" ? parseFloat(document.getElementById(id + "_120").value.replace(",","").replace(",","")): 0;
        var id135 = (document.getElementById(id + "_135").value) != "" ? parseFloat(document.getElementById(id + "_135").value.replace(",","").replace(",","")): 0;
        var id111 = (document.getElementById(id + "_111").value) != "" ? parseFloat(document.getElementById(id + "_111").value.replace(",","").replace(",","")): 0;
        var id116 = (document.getElementById(id + "_116").value) != "" ? parseFloat(document.getElementById(id + "_116").value.replace(",","").replace(",","")): 0;
        var id97 = (document.getElementById(id + "_97").value) != "" ? parseFloat(document.getElementById(id + "_97").value.replace(",","").replace(",","")): 0;


        document.getElementById(id + "_151").value = id120 + id135 + id111 + id116 + id97;
        document.getElementById(id + "_151").value = parseFloat(document.getElementById(id + "_151").value).toFixed(2);
    }


    function submitForm(txt) {
        document.form1.submit();
        alert ("Saving data. Click OK");
        document.location = txt;
    }
    function finalizeData(){
        document.getElementById("dataFinal").value = "done";
        //document.getElementById("userID").value = 1;
        // Check to see if all the required fields have input.
        var rslt = checkRequiredInput();
        if(!rslt){
            alert("Please fill in all required fields highlighted in red.");
            return false;
        }

        return true;
    }
</script>
<h4 class="headers">SBE Reporting System</h4>
<h5 class="headers">Location: <?=ucwords(urldecode($_GET['location']))?></h5>
<form method="post" action="/admin/reporting/hotel" name="form1">
    <input type="hidden" name="location" id="location" value="<?=rawurldecode($_GET['location'])?>">
    <input type="hidden" name="date" id="date" value="<?=$_GET['date']?>">
    <table border="1">
        <tr>
            <td>
                <table>
                    <tbody>
                    <tr style="background-color:lightgray;">
                        <td style="font-size:14px;">Total Revenue Sales</td>
                        <td style="font-size:14px;">Actual (Draft/GM)</td>
                        <td style="font-size:14px;"><div id="ac_hdr" style="display:none;">Actual (Accounting)</div></td>
                        <td style="font-size:14px;">Budget</td>
                        <td style="font-size:14px;">Forecast</td>
                        <td style="font-size:14px;">Last Year</td>
                    </tr>
                    <tr>
                        <!-- The controls id is a concatenation of dimVersion.fdPageControlIDMap, dimScenario.fdPageControlIDMap, dimAccount.fdPageControlIDMap in order to accommodate our db structure of each control value being a record in tblfctData-->
                        <td><div id="nm_119">Room Revenue - Daily - Hotel</div></td>
                        <!--<td><input type="number" name="gm_119" value="" id="gm_119" class="textField" size="6"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');" pattern="[0-9]+([\.|,][0-9]+)?" step="0.01" title="This should be a number with up to 2 decimal places."></td>-->
                        <td><input type="text" name="gm_119" value="" id="gm_119" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeAdr('gm');computeRevPar('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_119" value="" id="ac_119" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeAdr('ac');computeRevPar('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_119" value="" id="bd_119" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_119" value="" id="fc_119" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_119" value="" id="ly_119" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <!--
                    <tr>
                        <td><div id="nm_52">F&B Sales - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_52" value="" id="gm_52" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm'); commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_52" value="" id="ac_52" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac'); commaUpdate(this);"></td>
                        <td><input type="text" name="bd_52" value="" id="bd_52" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_52" value="" id="fc_52" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_52" value="" id="ly_52" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_86">IRD Sales $ - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_86" value="" id="gm_86" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm'); commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_86" value="" id="ac_86" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac'); commaUpdate(this);"></td>
                        <td><input type="text" name="bd_86" value="" id="bd_86" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_86" value="" id="fc_86" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_86" value="" id="ly_86" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_94">Minibar Sales - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_94" value="" id="gm_94" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm'); commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_94" value="" id="ac_94" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac'); commaUpdate(this);"></td>
                        <td><input type="text" name="bd_94" value="" id="bd_94" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_94" value="" id="fc_94" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_94" value="" id="ly_94" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    -->
                    <tr>
                        <td><div id="nm_134">Telephone Revenue - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_134" value="" id="gm_134" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_134" value="" id="ac_134" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_134" value="" id="bd_134" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_134" value="" id="fc_134" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_134" value="" id="ly_134" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_110">Parking & Transportation Revenue - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_110" value="" id="gm_110" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_110" value="" id="ac_110" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_110" value="" id="bd_110" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_110" value="" id="fc_110" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_110" value="" id="ly_110" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_115">Rentals & Other Revenue - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_115" value="" id="gm_115" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_115" value="" id="ac_115" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_115" value="" id="bd_115" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_115" value="" id="fc_115" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_115" value="" id="ly_115" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_96">Minibar $ - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_96" value="" id="gm_96" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_96" value="" id="ac_96" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_96" value="" id="bd_96" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_96" value="" id="fc_96" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_96" value="" id="ly_96" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_150">Total Revenue $ - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_150" value="" id="gm_150" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_150" value="" id="ac_150" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_150" value="" id="bd_150" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_150" value="" id="fc_150" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_150" value="" id="ly_150" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <!--
                    <tr>
                        <td><div id="nm_36">Comps $ - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_36" value="" id="gm_36" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_36" value="" id="ac_36" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_36" value="" id="bd_36" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_36" value="" id="fc_36" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_36" value="" id="ly_36" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_40">Comps % - Daily - Hotel</div></td>
                        <td>% <input type="text" name="gm_40" value="" id="gm_40" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><div id="ac_prc" style="display:none;">%  </div><input type="hidden" name="ac_40" value="" id="ac_40" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="text" name="bd_40" value="" id="bd_40" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="text" name="fc_40" value="" id="fc_40" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="text" name="ly_40" value="" id="ly_40" class="textField" size="11" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_100">Net Sales - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_100" value="" id="gm_100" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_100" value="" id="ac_100" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_100" value="" id="bd_100" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_100" value="" id="fc_100" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_100" value="" id="ly_100" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    -->
                    <tr>
                        <!-- The controls id is a concatenation of dimVersion.fdPageControlIDMap, dimScenario.fdPageControlIDMap, dimAccount.fdPageControlIDMap in order to accommodate our db structure of each control value being a record in tblfctData-->
                        <td><div id="nm_120">Room Revenue - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_120" value="" id="gm_120" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');computeAdrMtd('gm');computeRevParMtd('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_120" value="" id="ac_120" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');computeAdrMtd('ac');computeRevParMtd('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_120" value="" id="bd_120" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_120" value="" id="fc_120" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_120" value="" id="ly_120" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <!--
                    <tr>
                        <td><div id="nm_53">F&B Sales - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_53" value="" id="gm_53" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');computeCompPercentageMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_53" value="" id="ac_53" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');computeCompPercentageMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_53" value="" id="bd_53" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_53" value="" id="fc_53" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_53" value="" id="ly_53" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_87">IRD Sales $ - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_87" value="" id="gm_87" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');computeCompPercentageMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_87" value="" id="ac_87" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');computeCompPercentageMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_87" value="" id="bd_87" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_87" value="" id="fc_87" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_87" value="" id="ly_87" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_95">Minibar Sales - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_95" value="" id="gm_95" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');computeCompPercentageMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_95" value="" id="ac_95" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');computeCompPercentageMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_95" value="" id="bd_95" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_95" value="" id="fc_95" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_95" value="" id="ly_95" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    -->
                    <tr>
                        <td><div id="nm_135">Telephone Revenue - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_135" value="" id="gm_135" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_135" value="" id="ac_135" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_135" value="" id="bd_135" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_135" value="" id="fc_135" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_135" value="" id="ly_135" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_111">Parking & Transportation Revenue - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_111" value="" id="gm_111" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_111" value="" id="ac_111" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_111" value="" id="bd_111" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_111" value="" id="fc_111" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_111" value="" id="ly_111" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_116">Rentals & Other Revenue - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_116" value="" id="gm_116" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_116" value="" id="ac_116" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_116" value="" id="bd_116" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_116" value="" id="fc_116" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_116" value="" id="ly_116" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_97">Minibar $ - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_97" value="" id="gm_97" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_97" value="" id="ac_97" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_97" value="" id="bd_97" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_97" value="" id="fc_97" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_97" value="" id="ly_97" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_151">Total Revenue $ - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_151" value="" id="gm_151" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_151" value="" id="ac_151" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_151" value="" id="bd_151" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_151" value="" id="fc_151" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_151" value="" id="ly_151" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <!--
                    <tr>
                        <td><div id="nm_37">Comps $ - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_37" value="" id="gm_37" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('gm');computeCompPercentageMTD('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_37" value="" id="ac_37" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSalesMTD('ac');computeCompPercentageMTD('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_37" value="" id="bd_37" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_37" value="" id="fc_37" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_37" value="" id="ly_37" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_43">Comps % - MTD - Hotel</div></td>
                        <td>% <input type="text" name="gm_43" value="" id="gm_43" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><div id="ac_prc" style="display:none;">%  </div><input type="hidden" name="ac_43" value="" id="ac_43" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="text" name="bd_43" value="" id="bd_43" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="text" name="fc_43" value="" id="fc_43" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="text" name="ly_43" value="" id="ly_43" class="textField" size="11" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_175">Net Sales - WTD - Hotel</div></td>
                        <td><input type="text" name="gm_175" value="" id="gm_175" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_175" value="" id="ac_175" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_175" value="" id="bd_175" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_175" value="" id="fc_175" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_175" value="" id="ly_175" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_103">Net Sales - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_103" value="" id="gm_103" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_103" value="" id="ac_103" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_103" value="" id="bd_103" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_103" value="" id="fc_103" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_103" value="" id="ly_103" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    -->
                    <tr style="background-color:lightgray;">
                        <td style="font-size:14px;">Occupancy</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><div id="nm_17">Available Rooms - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_17" value="" id="gm_17" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOcc('gm');computeRevPar('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_17" value="" id="ac_17" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOcc('ac');computeRevPar('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_17" value="" id="bd_17" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_17" value="" id="fc_17" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_17" value="" id="ly_17" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>

                    </tr>
                    <tr>
                        <td><div id="nm_107">Occupied Rooms - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_107" value="" id="gm_107" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOcc('gm');computeAdr('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_107" value="" id="ac_107" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOcc('ac');computeAdr('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_107" value="" id="bd_107" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_107" value="" id="fc_107" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_107" value="" id="ly_107" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>

                    </tr>
                    <tr>
                        <td><div id="nm_105">Occ% - Daily - Hotel</div></td>
                        <td>% <input type="text" name="gm_105" value="" id="gm_105" class="textField" size="11"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="hidden" name="ac_105" value="" id="ac_105" class="textField" size="11"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_105" value="" id="bd_105" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_105" value="" id="fc_105" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_105" value="" id="ly_105" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>

                    </tr>
                    <tr>
                        <td><div id="nm_15">ADR - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_15" value="" id="gm_15" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_15" value="" id="ac_15" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_15" value="" id="bd_15" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_15" value="" id="fc_15" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_15" value="" id="ly_15" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_117">RevPar - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_117" value="" id="gm_117" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_117" value="" id="ac_117" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_117" value="" id="bd_117" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_117" value="" id="fc_117" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_117" value="" id="ly_117" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_19">Available Rooms - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_19" value="" id="gm_19" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOccMtd('gm');computeRevParMtd('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_19" value="" id="ac_19" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOccMtd('ac');computeRevParMtd('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_19" value="" id="bd_19" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_19" value="" id="fc_19" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_19" value="" id="ly_19" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_108">Occupied Rooms - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_108" value="" id="gm_108" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOccMtd('gm');computeAdrMtd('gm');commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_108" value="" id="ac_108" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeOccMtd('ac');computeAdrMtd('ac');commaUpdate(this);"></td>
                        <td><input type="text" name="bd_108" value="" id="bd_108" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_108" value="" id="fc_108" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_108" value="" id="ly_108" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>

                    </tr>
                    <tr>
                        <td><div id="nm_106">Occ% - MTD - Hotel</div></td>
                        <td>% <input type="text" name="gm_106" value="" id="gm_106" class="textField" size="11"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td>% <input type="hidden" name="ac_106" value="" id="ac_106" class="textField" size="11"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_106" value="" id="bd_106" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_106" value="" id="fc_106" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_106" value="" id="ly_106" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>

                    </tr>
                    <tr>
                        <td><div id="nm_16">ADR - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_16" value="" id="gm_16" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_16" value="" id="ac_16" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_16" value="" id="bd_16" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_16" value="" id="fc_16" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_16" value="" id="ly_16" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_118">RevPar - MTD - Hotel</div></td>
                        <td><input type="text" name="gm_118" value="" id="gm_118" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="hidden" name="ac_118" value="" id="ac_118" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="bd_118" value="" id="bd_118" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_118" value="" id="fc_118" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_118" value="" id="ly_118" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr style="background-color:lightgray;">
                        <td style="font-size:14px;">Statistics</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><div id="nm_18">Available Rooms - Full Month - Hotel</div></td>
                        <td><input type="text" name="gm_18" value="" id="gm_18" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_18" value="" id="ac_18" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_18" value="" id="bd_18" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_18" value="" id="fc_18" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_18" value="" id="ly_18" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_109">OTB - Full Month - Hotel</div></td>
                        <td><input type="text" name="gm_109" value="" id="gm_109" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_109" value="" id="ac_109" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_109" value="" id="bd_109" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_109" value="" id="fc_109" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_109" value="" id="ly_109" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_114">Remaining Available Rooms - Full Month - Hotel</div></td>
                        <td><input type="text" name="gm_114" value="" id="gm_114" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_114" value="" id="ac_114" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_114" value="" id="bd_114" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_114" value="" id="fc_114" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_114" value="" id="ly_114" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_11">3Day Pickup Current - Full Month - Hotel</div></td>
                        <td><input type="text" name="gm_11" value="" id="gm_11" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_11" value="" id="ac_11" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_11" value="" id="bd_11" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_11" value="" id="fc_11" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_11" value="" id="ly_11" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_12">3Day Pickup Next Month - Full Month - Hotel</div></td>
                        <td><input type="text" name="gm_12" value="" id="gm_12" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_12" value="" id="ac_12" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_12" value="" id="bd_12" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_12" value="" id="fc_12" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_12" value="" id="ly_12" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_13">3Day Pickup The Month After - Full Month - Hotel</div></td>
                        <td><input type="text" name="gm_13" value="" id="gm_13" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_13" value="" id="ac_13" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_13" value="" id="bd_13" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_13" value="" id="fc_13" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_13" value="" id="ly_13" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td><div id="nm_6"># of Emails Collected for CRM - Daily - Hotel</div></td>
                        <td><input type="text" name="gm_6" value="" id="gm_6" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="hidden" name="ac_6" value="" id="ac_6" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                        <td><input type="text" name="bd_6" value="" id="bd_6" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="fc_6" value="" id="fc_6" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        <td><input type="text" name="ly_6" value="" id="ly_6" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <!--<td valign="top" width="200">
                <span class="headers">Text Message Preview</span><br>
                <span style="color:#FF0000; font-size:10px">Make sure that the following preview has
                all the neccesary fields</span><hr>
                <span id="textPreview"></span>
                <input type="hidden" id="textMessage" name="theText">
            </td>-->
        </tr>
    </table>
    <table border="1">
        <tbody>
        <tr>
            <td>
                <table>
                    <tbody>
                    <tr style="background-color:lightgray;">
                        <td style="font-size:14px;">Daily Notes</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <!--
                    <tr>
                        <td>Weather - Daily - Hotel</td>
                        <td>Temperature<input type="text"  name="nt_159" value="" id="nt_159" class="textField" size="13"  onKeyPress="return handleEnter(this, event)">
                            <select name="nt_162" id="nt_162" class="textField">
                            <option value="Cold">Cold</option>
                            <option value="Mild">Mild</option>
                            <option value="Hot">Hot</option>
                            </select>
                            <select name="nt_165" id="nt_165" class="textField">
                            <option value="Clear">Clear</option>
                            <option value="Cloudy">Cloudy</option>
                            <option value="Raining/Snowing">Raining/Snowing</option>
                            </select>
                            <select name="nt_168" id="nt_168" class="textField">
                            <option value="Mild">Mild</option>
                            <option value="Low">Low</option>
                            <option value="High">High</option>
                            </select>
                        </td>
                    </tr>
                    -->
                    <tr>
                        <td>Critical Notes - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_44" value="" id="nt_44" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>Incident - Daily - Hotel</td>
                        <td><input type="checkbox" name="nt_80" id="nt_80" >Check</td>
                    </tr>
                    <tr>
                        <td>Incident Notes - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_83" value="" id="nt_83" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>Security Notes - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_121" value="" id="nt_121" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>Celebrity Notes - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_33" value="" id="nt_33" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>VIP Guests - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_154" value="" id="nt_154" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>Local Events / Shows - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_88" value="" id="nt_88" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>HR Notes - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_75" value="" id="nt_75" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>GM Names - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="gn_3" value="" id="gn_3" class="textField" size="13"  onKeyPress="return handleEnter(this, event)" disabled="disabled"><?=\Model\GmNote::getGm(urldecode($_GET['location']))?></textarea></td>
                    </tr>
                    <tr>
                        <td>General Notes - Daily - Hotel</td>
                        <td><textarea rows="3" cols="60"  name="nt_71" value="" id="nt_71" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                    </tr>
                    <tr>
                        <td>Manager - Daily - Hotel</td>
                        <td><input type="text" name="mgr" value="<?=urldecode($_GET['location'])?>" id="mgr" class="textField" size="13" style="background-color:lightgray;" readonly="readonly"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Submit Manager Report" class="button" onClick="window.onbeforeunload=null; return finalizeData();"></td>
        </tr>
        </tbody>
    </table>

</form>
<?php echo footer(); ?>
<script type="text/javascript">

    //====================================================

    // Loop through php server side code and setup javascript variables that contain budget, last year, forecast data...
    var fieldData = <?=json_encode($model->fields?:[])?>;

    //===================================================
    function loadData() {
        for(var key in fieldData){
            var value = fieldData[key];
            var $field = $("#" + key);

            $field.val(value);

            if(key == 'nt_80' && value == '1'){
                $field.attr('checked', true);
            }

            $field.trigger('change');
        }
    }

    // Display accounting fields if user has access i.e. admin/Accountant
    function showAccountingColumns(){
        var position = "GM";

        // Check if user should have access
        if(position == "Accountant" || position == "admin"){
            var element;
            // We want to loop through all controls and set to display.
            for (var i = 0; i < document.forms[0].elements.length; i++)
            {
                element = document.forms[0].elements[i];
                switch (element.type)
                {
                    case 'hidden':

                        var prefix = element.id.substring(0,2);

                        // If the field is for accounting i.e. ac then show
                        if(prefix == "ac")
                        {
                            element.type = "text";
                        }
                        break;
                }
            }

        }
    }


    /***********************************************
     * Disable "Enter" key in Form script- By Nurul Fadilah(nurul@REMOVETHISvolmedia.com)
     * This notice must stay intact for use
     * Visit http://www.dynamicdrive.com/ for full source code
     ***********************************************/

    loadData();showAccountingColumns();computeBudgetTotalRevenue('bd');computeBudgetTotalRevenueMTD('bd');computeBudgetTotalRevenue('ly');computeBudgetTotalRevenueMTD('ly');computeOcc('ly');
    computeAdr('ly');
    computeRevPar('ly');
    computeOccMtd('ly');
    computeAdrMtd('ly');
    computeRevParMtd('ly');
    computeOcc('bd');
    computeAdr('bd');
    computeRevPar('bd');
    computeOccMtd('bd');
    computeAdrMtd('bd');
    computeRevParMtd('bd');


</script>
<script src="/content/admin/js/core.js"></script>