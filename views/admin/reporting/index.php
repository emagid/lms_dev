<div class="box box-table">
    <div id="step1">
    <?php if($model->isAdmin){ ?>
    <h3>SELECT A VENUE TO CREATE REPORT</h3>
        <select id="report-venue-select" >
        <?foreach($model->locations as $location){ ?>
            <option value="<?=$location->name?>" data-type="<?=$location->type?>"><?=$location->getPrefix()?><?=$location->name?></option>
        <? } ?>
        </select>
    <? } ?>
    </div>
    <div id="step2">
        <h3>SELECT DATE TO PREVIEW REPORT</h3>
        <input id="report-date" type="text">
    </div>
    <div>
        <button class="btn btn-prime" id="create-report">CREATE REPORT</button>
    </div>
</div>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'commercial';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
    $('#report-venue-select').multiselect({
        enableFiltering: true,
        filterBehavior: 'both'
    });

    $("#report-date").datepicker({
        format: 'yyyy-mm-dd'
    });
    var today = new Date();
    today.setDate(today.getDate() - 1);
    $("#report-date").datepicker('setDate', today);

    $("#create-report").on('click', function(){
        var location = $("#report-venue-select").val();
        var locationType  = $("#report-venue-select").find("option[value='" + location + "']").data('type');
        var date = $("#report-date").val();
        window.location.href = '/admin/reporting/' + locationType.toLowerCase() + '?date=' + date + '&location=' + encodeURIComponent(location);
    });

//    $("#preview-report").on('click', function(){
//        $.post('/reporting/pdf', {data: formData}, function(res){
//
//        });
//    })
</script>

