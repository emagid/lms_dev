<style>
    input {
        width: inherit !important;
        display: inherit !important;
        text-align: right !important;
    }
</style>
<script>
    function handleEnter (field, event) {
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        if (keyCode == 13) {
            var i;
            for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                    break;
            i = (i + 1) % field.form.elements.length;
            field.form.elements[i].focus();
            return false;
        }
        else
            return true;
    }

    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /-?[0-9]*|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    function checkRequiredInput(){
        var rslt = true;
        var textPreview = "";
        var keyVal = "";
        var selectedCount = 0;
        var element;
        // We want to loop through all controls and make sure the user has inputted values for required fields.
        for (var i = 0; i < document.forms[0].elements.length; i++)
        {
            element = document.forms[0].elements[i];
            switch (element.type)
            {
                case 'text':

                    var nm = "nm_" + element.id.substring(3,element.id.length);

                    // If the field is required i.e. gm, ac, and enabled set flag and change color of header
                    if( (element.id.substring(0,2) == "gm" || element.id.substring(0,2) == "ac") && (!element.hidden) && (!element.readOnly) && (!element.disabled) && element.value.length == 0 )
                    {
                        rslt = false;
                        //document.contains(document.getElementById(nm));
                        if(document.getElementById(nm) != null)
                        {
                            document.getElementById(nm).style.color = "red";
                            //textPreview += document.getElementById(nm).innerHTML + " \n";
                        }
                    }
                    else if( (element.id.substring(0,2) == "gm" || element.id.substring(0,2) == "ac") && (!element.hidden) && (!element.readOnly) && (!element.disabled) && element.value.length > 0 )
                    {

                        // If we are only checking GM
                        if(element.id.substring(0,2) == "gm" && element.value.length > 0){
                            if(document.getElementById(nm) != null)
                            {
                                document.getElementById(nm).style.color = "black";
                                //textPreview += document.getElementById(nm).innerHTML + " \n";
                            }
                        }
                        else{
                            // If we are checking both required inputs (GM/Accounting) are filled in before setting to black.
                            var gm = (element.id.substring(0,2) == "gm") ? "ac_" + element.id.substring(3, element.id.length) : "gm_" + element.id.substring(3, element.id.length);
                            if(document.getElementById(gm) != null && document.getElementById(gm).value > 0)
                            {
                                if(document.getElementById(nm) != null)
                                {
                                    document.getElementById(nm).style.color = "black";
                                    //textPreview += document.getElementById(nm).innerHTML + " \n";
                                }
                            }
                        }
                    }
                    break;
            }
        }
        //document.getElementById("textPreview").innerHTML= textPreview;
        return rslt;

    }

    // Add commas to numerical value inputted
    function commaUpdate(input){
        // Check if user inputted data.
        if(input.value != ""){
            var val = input.value.replace(",","");
            val = parseFloat(val).toFixed(2);
            val = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            document.getElementById(input.id).value = val;
        }
    }

    // Add Commas
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //var strVal = x.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //document.getElementById(x.id).value = strVal;
    }

    // Computation Functions
    function computeTotalSales(id) {

        var id64 = (document.getElementById(id + "_64").value) != "" ? parseFloat(document.getElementById(id + "_64").value.replace(",","")): 0;
        var id20 = (document.getElementById(id + "_20").value) != "" ? parseFloat(document.getElementById(id + "_20").value.replace(",","")): 0;
        var id21 = (document.getElementById(id + "_21").value) != "" ? parseFloat(document.getElementById(id + "_21").value.replace(",","")): 0;
        var id50 = (document.getElementById(id + "_50").value) != "" ? parseFloat(document.getElementById(id + "_50").value.replace(",","")): 0;
        var id139 = (document.getElementById(id + "_139").value) != "" ? parseFloat(document.getElementById(id + "_139").value.replace(",","")): 0;
        var id51 = (document.getElementById(id + "_51").value) != "" ? parseFloat(document.getElementById(id + "_51").value.replace(",","")): 0;
        var id98 = (document.getElementById(id + "_98").value) != "" ? parseFloat(document.getElementById(id + "_98").value.replace(",","")): 0;
        var id38 = (document.getElementById(id + "_38").value) != "" ? parseFloat(document.getElementById(id + "_38").value.replace(",","")): 0;
        /**
         * Special events, for gm and ly for now
         */
        var id126 = 0;
        var id124 = 0;
        var id128 = 0;

        if(id == 'gm' || id == 'ly'){
            id126 = (document.getElementById(id + "_126").value) != "" ? parseFloat(document.getElementById(id + "_126").value.replace(",","")): 0;
            id124 = (document.getElementById(id + "_124").value) != "" ? parseFloat(document.getElementById(id + "_124").value.replace(",","")): 0;
            id128 = (document.getElementById(id + "_128").value) != "" ? parseFloat(document.getElementById(id + "_128").value.replace(",","")): 0;
        }

        document.getElementById(id + "_152").value  = id64 + id20 + id21 + id50 + id139 + id51 + id98 + id126 + id124 + id128;
        document.getElementById(id + "_152").value = parseFloat(document.getElementById(id + "_152").value).toFixed(2);
        document.getElementById(id + "_101").value = id64 + id20 + id21 + id50 + id139 + id51 + id98 - id38 + id126 + id124 + id128;
        document.getElementById(id + "_101").value = parseFloat(document.getElementById(id + "_101").value).toFixed(2);
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        if(id64 > 0 && id20 > 0 && id21 > 0 && id50 > 0 && id139 > 0 && id51 > 0 && id98 > 0 && id152 > 0){
            var food = parseFloat((id64/id152) * 100).toFixed(0);
            var bev = parseFloat(((id21 + id20)/id152) * 100).toFixed(0);
            var misc = parseFloat(((id50 + id139 + id51 + id98)/id152) * 100).toFixed(0);
            document.getElementById(id + "_68").value = food.toString() + "/" + bev.toString() + "/" + misc.toString();
        }
        if(id64 > 0 && id20 > 0 && id21 > 0 && id152 > 0){
            var food = parseFloat((id64/id152) * 100).toFixed(0);
            var bev = parseFloat(((id21 + id20)/id152) * 100).toFixed(0);
            document.getElementById(id + "_66").value = food.toString() + "/" + bev.toString();

        }
    }

    function computeTotalSalesBudget(id) {

        var id64 = (document.getElementById(id + "_64").value) != "" ? parseFloat(document.getElementById(id + "_64").value.replace(",","")): 0;
        var id176 = (document.getElementById(id + "_176").value) != "" ? parseFloat(document.getElementById(id + "_176").value.replace(",","")): 0;
        var id50 = (document.getElementById(id + "_50").value) != "" ? parseFloat(document.getElementById(id + "_50").value.replace(",","")): 0;
        var id139 = (document.getElementById(id + "_139").value) != "" ? parseFloat(document.getElementById(id + "_139").value.replace(",","")): 0;
        var id51 = (document.getElementById(id + "_51").value) != "" ? parseFloat(document.getElementById(id + "_51").value.replace(",","")): 0;
        var id98 = (document.getElementById(id + "_98").value) != "" ? parseFloat(document.getElementById(id + "_98").value.replace(",","")): 0;
        var id38 = (document.getElementById(id + "_38").value) != "" ? parseFloat(document.getElementById(id + "_38").value.replace(",","")): 0;
        document.getElementById(id + "_152").value  = id64 + id176 + id50 + id139 + id51 + id98;
        document.getElementById(id + "_152").value = parseFloat(document.getElementById(id + "_152").value).toFixed(2);
        document.getElementById(id + "_101").value = id64 + id176 + id50 + id139 + id51 + id98 - id38;
        document.getElementById(id + "_101").value = parseFloat(document.getElementById(id + "_101").value).toFixed(2);
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;

    }

    function computeTotalBev(id){

        var id20 = (document.getElementById(id + "_20").value) != "" ? parseFloat(document.getElementById(id + "_20").value.replace(",","")): 0;
        var id21 = (document.getElementById(id + "_21").value) != "" ? parseFloat(document.getElementById(id + "_21").value.replace(",","")): 0;

        document.getElementById(id + "_176").value  = id20 + id21;
        document.getElementById(id + "_176").value = parseFloat(document.getElementById(id + "_176").value).toFixed(2);


    }

    function computeCompPercentage(id){

        var id101 = (document.getElementById(id + "_101").value) != "" ? parseFloat(document.getElementById(id + "_101").value.replace(",","")): 0;
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        var id38 = (document.getElementById(id + "_38").value) != "" ? parseFloat(document.getElementById(id + "_38").value.replace(",","")): 0;
        if(id152 > 0 && id38 > 0)
        {
            //document.getElementById(id + "_41").value  = Math.round((id38/id152) * 100);
            document.getElementById(id + "_41").value  = (id38/id152) * 100;
            document.getElementById(id + "_41").value = parseFloat(document.getElementById(id + "_41").value).toFixed(2);
            //document.getElementById(id + "_101").value = id101 - id38;
            //document.getElementById(id + "_101").value = parseFloat(document.getElementById(id + "_101").value).toFixed(2);
        }
    }
    function computeSeTotal(id) {
        if(document.getElementById(id + "_130").value && (id != 'gm' && id != 'ly')){
            return ;
        }

        var id126 = (document.getElementById(id + "_126").value) != "" ? parseFloat(document.getElementById(id + "_126").value.replace(",","")): 0;
        var id124 = (document.getElementById(id + "_124").value) != "" ? parseFloat(document.getElementById(id + "_124").value.replace(",","")): 0;
        var id128 = (document.getElementById(id + "_128").value) != "" ? parseFloat(document.getElementById(id + "_128").value.replace(",","")): 0;
        document.getElementById(id + "_130").value  = id126 + id124 + id128;
        document.getElementById(id + "_130").value = parseFloat(document.getElementById(id + "_130").value).toFixed(2);
    }

    function computeFOHPercentage(id){
        var id140 = (document.getElementById(id + "_140").value) != "" ? parseFloat(document.getElementById(id + "_140").value.replace(",","")): 0;
        var id60 = (document.getElementById(id + "_60").value) != "" ? parseFloat(document.getElementById(id + "_60").value.replace(",","")): 0;
        if(id140 > 0 && id60 > 0){
            document.getElementById(id + "_58").value  = (id60/id140) * 100;
            document.getElementById(id + "_58").value = parseFloat(document.getElementById(id + "_58").value).toFixed(2);
        }
    }

    function computeBOHPercentage(id){
        var id140 = (document.getElementById(id + "_140").value) != "" ? parseFloat(document.getElementById(id + "_140").value.replace(",","")): 0;
        var id29 = (document.getElementById(id + "_29").value) != "" ? parseFloat(document.getElementById(id + "_29").value.replace(",","")): 0;
        if(id140 > 0 && id29 > 0){
            document.getElementById(id + "_27").value  = (id29/id140) * 100;
            document.getElementById(id + "_27").value = parseFloat(document.getElementById(id + "_27").value).toFixed(2);
        }
    }

    function computeTotalHours(id){
        var id62 = (document.getElementById(id + "_62").value) != "" ? parseFloat(document.getElementById(id + "_62").value.replace(",","")): 0;
        var id31 = (document.getElementById(id + "_31").value) != "" ? parseFloat(document.getElementById(id + "_31").value.replace(",","")): 0;
        document.getElementById(id + "_142").value  = id62 + id31;
        document.getElementById(id + "_142").value = parseFloat(document.getElementById(id + "_142").value).toFixed(2);

    }

    function computeTotalCost(id){
        var id60 = (document.getElementById(id + "_60").value) != "" ? parseFloat(document.getElementById(id + "_60").value.replace(",","")): 0;
        var id29 = (document.getElementById(id + "_29").value) != "" ? parseFloat(document.getElementById(id + "_29").value.replace(",","")): 0;
        if(id == 'bd' && document.getElementById(id + "_140").value != '' && document.getElementById(id + "_140").value != '0'){
            return;
        }
        document.getElementById(id + "_140").value  = id60 + id29;
        document.getElementById(id + "_140").value = parseFloat(document.getElementById(id + "_140").value).toFixed(2);

    }

    function computeTotalLaborPercentage(id){
        var id140 = (document.getElementById(id + "_140").value) != "" ? parseFloat(document.getElementById(id + "_140").value.replace(",","")): 0;
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        if(id152 > 0 && id140 > 0){
            document.getElementById(id + "_148").value  = (id140/id152) * 100;
            document.getElementById(id + "_148").value = parseFloat(document.getElementById(id + "_148").value).toFixed(2);
        }

    }

    function computeFOHCover(id){
        var id60 = (document.getElementById(id + "_60").value) != "" ? parseFloat(document.getElementById(id + "_60").value.replace(",","")): 0;
        var id4 = (document.getElementById(id + "_4").value) != "" ? parseFloat(document.getElementById(id + "_4").value.replace(",","")): 0;
        if(id60 > 0 && id4 > 0){
            document.getElementById(id + "_54").value  = (id60/id4) * 100;
            document.getElementById(id + "_54").value = parseFloat(document.getElementById(id + "_54").value).toFixed(2);
        }
    }

    function computeBOHCover(id){
        var id29 = (document.getElementById(id + "_29").value) != "" ? parseFloat(document.getElementById(id + "_29").value.replace(",","")): 0;
        var id4 = (document.getElementById(id + "_4").value) != "" ? parseFloat(document.getElementById(id + "_4").value.replace(",","")): 0;
        if(id29 > 0 && id4 > 0){
            document.getElementById(id + "_23").value  = (id29/id4) * 100;
            document.getElementById(id + "_23").value = parseFloat(document.getElementById(id + "_23").value).toFixed(2);
        }
    }

    function computeTotalLaborCover(id){
        var id140 = (document.getElementById(id + "_140").value) != "" ? parseFloat(document.getElementById(id + "_140").value.replace(",","")): 0;
        var id4 = (document.getElementById(id + "_4").value) != "" ? parseFloat(document.getElementById(id + "_4").value.replace(",","")): 0;
        if(id140 > 0 && id4 > 0){
            document.getElementById(id + "_144").value  = (id140/id4) * 100;
            document.getElementById(id + "_144").value = parseFloat(document.getElementById(id + "_144").value).toFixed(2);
        }
    }

    function computeFOHSales(id){
        var id60 = (document.getElementById(id + "_60").value) != "" ? parseFloat(document.getElementById(id + "_60").value.replace(",","")): 0;
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        if(id60 > 0 && id152 > 0){
            document.getElementById(id + "_56").value  = (id60/id152) * 100;
            document.getElementById(id + "_56").value = parseFloat(document.getElementById(id + "_56").value).toFixed(2);
        }
    }

    function computeBOHSales(id){
        var id29 = (document.getElementById(id + "_29").value) != "" ? parseFloat(document.getElementById(id + "_29").value.replace(",","")): 0;
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        if(id29 > 0 && id152 > 0){
            document.getElementById(id + "_25").value  = (id29/id152) * 100;
            document.getElementById(id + "_25").value = parseFloat(document.getElementById(id + "_25").value).toFixed(2);
        }
    }

    function computeTotalLaborSales(id){
        var id140 = (document.getElementById(id + "_140").value) != "" ? parseFloat(document.getElementById(id + "_140").value.replace(",","")): 0;
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        if(id140 > 0 && id152 > 0){
            document.getElementById(id + "_146").value  = (id140/id152) * 100;
            document.getElementById(id + "_146").value = parseFloat(document.getElementById(id + "_146").value).toFixed(2);
        }
    }

    function computeDJEntertainment(id){
        var id48 = (document.getElementById(id + "_48").value) != "" ? parseFloat(document.getElementById(id + "_48").value.replace(",","")): 0;
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        if(id48 > 0 && id152 > 0){
            document.getElementById(id + "_49").value  = (id48/id152) * 100;
            document.getElementById(id + "_49").value = parseFloat(document.getElementById(id + "_49").value).toFixed(2);
        }
    }

    function computeSpendPerPerson(id){
        //Net Sales - Daily - Nightlife/# of Covers - Daily - Nightlife (102 / 5)

        var id101 = (document.getElementById(id + "_101").value) != "" ? parseFloat(document.getElementById(id + "_101").value.replace(",","")): 0;
        var id4 = (document.getElementById(id + "_4").value) != "" ? parseFloat(document.getElementById(id + "_4").value.replace(",","")): 0;
        if(id101 > 0 && id4 > 0){
            document.getElementById(id + "_177").value  = (id101/id4);
            document.getElementById(id + "_177").value = parseFloat(document.getElementById(id + "_177").value).toFixed(2);
        }
    }
    function computePromoterExpense(id){
        //Promoter % Expense - Daily - Nightlife = (Promoter $ Expense - Daily -Nightlife/Total Sales $ - Daily - Nightlife)

        var id178 = (document.getElementById(id + "_178").value) != "" ? parseFloat(document.getElementById(id + "_178").value.replace(",","")): 0;
        var id152 = (document.getElementById(id + "_152").value) != "" ? parseFloat(document.getElementById(id + "_152").value.replace(",","")): 0;
        if(id178 > 0 && id152 > 0){
            document.getElementById(id + "_112").value  = (id178/id152) * 100;
            document.getElementById(id + "_112").value = parseFloat(document.getElementById(id + "_112").value).toFixed(2);
        }
    }

    function submitForm(txt) {
        document.form1.submit();
        alert ("Saving data. Click OK");
        document.location = txt;
    }
    function finalizeData(){
        document.getElementById("dataFinal").value = "done";
        //document.getElementById("userID").value = 1;
        // Check to see if all the required fields have input.
        var rslt = checkRequiredInput();
        if(!rslt){
            alert("Please fill in all required fields highlighted in red.");
            return false;
        }

        return true;
    }
</script>
<div class="row">
    <h4 class="headers">SBE Reporting System</h4>
    <h5 class="headers">Location: <?=ucwords(urldecode($_GET['location']))?></h5>
    <hr>
    <form method="post" action="/admin/reporting/nightlife" name="form1">
        <input type="hidden" name="location" id="location" value="<?=rawurldecode($_GET['location'])?>">
        <input type="hidden" name="date" id="date" value="<?=$_GET['date']?>">
        <table border="1">
            <tr>
                <td>
                    <table>
                        <tbody>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Total Daily Sales</td>
                            <td style="font-size:14px;">Actual (Draft/GM)</td>
                            <td style="font-size:14px;"><div id="ac_hdr" style="display:none;">Actual (Accounting)</div></td>
                            <td style="font-size:14px;">Budget</td>
                            <td style="font-size:14px;">Forecast</td>
                            <td style="font-size:14px;">Last Year</td>
                        </tr>
                        <tr>
                            <!-- The controls id is a concatenation of dimVersion.fdPageControlIDMap, dimScenario.fdPageControlIDMap, dimAccount.fdPageControlIDMap in order to accommodate our db structure of each control value being a record in tblfctData-->
                            <td><div id="nm_64">Food $ - Daily - Nightlife</div></td>
                            <!--<td><input type="number" name="gm_64" value="" id="gm_64" class="textField" size="6"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');" pattern="[0-9]+([\.|,][0-9]+)?" step="0.01" title="This should be a number with up to 2 decimal places."></td>-->
                            <td><input type="text" name="gm_64" value="" id="gm_64" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_64" value="" id="ac_64" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_64" value="" id="bd_64" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_64" value="" id="fc_64" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_64" value="" id="ly_64" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_20">Beverage Bar $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_20" value="" id="gm_20" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeTotalBev('gm');computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_20" value="" id="ac_20" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeTotalBev('ac');computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_20" value="" id="bd_20" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_20" value="" id="fc_20" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_20" value="" id="ly_20" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_21">Beverage Table $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_21" value="" id="gm_21" class="textField" size="13"  onKeyPress="validate(event);" readonly="readonly" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeTotalBev('gm');computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_21" value="" id="ac_21" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeTotalBev('ac');computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_21" value="" id="bd_21" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_21" value="" id="fc_21" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_21" value="" id="ly_21" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_176">Total Beverage $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_176" value="" id="gm_176" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_176" value="" id="ac_176" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_176" value="" id="bd_176" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_176" value="" id="fc_176" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_176" value="" id="ly_176" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_50">Door $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_50" value="" id="gm_50" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm'); computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_50" value="" id="ac_50" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac'); computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_50" value="" id="bd_50" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_50" value="" id="fc_50" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_50" value="" id="ly_50" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_139">Tickets $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_139" value="" id="gm_139" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_139" value="" id="ac_139" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_139" value="" id="bd_139" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_139" value="" id="fc_139" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_139" value="" id="ly_139" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_51">Entertainment Fee $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_51" value="" id="gm_51" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm'); computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_51" value="" id="ac_51" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_51" value="" id="bd_51" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_51" value="" id="fc_51" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_51" value="" id="ly_51" class="textField" size="13"  onKeyPress="validate(event);" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_98">Miscellaneous $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_98" value="" id="gm_98" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm');computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_98" value="" id="ac_98" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac');computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_98" value="" id="bd_98" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_98" value="" id="fc_98" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_98" value="" id="ly_98" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_152">Total Sales $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_152" value="" id="gm_152" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_152" value="" id="ac_152" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_152" value="" id="bd_152" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_152" value="" id="fc_152" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_152" value="" id="ly_152" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_38">Comps $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_38" value="" id="gm_38" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('gm');computeCompPercentage('gm'); computeFOHPercentage('gm'); computeBOHPercentage('gm');computeTotalLaborPercentage('gm');computeSpendPerPerson('gm');computePromoterExpense('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_38" value="" id="ac_38" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalSales('ac');computeCompPercentage('ac'); computeFOHPercentage('ac'); computeBOHPercentage('ac');computeTotalLaborPercentage('ac');computeSpendPerPerson('ac');computePromoterExpense('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_38" value="" id="bd_38" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_38" value="" id="fc_38" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_38" value="" id="ly_38" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_41">Comps % - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_41" value="" id="gm_41" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <div id="ac_prc" style="display:none;">%  </div><input type="hidden" name="ac_41" value="" id="ac_41" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_41" value="" id="bd_41" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_41" value="" id="fc_41" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_41" value="" id="ly_41" class="textField" size="11" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_101">Net Sales - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_101" value="" id="gm_101" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_101" value="" id="ac_101" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_101" value="" id="bd_101" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_101" value="" id="fc_101" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_101" value="" id="ly_101" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_68">Food/Beverage/Other Mix - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_68" value="" id="gm_68" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_68" value="" id="ac_68" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_68" value="" id="bd_68" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_68" value="" id="fc_68" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_68" value="" id="ly_68" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_173">Total Sales - WTD - Nightlife</div></td>
                            <td><input type="text" name="gm_173" value="" id="gm_173" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_173" value="" id="ac_173" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_173" value="" id="bd_173" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_173" value="" id="fc_173" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_173" value="" id="ly_173" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_174">Total Sales - MTD - Nightlife</div></td>
                            <td><input type="text" name="gm_174" value="" id="gm_174" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_174" value="" id="ac_174" class="textField" size="13" onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_174" value="" id="bd_174" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_174" value="" id="fc_174" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_174" value="" id="ly_174" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Special Events</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div id="nm_126">Special Events - Food - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_126" value="" id="gm_126" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_126" value="" id="ac_126" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ac');commaUpdate(this);"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_126" value="" id="bd_126" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_126" value="" id="ly_126" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_124">Special Events - Drink - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_124" value="" id="gm_124" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_124" value="" id="ac_124" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ac');commaUpdate(this);"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_124" value="" id="bd_124" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_124" value="" id="ly_124" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_128">Special Events - Misc. - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_128" value="" id="gm_128" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_128" value="" id="ac_128" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ac');commaUpdate(this);"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_128" value="" id="bd_128" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_128" value="" id="ly_128" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_130">Special Events - Total - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_130" value="" id="gm_130" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_130" value="" id="ac_130" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="bd_130" value="" id="bd_130" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('bd');commaUpdate(this);"></td>
                            <td></td>
                            <td><input type="text" style="background-color:lightgray;"  readonly="readonly" name="ly_130" value="" id="ly_130" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSeTotal('ly');commaUpdate(this);"></td>
                        </tr>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Statistics</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div id="nm_4"># of Covers - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_4" value="" id="gm_4" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSpendPerPerson('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_4" value="" id="ac_4" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeSpendPerPerson('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_4" value="" id="bd_4" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_4" value="" id="fc_4" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_4" value="" id="ly_4" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_177">Spend Per Person - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_177" value="" id="gm_177" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_177" value="" id="ac_177" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_177" value="" id="bd_177" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_177" value="" id="fc_177" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_177" value="" id="ly_177" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_2"># of Checks - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_2" value="" id="gm_2" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_2" value="" id="ac_2" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_2" value="" id="bd_2" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_2" value="" id="fc_2" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_2" value="" id="ly_2" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_1"># of Bottles - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_1" value="" id="gm_1" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_1" value="" id="ac_1" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_1" value="" id="bd_1" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_1" value="" id="fc_1" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_1" value="" id="ly_1" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_132">Table Reservations # - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_132" value="" id="gm_132" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_132" value="" id="ac_132" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_132" value="" id="bd_132" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_132" value="" id="fc_132" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_132" value="" id="ly_132" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_133">Table Reservations $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_133" value="" id="gm_133" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_133" value="" id="ac_133" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_133" value="" id="bd_133" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_133" value="" id="fc_133" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_133" value="" id="ly_133" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_10">% of Males - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_10" value="" id="gm_10" class="textField" size="11"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td>% <input type="hidden" name="ac_10" value="" id="ac_10" class="textField" size="11"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td>% <input type="text" name="bd_10" value="" id="bd_10" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_10" value="" id="fc_10" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_10" value="" id="ly_10" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_9">% of Females - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_9" value="" id="gm_9" class="textField" size="11"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td>% <input type="hidden" name="ac_9" value="" id="ac_9" class="textField" size="11"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td>% <input type="text" name="bd_9" value="" id="bd_9" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_9" value="" id="fc_9" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_9" value="" id="ly_9" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_136">Third Party Tables # - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_136" value="" id="gm_136" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_136" value="" id="ac_136" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_136" value="" id="bd_136" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_136" value="" id="fc_136" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_136" value="" id="ly_136" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_137">Third Party Tables $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_137" value="" id="gm_137" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_137" value="" id="ac_137" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_137" value="" id="bd_137" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_137" value="" id="fc_137" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_137" value="" id="ly_137" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_78">In house Tables # - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_78" value="" id="gm_78" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_78" value="" id="ac_78" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_78" value="" id="bd_78" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_78" value="" id="fc_78" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_78" value="" id="ly_78" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_79">In house Tables $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_79" value="" id="gm_79" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_79" value="" id="ac_79" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_79" value="" id="bd_79" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_79" value="" id="fc_79" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_79" value="" id="ly_79" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_157">Walkup Tables # - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_157" value="" id="gm_157" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_157" value="" id="ac_157" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_157" value="" id="bd_157" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_157" value="" id="fc_157" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_157" value="" id="ly_157" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_158">Walkup Tables $ - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_158" value="" id="gm_158" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_158" value="" id="ac_158" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_158" value="" id="bd_158" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_158" value="" id="fc_158" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_158" value="" id="ly_158" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_138">Ticket Sales # - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_138" value="" id="gm_138" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_138" value="" id="ac_138" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_138" value="" id="bd_138" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_138" value="" id="fc_138" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_138" value="" id="ly_138" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_7"># of Emails Collected for CRM - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_7" value="" id="gm_7" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_7" value="" id="ac_7" class="textField" size="13"  onKeyPress="validate(event);" onchange="commaUpdate(this);"></td>
                            <td><input type="text" name="bd_7" value="" id="bd_7" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_7" value="" id="fc_7" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_7" value="" id="ly_7" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_66">Food / Bev Mix - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_66" value="" id="gm_66" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_66" value="" id="ac_66" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_66" value="" id="bd_66" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_66" value="" id="fc_66" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_66" value="" id="ly_66" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_54">FOH $ / Cover - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_54" value="" id="gm_54" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_54" value="" id="ac_54" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_54" value="" id="bd_54" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_54" value="" id="fc_54" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_54" value="" id="ly_54" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_23">BOH $ / Cover - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_23" value="" id="gm_23" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_23" value="" id="ac_23" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_23" value="" id="bd_23" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_23" value="" id="fc_23" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_23" value="" id="ly_23" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_144">Total Labor $ / Cover - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_144" value="" id="gm_144" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_144" value="" id="ac_144" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_144" value="" id="bd_144" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_144" value="" id="fc_144" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_144" value="" id="ly_144" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_56">FOH $ / Sales - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_56" value="" id="gm_56" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_56" value="" id="ac_56" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_56" value="" id="bd_56" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_56" value="" id="fc_56" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_56" value="" id="ly_56" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_25">BOH $ / Sales - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_25" value="" id="gm_25" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_25" value="" id="ac_25" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_25" value="" id="bd_25" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_25" value="" id="fc_25" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_25" value="" id="ly_25" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_146">Total Labor $ / Sales $ - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_146" value="" id="gm_146" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_146" value="" id="ac_146" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_146" value="" id="bd_146" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_146" value="" id="fc_146" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_146" value="" id="ly_146" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Labor</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div id="nm_62">FOH Hours - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_62" value="" id="gm_62" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_62" value="" id="ac_62" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_62" value="" id="bd_62" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_62" value="" id="fc_62" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_62" value="" id="ly_62" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_60">FOH Cost - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_60" value="" id="gm_60" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeFOHPercentage('gm');computeTotalCost('gm');computeTotalLaborPercentage('gm');computeFOHCover('gm');computeTotalLaborCover('gm');computeFOHSales('gm');computeTotalLaborSales('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_60" value="" id="ac_60" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeFOHPercentage('ac');computeTotalCost('ac');computeTotalLaborPercentage('ac');computeFOHCover('ac');computeTotalLaborCover('ac');computeFOHSales('ac');computeTotalLaborSales('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_60" value="" id="bd_60" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_60" value="" id="fc_60" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_60" value="" id="ly_60" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_58">FOH % - Daily - Nightlife</td>
                            <td>% <input type="text" name="gm_58" value="" id="gm_58" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_58" value="" id="ac_58" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_58" value="" id="bd_58" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_58" value="" id="fc_58" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_58" value="" id="ly_58" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_31">BOH Hours - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_31" value="" id="gm_31" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_31" value="" id="ac_31" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeTotalHours('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_31" value="" id="bd_31" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_31" value="" id="fc_31" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_31" value="" id="ly_31" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_29">BOH Cost - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_29" value="" id="gm_29" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeBOHPercentage('gm');computeTotalCost('gm');computeTotalLaborPercentage('gm');computeBOHCover('gm');computeTotalLaborCover('gm');computeBOHSales('gm');computeTotalLaborSales('gm');commaUpdate(this);"></td>
                            <td><input type="hidden" name="ac_29" value="" id="ac_29" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeBOHPercentage('ac');computeTotalCost('ac');computeTotalLaborPercentage('ac');computeBOHCover('ac');computeTotalLaborCover('ac');computeBOHSales('ac');computeTotalLaborSales('ac');commaUpdate(this);"></td>
                            <td><input type="text" name="bd_29" value="" id="bd_29" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_29" value="" id="fc_29" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_29" value="" id="ly_29" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_27">BOH % - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_27" value="" id="gm_27" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_27" value="" id="ac_27" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_27" value="" id="bd_27" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_27" value="" id="fc_27" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_27" value="" id="ly_27" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_142">Total Hours - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_142" value="" id="gm_142" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_142" value="" id="ac_142" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_142" value="" id="bd_142" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_142" value="" id="fc_142" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_142" value="" id="ly_142" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_140">Total Cost - Daily - Nightlife</div></td>
                            <td><input type="text" name="gm_140" value="" id="gm_140" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="hidden" name="ac_140" value="" id="ac_140" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="bd_140" value="" id="bd_140" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="fc_140" value="" id="fc_140" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td><input type="text" name="ly_140" value="" id="ly_140" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_148">Total Labor % - Daily - Nightlife</div></td>
                            <td>% <input type="text" name="gm_148" value="" id="gm_148" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="hidden" name="ac_148" value="" id="ac_148" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="bd_148" value="" id="bd_148" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="fc_148" value="" id="fc_148" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                            <td>% <input type="text" name="ly_148" value="" id="ly_148" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <!--<td valign="top" width="200">
                    <span class="headers">Text Message Preview</span><br>
                    <span style="color:#FF0000; font-size:10px">Make sure that the following preview has
                    all the neccesary fields</span><hr>
                    <span id="textPreview"></span>
                    <input type="hidden" id="textMessage" name="theText">
                </td>-->
            </tr>
        </table>
        <table border="1">
            <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Promotions</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div id="nm_113">Promotion Name - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="85"  name="nt_113" value="" id="nt_113" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td><div id="nm_47">DJ - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="85"  name="nt_47" value="" id="nt_47" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td><div id="nm_48">DJ/Entertainment Expenses $ - Daily - Nightlife</td>
                            <td><input type="text" name="gm_48" value="" id="gm_48" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeDJEntertainment('gm');commaUpdate(this);">
                                <input type="hidden" name="ac_48" value="" id="ac_48" class="textField" size="13"  onKeyPress="validate(event);" onchange="computeDJEntertainment('ac');commaUpdate(this);">
                                <input type="text" name="bd_48" value="" id="bd_48" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly">
                                <input type="text" name="fc_48" value="" id="fc_48" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly">
                                <input type="text" name="ly_48" value="" id="ly_48" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_49">DJ/Entertainment Expenses % - Daily - Nightlife</td>
                            <td>% <input type="text" name="gm_49" value="" id="gm_49" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="hidden" name="ac_49" value="" id="ac_49" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="text" name="bd_49" value="" id="bd_49" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="text" name="fc_49" value="" id="fc_49" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="text" name="ly_49" value="" id="ly_49" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_178">Promoter $ Expense - Daily - Nightlife</td>
                            <td><input type="text" name="gm_178" value="" id="gm_178" class="textField" size="13"  onKeyPress="validate(event);" onchange="computePromoterExpense('gm');commaUpdate(this);">
                                <input type="hidden" name="ac_178" value="" id="ac_178" class="textField" size="13"  onKeyPress="validate(event);" onchange="computePromoterExpense('ac');commaUpdate(this);">
                                <input type="text" name="bd_178" value="" id="bd_178" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly">
                                <input type="text" name="fc_178" value="" id="fc_178" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly">
                                <input type="text" name="ly_178" value="" id="ly_178" class="textField" size="13"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td><div id="nm_112">Promoter % Expense - Daily - Nightlife</td>
                            <td>% <input type="text" name="gm_112" value="" id="gm_112" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="hidden" name="ac_112" value="" id="ac_112" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="text" name="bd_112" value="" id="bd_112" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="text" name="fc_112" value="" id="fc_112" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly">
                                % <input type="text" name="ly_112" value="" id="ly_112" class="textField" size="11"  style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        <tr style="background-color:lightgray;">
                            <td style="font-size:14px;">Daily Notes</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Weather - Daily - Nightlife</td>
                            <td>Temperature<input type="text"  name="nt_160" value="" id="nt_160" class="textField" size="13"  onKeyPress="return handleEnter(this, event)">
                                <select name="nt_163" id="nt_163" class="textField">
                                    <option value="Cold">Cold</option>
                                    <option value="Mild">Mild</option>
                                    <option value="Hot">Hot</option>
                                </select>
                                <select name="nt_166" id="nt_166" class="textField">
                                    <option value="Clear">Clear</option>
                                    <option value="Cloudy">Cloudy</option>
                                    <option value="Raining/Snowing">Raining/Snowing</option>
                                </select>
                                <select name="nt_169" id="nt_169" class="textField">
                                    <option value="Mild">Mild</option>
                                    <option value="Low">Low</option>
                                    <option value="High">High</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Critical Notes - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_45" value="" id="nt_45" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Incident - Daily - Nightlife</td>
                            <td><input type="checkbox" name="nt_81" id="nt_81" >Check</td>
                        </tr>
                        <tr>
                            <td>Incident Notes - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_84" value="" id="nt_84" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Security Notes - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_122" value="" id="nt_122" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Celebrity Notes - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_34" value="" id="nt_34" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>VIP Guests - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_155" value="" id="nt_155" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Local Events / Shows - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_89" value="" id="nt_89" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>HR Notes - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_76" value="" id="nt_76" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>GM Names - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="gn_1" value="" id="gn_1" class="textField" size="13"  onKeyPress="return handleEnter(this, event)" disabled="disabled"><?=\Model\GmNote::getGm(urldecode($_GET['location']))?></textarea></td>
                        </tr>
                        <tr>
                            <td>General Notes - Daily - Nightlife</td>
                            <td><textarea rows="3" cols="60"  name="nt_72" value="" id="nt_72" class="textField" size="13"  onKeyPress="return handleEnter(this, event)"></textarea></td>
                        </tr>
                        <tr>
                            <td>Manager - Daily - Nightlife</td>
                            <td><input type="text" name="mgr" value="<?=urldecode($_GET['location'])?>" id="mgr" class="textField" size="13" style="background-color:lightgray;" readonly="readonly"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Submit Manager Report" class="button" onClick="window.onbeforeunload=null; return finalizeData();"></td>
            </tr>
            </tbody>
        </table>

    </form>
</div>
<?php echo footer(); ?>
<script type="text/javascript">

    //====================================================

    var fieldData = <?=json_encode($model->fields?:[])?>;

    //===================================================
    function loadData() {
        for(var key in fieldData){
            var value = fieldData[key];

            if(key == 'gm_21'){
                value = 0;
            }
            var $field = $("#" + key);

            $field.val(value);

            if(key == 'nt_81' && value == '1'){
                $field.attr('checked', true);
            }

            $field.trigger('change');
        }
    }

    // Display accounting fields if user has access i.e. admin/Accountant
    function showAccountingColumns(){
        var position = "GM";

        // Check if user should have access
        if(position == "Accountant" || position == "admin"){
            var element;
            // We want to loop through all controls and set to display.
            for (var i = 0; i < document.forms[0].elements.length; i++)
            {
                element = document.forms[0].elements[i];
                switch (element.type)
                {
                    case 'hidden':

                        var prefix = element.id.substring(0,2);

                        // If the field is for accounting i.e. ac then show
                        if(prefix == "ac")
                        {
                            element.type = "text";
                        }
                        break;
                }
            }

        }
    }


    /***********************************************
     * Disable "Enter" key in Form script- By Nurul Fadilah(nurul@REMOVETHISvolmedia.com)
     * This notice must stay intact for use
     * Visit http://www.dynamicdrive.com/ for full source code
     ***********************************************/


    loadData();showAccountingColumns();computeTotalSales('ac');
    computeTotalSales('bd');computeTotalBev('gm');computeTotalBev('bd');
    computeTotalBev('ac');computeTotalSalesBudget('bd');computeDJEntertainment('bd');computeBOHSales('bd');
    computeFOHSales('bd');computeFOHCover('bd');computeBOHCover('bd');
    computeTotalCost('bd');computeTotalLaborSales('bd');
    computeTotalLaborCover('bd');computeTotalHours('bd');
    computeTotalLaborPercentage('bd');computeSpendPerPerson('bd');computeSpendPerPerson('gm');computeSpendPerPerson('ac');computePromoterExpense('ac');computePromoterExpense('gm');
    computePromoterExpense('bd');
    computeSeTotal('gm');
    computeBOHSales('gm');
    computeFOHSales('gm');
    computeFOHCover('gm');
    computeBOHCover('gm');
    computeTotalCost('gm');
    computeTotalLaborSales('gm');
    computeTotalHours('gm');
    computeTotalLaborCover('gm');
    computeTotalLaborPercentage('gm');
    computeFOHPercentage('gm');computeFOHPercentage('bd');
    computeBOHPercentage('gm');computeBOHPercentage('bd');
    computeTotalSales('gm');computeCompPercentage('bd');

    /**
     * Do not calculate last year data
     * DO CALCULATE NOW, TODO: turn this shit off
     */
    computeSeTotal('ly');
    computeTotalSales('ly');
    computeBOHSales('ly');
    computeFOHSales('ly');
    computeTotalCost('ly');
    computeTotalLaborCover('ly');
    computePromoterExpense('ly');
    computeFOHCover('ly');
    computeTotalHours('ly');
    computeTotalLaborPercentage('ly');
    computeSpendPerPerson('ly');
    computeDJEntertainment('ly');
    computeTotalBev('ly');
    computeTotalSalesBudget('ly');
    computeBOHCover('ly');
    computeTotalLaborSales('ly');
    computeFOHPercentage('ly');
    computeBOHPercentage('ly');
    computeTotalSales('ly');

</script>

<script src="/content/admin/js/core.js"></script>