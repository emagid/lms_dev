<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
	<input type="hidden" name="id" value="<?php echo $model->listing->id;?>" />
	<input type="hidden" name="type" value="<?php echo \Model\Listing::COMMERCIAL;?>" />
	<div class="row">
		<div class="col-md-8">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Address</label>
					<?php echo $model->form->editorFor("address"); ?>
				</div>
				<div class="form-group">
					<label>City</label>
					<?php echo $model->form->editorFor("city"); ?>
				</div>
				<div class="form-group form-group-half">
					<label>Zipcode</label>
					<?php echo $model->form->editorFor("zipcode"); ?>
				</div>
				<div class="form-group form-group-half">
					<label>State</label>
					<?php echo $model->form->editorFor("state"); ?>
				</div>
				<div class="form-group form-group-half">
					<label>Square feet</label>
					<input type="text" name="sq" value="<?=$model->listing->sq?:0?>">
				</div>
				<div class="form-group form-group-half">
					<label>Use for</label>
					<?php echo $model->form->editorFor("use_for"); ?>
				</div>

				<!--for residential only-->
				<div class="form-group form-group-half">
					<label>Bedroom</label>
					<input type="text" name="bedroom" value="<?=$model->listing->bedroom?:0?>">
				</div>
				<div class="form-group form-group-half">
					<label>Bathroom</label>
					<input type="text" name="bathroom" value="<?=$model->listing->bathroom?:0?>">
				</div>
				<div class="form-group">
					<label>Condo fee</label>
					<input type="text" name="condo_fee" value="<?=$model->listing->condo_fee?:0?>">
				</div>
				<!-- -->

				<div class="form-group">
					<label>Description</label>
					<?php echo $model->form->editorFor("description"); ?>
				</div>

				<div class="form-group form-group-half">
					<label>Price Daily</label>
					<input type="text" name="price_day" value="<?=$model->listing->price_day?:0?>">
				</div>
				<div class="form-group form-group-half">
					<label>Price Weekly</label>
					<input type="text" name="price_week" value="<?=$model->listing->price_week?:0?>">
				</div>
				<div class="form-group form-group-half">
					<label>Price Monthly</label>
					<input type="text" name="price_month" value="<?=$model->listing->price_month?:0?>">
				</div>

				<div class="form-group form-group-half">
					<label>Security Deposit</label>
					<input type="text" name="deposit" value="<?=$model->listing->deposit?:0?>">
				</div>
				<!--div class="form-group checkbox">
					<label>
						<?php echo $model->form->checkBoxFor("status", 1); ?> Active?
					</label>
				</div-->
			</div>
		</div>
		<div class="col-md-8">
			<div class="box">
				<h4>Information</h4>
				<div class="form-group">
					<label>Building Name</label>
					<?php echo $model->form->editorFor("building_name"); ?>
				</div>
				<div class="form-group">
					<label>Access Type</label>
						<?$access = json_decode($model->listing->access_type);
						foreach(\Model\Listing::$accessType as $accessType){?>
							<br><input name="access_type[]" type="checkbox" value="<?=$accessType?>" <?=count($access)>0 && in_array($accessType,$access)? 'checked':'' ?>> <?=$accessType?>
						<?}?>
				</div>
				<div class="form-group">
					<label>Payout</label>
					<?$modelPayout = json_decode($model->listing->payout);
					foreach(\Model\Listing::$payout as $payout){?>
						<br><input name="payout[]" type="checkbox" value="<?=$payout?>" <?=count($modelPayout)>0 && in_array($payout,$modelPayout)? 'checked':'' ?>> <?=$payout?>
					<?}?>
				</div>
				<div class="form-group">
					<label>Company</label>
					<?php echo $model->form->editorFor("company"); ?>
				</div>
				<div class="form-group">
					<label>Contact Name</label>
					<?php echo $model->form->editorFor("contact_name"); ?>
				</div>
				<div class="form-group">
					<label>Contact Phone Number</label>
					<?php echo $model->form->editorFor("contact_phone"); ?>
				</div>
				<div class="form-group">
					<label>Company Name</label>
					<?php echo $model->form->editorFor("company_name"); ?>
				</div>
				<div class="form-group">
					<label>Company Phone</label>
					<?php echo $model->form->editorFor("company_phone"); ?>
				</div>
				<div class="form-group">
					<label>Unit Num</label>
					<?php echo $model->form->editorFor("unit_num"); ?>
				</div>
				<div class="form-group">
					<label>Open House</label>
					<input name="open_house" type="text" value="<?=$model->listing->open_house?:date('Y-m-d h:i:s')?>">
				</div>
				<div class="form-group">
					<label>Close Open House</label>
					<input name="close_house" type="text" value="<?=$model->listing->close_house?:date('Y-m-d h:i:s')?>">
				</div>
				<div class="form-group">
					<label>Availability</label>
					<input name="availability" type="text" value="<?=$model->listing->availability?:date('Y-m-d h:i:s')?>">
				</div>
				<div class="form-group">
					<label>Available Immediately</label>
					<?php echo $model->form->checkboxFor("available_immediately",1); ?>
				</div>
				<div class="form-group">
					<label>Status</label>
					<input name="status" type="text" value="<?=$model->listing->status?:0?>">
				</div>
				<div class="form-group">
					<label>Active Listing</label>
					<input name="active_listing" type="text" value="<?=$model->listing->active_listing?:0?>">
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="box">
				<div class="form-group">
					<h4>Image</h4>
					<div class="col-md-24">
						<div class="box">
							<div class="dropzone" id="dropzoneForm"></div>
						</div>

						<?php foreach($model->listing->getMedias() as $index => $media){ ?>
							<div>Name: <?='Image '.($index + 1)?></div>
							<div>Image: <img class="img" src="<?=$media->path()?>"></div>
							<div>Delete: <a href="/admin/media/delete/<?=$media->id?>?&redirect=/admin/commercial/update/<?=$model->listing->id?>">DELETE</a></div>
						<?php } ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>
<link rel = "stylesheet" type = "text/css" href = "<?=ADMIN_CSS.'jquery.datetimepicker.css'?>">
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>
<script type='text/javascript'>
	$(document).ready(function() {

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container").html(img);


					//$('#previewupload').attr('src', e.target.result);
					//$("a.passport-img").attr("href", e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
			$('#previewupload').show();
		});
		$('input[name=open_house],input[name=close_house],input[name=availability]').datetimepicker();

		Dropzone.autoDiscover = false;
		$("#dropzoneForm").dropzone({
			url: '/admin/media/upload',
			sending: function(file, xhr, formData){
				formData.append('listing', "<?=$model->listing->id?>");
			}
		});
	});

</script>