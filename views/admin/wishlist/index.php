<?php if (count($this->_viewData->wishlist)>0) { ?>
<form action="/admin/wishlist/send_coupon" method="post">
  <div class="row">
    <div class="col-md-32">
      <div class="box">
       
       <select name="coupon" class="coupon_send" product="<?=$Product->id?>"  user="<?=$model->user->id?>">
                <option value="none">Select coupon...</option>  
              <?foreach (\Model\Coupon::getList() as $coupon) {?>
               <option value="<?=$coupon->id;?>"><?=$coupon->name;?></option>  
              <?}?>
              </select>
     <button type="submit" class="btn btn-success">Send coupons</button>
      </div>  
    </div>
    
</div>
<div class="box box-table">
<table class="table">
  <thead>
    <tr>
            <th></th>
            <th>User</th>
            <th>MPN</th>
            <th>Product</th>
            <th>Price</th> 
            <th>View</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($this->_viewData->wishlist as $wish) { ?>
    <?$product = \Model\Product::getItem($wish->product_id);?>
    <?$user = \Model\User::getItem($wish->user_id);?>
    <tr>
       <td>
         <input type="checkbox" name="id[]" value="<?=$wish->id?>">
      </td>
      <td>
         <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $user->id; ?>">
           <?php echo $user->first_name;?>  <?php echo $user->last_name;?>
         </a>
      </td>
       
      <td>
      	 <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->id; ?>">
           <?php echo $product->mpn;?>
         </a>
      </td>
      <td>
      	 <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->id; ?>">
           <?php echo $product->name;?>
         </a>
      </td>
      <td>
         <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->id; ?>">
           <?php echo $product->price;?>
         </a>
      </td>
      <td>
         <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->id; ?>">
           <i class="icon-eye"></i> 
         </a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
  
</table>


            </form>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>

<?php footer();?>

 
