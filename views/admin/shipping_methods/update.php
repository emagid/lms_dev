<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<input type="hidden" name="id" value="<?php echo $model->shipping_method->id;?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>
						<?php echo $model->form->checkboxFor("is_default", 1); ?>
						Default
					</label>
				</div>
				<div class="form-group">
					<label>
						<?php echo $model->form->checkboxFor("international", 1); ?>
						International
					</label>
				</div>
				<div class="form-group">
					<label>Name</label>
					<?php echo $model->form->editorFor("name"); ?>
				</div>
				<div class="row clonable">
					<div class="col-xs-8">
						<div class="form-group">
							<label>Cost</label>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="text" name="cost[]" value="<?=(count($model->shipping_method->cost) > 0)?$model->shipping_method->cost[0]:''?>" />
							</div>
						</div>
					</div>
					<div class="col-xs-8">
						<div class="form-group">
							<label>Min cart subtotal</label>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="text" name="cart_subtotal_range_min[]" value="<?=(count($model->shipping_method->cart_subtotal_range_min) > 0)?$model->shipping_method->cart_subtotal_range_min[0]:''?>" />
							</div>
							<? $max = max(count($model->shipping_method->cost), count($model->shipping_method->cart_subtotal_range_min));?>
							<p class="help-block" style="<?=($max > 1)?'display:none;':''?>" >Input 0 if not a range method</p>
						</div>
					</div>
					<div class="col-xs-8">
						<div class="form-group">
							<label>&nbsp;</label>
							<div class="input-group">
								<button class="btn btn-info add-range">Add range</button>
							</div>
						</div>
					</div>
				</div>
				<? 
				for($i = 1; $i < $max; $i++){ 
				?>
					<div class="row clone">
						<div class="col-xs-8">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" name="cost[]" value="<?=(isset($model->shipping_method->cost[$i]))?$model->shipping_method->cost[$i]:''?>" />
								</div>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" name="cart_subtotal_range_min[]" value="<?=(isset($model->shipping_method->cart_subtotal_range_min[$i]))?$model->shipping_method->cart_subtotal_range_min[$i]:''?>" />
								</div>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<div class="input-group">
									<button class="btn btn-warning remove-range">Remove</button>
								</div>
							</div>
						</div>
					</div>
				<? } ?>
			</div>
		</div>
	</div>
	<button type="submit" class="btn btn-save">Save</button>
</form>

<?php footer();?>

<script>
	$(function(){
		$('.add-range').click(function(e){
			e.preventDefault();
			var row = $('.clonable').clone();
			row.removeClass('clonable').addClass('clone').appendTo('form .box');
			row.find('label').remove();
			var rowBtn = row.find('button');
			rowBtn.removeClass('add-range').removeClass('btn-info').addClass('btn-warning');
			rowBtn.text('Remove');
			rowBtn.click(function(e){
				e.preventDefault();
				if ($('form .clone').size() == 1) {
					$('form p').show();		
				}
				$(this).parents('.clone').remove();
				return false;
			});
			$('form p').hide();
			return false;
		});
		$('.remove-range').click(function(e){
			e.preventDefault();
			if ($('form .clone').size() == 1) {
				$('form p').show();		
			}
			$(this).parents('.clone').remove();
			return false;
		})
	})
</script>