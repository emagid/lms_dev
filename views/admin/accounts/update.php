<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->account->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />


    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#certificate-tab" aria-controls="certificate" role="tab" data-toggle="tab">Certificate</a></li>
            <li role="presentation"><a href="#plan-tab" aria-controls="plan" role="tab" data-toggle="tab">Plan</a></li>
            <li role="presentation"><a href="#manager-tab" aria-controls="plan" role="tab" data-toggle="tab">Managers</a></li>
            <? if($model->account->id) {?>
                <li role="presentation"><a href="#categories-tab" aria-controls="categories"  role="tab" data-toggle="tab">Categories</a></li>
                <li role="presentation"><a href="#courses-tab" aria-controls="courses"  role="tab" data-toggle="tab">Courses</a></li>
                <li role="presentation"><a href="#files-tab" aria-controls="files"  role="tab" data-toggle="tab">Assign Files</a></li>
            <? }?>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

                            <div class="form-group">
                                <div class='custom-select'>
                                    <label>Status</label>
                                    <?php echo $model->form->dropDownListFor('status',\Model\Account::$statuses); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class='custom-select'>
                                    <label>Brand</label>
                                    <?php echo $model->form->dropDownListFor("brand_id",$model->brands); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Subdomain</label>
                                <?php echo $model->form->textBoxFor("subdomain"); ?>
                            </div>

                            <div class="form-group">
                                <label>Main Color</label>
                                <?php echo $model->form->textBoxFor("main_color",['class'=>'jscolor']); ?>
                            </div>
                            

                            <div class="form-group">
                                <label>Button Color</label>
                                <?php echo $model->form->textBoxFor("button_color",['class'=>'jscolor']); ?>
                            </div>

                            <div class="form-group">
                                <label>Videos Enabled</label>
                                <?php echo $model->form->checkBoxFor("videos_enabled",1); ?>
                            </div>

                            <!-- <div class="form-group">
                                <label>Logo</label>
                                <input type="file" name="logo" class="image" />
                            </div> -->

                            <div class="form-group">
                                <label>Logo</label>
                                <p>
                                    <small>(360px x 70px)</small>
                                </p>
                                <p><input type="file" name="logo" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->account->logo != "" && file_exists(UPLOAD_PATH . 'account' . DS . $model->account->logo)) {
                                        $img_path = UPLOAD_URL . 'account' . $model->account->logo;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'account/delete_image/' . $model->account->id; ?>?logo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="logo"
                                                   value="<?= $model->account->logo ?>"/>
                                        </div>
                                    <?php } else if($model->account->logo != "") {
                                        $img_path = UPLOAD_URL .$model->account->logo; ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'account/delete_image/' . $model->account->id; ?>?logo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="logo"
                                                   value="<?= $model->account->logo ?>"/>
                                        </div>
                                    <?}?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Contact Info</h4>
                            <div class="form-group">
                                <label>Contact Name</label>
                                <?php echo $model->form->editorFor("contact_name"); ?>
                            </div>

                            <div class="form-group">
                                <label>Contact Email</label>
                                <?php echo $model->form->textBoxFor("contact_email"); ?>
                            </div>

                            <div class="form-group">
                                <label>Contact Phone Number</label>
                                <?php echo $model->form->editorFor("contact_phone"); ?>
                            </div>

                            <div class="form-group">
                                <label>Available Hours </label>
                                <?php echo $model->form->textBoxFor("contact_available_hours"); ?>
                            </div>

                            <div class="form-group">
                                <label>Contact Additional Comments</label>
                                <?php echo $model->form->textAreaFor("contact_additional_comments"); ?>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-md-12">
                        <div class="box">                            
                            <div class="form-group">
                                <h4>Logo</h4>

                                <p>
                                    <small>(ideal image size is 1200 x 344)</small>
                                </p>
                                <?php
                               $img_path = "";
                                if ($model->account->logo != "") {
                                    $img_path = UPLOAD_URL . 'accounts/logos' . $model->account->logo;
                                    var_dump($img_path);
                                }
                                ?>
                                <p><input type="file" name="logo" class='image'/></p>
                                <?php if ($model->account->logo != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <div id='image-preview'>
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href=<?= ADMIN_URL . 'accounts/delete_image' . $model->account->id . '?logo=1'; ?> class="btn
                                               btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="logo" value="<?= $model->account->logo ?>"/>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class='preview-container'></div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div> -->
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="certificate-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Contact Info</h4>
                            <div class="form-group">
                                <label>Terms and Conditions</label>
                                <?php echo $model->form->textAreaFor("terms_and_conditions"); ?>
                            </div>

                            <div class="form-group">
                                <label>Certificate Awarded by</label>
                                <?php echo $model->form->textAreaFor("certificate_awarded_by"); ?>
                            </div>

                            <div class="form-group">
                                <label>Certificate Signature Name</label>
                                <?php echo $model->form->textBoxFor("certificate_signature_name"); ?>
                            </div>

                            <div class="form-group">
                                <label>Certificate Signature Title</label>
                                <?php echo $model->form->textBoxFor("certificate_signature_title"); ?>
                            </div>

                            <div class="form-group">
                                <label>Certificate Logo</label>
                                <p>
                                    <small>(190px x 120px)</small>
                                </p>
                                <p><input type="file" name="certificate_logo" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if($model->account->certificate_logo != "") {
                                        $img_path = UPLOAD_URL .$model->account->certificate_logo; ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'account/delete_image/' . $model->account->id; ?>?certificate_logo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="certificate_logo"
                                                   value="<?= $model->account->certificate_logo ?>"/>
                                        </div>
                                    <?}?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Signature Image</label>
                                <p>
                                    <small>(200px x 85px)</small>
                                </p>
                                <p><input type="file" name="certificate_signature_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if($model->account->certificate_signature_image != "") {
                                        $img_path = UPLOAD_URL .$model->account->certificate_signature_image; ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'account/delete_image/' . $model->account->id; ?>?certificate_signature_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="certificate_signature_image"
                                                   value="<?= $model->account->certificate_signature_image ?>"/>
                                        </div>
                                    <?}?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="plan-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Assign a Plan to the Account</h4>
                                <div class="form-group">
                                    <div class='custom-select'>
                                        <label>Plan</label>
                                        <select name="plan_id" class="form-control">
                                            <option value="">None</option>
                                            <?php foreach ($model->plans as $plan) {
                                                $select = ($plan->id) ? " selected='selected'" : "";
                                                ?>
                                                <option value="<?php echo $plan->id; ?>" <?php echo $select; ?> ><?php echo $plan->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="manager-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Account Managers</h4>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Email</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="managers-list">
                                    <?php foreach($model->managers as $obj){ ?>
                                        <tr class="originalProducts">
                                            <td colspan="2"><a href="<?php echo ADMIN_URL; ?>managers/update/<?php echo $obj->id; ?>"><?php echo $obj->full_name(); ?></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>managers/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></td>
                                            <td class='text-center'>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>managers/update/<?php echo $obj->id; ?>">
                                                    <i class="icon-pencil"></i> 
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>managers/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th><input id="new_man_first_name" placeholder="First Name"/></th>
                                        <th><input id="new_man_last_name" placeholder="Last Name"/></th>
                                        <th><input id="new_man_email" placeholder="Email"/></th>
                                        <th><input id="new_man_password" placeholder="Password"/></th>
                                        <th><a class="add_man btn btn-flat"><i class="icon-plus"></i>Add New</a></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="categories-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Categories</h4>
                            <table id="categories-list" class="table">
                                <thead>
                                <tr>
                                    <a class="add_cat btn btn-flat"><i class="icon-plus"></i>Add New</a>
                                </tr>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Number of users</th>
                                    <th>Status</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($model->categories as $obj){ ?>
                                        <tr class="category">
                                            <td><input type="text" class="upd_cat" data-cat_id="<?php echo $obj->id; ?>" value="<?php echo $obj->name; ?>" /></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>categories/update/<?php echo $obj->id; ?>"><?php echo count($obj->get_users()); ?></a></td>
                                            <td>
                                                <span class="upd_cat upd_cat_<?php echo $obj->id; ?>"></span>
                                            </td>
                                            <td>
                                                <a class="btn-actions delete_cat" data-cat_id="<?php echo $obj->id; ?>">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="courses-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Course Assignments</h4>
                            <table id="assignment-table" class="table">
                                <thead>
                                    <tr>
                                        <div class='form-group'>
                                            <label>Select A course</label>
                                        </div>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <div class="custom-select">
                                                <select id="course_select">
                                                    <option disabled selected> -- SELECT A COURSE -- </option>
                                                    <? foreach ($model->courses as  $course) { ?>
                                                        <option value="<?=$course->id?>"><?=$course->title?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Module</th>
                                        <th>Students</th>
                                        <th>Status</th>
                                        <th colspan="3">Assigned Groups</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="files-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Assign Files to this account</h4>
                            <div class="form-group">
                                <label>Files Available</label>
                                <select multiple class="multiselect" name="files[]">
                                    <? foreach (\Model\File::getList() as $file){ ?>
                                        <? $selected = in_array($file->id,$model->files)?'selected':'' ?>
                                        <option <?=$selected?> value="<?=$file->id?>"><?=$file->name?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <table id="files-table" class="table">
                                <thead>
                                    <tr>
                                        <th>File</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? foreach(\Model\Account_File::getList(['where'=>"account_id = " .$model->account->id]) as $obj) { 
                                        $file = \Model\File::getItem($obj->file_id);
                                    ?>
                                    <tr>
                                        <td><?=$file->name?></td>
                                        <td>
                                            <a class="btn-actions delete_file" data-cat_id="<?php echo $obj->id; ?>">
                                                <i class="icon-cancel-circled"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <button type="submit" class="btn btn-save">Save</button>
  
</form>

<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<?php footer();?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.min.css" />
<script type="text/javascript">
$(document).ready(function() {

    var files = <?= json_encode($model->files)?>;

    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    $(document).on("change",".upd_cat",function(){
        var cat_id = $(this).data('cat_id');
        var cat_name = $(this).val();
        $.post('/admin/accounts/update_category',{cat_id:cat_id,cat_name:cat_name},function(data){
            $('span.upd_cat').text('');
            if(data.status){
                $('span.upd_cat_'+cat_id).text("UPDATED!");
            } else {
                $('span.upd_cat_'+cat_id).text("UPDATE FAILED!");
            }
        });
    });
    $(document).on("click",".delete_cat",function(e){
        var category = $(this).parents('.category');
        var cat_id = $(this).data('cat_id');
        $.post('/admin/accounts/delete_category',{cat_id:cat_id},function(data){
            $('span.upd_cat').text('');
            if(data.status){
                category.remove();
            } else {
                $('span.upd_cat_'+cat_id).text(data.msg);
            }
        });
    });
    $(document).on("click",".add_cat",function(){
        var cat_name = prompt("What is the name of this category?");
        if(cat_name == null){
            return false;
        }
        var account_id = <?=$model->account->id?:0?>;
        var categories = $('#categories-list tbody');
        $.post('/admin/accounts/add_category',{cat_name:cat_name,account_id:account_id},function(data){
            if(data.status) {
                var category = $("<tr class='category'/>")
                category.append($('<td>').append('<input type="text" class="upd_cat" data-cat_id="' + data.cat_id + '" value="' + cat_name + '" />'))
                    .append($('<td>').append('<a href="<?php echo ADMIN_URL; ?>categories/update/' + data.cat_id + '">0</a>'))
                    .append($('<td>').append('<span class="upd_cat upd_cat_' + data.cat_id + '"></span>'))
                    .append($('<td>').append($('<a class="btn-actions delete_cat" data-cat_id=' + data.cat_id + '" />').append($('<i class="icon-cancel-circled" />'))))
                categories.append(category);
                $('span.upd_cat').text('');
                $('span.upd_cat_' + data.cat_id).text("CREATED!");
            }
        });
    });
    $(document).on("click",".add_man",function(){
        var manager = {};
        manager.role = 'manager';
        manager.first_name = $('#new_man_first_name').val();
        manager.last_name = $('#new_man_last_name').val();
        manager.email =  $('#new_man_email').val();
        manager.password =  $('#new_man_password').val();
        manager.account_id = <?=$model->account->id?:0?>;
        var account_id = <?=$model->account->id?:0?>;
        var categories = $('#categories-list tbody');
        $.post('/admin/accounts/add_manager',manager,function(data){
            if(data.status) {
                $('#new_man_first_name').val('');
                $('#new_man_last_name').val('');
                $('#new_man_email').val('');
                $('#new_man_password').val('');
                $('#managers-list').append(data.html);
            }
        });
    });
    $(document).on("change",".module_groups",function(){
        var postData = {
            categories: $(this).val(),
            account_id: <?=$model->account->id?:0?>,
            module_id: $(this).data('module_id')
        }
        $.post('/admin/accounts/update_module_cats',postData,function(data){
            $('.module_status').text('');
            if(data.status){
                $('#module_status_'+postData.module_id).text('UPDATED!');
            } else {
                $('#module_status_'+postData.module_id).text('UPDATE FAILED!');
            }
        });

    });
    $('#course_select').change(function () {
        var account_id = <?=$model->account->id?:0?>;
        var course_id = this.value;
        $.post('/admin/accounts/get_modules',{account_id:account_id,course_id:course_id},function(data){
            $("#assignment-table tbody").html(data);
            $(".module_groups").chosen();
        });
    })

    $("select.multiselect").each(function(i, e) {
        $(e).val('');
        var placeholder = $(e).data('placeholder');
        $(e).multiselect({
            nonSelectedText: placeholder,
            includeSelectAllOption: true,
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%'
        });
    });

    $('select[name="files[]"]').val(files);
    $('select.multiselect').multiselect('rebuild');

    // $(document).on("click",".add_files",function(){
        
    //     var files = $('#files-list tbody');
    //     $.post('/admin/accounts/add_files',manager,function(data){
    //         if(data.status) {
    //             $('#new_man_first_name').val('');
    //             $('#new_man_last_name').val('');
    //             $('#new_man_email').val('');
    //             $('#new_man_password').val('');
    //             $('#managers-list').append(data.html);
    //         }
    //     });
    // });

});
</script>

