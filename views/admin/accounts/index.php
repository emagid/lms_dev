<div class="row filter_holder">
    <form id="filters">
        <div>
            Filter Account by Plan:
            <div class="custom-select">
              <select name="plan">
                <option disabled selected>Select a Plan</option>
                <? foreach($model->plans AS $plan) { ?>
                    <option value="<?=$plan->id?>" <?= isset($_GET['plan']) && $_GET['plan'] == $plan->id?'selected':'' ?>>
                        <?=$plan->name?>
                    </option>
                <? } ?>
              </select>
            </div>
        </div> 
    </form>
    <form>
      <div>
          <label>Search:</label> 
          <input id="search" class="form-control" type="text" name="search" placeholder="Search by Name"/>
      </div> 
    </form>
</div>

<?php if(count($model->accounts)>0) { ?>
  <div class="box box-table">
    <table id="data-list" class="table">
      <thead>
        <tr>
          <th width="20%">Logo</th>
          <th width="20%">Status</th>
          <th width="20%">Name</th>
          <th width="20%">Subdomain</th>
          <th width="10%">Managers</th>
          <th width="10%">Students</th>
          <th width="10%">Certified</th>
          <th width="10%">Plan</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->accounts as $obj){ 
          // $account_plan = \Model\Plan::getItem(null,['where'=>"id = $obj->plan_id "]);
        ?>
        <tr class="originalProducts">
         <td><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><img src="https://sm2elearning.com/uploads/account/logo/<?=$obj->id?>/<?=$obj->logo?>"/></a></td>
         <td>
             <?=$obj->status?>
         </td>
         <td><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><?php echo $obj->subdomain; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><?php echo $obj->getUserCount('manager'); ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><?php echo $obj->getUserCount(); ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><?php echo $obj->getCertifiedUserCount(); ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>"><?php echo $obj->getAccountPlan(); ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>accounts/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>accounts/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'accounts';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;

  $('#filters :input').change(function(e){
     $(this).parents('form').submit();
  });

  $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>accounts/search";
        var keywords = $(this).val();
        if (keywords.length > 2) {
            $.get(url, {keywords: keywords}, function(data) {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').hide();
                $('.originalProducts').hide();

                var list = JSON.parse(data);

                for (key in list) {
                    var tr = $('<tr />');
                    var img = $('<img />').prop('src', "https://sm2elearning.com/uploads/account/logo/" + list[key].id + "/" + list[key].logo); 
                    
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(img));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(list[key].status));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(list[key].name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(list[key].subdomain));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(list[key].managers));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(list[key].students));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(list[key].certified));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id).html(list[key].plan));

                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>accounts/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>accounts/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.originalProducts').remove();
            $('.paginationContent').show();
            $('.originalProducts').show();
        }
    }).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
</script>

