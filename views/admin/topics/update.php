<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
	<input type="hidden" name="id" value="<?php echo $model->location->id;?>" />
	<input type="hidden" name="redirectTo" value="/admin/location" />

	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<div class="form-group">
					<label>Name</label>
					<?php echo $model->form->editorFor("name"); ?>
				</div>
				<div class="form-group">
					<label>Type</label>
					<select name="type">
						<option value="">--SELECT--</option>
						<option value="nightlife" <?=$model->location->type == 'nightlife'?'selected':null?>>Nightlife</option>
						<option value="hotel" <?=$model->location->type == 'hotel'?'selected':null?>>Hotel</option>
						<option value="restaurant" <?=$model->location->type == 'restaurant'?'selected':null?>>Restaurant</option>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>
<link rel="stylesheet" type="text/css" href="/content/frontend/css/jquery.tagsinput.css">
<script src="/content/frontend/js/jquery.tagsinput.js"></script>
<script type='text/javascript'>
	$(document).ready(function() {

	});

</script>