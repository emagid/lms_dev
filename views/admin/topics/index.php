<?php if(count($model->locations)>0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="30%">Name</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($model->locations as $location): ?>
                <tr>
                    <td><a href="/admin/location/update/<?php echo $location->id; ?>"><?php echo $location->name; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="/admin/location/update/<?php echo $location->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="/admin/location/delete/<?php echo $location->id; ?>" onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>'
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'location';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

