<?php
$gender = [1=>'Unisex',2=>"Men",3=>"Women"];
?>

<form class="form" action="/admin/price_alerts/do_respond" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo  $this->_viewData->alert->id;?>" />
    <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
       
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general-tab">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Answer</label>
                       <textarea name="respond">You requested a price alert if the price for this watch was reduced - Today is your lucky day! <br>Please click on the link below to purchase this watch at the reduced price!</textarea>
                    </div>
                    
                     
                    
                    
					
                    
                </div>
            </div>
          
           
          

          
        </div>
      </div>
            
    </div>
  </div>
  <button type="submit" class="btn btn-save">Respond</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript"> 
  var categories = <?php echo json_encode($model->product_categories); ?>;
    var collections = <?php echo json_encode($model->product_collections); ?>;
    var materials = <?php echo json_encode($model->product_materials); ?>;
  //console.log(categories);
  var site_url=<?php echo json_encode(ADMIN_URL.'products/'); ?>;
  $(document).ready(function() {
    //var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
    var video_thumbnail = new mult_image($("input[name='video_thumbnail']"),$("#preview-thumbnail-container"));
    	
    	function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				var img = $("<img />");
				reader.onload = function (e) {
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
				};
				$(input).parent().parent().find('.preview-container').html(img);
				$(input).parent().parent().find('input[type="hidden"]').remove();

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
		});	
        

		$("input[name='name']").on('keyup',function(e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g,'-');
			$("input[name='slug']").val(val.toLowerCase());
		});
        
  $("select.multiselect").each(function(i,e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText:placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
          $("select[name='product_category[]']").val(categories);
          $("select[name='product_collection[]']").val(collections);
          $("select[name='product_material[]']").val(materials);
          $("select.multiselect").multiselect("rebuild");
          
          function sort_number_display() {
        var counter = 1;
         $('#image-container >tbody>tr').each(function(i,e) {
           $(e).find('td.display-order-td').html(counter);
           counter++;
         });
      }
      $("a.delete-product-image").on("click",function() {
        if(confirm("are you sure?")) {
          location.href=$(this).attr("href");
        } else {
          return false;
        }
      });
      $('#image-container').sortable({
          containerSelector: 'table',
          itemPath: '> tbody',
          itemSelector: 'tr',
          placeholder: '<tr class="placeholder"/>',
          onDrop: function ($item, container, _super, event) {
            
              if($(event.target).hasClass('delete-product-image')) {
                  $(event.target).trigger('click');
              } 

              var ids = [];
              var tr_containers = $("#image-container > tbody > tr");
              tr_containers.each(function(i,e) {
                  ids.push($(e).data("image_id"));

              });
              $.post(site_url+'sort_images', {ids: ids}, function(response){
                sort_number_display();
              });
              _super($item);
          }
        });
      
      });
</script>
<script type="text/javascript">
Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

  // The configuration we've talked about above
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100,
  maxFiles: 100,
   url: <?php  echo json_encode(ADMIN_URL.'products/upload_images/'.$model->product->id);?>,
  // The setting up of the dropzone
  init: function() {
    var myDropzone = this;

    // First change the button to actually tell Dropzone to process the queue.
    $("#upload-dropzone").on("click", function(e) {
      // Make sure that the form isn't actually being sent.
      e.preventDefault();
      e.stopPropagation();
      myDropzone.processQueue();
    });

    // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
    // of the sending event because uploadMultiple is set to true.
    this.on("sendingmultiple", function() {
      // Gets triggered when the form is actually being sent.
      // Hide the success button or the complete form.
      $("#upload-dropzone").prop("disabled",true);
    });
    this.on("successmultiple", function(files, response) {
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
      window.location.reload();
      $("#upload-dropzone").prop("disabled",false);
    });
    this.on("errormultiple", function(files, response) {
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    });
  }

}
</script>
