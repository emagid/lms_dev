<?php if(count($model->appointments)>0) { ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="20%">Applicant</th>
          <th width="20%">Listings</th>
          <th width="20%">Date</th>
          <th width="20%">Location</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->appointments as $obj){ ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>"><?php echo $obj->getOpportunity()->getContact()->fullName(); ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>"><?php echo $obj->getListingList('<br>')?></td>
         <td><a href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>"><?php echo $obj->getFormattedDate('Y-m-d h:i')?></td>
         <td><a href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>"><?php echo $obj->location?></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>appointments/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'appointments';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;
</script>

