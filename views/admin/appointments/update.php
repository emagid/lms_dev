<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->appointment->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Opportunity</label>
                    <select class="form-control" name="opportunities_id" id="opportunity_id" >
                        <? foreach (\Model\Opportunities::getList() as $opportunity) {
                            if($opportunity->getContact()){?>
                            <option value="<?= $opportunity->id ?>" data-request-type="<?=$opportunity->building_type?>" <?=$model->appointment->opportunities_id == $opportunity->id?'selected':''?>><?= $opportunity->getContact()->fullName().' | '.$opportunity->getFormattedDate()?></option>
                            <? } ?>
                        <? } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="listings">Listing to show</label>
                    <select class="form-control" name="lists[]" id="listings-selected" multiple required>
                        <? $listings = json_decode($model->appointment->lists,true);
                        foreach ($model->listings as $listing) {?>
                            <option value="<?= $listing->id ?>" data-building-type="<?=$listing->type?>" <?= count($listings) > 0 && in_array($listing->id, $listings) ? 'selected' : '' ?>><?= $listing->getAddress() ?></option>
                        <? } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Meeting Location</label>
                    <?php echo $model->form->editorFor('location')?>
                </div>
                <div class="form-group">
                    <label>Date & Time</label>
                    <input name="date" type="text" value="<?=$model->appointment->date?date('Y-m-d h:i',strtotime($model->appointment->date)):null?>">
                </div>
                <div class="form-group">
                    <label>Notify via Email</label>
                    <?php echo $model->form->checkboxFor('email_notification',1,['checked'=>'checked'])?>
                </div>
                <?php if($model->appointment->location){ $urlencodedLocation = urlencode($model->appointment->location);?>
                <div class="form-group">
                    <label>Map</label>
                    <br>
                    <img class='group-google-maps-preview'
                         src='http://maps.googleapis.com/maps/api/staticmap?center=<?=$urlencodedLocation?>&zoom=15&size=600x300&sensor=true&markers=color:red|<?=$urlencodedLocation?>'>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<link rel="stylesheet" type="text/css" href="<?= ADMIN_CSS . 'jquery.datetimepicker.css' ?>">
<script src="<?= ADMIN_JS . 'plugins/jquery.datetimepicker.full.min.js' ?>"></script>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('input[name=date]').datetimepicker({
            format: 'Y-m-d h:i'
        });

        $('#opportunity_id').multiselect({
            enableFiltering: true,
            filterBehavior: 'both'
        });

        $('#listings-selected').multiselect({
            enableFiltering: true,
            filterBehavior: 'both'
        });

        $('#opportunity_id').on('change',function(){
            var listingId = $(this).find(':selected').data('request-type');
            var listings = $('#listings-selected > option');
            $.each(listings,function(key,obj){
                var data = $(obj).attr('data-building-type');
                if(data == listingId){
                    $(obj).prop('disabled',false);
                } else {
                    $(obj).prop('disabled',true);
                }
            });
        }).trigger('change');
    });
</script>