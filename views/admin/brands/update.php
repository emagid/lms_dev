<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input name="id" type="hidden" value="<?php echo $model->brand->id;?>" />
  <input name="token" type="hidden" value="<?php echo get_token();?>" />

    <div class="row">
      <div class="col-md-12">
          <div class="box">
            <h4>General</h4>
              <div class="form-group">
                <label>Name</label>
                <?php echo $model->form->editorFor('name'); ?>
              </div> 
          </div>
      </div>
    </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
