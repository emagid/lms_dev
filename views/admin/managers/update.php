<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#permissions-tab" aria-controls="general"  role="tab" data-toggle="tab">Permissions</a></li>
            <!-- <li role="presentation" ><a href="#courses-tab" aria-controls="courses"  role="tab" data-toggle="tab">Enrolled Courses</a></li> -->
            <? if($model->user->id) {?>
                <li role="presentation"><a href="#files-tab" aria-controls="files"  role="tab" data-toggle="tab">Files</a></li>
            <? } ?>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->editorFor("first_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->editorFor("last_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <?php echo $model->form->editorFor("phone"); ?>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <?php echo $model->form->editorFor("email"); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->editorFor("address"); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->editorFor("city"); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?php echo $model->form->editorFor("state"); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip</label>
                                <?php echo $model->form->editorFor("zip"); ?>
                            </div>
                            <div class="form-group">
                                <label>Signin Count</label>
                                <?php echo $model->form->textBoxFor("sign_in_count",['readonly'=>true]); ?>
                            </div>
                            <div class="form-group">
                                <label>Password (leave blank to keep current password)</label>
                                <input type="password" name="password" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="permissions-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Permissions</h4>
                            <ul class="permissiontable list-unstyled list-group">
                            <?php
                            foreach ($model->all_sections as $parent => $children) { ?>
                                <li class="parent_<?= strtolower(str_replace(' ','_',$parent));?>" data-name="<?= strtolower(str_replace(' ','_',$parent));?>">
                                    <label>
                                        <input type="checkbox" name="permissions[]" class="parent" value="<?php echo $parent; ?>" <?php if(in_array($parent, $model->manager_permissions)){echo 'checked="checked"';} ?> />
                                    <?php echo $parent; ?></label>
                                </li>
                                <?php 
                                if(is_array($children) && count($children) > 0) {
                                    foreach ($children as $child) { ?>
                                        <li class="child_<?= strtolower(str_replace(' ','_',$parent));?>" data-parent_name="<?= strtolower(str_replace(' ','_',$parent));?>" style="padding-left:40px;">
                                            <label style="font-weight:400">
                                            <input type="checkbox" name="permissions[]" class="child" value="<?php echo $child; ?>" <?php if(in_array($child, $model->manager_permissions)){echo 'checked="checked"';} ?> />
                                            <?php echo $label; ?></label>
                                        </li>      
                                    <? }
                                } ?>
                            <? } 
                            ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="files-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Files</h4>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>File Name</th>
                                    <th>File</th>
                                    <th>Download</th>
                                </tr>
                                </thead>
                                <tbody id="files-list">
                                    <? //var_dump($model->user->id); 
                                    $manager_id = $model->user->id;
                                    $sql = "SELECT * FROM files where id IN (SELECT file_id from account_files WHERE account_id = (SELECT account_id from users where role='manager' AND id=$manager_id));"; 
                                    // var_dump($sql);
                                    ?>
                                    <?php foreach(\Model\File::getList(['sql'=>$sql]) as $obj){ ?>
                                        <tr class="originalProducts">
                                            <td colspan="2"><?php echo $obj->name; ?></td>
                                            <td>                            
                                                <img src="https://sm2elearning.com/uploads/files/<?=$obj->id?>/<?=$obj->file?>">
                                            </td>
                                            <td>
                                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>managers/download/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>">
                                                    <i class="fas fa-arrow-alt-circle-down"></i> 
                                                </a>
                                            </td>
                                        </tr>
                                    <? } ?>
                                </tbody>
                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <input type="hidden" name="id" value="<?php echo $model->user->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <button type="submit" class="btn btn-save">Save</button>
  
</form>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });

    $("input.parent").on('change',function() {
        if($(this).is(":checked")) {
            var name = $(this).closest('li').data('name');
            $(".child_"+name).find('input.child').each(function(i,e) {
                $(e).prop('checked',true);
            });
        } else {
            var name = $(this).closest('li').data('name');
            $(".child_"+name).find('input.child').each(function(i,e) {
                $(e).prop('checked',false);
            });
        }
    });

    $("input.child").on('change',function() {
        var name = $(this).closest("li").data('parent_name');
        if($(this).is(":checked")) {
            $(".parent_"+name).find("input.parent").each(function(i,e) {
                $(e).prop('checked',true); 
            });
        } else {
            var empty = true;
            $(".child_"+name).find("input.child").each(function(i,e) {
                if ($(this).is(":checked")){
                    empty = false;
                } 
            });
            if (empty){
                $(".parent_"+name).find("input.parent").each(function(i,e) {
                    $(e).prop('checked',false); 
                });
            }
        }
    });
    $("select").multiselect();

});
</script>