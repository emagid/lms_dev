<div class="row filter_holder">
    <form id="filters">
        <div>
          Filter Managers by Account:
          <div class="custom-select">
            <select name="account">
                <option disabled selected>Select an Account</option>
                <? foreach($model->accounts AS $account) { ?>
                    <option value="<?=$account->id?>" <?= isset($_GET['account']) && $_GET['account'] == $account->id?'selected':'' ?>>
                        <?=$account->name?>
                    </option>
                <? } ?>
            </select>
          </div>
        </div>

        <div style="margin: 15px; margin-left: 0;">
            Show on page:
            <div class="custom-select">
              <select class="how_many" name="how_many" style="cursor:pointer">

                  <option <? if (isset($_GET['how_many'])) {
                      if ($_GET['how_many'] == 10) {
                          echo "selected";
                      }
                  } ?> selected value="10">10
                  </option>
                  <option <? if (isset($_GET['how_many'])) {
                      if ($_GET['how_many'] == 50) {
                          echo "selected";
                      }
                  } ?> value="50">50
                  </option>
                  <option <? if (isset($_GET['how_many'])) {
                      if ($_GET['how_many'] == 100) {
                          echo "selected";
                      }
                  } ?> value="100">100
                  </option>
                  <option <? if (isset($_GET['how_many'])) {
                      if ($_GET['how_many'] == 500) {
                          echo "selected";
                      }
                  } ?> value="500">500
                  </option>
                  <option <? if (isset($_GET['how_many'])) {
                      if ($_GET['how_many'] == 1000) {
                          echo "selected";
                      }
                  } ?> value="1000">1000
                  </option>
              </select>
          </div>
        </div>
    </form>

    <form>
      <div >
          <div class="custom-select">
              <label>Search:</label> 
              <input id="search" class="form-control" type="text" name="search" placeholder="Search by Name or Email"/>
          </div>
      </div> 
    </form>
</div> 

<?php
    // $model->students = $model->users;
if(count($model->managers)>0) { ?>
  <div class="box box-table">
    <table id="data-list" class="table">
      <thead>
        <tr>
          <th width="20%">Name</th>
          <th width="20%">Email</th>
          <th width="20%">Account</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->managers as $obj){ ?>
        <tr class="originalProducts">
         <td><a href="<?php echo ADMIN_URL; ?>managers/update/<?php echo $obj->id; ?>"><?php echo $obj->full_name(); ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>managers/update/<?php echo $obj->id; ?>"><?php echo $obj->email?></td>
         <td>
             <a href="<?php echo ADMIN_URL; ?>managers/update/<?php echo $obj->id; ?>">
             <? $account = \Model\Account::getItem($obj->account_id); ?>
                 <img src="https://sm2elearning.com/uploads/account/logo/<?=$account?$account->id:''?>/<?=$account?$account->logo:''?>">
             </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>managers/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>managers/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'students';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;

  $('#filters :input').change(function(e){
     $(this).parents('form').submit();
  });

  $("#search").keyup(function () {
        var url = "<?php echo ADMIN_URL; ?>managers/search";
        var keywords = $(this).val();
        if (keywords.length > 2) {
            $.get(url, {keywords: keywords}, function(data) {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').hide();
                $('.originalProducts').hide();

                var list = JSON.parse(data);

                for (key in list) {
                    var tr = $('<tr />');
                    var img = $('<img />').prop('src', "https://sm2elearning.com/uploads/account/logo/" + list[key].account_id + "/" + list[key].account_logo);
                    
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>managers/update/' + list[key].id).html(list[key].name));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>managers/update/' + list[key].id).html(list[key].email));
                    $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>managers/update/' + list[key].id).html(img));

                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>managers/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                    var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>managers/delete/' + list[key].id);
                    deleteLink.click(function () {
                        return confirm('Are You Sure?');
                    });
                    var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                    tr.appendTo($("#data-list tbody"));
                }

            });
        } else {
            $("#data-list tbody tr").not('.originalProducts').remove();
            $('.paginationContent').show();
            $('.originalProducts').show();
        }
    }).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
</script>

