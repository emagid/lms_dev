<?php
    if(count($model->questions)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="25%">Question</th>
          <th width="20%">Course Module</th>
          <th width="20%">Correct Answer</th>
          <th width="15%">Edit</th>
          <th width="10%">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->questions as $obj){ 
        $course_module = \Model\Course_Modules::getItem($obj->course_module_id); 
        $answer = \Model\Answers::getItem(null,['where'=>"question_id = $obj->id AND correct = '1'"]); ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>"><?php echo $obj->title; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>"><?php echo $course_module->title; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>"><?php echo $answer->title; ?></a></td>
        
         <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?= $obj->id ?>"><?php echo $obj->source; ?></a></td>
         <td>
            <a class="btn-actions" href="<?= ADMIN_URL ?>questions/update/<?= $obj->id ?>">
              <i class="icon-pencil"></i> 
            </a>
         </td>
         <td>
            <a class="btn-actions" href="<?= ADMIN_URL ?>questions/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
              <i class="icon-cancel-circled"></i> 
            </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
  var site_url = '<?= ADMIN_URL.'questions';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>