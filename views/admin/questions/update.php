<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
      	<li role="presentation" class="active"><a href="#questions-tab" aria-controls="questions" role="tab" data-toggle="tab">Questions</a></li>
      	<li role="presentation"><a href="#answers-tab" aria-controls="answers" role="tab" data-toggle="tab">Answers</a></li>
    </ul>

	<div class="tab-content">
    	<div role="tabpanel" class="tab-pane active" id="questions-tab">
			<div class="row">
				<div class="col-md-24">
					<div class="box">
						<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
							<input type="hidden" name="id" value="<?php echo $model->question->id;?>" />
							<h4>Questions</h4>
							 <div class="form-group">
								<label>Title</label>
								<?php echo $model->form->textAreaFor('title');?>
							</div>
							
							<div class="form-group">
								<label>Question Number</label>
								<?php echo $model->form->textBoxFor('number');?>
							</div>
							<div class="form-group">
								<label>Course Module</label>
								<? $course_module = \Model\Course_Modules::getItem($model->question->course_module_id); ?>
								<input type="hidden" name="course_module_id" value="<?php echo $model->question->course_module_id?>">
								<textarea disabled="disabled"><?=$course_module->title?></textarea>
							</div>
							<button type="submit" class="btn btn-save">Save</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane" id="answers-tab">
			<div class="row">
				<div class="col-md-24">
					<div class="box">
						<h4>Answers</h4>
						<table id="answers-container" class="table">
							<thead>
								<tr>
									<th>Answer</th>
									<th>Answer Number</th>
									<th>Correct Answer</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<? foreach($model->answers as $answer) { ?>
									<tr id="answer<?=$answer->id?>">
										<td><?= $answer->title ?></td>
										<td><?= $answer->number?></td>
										<? if($answer->correct == '1'){ ?>
											<td>Yes</td>
										<? } else { ?>
											<td>No</td>
										<? } ?>
										<td>
	                                        <a class="btn-actions edit_answer" data-answer="<?= $answer->id ?>" href="">
	                                            <i class="icon-pencil"></i>
	                                       </a>
	                                    </td>
	                                    <td>
	                                        <a class="btn-actions del_answer" data-answer="<?= $answer->id ?>" href="">
	                                            <i class="icon-cancel-circled"></i>
	                                       </a>
	                                    </td>
									</tr>
									<tr id="edit_answer<?=$answer->id?>" style="display: none">
										<form action="/admin/questions/update_answer" method="post" enctype="multipart/form-data">
											<input name="id" type="hidden" value="<?=$answer->id?>">
	                                      	<input name="question_id" type="hidden" value="<?=$model->question->id?>">
	                                      	<td>
	                                          <label>Answer</label>
	                                          <input type="text" name="title" placeholder="Answer" value="<?=$answer->title?>">
	                                      	</td>
	                                      	<td>
	                                          <label>Answer Number</label>
	                                          <input type="text" name="number" placeholder="Answer Number" value="<?=$answer->number?>">
	                                      	</td>
	                                      	<td>
	                                          <label>Correct</label>
	                                          <? $checked = ($answer->correct == '1') ? 'checked="checked"' : ''; ?>
	                                          <input type="checkbox" name="correct" value="1" <?php echo $checked; ?>>
	                                      	</td>
	                                      	<td>
	                                          <button type="submit" class="btn btn-save">Save</button>
	                                      	</td>
										</form>
									</tr>
								<? } ?>
								<tr>
									<form action="/admin/questions/update_answer" method="post" enctype="multipart/form-data">
										<input name="question_id" type="hidden" value="<?=$model->question->id?>">
										<td >
	                                        <label>New Answer Option</label>
	                                        <input type="text" name="title" placeholder="Answer">
	                                    </td>
	                                    <td>
	                                        <label>Answer Number</label>
	                                        <input type="text" name="number" placeholder="New Answer Number">
	                                    </td>
	                                    <td >
	                                        <label>Correct</label>
	                                        <?php echo $model->form->checkboxFor('correct',1);?>
	                                    </td>
	                                    <td>
                                      		<button type="submit" class="btn btn-save">Save</button>
                                  		</td>
									</form>
								</tr>
							</tbody>
						</table>			
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function() {
        $(".edit_answer").click( function(e) {
            e.preventDefault();
            $("#answer" + $(this).data('answer')).toggle();
            $("#edit_answer" + $(this).data('answer')).toggle();
        });
        $(".del_answer").click( function(e) {
            e.preventDefault();
            $("#answer"+$(this).data('answer')).remove();
            $.post('/admin/questions/delete_answer', {id: $(this).data('answer')}, function (response) {
                console.log(response);
            });
        });
    });
</script>