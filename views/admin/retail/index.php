<?php if(count($model->retail)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="70%">Address</th>
            <th width="70%">State</th>
            <th width="70%">City</th>
            <th width="70%">Zip</th>
            <th width="70%">Price/month</th>
            <th width="15%" class="text-center">Edit</th>
            <th width="15%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->retail as $retail): ?>
        <tr>
            <td><a href="<?php echo ADMIN_URL; ?>retail/update/<?php echo $retail->id; ?>"><?php echo $retail->address; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>retail/update/<?php echo $retail->id; ?>"><?php echo $retail->state; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>retail/update/<?php echo $retail->id; ?>"><?php echo $retail->city; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>retail/update/<?php echo $retail->id; ?>"><?php echo $retail->zipcode; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>retail/update/<?php echo $retail->id; ?>"><?php echo $retail->price_month; ?></a></td>
            <td class="text-center">
                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>retail/update/<?php echo $retail->id; ?>">
                 <i class="icon-pencil"></i>
                </a>
            </td>
            <td class="text-center">
                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>retail/delete/<?php echo $retail->id; ?>" onClick="return confirm('Are You Sure?');">
                 <i class="icon-cancel-circled"></i>
                </a>
            </td>
       </tr>
     <?php endforeach; ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'retail';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;
</script>

