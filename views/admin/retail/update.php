<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
	<input type="hidden" name="id" value="<?php echo $model->listing->id;?>" />
	<input type="hidden" name="type" value="<?php echo \Model\Listing::COMMERCIAL;?>" />
	<div class="row">
		<div class="col-md-8">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Address</label>
					<?php echo $model->form->editorFor("address"); ?>
				</div>
				<div class="form-group">
					<label>City</label>
					<?php echo $model->form->editorFor("city"); ?>
				</div>
				<div class="form-group">
					<label>Zipcode</label>
					<?php echo $model->form->editorFor("zipcode"); ?>
				</div>
				<div class="form-group">
					<label>State</label>
					<?php echo $model->form->editorFor("state"); ?>
				</div>
				<div class="form-group">
					<label>Square feet</label>
					<input type="text" name="sq" value="<?=$model->listing->sq?:0?>">
				</div>
				<div class="form-group">
					<label>Use for</label>
					<?php echo $model->form->editorFor("use_for"); ?>
				</div>

				<!--for residential only-->
				<div class="form-group">
					<label>Bedroom</label>
					<input type="text" name="bedroom" value="<?=$model->listing->bedroom?:0?>">
				</div>
				<div class="form-group">
					<label>Bathroom</label>
					<input type="text" name="bathroom" value="<?=$model->listing->bathroom?:0?>">
				</div>
				<div class="form-group">
					<label>Condo fee</label>
					<input type="text" name="condo_fee" value="<?=$model->listing->condo_fee?:0?>">
				</div>
				<!-- -->

				<div class="form-group">
					<label>Description</label>
					<?php echo $model->form->editorFor("description"); ?>
				</div>

				<div class="form-group">
					<label>Price Daily</label>
					<input type="text" name="price_day" value="<?=$model->listing->price_day?:0?>">
				</div>
				<div class="form-group">
					<label>Price Weekly</label>
					<input type="text" name="price_week" value="<?=$model->listing->price_week?:0?>">
				</div>
				<div class="form-group">
					<label>Price Monthly</label>
					<input type="text" name="price_month" value="<?=$model->listing->price_month?:0?>">
				</div>

				<div class="form-group">
					<label>Security Deposit</label>
					<input type="text" name="deposit" value="<?=$model->listing->deposit?:0?>">
				</div>
				<!--div class="form-group checkbox">
					<label>
						<?php echo $model->form->checkBoxFor("status", 1); ?> Active?
					</label>
				</div-->
			</div>
		</div>
		<div class="col-md-8">
			<div class="box">
				<h4>Information</h4>
				<div class="form-group">
					<label>Building Name</label>
					<?php echo $model->form->editorFor("building_name"); ?>
				</div>
				<div class="form-group">
					<label>Access Type</label>
					<?php echo $model->form->editorFor("access_type"); ?>
				</div>
				<div class="form-group">
					<label>Payout</label>
					<?php echo $model->form->editorFor("payout"); ?>
				</div>
				<div class="form-group">
					<label>Company</label>
					<?php echo $model->form->editorFor("company"); ?>
				</div>
				<div class="form-group">
					<label>Contact Name</label>
					<?php echo $model->form->editorFor("contact_name"); ?>
				</div>
				<div class="form-group">
					<label>Contact Phone Number</label>
					<?php echo $model->form->editorFor("contact_phone"); ?>
				</div>
				<div class="form-group">
					<label>Company Name</label>
					<?php echo $model->form->editorFor("company_name"); ?>
				</div>
				<div class="form-group">
					<label>Company Phone</label>
					<?php echo $model->form->editorFor("company_phone"); ?>
				</div>
				<div class="form-group">
					<label>Listing Id</label>
					<?php echo $model->form->editorFor("listing_id"); ?>
				</div>
				<div class="form-group">
					<label>Unit Num</label>
					<?php echo $model->form->editorFor("unit_num"); ?>
				</div>
				<div class="form-group">
					<label>Open House</label>
					<?php echo $model->form->editorFor("open_house"); ?>
				</div>
				<div class="form-group">
					<label>Close Open House</label>
					<?php echo $model->form->editorFor("close_house"); ?>
				</div>
				<div class="form-group">
					<label>Availability</label>
					<?php echo $model->form->editorFor("availability"); ?>
				</div>
				<div class="form-group">
					<label>Available Immediately</label>
					<?php echo $model->form->editorFor("available_immediately"); ?>
				</div>
				<div class="form-group">
					<label>Status</label>
					<?php echo $model->form->editorFor("status"); ?>
				</div>
				<div class="form-group">
					<label>Active Listing</label>
					<?php echo $model->form->editorFor("active_listing"); ?>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="box">
				<div class="form-group">
					<h4>Image</h4>
					<!--					<p><small>(ideal featured image size is 1920 x 300)</small></p>-->
					<!--					--><?php //
					//					$img_path = "";
					//					if($model->banner->image != ""){
					//						$img_path = UPLOAD_URL . 'banners/' . $model->banner->image;
					//					}
					//					?>
					<!--					<p><input type="file" name="image" class='image' /></p>-->
					<!--					--><?php //if($model->banner->image != ""){ ?>
					<!--					<div class="well well-sm pull-left">-->
					<!--						<div id='image-preview'>-->
					<!--							<img src="--><?php //echo $img_path; ?><!--" width="100" height="100" />-->
					<!--							<br />-->
					<!--							<a href=--><?//= ADMIN_URL.'banners/delete_image/'.$model->banner->id;?><!-- class="btn btn-default btn-xs">Delete</a>-->
					<!--						</div>-->
					<!--					</div>-->
					<!--					--><?php //} ?>
					<div id='preview-container'></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>
<link rel = "stylesheet" type = "text/css" href = "<?=ADMIN_CSS.'jquery.datetimepicker.css'?>">
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>
<script type='text/javascript'>
	$(document).ready(function() {

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container").html(img);


					//$('#previewupload').attr('src', e.target.result);
					//$("a.passport-img").attr("href", e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
			$('#previewupload').show();
		});
		$('input[name=open_house],input[name=close_house],input[name=availability]').datetimepicker();

	});

</script>