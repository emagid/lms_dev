<?php if(count($model->newsletters)>0) { ?>
<div class="box box-table">
  <table class="table">
    <thead>
      <tr>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
     <?php foreach($model->newsletters as $newsletter): ?>	
      <tr>
       <td><?php echo $newsletter->email; ?></td>
     </tr>
   <?php endforeach; ?>
 </tbody>
</table>
<div class="box-footer">
	<div class='paginationContent'></div>
</div>
</div>
<?php };?>

<?= footer();?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'newsletter';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>