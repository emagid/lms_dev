<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general"  role="tab" data-toggle="tab">General</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Accouunt Category/Type</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <input type="hidden" name="id" value="<?php echo $model->account_category->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <button type="submit" class="btn btn-save">Save</button>
  
</form>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'account_categories/');?>;

    $('#categories').removeClass('active');
</script>