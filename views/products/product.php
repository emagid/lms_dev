<?//dd(\Model\Product_Image::getList(['where'=>"product_id=".$model->product->id,'orderBy'=>'display_order','sort'=>'DESC']));?>
<div class="product">
		<div class="container container_inner">
			<div class="row">
				<div class="col-sm-14">

					<ol class="breadcrumb">
						<li><a href="<?=SITE_URL?>">Home</a></li>
						<li><a href="<?=SITE_URL?>collections">Collections</a></li>
						<li class="active"><?php echo $model->categories[0]->name ?></li>
						<li class="active"><?php echo $model->product->alias!=''?$model->product->alias:$model->product->name; ?></li>
					</ol>

					<div class="flex-product">
						<div id="slider" class="flexslider product_mainslider">
<!--						<div id="slider" class="flexslider">-->
							<ul class="slides">
								<li>
									<?php $images = \Model\Product_Image::getList(['where'=>"product_id=".$model->product->id,'orderBy'=>'display_order','sort'=>'DESC']);
							             	$img_path = UPLOAD_URL . 'products/' . $model->product->featuredImage();
							        ?>
								    <img src="<?php echo $img_path; ?>" />
								</li>
								<?if(count($images) > 0){
									foreach($images as $image){?>
											<?php
											$img_path3 = UPLOAD_URL . 'products/' . $image->image;
											?>
										<li><img src="<?php echo $img_path3; ?>" /></li>
									<?}?>
								<?}?>
							</ul>
						</div>

						<div id="carousel" class="flexslider product_subslider">
<!--						<div id="carousel" class="flexslider">-->
							<ul class="slides">
								<li><img src="<?php echo $img_path; ?>" /></li>
								<?if(count($images)>0){?>
								<?foreach ($images as $p_i) {?>
									<?php $img_path3 = $p_i->image;?>
									<?php $img_path3 = UPLOAD_URL . 'products/' . $p_i->image;?>
								   <li><img src="<?php echo $img_path3; ?>" /></li>
								<?}?>
								<?}?>
							</ul>
						</div>
					</div> <!-- .flex-product -->
				</div> <!-- .col-sm-14 -->

				<div class="col-sm-1"></div>
				<div class="col-sm-9">
					<div class="right">
						<p class="brand">
							<a href=""><?php echo $model->product->name; ?></a>
						</p>
						<!-- <h1>devon Tread 1A</h1> -->
<!--						<p class="item-n">MPN No. --><?php //echo $model->product->mpn; ?><!--</p>-->
						 
						<p class="price">$<?if(isset($_SESSION['price'])){
							if($_SESSION['id_product']==$model->product->id){
								echo number_format($_SESSION['price'],2);
							}else{
								echo number_format($model->product->price,2);
							}}else{echo number_format($model->product->price,2);}?></p>
						<h3>Product Details</h3>
						<select id="color" name="color">
							<? $colors = json_decode($model->product->color);
							foreach($colors as $color){?>
								<option value="<?=$color?>"><?=\Model\Color::getItem($color)->name?></option>
							<?}?>
						</select>
						<select id="size" name="size">
							<? $sizes = json_decode($model->product->size);
							foreach($sizes as $size){?>
								<option value="<?=$size?>"><?=$size?></option>
							<?}?>
						</select>
						<p id="wire_blurb">"Wire and Save 2% on purchase price"</p>
						<p id="wire_blurb"><?php $final_sale = $model->product->final_sale; If ($final_sale==1){echo 'This is final sale!';} else{}?></p>
						 
						<dl class="dl-horizontal">
							<dt><?=isset($model->hottestDeal) && $model->hottestDeal != ""? 'Hottest Price':'Our Price';?></dt>

							<dd>$<?if(isset($_SESSION['price'])){
							if($_SESSION['id_product']==$model->product->id){
								echo number_format($_SESSION['price'],2);
							}else{
								echo number_format($model->product->price,2);
							}}else{echo number_format($model->product->price,2);}?></dd>
							<?if(!isset($_SESSION['price'])){?>
							<? if (!is_null($model->product->msrp) && $model->product->msrp > 0) { ?>
								<dt>Retail</dt>
								<dd class="retail">$<?=number_format($model->product->msrp,2); ?></dd>
								<dt>Savings</dt>
								<dd>$<?=($model->product->msrp > 0)?number_format($model->product->msrp-$model->product->price,2):0?></dd>
							<? } ?>
							<?}?>
							<dt>Shipping</dt>
							<dd>Free</dd>
						</dl>
						<p class="ships"><?php $availability = $model->product->availability;
						If ($availability==1){echo 'Normally ships in 1 business day';}
						elseif ($availability==2){echo "Normally ships in 5-7 business days!";}
						else{echo "Discontinued";}?></p>
						
						<?if ($availability==0){?>
						<?}else{?>
						<a id="btn-add-cart" class="btn btn-primary btn-cart">Add to Cart</a>
						<?}?>
						<ul class="list-unstyled help">
							<li>
								<a href="javascript:void(0);" onclick="olark('api.box.expand')" alt="Live Chat"><img src="/content/frontend/img/i-p-chat.png" alt="Live Chat" />Live Chat</a>
							</li>
							<li>
								<? if(\Emagid\Core\Membership::isAuthenticated()) { ?>
									<a href="#" data-toggle="modal" id="wishlist-add" data-target="#wishlist"><img src="/content/frontend/img/i-p-wishlist.png" alt="Add to Wishlist" />Add to Wishlist</a> 
								<? } else { ?>
									<a href="#" data-toggle="modal" data-target="#login"><img src="/content/frontend/img/i-p-wishlist.png" alt="Add to Wishlist" />Add to Wishlist</a> 
								<? } ?>
							</li>
							<li>
								<a href="#" data-toggle="modal" data-target="#question"><img src="/content/frontend/img/i-p-ask.png" alt="Ask a Question" />Ask a Question</a>
							</li>
							<li>
								<a href="#" data-toggle="modal" data-target="#price_alert"><img src="/content/frontend/img/i-p-ask.png" alt="Ask a Question" />Price alert</a>
							</li>
						</ul>

						<div class="clearfix"></div>
						<h3 class="share">Share</h3>
						<ul class="list-unstyled list-inline social">
							<!-- Facebook -->
							<li>
								<a href="javascript:fbshareCurrentPage()" target="_blank" alt="Share on Facebook">
									<img src="/content/frontend/img/i-fb.jpg" alt="">
								</a>
								<script language="javascript">
								    function fbshareCurrentPage()
								    {window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(window.location.href)+"&t="+document.title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false; }

								</script>
							</li>
							

							<!-- Twitter -->
<!--							 <li>-->
<!--								<a href="https://twitter.com/share" class="twitter-share-button" data-text="Check out this hot watch!" data-count="none" data-hashtags="modernvice">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>-->
<!--							</li>-->
							<li>
								<!--TODO st-dev share on Twitter-->
								<a href="https://twitter.com/share?url=http%3A%2F%2Fbeta.{modernvice}.com%0A&text=Check%20out%20the%20<?php echo $model->product->name; ?>%20at%20beta.{modernvice}.com&hashtags={modernvice}" target="_blank" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false">
									<img src="/content/frontend/img/i-twitter.jpg" alt="">
								</a>
							</li>

							
							<!-- Google Plus -->
<!--							 <li>-->
<!--								<a id="ref_gp" href="https://plus.google.com/share?url=beta.{modernvice}.com"-->
<!--									onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false">-->
<!--								</a>-->
<!--							</li>-->
							
							<!-- Pinterest -->
							<li>
								<!--TODO st-dev share on Pinterest-->
								<a href="http://pinterest.com/pin/create/button/?url=http://beta.{modernvice}.com&amp;media=<?php echo $img_path; ?>&amp;description=<?php echo $model->product->name; ?>" class="pin-it-button" count-layout="none" target="blank" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false">
									<img src="/content/frontend/img/i-pinterest1.png" alt="">
								</a>
							</li>

							<!-- Email -->
							<li>
								<!--TODO st-dev share with email-->
								<a href="mailto:?Subject=Great Product at Modern Vice!&body= Checkout the <?php echo $model->product->name; ?> at www.modernvice.com" target="_top">
									<img src="/content/frontend/img/i-email.png" alt="">
								</a>
							</li>

						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
				<br /><br /><br />
				<div class="col-sm-24">
					<div role="tabpanel">

						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-justified" role="tablist">
							<li role="presentation"  class="active"><a href="#details" aria-controls="home" role="tab" data-toggle="tab">Details</a></li>
<!--							<li role="presentation"><a href="#shipping" aria-controls="profile" role="tab" data-toggle="tab">Shipping</a></li>-->
<!--							<li role="presentation"><a href="#guarantee" aria-controls="messages" role="tab" data-toggle="tab">Guarantee</a></li>-->
							<li role="presentation"><a href="#reviews" aria-controls="settings" role="tab" data-toggle="tab">Reviews</a></li>
							<li role="presentation"><a href="#questions" aria-controls="settings" role="tab" data-toggle="tab">Questions</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="details">
								<?=$model->product->description?>
								<?$length = $model->product->length;$width = $model->product->width;$height = $model->product->height;
								if($length > 0 && $width > 0 && $height > 0){echo '<p>'.$length.'" x '.$width.'" x '.$height.'"</p>';}?>
							</div>
<!--							<div role="tabpanel" class="tab-pane" id="shipping">-->
<!--								--><?php //echo \Model\Page::getItem(12)->description ?>
<!--							</div>-->
<!--							<div role="tabpanel" class="tab-pane" id="guarantee">-->
<!--								--><?php //echo \Model\Page::getItem(11)->description ?>
<!--							</div>-->
							<div role="tabpanel" class="tab-pane" id="reviews">
								<p><b>TOP NOTCH!</b> <i>Review by Frank</i><repeat n="5">★★★★★</repeat></p>
								<p>Extremely great gift, even if it was for myself, lol. (Posted on 11/15/14)</p>
							</div>
							<div role="tabpanel" class="tab-pane" id="questions">
								<p class="cust_question">Will my watch come with a box?</p>
								<p class="cust_answer">All watches include their original manufacturer’s box and serial numbers.</p>
								<ul>
									<?php foreach ($model->product_questions as $question) {?>
										<li><?=$question->insert_time?></li>
											<ul>
												<li><?=$question->subject?></li>
												<li><?=$question->text_question?></li>
											</ul>
									<? } ?>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

<? if (isset($model->similar_products) && count($model->similar_products) > 0) { ?>
		<div class="similar">
			<div class="container container_inner">
				<h4>Similar Products</h4>
			</div>
			<hr />
			<style>
			.flex-direction-nav {
     
}
			.flex-viewport{
				z-index: 10000;
			}
			</style>
			<div class="container">
				<div class="flexslider flex-similar simproduct_slider">
					<ul class="slides">
						<? foreach($model->similar_products as $similar_product) { ?>
						<li>
							<a href="<?=SITE_URL?>products/<?=$similar_product->slug?>">
								<?php 
						            $img_path = ADMIN_IMG.'modernvice_shoe.png';//TODO st-dev default product image
						            if(!is_null($similar_product->featured_image) && $similar_product->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$similar_product->featured_image)){ 
						             	$img_path = UPLOAD_URL . 'products/' . $similar_product->featured_image;
						            }
						        ?>
								<img src="<?php echo $img_path; ?>" />
								<h5><?php echo $similar_product->name; ?></h5>
								<span><?php echo $similar_product->mpn; ?></span>
								<dd>$<?php echo $similar_product->price; ?></dd>
							</a>
						</li>
						<? } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<? } ?>

<!-- wishlist -->
<? if(\Emagid\Core\Membership::isAuthenticated()) { ?>
<div class="modal fade" id="wishlist" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title">This item has been added to your wishlist!</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-10">
						<a href="<?=SITE_URL?>user/wishlist" class="btn btn-primary btn-block">Wishlist</a>
					</div>
					<div class="col-sm-14">
						<a href="#" class="btn btn-default btn-block" data-dismiss="modal">Continue Shopping</a>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<? } ?>
<!-- Modal - Add to Wishlist/My Account(Trigger) -->
<!-- 	<div class="modal fade" id="wishlist" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Log In</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="loginemail" class="sr-only">Email address</label>
                            <input type="email" class="form-control" id="loginemail" placeholder="Email address *">
                        </div>
                        <div class="form-group">
                            <label for="loginpass" class="sr-only">Password</label>
                            <input type="password" class="form-control" id="loginpass" placeholder="Password *">
                        </div>
                        <div class="form-group">
                            <div class="pull-left">
                                <label>
                                    <input type="checkbox"> Remember Me
                                </label>
                            </div>
                            <div class="pull-right">
                                <a href="#">Forgot your password?</a>
                            </div>
                        </div>
                        <br /><br /><br /> -->
                        <!-- Remove data-dismiss once login/create account is created -->
<!--                         <div class="form-group">
                            <a href="#" class="btn btn-primary btn-block" data-dismiss="modal">Log In</a>
                        </div>
                        <a href="#" class="btn btn-default btn-block" data-dismiss="modal">Create Account</a>
                    </form>
                </div>
            </div>
        </div>
    </div> -->

<!-- Modal - Ask A Question -->
    <div class="modal fade" id="question" tabindex="-1" role="dialog" aria-labelledby="question" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Customer Inquiry</h4>
                    <hr />
                </div>
                <div class="modal-body">
                    <!-- <div class="inner">
                        <p>Up To</p> 
                        <span class="number">
                            70
                            <span class="right">
                                <span class="top">%</span>
                                <span class="bottom">Off</span>
                            </span>
                        </span>
                    </div> -->
                    <form action="<?= SITE_URL ?>products/addQuestion/" method="post" >
                    	<input type="hidden" name="product_id" value="<?=$model->product->id?>" />
                        <div class="form-group ask_question">
                        	<p>Please enter question below.</p>
                            <label for="question" class="sr-only">Question</label>
                            <input type="text" class="form-control" id="submitquestion" placeholder="Subject" name="subject">
                            <textarea class"text_question" rows="10" placeholder="Question" name="text_question"></textarea>
                        </div>
						<div class="reply-group">
							<div class="form-group reply_box">
								<input type="checkbox" name="reply" value="email" checked> Reply to my email: 
							</div>
							<div class="form-group reply_email">
								<input type="email" class="form-control" id="submitquestion" placeholder="Enter Email" name="reply_email">
							</div>
							<div class="form-group reply_email">
								Type captcha: <img src="/products/captcha"/>
								
								<input type="text" class="form-control" id="submitquestion" placeholder="Enter captcha" name="captcha">
							</div>
							 
						</div>
						

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </div>
                        <div class="text-center">
                            <p>We will do our best to respond to your question within 24 hours. Thank You!</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal - Ask A price alert -->
    <div class="modal fade" id="price_alert" tabindex="-1" role="dialog" aria-labelledby="question" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Please alert me if the price drops under this amount</h4>
                    <hr />
                </div>
                <div class="modal-body">
                    <!-- <div class="inner">
                        <p>Up To</p> 
                        <span class="number">
                            70
                            <span class="right">
                                <span class="top">%</span>
                                <span class="bottom">Off</span>
                            </span>
                        </span>
                    </div> -->
                    <form action="<?= SITE_URL ?>products/addPriceAlert/" method="post" >
                    	<input type="hidden" name="product_id" value="<?=$model->product->id?>" />
                        <div class="form-group ask_question">
                        	<p>Please enter your price below.</p>
                            <label for="question" class="sr-only">Your price</label>
                            <input type="text" class="form-control" id="submitquestion" placeholder="Price" name="price">
                        </div>
                        <?if(!isset($model->user)){?>
                        <div class="form-group ask_question">
                        	 <p>What is your name?</p>
                            <label for="question" class="sr-only">Your name</label>
                            <input type="text" class="form-control" id="submitquestion" placeholder="Jimmy" name="name">
                        </div>
                        <?}?>
						<div class="reply-group">
							<div class="form-group reply_box">
								 Reply to my email: 
							</div>
							<div class="form-group reply_email">
								<input type="email" class="form-control" id="submitquestion" placeholder="Enter Email" name="email" value="<?if(isset($model->user)){echo $model->user->email; } ?>">
							</div>
						</div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </div>
                        <div class="text-center">
                            <p> Thank You!</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php footer(); ?>

<script>
$(document).ready(function(){
	$('#btn-add-cart').on('click',function(){
		var productId = <?=$model->product->id?>;
		var colorId = $('#color').find(':selected').val();
		var sizeId = $('#size').find(':selected').val();

		var data = {
			colorId: colorId,
			sizeId: sizeId
		};
		$.post('/cart/add/'+productId, data, function(data){
			console.log(data);
			var json = $.parseJSON(data);
			if(json.status == 'success'){
				window.location.replace('/cart');
			} else {
				alert(json.message);
			}
		})
	});
	$('.zoom-wrap').zoom();
//	$('.zoom-wrap').wrap('<span style="display:inline-block"></span>').css('display', 'block').parent().zoom();
});

$(function(){
	$('#wishlist-add').click(function(e){
		$.ajax({
			'type':'get',
			'url':'<?=SITE_URL?>user/wishlist_add/<?=$model->product->id?>'
		})
	})
})

// function updateCartInfo(quantity, total){
// 	$('#cartData').html(quantity + ' ITEMS | $' + total);
// }

// $(function(){
// 	$('#btn-add-cart').click(function(){
// 		$.ajax({
// 			url: '<?=SITE_URL?>cart/add',
// 			method: 'POST',
// 			data: { product_id: '<?=$model->product->id;?>' },
// 			success: function(data) {
// 				data = JSON.parse(data);
// 				cart = data.cart;
// 				updateCartInfo(cart.quantity, cart.total);
// 			}
// 		});
// 	});
// });

</script>
