<div class="banner default" style="background-image:url('content/frontend/img/banner_brands.jpg');">
	<div class="container">
		<div class="inner">
			<h1>Shop By Brand</h1>
		</div>
	</div>
</div>


<div class="container">
	<div class="alphabet">
		<ul class="list-unstyled list-inline text-center my_filters">
			<li data-type="A"><a href="#">A</a></li>
			<li><a href="#">B</a></li>
			<li><a href="#">C</a></li>
			<li><a href="#">D</a></li>
			<li><a href="#">E</a></li>
			<li><a href="#">F</a></li>
			<li><a href="#">G</a></li>
			<li><a href="#">H</a></li>
			<li><a href="#">I</a></li>
			<li><a href="#">J</a></li>
			<li><a href="#">K</a></li>
			<li><a href="#">L</a></li>
			<li><a href="#">M</a></li>
			<li><a href="#">N</a></li>
			<li><a href="#">O</a></li>
			<li><a href="#">P</a></li>
			<li><a href="#">Q</a></li>
			<li><a href="#">R</a></li>
			<li><a href="#">S</a></li>
			<li><a href="#">T</a></li>
			<li><a href="#">U</a></li>
			<li><a href="#">V</a></li>
			<li><a href="#">W</a></li>
			<li><a href="#">X</a></li>
			<li><a href="#">Y</a></li>
			<li><a href="#">Z</a></li>
		</ul>
	</div>
</div>



	<div class="container">
		<div class="brands">
			<h2>Watches by Brand</h2>
				<ul class="my_filter_items">
				<!-- <li><a href=""><img src="frontend/img/b1.jpg" alt=""><span>AQUANAUTIC AQUANAUTIC AQUANAUTIC AQUANAUTIC AQUANAUTIC</span></a></li>
				<li><a href=""><img src="frontend/img/b2.jpg" alt="">3 H</a></li>
				<li><a href=""><img src="frontend/img/b3.jpg" alt="">ALPINA</a></li>
				<li><a href=""><img src="frontend/img/b5.jpg" alt="">AQUANAUTIC</a></li>
				<li><a href=""><img src="frontend/img/b6.jpg" alt="">AZIMUTH</a></li>
				<li><a href=""><img src="frontend/img/b7.jpg" alt="">B.R.M.</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BALL</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BAUME-MERCIER</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BEDAT</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BELL &amp; ROSS</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BERTOLUCCI</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BLU</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BREITLING</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BULOVA</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">BULOVA ACCUTRON</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">TB BUTI</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">CARL F. BUCHERER</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">CHARRIOL</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">CHRONOSWISS</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">CLAUDE BERNARD</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">CLERC</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">CORUM</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">DANIEL ROTH</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">DANIEL WELLINGTON</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">DEGRISOGONO</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">DEVON</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">EBEL</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">EBERHARD</a></li>
				<li><a href=""><img src="frontend/img/b1.jpg" alt="">PRE-OWNED WATCHES</a></li> -->
			
				<?php foreach ($model->brands as $brand) {?>
						<li>
							<a href="<?=SITE_URL.'brand/'.$brand->slug?>"><span><?=$brand->name?></span></a>
						</li>
				<?php } ?>

				</ul>	
		</div>	
	</div>
</div>
