<section class='account_page accounts_page'>
    
<!--     <section class='hero account_hero'>
        <h1>SCHOOL NAME</h1>
    </section> -->


    <section class='filters_holder'>
        <div class='filters'>
            <div class='account_stats'>
              <div>
                <p class='amount'>545</p>
                <p>STUDENTS</p>
              </div>
              <div>
                <p class='amount'>83</p>
                <p>COURSES</p>
              </div>
            </div>
          </div>
    </section>

    <section class='account_info'>
      <h2>ACCOUNT INFO</h2>

      <div class='form_holder'>
        <div class='form register_holder'>
            <form method="POST" action="/account/register">
                <input class='input half' type="text" name="contact_name" placeholder='Name'>
                <input class='input half' type="text" name="email" placeholder='Email'>
                <input class='input half' type="password" name="password" placeholder='Password'>
                <input class='input half' type="password" name="confirm_password" placeholder='Confirm Password'>
                <input class='input half' type="text" name="contact_phone" placeholder='Phone'>
                <input class='input half' type="text" name="name" placeholder='School Name'>
                <textarea class='input full' type="textarea" wrap="hard" name="contact_available_hours" placeholder='Hours'></textarea>

                <div class='upload_button half'>
                    <input class='upload_field' id="uploadFile" placeholder="Upload Logo" disabled="disabled" />
                    <div class="fileUpload button">
                        <span>Upload</span>
                        <input id="uploadBtn" type="file" class="upload" />
                    </div>
                </div>

                <div class='upload_button half'>
                    <input class='upload_field' id="uploadFile" placeholder="Upload School Image" disabled="disabled" />
                    <div class="fileUpload button">
                        <span>Upload</span>
                        <input id="uploadBtn" type="file" class="upload" />
                    </div>
                </div>


                <div class='adding_pics'>
                    <!-- <label>Certificate</label> -->
                    <input class='input half' type="text" name="certificate_signature_title" placeholder='Certificate title'>
                    <input class='input half' type="text" name="certificate_signature_name" placeholder='Certificate Name'>
                    <div class='upload_button'>
                        <input class='upload_field' id="uploadFile" placeholder="Upload School Image" disabled="disabled" />
                        <div class="fileUpload button">
                            <span>Upload</span>
                            <input id="uploadBtn" type="file" class="upload" />
                        </div>
                    </div>
                <div class='upload_button'>
                        <input class='upload_field' id="uploadFile" placeholder="Upload School Image" disabled="disabled" />
                        <div class="fileUpload button">
                            <span>Upload</span>
                            <input id="uploadBtn" type="file" class="upload" />
                        </div>
                    </div>
                </div>
                <input class='button' type="submit" value='UPDATE'>
            </form>
        </div>
    </div>
    </section>

</section>