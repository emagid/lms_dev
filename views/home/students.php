<section class='students_page account_page'>
    
<!--     <section class='hero students_hero'>
        <h1>STUDENTS</h1>
    </section> -->

    


    <section class='filters_holder'>
        <div class='filters'>
            <div class='select'>
                <span class="arr"></span>
                <select>
                    <option>All Years</option>
                    <option>1st Year</option>
                    <option>2nd Year</option>
                    <option>3rd Year</option>
                    <option>4th Year</option>
                    <option>5th Year</option>
                </select>
            </div>

            <form class='search'>
                <input type="text" name="search" placeholder='Search by email or name'>
                <input class='submit' type="submit" value='search'>
            </form>

            <div>
              <a style='margin-left: 50px; height: 52px !important;' href="<?= SITE_URL ?>account/index"><button class='button'>UPLOAD</button></a>
            </div>
        </div>
    </section>

 <!--    <section class='hero'>
      <h1>STUDENTS</h1>
    </section> -->


    <section class='table_holder'>
        <table class='general_table head_table'>
            <thead>
                <tr>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Year</p></td>
                    <td><p>Edit</p></td>
                    <td><p>Delete</p></td>
                </tr>
            </thead>
          </table>

          <table class='general_table'>
            <tbody>
                <tr class='added'>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Year</p></td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                </tr>
                <tr  class='updated'>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Year</p></td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                </tr>
                <tr  class='removed'>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Year</p></td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                </tr>
                <tr>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Year</p></td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="icon-pencil"></i> 
                       </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>

</section>