<section class='account_page courses_page filter_hide'>
    
<!--     <section class='hero courses_hero'>
        <h1>COURSES</h1>
    </section> -->
<!--     <section class='hero'>
      <h1>COURSES</h1>
    </section> -->

    <section class='filters_holder'>
        <div class='filters'>
            <div class='select'>
                <span class="arr"></span>
                <select>
                    <option>All Year Levels</option>
                    <option>1st Year Level</option>
                    <option>2nd Year Level</option>
                    <option>3rd Year Level</option>
                    <option>4th Year Level</option>
                    <option>5th Year Level</option>
                </select>
            </div>

            <form class='search'>
                <input type="text" name="search" placeholder='Search by email or name'>
                <input class='submit' type="submit" value='search'>
            </form>
        </div>
    </section>


    <section class='table_holder'>
        <table class='general_table head_table'>
            <thead>
                <tr>
                    <td><p>Title</p></td>
                    <td><p># of Modules</p></td>
                    <td><p>Description</p></td>
                    <td><p>Edit</p></td>
                    <td><p>Delete</p></td>
                </tr>
            </thead>
          </table>

        <table class='general_table'>
            <tbody>
                <? $obj = (Object)['id'=>5]?>
                <tr>
                    <td><p>Title</p></td>
                    <td><p># of Modules</p></td>
                    <td><p>Description</p></td>
                    <td>
                       <a class="btn-actions" href="<?php echo SITE_URL; ?>courses/update/<?php echo $obj->id; ?>">
                            <i class="fas icon-pencil fa-pencil-alt"></i>
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="<?php echo SITE_URL; ?>courses/update/<?php echo $obj->id; ?>">
                           <i class="fas fa-times-circle"></i>
                       </a>
                    </td>
                </tr>
                <tr>
                    <td><p>Title</p></td>
                    <td><p># of Modules</p></td>
                    <td><p>Description</p></td>
                    <td>
                       <a class="btn-actions" href="<?php echo SITE_URL; ?>courses/update/<?php echo $obj->id; ?>">
                           <i class="fas icon-pencil fa-pencil-alt"></i>
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="<?php echo SITE_URL; ?>courses/update/<?php echo $obj->id; ?>">
                           <i class="fas fa-times-circle"></i>
                       </a>
                    </td>
                </tr>
                <tr>
                    <td><p>Title</p></td>
                    <td><p># of Modules</p></td>
                    <td><p>Description</p></td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="fas icon-pencil fa-pencil-alt"></i>
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="">
                           <i class="fas fa-times-circle"></i>
                       </a>
                    </td>
                </tr>
                <tr>
                    <td><p>Title</p></td>
                    <td><p># of Modules</p></td>
                    <td><p>Description</p></td>
                    <td>
                       <a class="btn-actions" href="<?php echo SITE_URL; ?>courses/update/<?php echo $obj->id; ?>">
                           <i class="fas icon-pencil fa-pencil-alt"></i>
                       </a>
                    </td>
                    <td>
                       <a class="btn-actions" href="<?php echo SITE_URL; ?>courses/update/<?php echo $obj->id; ?>">
                           <i class="fas fa-times-circle"></i>
                       </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>

</section>