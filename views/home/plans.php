<section class='plans_page'>
    <? if( isset($model->user) && $model->user->id != 0  ) {?>
        <section class='filters_holder'>
        <div class='filters'>
            <div class='account_stats' style='font-size: 17px; margin-bottom: 25px;'>
                <p>YOU CURRENTLY HAVE THE GOLD PLAN</p>
                <img src="<?= FRONT_IMG ?>../img/gold.png">
            </div>
        </div>
        </section>

        <section class='my_plan'>
            <div class='my_plan_holder'>
                <div class='curr_users'>
                    <p>TOTAL USERS</p>
                    <p class='total_users'>326 <span>/ 600</span></p>
                    <p style='opacity: .5;'>Your plan allows 600 max users per month.</p>
                    <button class='button change_plan'>CHANGE PLAN</button>
                </div>
                <div class='billing_plan'>
                    <h3>YOUR BILLING INFORMATION</h3>
                    <form>
                        <div class='form-group'>
                            <label>Full Name</label>
                            <input type="text" name="full_name">
                        </div>
                        <div class='form-group'>
                            <label>Card Number</label>
                            <input type="text" name="card_number">
                        </div>
                        <div class='form-group'>
                            <label>Epiration Date</label>
                            <input type="text" name="expiration_date">
                        </div>
                        <div class='form-group'>
                            <label>CCV</label>
                            <input type="text" name="ccv">
                        </div>
                        <input type="sybmit" class='button' value='UPDATE'>
                        <!-- <input type="" name=""> -->
                    </form>
                </div>
            </div>
        </section>
    <? } ?>

    <div class='plans_holder'>
        <? $icons = ['bronze','silver','gold'];?>
        <? foreach(\Model\Plan::getList(['where'=>'display = 1']) AS $i => $plan) { ?>
            <div class='plan <?=$icons[$i]?> <? if(isset($model->user) && $model->user->id != 0 && $model->user->getSchool()->plan_id = $plan->id){echo "active_plan ";}?>'>
                <img src="<?= FRONT_IMG ?>../img/<?=$icons[$i]?>.png">
                <p class='plan_name'><?=$plan->name?></p>
                <div class='deals_price'>
                    <? foreach($plan->getTiers() as $tier) { ?>
                        <div>
                            <div class='deal'><span>$</span><?=number_format($tier->price,2)?>
                                <p class='mpu'>Monthly per user</p>
                                <div class='show_amount' style="display: block">
                                    <p class='amt'><?=$tier->min_users?>-<?=$tier->max_users?></p>
                                </div>
                            </div>
                        </div>
                    <? } ?>

                </div>

                <? if( isset($model->user) && $model->user->id != 0  ) {?>
                    <button class='button pay'>CHOOSE</button>
                <? } else { ?>
                    <a href='/' class='button'>SIGN UP</a>
                <? } ?>

                <!-- <p class='plan_details'>VIEW PLAN DETAILS</p> -->

                <div class='includes'>
                    <p>PLAN INCLUDES</p>
                    <? foreach ($plan->getFeatures() as $feature) {?>
                       <p> <?=$feature?> </p>
                    <? } ?>
                </div>
            </div>
        <? } ?>

        <div class='plan enterprise' style='height: 220px;'>
            <!-- <img src="<?= FRONT_IMG ?>../img/gold.png"> -->
            <p class='plan_name'>ENTERPRISE</p>
            <? if( isset($model->user) && $model->user->id != 0 ) {?>
                <button style='position: relative; top: 39px;' class='button pay'>CONTACT US</button>
            <? } else { ?>
                <a style='position: relative; top: 39px;' href='/' class='button' >CONTACT US</a>
            <? } ?>
        </div>
    </div>
</section>

<section class='pay_plan'>
    <div class='off_click'></div>
	<form>
        <div id='x'><i class="fas fa-times-circle"></i></div>
        <img src="<?= FRONT_IMG ?>../img/gold.png">
        <p class='slogan'>Purchase gold!</p>
        <div class='payment_plan'>
            <div class='form-group'>
                <label>Full Name</label>
                <input type="text" name="full_name">
            </div>
            <div class='form-group'>
                <label>Card Number</label>
                <input type="text" name="card_number">
            </div>
            <div class='form-group'>
                <label>Epiration Date</label>
                <input type="text" name="expiration_date">
            </div>
            <div class='form-group'>
                <label>CCV</label>
                <input type="text" name="ccv">
            </div>
            <input type="sybmit" class='button' value='PURCHASE'>
        </div>
		<!-- <input type="" name=""> -->
	</form>
</section>

    <?php echo footer(); ?>

<script type="text/javascript">


    $(document).ready(function(){
    	$('.plan_details').click(function(){
    		$(this).parents('.plan').children('.includes').slideDown()
    	});

    	$('.pay').click(function(){
            $('.pay_plan').fadeIn();
    		$('.pay_plan').css('display', 'flex');
    	});

        $('.off_click').click(function(){
            $('.pay_plan').fadeOut();
        });

        $('#x').click(function(){
            $('.pay_plan').fadeOut();
        });   

        $('.change_plan').click(function(){
            $('html, body').animate({
                        scrollTop: $(".plans_holder").offset().top
                }, 1000);
        }); 


        $('.plans_holder .active_plan .deals_price > div:last-child').addClass('active_tier');

        
    });
</script>