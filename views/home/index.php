<!-- HOME PAGE -->
<body>
    <div class="home">
        <? if (count($model->banners) > 0) {?>
        <div class="flexslider flex-home">
            <ul class="slides">
                <? foreach($model->banners as $banner) { ?>
                    <? if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)) { ?>
                    <li>
                        <div class="image" style="background-image: url('<?=UPLOAD_URL.'banners/'.$banner->image?>');">
                            <div class="container">
                                <div class="inner">
                                    <!-- <h1>Premium Brands</h1> -->
                                    <a href="<?=$banner->link?>"><!-- Shop Now< --></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <? } ?>
                <? } ?>
            </ul>
        </div>
        <? } ?>

        <div class="box box-one box_featured_brands">
            <h2>Featured Deals</h2>
            <div class="container featured_brands_container">
                <div class="row featured_brands_row">
                    <div class="col-sm-8">
                        <? if(isset($model->featured[1]) && !is_null($model->featured[1]->image)) { ?>
                        <a style = "background-image:url(<?=UPLOAD_URL.'banners/370_310'.$model->featured[1]->image?>)" href="<?=SITE_URL.$model->featured[1]->link?>">
                            <img src="<?=UPLOAD_URL.'banners/370_310'.$model->featured[1]->image?>" />
                            <div class = "dark_overlay"></div>
                        </a>
                        <? } ?>
                        <? if(isset($model->featured[2]) && !is_null($model->featured[2]->image)) { ?>
                        <a style = "background-image:url(<?=UPLOAD_URL.'banners/370_220'.$model->featured[2]->image?>)" href="<?=SITE_URL.$model->featured[2]->link?>">
                            <img src="<?=UPLOAD_URL.'banners/370_220'.$model->featured[2]->image?>" />
                            <div class = "dark_overlay"></div>
                        </a>
                        <? } ?>
                    </div>
                    <div class="col-sm-8">
                        <? if(isset($model->featured[3]) && !is_null($model->featured[3]->image)) { ?>
                        <a style = "background-image:url(<?=UPLOAD_URL.'banners/370_220'.$model->featured[3]->image?>)" href="<?=SITE_URL.$model->featured[3]->link?>">
                            <img src="<?=UPLOAD_URL.'banners/370_220'.$model->featured[3]->image?>" />
                            <div class = "dark_overlay"></div>
                        </a>
                        <? } ?>
                        <? if(isset($model->featured[4]) && !is_null($model->featured[4]->image)) { ?>
                        <a style = "background-image:url(<?=UPLOAD_URL.'banners/370_310'.$model->featured[4]->image?>)" href="<?=SITE_URL.$model->featured[4]->link?>">
                            <img src="<?=UPLOAD_URL.'banners/370_310'.$model->featured[4]->image?>" />
                            <div class = "dark_overlay"></div>
                        </a>
                        <? } ?>
                    </div>
                    <div class="col-sm-8">
                        <? if(isset($model->featured[5]) && !is_null($model->featured[5]->image)) { ?>
                        <a style = "background-image:url(<?=UPLOAD_URL.'banners/370_310'.$model->featured[5]->image?>)" href="<?=SITE_URL.$model->featured[5]->link?>">
                            <img src="<?=UPLOAD_URL.'banners/370_310'.$model->featured[5]->image?>" />
                            <div class = "dark_overlay"></div>
                        </a>
                        <? } ?>
                        <? if(isset($model->featured[6]) && !is_null($model->featured[6]->image)) { ?>
                        <a style = "background-image:url(<?=UPLOAD_URL.'banners/370_220'.$model->featured[6]->image?>)" href="<?=SITE_URL.$model->featured[6]->link?>">
                            <img src="<?=UPLOAD_URL.'banners/370_220'.$model->featured[6]->image?>" />
                            <div class = "dark_overlay"></div>
                        </a>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>

        <?php 
            $img_path = '';
            if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.jpg')){
                $img_path = UPLOAD_URL.'banners/deal_of_the_week.jpg';
            } else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.jpeg')){
                $img_path = UPLOAD_URL.'banners/deal_of_the_week.jpeg';
            } else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.png')){
                $img_path = UPLOAD_URL.'banners/deal_of_the_week.png';
            } else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.gif')){
                $img_path = UPLOAD_URL.'banners/deal_of_the_week.gif';
            }
            if ($img_path != ''){
        ?>
            <div class="box box-two">
                <h2>Deal of the Week</h2>
                <a href="<?=SITE_URL.$model->deal_banner->link?>">
                    <div class="image" style="background-image: url('<?=$img_path?>');">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">                     
                                    <div class="inner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <? } ?>

        <? if (count($model->articles) > 0) { ?>
        <div class="box box-three">
            <h2>Recent News</h2>
            <div class="container">
                <div class="row">
                	<? foreach($model->articles as $article) { ?>
	                    <div class="col-sm-8">  
	                        <div class="wrap">                  
	                        	<?php 
	                        		if($article->image != "" && file_exists(UPLOAD_PATH.'articles'.DS.$article->image)){
			                    		$img_path = UPLOAD_URL . 'articles/' . $article->image;
			                    	} else {
			                    		$img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev make default shoe
			                    	}
			                    ?>
	                            <a href="<?=SITE_URL?>news/article/<?=$article->slug?>"><img src="<?=$img_path?>" alt="" class="img-responsive" /></a>
	                            <div class="inner">
	                                <a href="<?=SITE_URL?>news/article/<?=$article->slug?>"><?=$article->name?></a>
	                                <p><?=substr($article->content, 0, 100)?>...</p>
	                            </div>
	                            <span><?=$article->insert_time?></span>
	                        </div>
	                    </div>
                    <? } ?>
                </div>
            </div>
        </div>
        <? } ?>

        <?if(count($model->hottest_deals) > 0) {?>
        <div class="box">
            <h2>Hottest Deals</h2>
            <div class="container">
                <div class="row">
                    <?foreach($this->viewData->hottest_deals as $hottest_deal){?>
                        <div class="col-sm-8">
                            <div class="wrap">
                                <div class="inner"><?=$hottest_deal->title?></div>
                                <div class="inner"><?=$hottest_deal->subtitle?></div>
                                <?if($hottest_deal->getProduct()->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$hottest_deal->getProduct()->featured_image)){
                                    $img_path = UPLOAD_URL . 'products/' . $hottest_deal->getProduct()->featured_image;
                                } else {
                                    $img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev make default shoe
                                }?>
                                <a href="<?=SITE_URL?>product/<?=$hottest_deal->getProduct()->slug?>"><img src="<?=$img_path?>" alt="" class="img-responsive" /></a>
                                <div class="inner">
                                    <a href="<?=SITE_URL?>product/<?=$hottest_deal->slug?>"><?=$hottest_deal->getProduct()->name?></a>
                                    <p>$<?=$hottest_deal->getProduct()->price?></p>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
        <?}?>
    </div>
</body>
<!-- END HOME PAGE -->

<?php echo footer(); ?>

<script type="text/javascript">
    $(window).load(function() {

        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 100,
            itemMargin: 5,
            asNavFor: '#slider',
            start: function(){
                $('#carousel .slides img').show(); 
            }
        });

        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel",
            prevText: "",           
            nextText: "",
            start: function(){
                $('#slider .slides img').show(); 
            }
        });
    });

    $(document).ready(function() {
        $('.slides li').magnificPopup({
            type:'image',
            delegate: 'a',
            gallery:{
                enabled:true
            }
        });
    });
</script>
