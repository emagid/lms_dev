<section class='categories_page account_page filter_hide'>


    <section class='filters_holder'>
        <div class='filters'>
             <div class='no_filters'>
                <i class="fas fa-times"></i>
            </div>
            <form class='search'>
                <input type="text" name="search" placeholder='Type in a Category name'>
                <input class='submit' type="submit" value='search'>
            </form>

            <div style='margin-left: 50px;'>
                <a style=' height: 52px !important;' href="<?= SITE_URL ?>categories/update/0"><button class='button'>CREATE NEW</button></a>
            </div>
        </div>
        <div class='filters mobile_filter'>
            <a style='margin-left: 50px; height: 52px !important;' href="<?= SITE_URL ?>categories/update/0"><button class='button'>CREATE NEW</button></a>
            <img src="<?= FRONT_IMG ?>../img/setting.png">
        </div>
    </section>


    <section class='table_holder'>
        <table class='general_table head_table'>
            <thead>
                <tr>
                    <td width='60%'><p>Category Name</p></td>
                    <td width='20%'><p># Active</p></td>
                    <td width='10%'><p>Edit</p></td>
                    <td width='10%'><p>Delete</p></td>
                </tr>
            </thead>
        </table>

        <table class='general_table'>
            <tbody>
                <?php if(count($model->categories)>0) { ?>
                    <? foreach($model->categories as $obj){ ?>
                    <tr>
                        <td width='60%'><p><?php echo $obj->name; ?></p></td>
                        <td width='20%'><p><?php echo count($obj->get_users()); ?></p></td>
                        <td width='10%'>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>categories/update/<?php echo $obj->id; ?>">
                                <i class="fas icon-pencil fa-pencil-alt"></i>
                            </a>
                        </td>
                        <td width='10%'>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>categories/delete/<?php echo $obj->id; ?>">
                                <i class="fas fa-times-circle"></i>
                            </a>
                        </td>
                    </tr>
                    <? } ?>
                <? } else { ?>
                <? }?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </section>
    <?php echo footer(); ?>
    <script type="text/javascript">
        $('.mobile_filter img').click(function(){
            $('.filters').slideDown();
            $('.mobile_filter').hide();
            $('.no_filters').fadeIn();
        });

        $('.no_filters').click(function(){
            $('.filters').slideUp();
            $('.mobile_filter').fadeIn();
            $('.no_filters').hide();
        });


        var site_url = '<?= SITE_URL.'students';?>';
        var total_pages = <?= $model->pagination->total_pages;?>;
        var page = <?= $model->pagination->current_page_index;?>;

        $('form :input').change(function(e){
            $(this).parents('form').submit();
        });
    </script>

</section>

