<section class='update_page'>
    <section class='filters_holder'>
        <h2>CATEGORY UPDATE</h2>
    </section>
    <section class='table_holder'>
        
        <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
            <input type="hidden" name='account_id' value="<?=$model->user->account_id?>" />
            <div class="form-group">
                <label>Name</label>
                <?php echo $model->form->editorFor("name"); ?>
            </div>
            <div class="form-group">
                <label>code</label>
                <?php echo $model->form->editorFor("slug"); ?>
            </div>
          <input type="hidden" name="id" value="<?php echo $model->user_category->id;?>" />
          <input type=hidden name="token" value="<?php echo get_token(); ?>" />
          <button type="submit" class=" button">Save</button>
          
        </form>
    </section>
</section>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
});
</script>