<link href="https://fonts.googleapis.com/css?family=Bad+Script" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<? $account = \Model\Account::getItem($model->user->account_id); ?>
<style type="text/css">
    .certificate_page {
        width: 800px;
        margin: auto;
        padding-top: 200px;
    }

    .certificate_page img {
        margin: auto;
    }

    .certificate_holder {
        position: relative;
    }

    .certificate_holder .text_holder {
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
        top: 75px;
        width: 600px;
        text-align: center;
        font-size: 25px;
        font-family: 'sansationbold', arial, helvetica, sans-serif !important;
    }

    .text_holder .award_to {
        font-family: 'sansationbold', arial, helvetica, sans-serif !important;
        font-size: 15px;
        opacity: .6;
        margin-top: 15px;
    }

    .name {
        font-family: 'Bad Script', cursive;
        font-size: 41px;
        margin-top: 34px;
    }

    .success, .success_light {
        font-size: 14px;
    }

    .success_light {
        font-family: 'sansationregular', arial, helvetica, sans-serif !important;
        margin-top: -5px;
        font-size: 12px;
    }

    img.signature {
        margin-top: 30px;
    }

    .footer_bold, .footer_light {
        font-size: 8px;
        margin-top: 20px;
    }

    .footer_light {
        margin-top: -7px;
        opacity: .6;
    }

    .hide {
        position: absolute;
        top: 0;
        z-index: 1;
        height: 100%;
        width: 100%;
        background-color: white;
    }
</style>

<div class='hide'></div>

<section class='certificate_page'>
    <div class='certificate_holder'>
        <img src="<?= FRONT_IMG ?>../img/certificate.png">
        <div class='text_holder'>
            <!-- <p class='certificate_title'>2017 Social Media Foundation Training for Collegiate Athletics Certificate</p>
            <p class='award_to'>Awarded by SM2and Limestone College to:</p>
            <p class='name'>Name Placeholder</p>
            <p class='success'>For successful completion of the: 2017 Social Media Foundation Training for Collegiate Athletics</p>
            <p class='success_light'>The SM2 social media training programs are part of a national initiative designed to educate, equip and empower social media users and provide necessary tools, policies and procedures.</p>
            <img class='signature' src="<?= FRONT_IMG ?>../img/signatures.png">
            <p class='footer_bold'>HONOR CODE CERTIFICATE</p>
            <p class='footer_light'>* Authenticity of this certificate can be verified at https://limestone.sm2elearning.com/certificates/37885d0dff28be5d8f07541be4b0b17c.pdf</p> -->
            <p class='certificate_title'><?=$model->course->title?></p>
            <p class='award_to'>Awarded by SM2 and <?=$account->name?> to:</p>
            <p class='name'><?=$model->user->full_name()?></p>
            <p class='success'>For successful completion of the: <?=$model->course->title?></p>
            <p class='success_light'>The SM2 social media training programs are part of a national initiative designed to educate, equip and empower social media users and provide necessary tools, policies and procedures.</p>
            <img class='signature' src="<?= FRONT_IMG ?>../img/signatures.png">
            <p class='footer_bold'>HONOR CODE CERTIFICATE</p>
            <p class='footer_light'>* Authenticity of this certificate can be verified at https://limestone.sm2elearning.com/certificates/37885d0dff28be5d8f07541be4b0b17c.pdf</p>
        </div>
    </div>
</section>


<script src="content/frontend/libs/divTOcanvas/html2canvas.js"></script>
<script src="content/frontend/libs/divTOcanvas/html2canvas.min.js"></script>
<script type="text/javascript">
    html2canvas(document.querySelector(".certificate_holder")).then(canvas => {
        document.body.appendChild(canvas);
        $('canvas').attr('id', 'canvas');

      function convertCanvasToImage(canvas) {
        var image = new Image();
        var pdf = new jsPDF();
        var textX = 25, textY = 25;
        
        image.src = canvas.toDataURL("image/png");
        // debugger
        $('.certificate_page').html(image);
        $('img').addClass('print_img');
        $('.white').fadeOut();
        $('canvas').hide();

        pdf.addImage(image.src, 'PNG', 0, 0);
        pdf.textWithLink('test', textX, textY, {url: 'https://www.google.com/'});
        pdf.save("<?=$model->course->title?>_completion_certificate.pdf");
        // debugger
        sendEmail(image.src);

      }

     convertCanvasToImage($('#canvas')[0])
    });

    function sendEmail(certificate){
        $.post("/certificate/sendEmail",{certificate:certificate, user: <?=$model->user->id?>, course: '<?=$model->course->title?>'}, function(data){
            console.log(data);
        })
    }


    $('').click(function(){
      var source = $('img').attr('src');
        VoucherPrint(source);
    });

   function VoucherSourcetoPrint(source) {
      return "<html><head><title>LMS</title><style>/* style sheet for 'A4' printing */"+
        "@page {" +
         "margin: 0%;"+
         "}"+ 
         "</style><script>function step1(){\n" +
          "setTimeout('step2()', 10);}\n" +
          "function step2(){window.print();window.close()}\n" +
          "</scri" + "pt></head><body onload='step1()'>\n" +
          "<img src=" + source + " /></body></html>";
    }
    function VoucherPrint(source) {
      Pagelink = "about:blank";
      var pwa = window.open(Pagelink, "_new");
      pwa.document.open();
      pwa.document.write(VoucherSourcetoPrint(source));
      pwa.document.close();
    }
</script>