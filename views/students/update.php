<section class='update_page'>
    <section class='filters_holder'>
        <h2>STUDENT UPDATE</h2>
    </section>
    <section class='table_holder'>
        
        <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
            <?php echo $model->form->hiddenFor("encrypted_password",['type'=>'hidden']); ?>
            <?php echo $model->form->hiddenFor("sign_in_count",['type'=>'hidden']); ?>
            <?php echo $model->form->hiddenFor("invitations_count",['type'=>'hidden']); ?>
            <?php echo $model->form->hiddenFor("status",['type'=>'hidden']); ?>
            <?php echo $model->form->hiddenFor("year_level",['type'=>'hidden']); ?>
            <?php echo $model->form->hiddenFor("permissions",['type'=>'hidden']); ?>
            <div class="form-group">
                <label>First Name</label>
                <?php echo $model->form->editorFor("first_name"); ?>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <?php echo $model->form->editorFor("last_name"); ?>
            </div>
            <div class="form-group">
                <label>Email</label>
                <?php echo $model->form->editorFor("email"); ?>
            </div>
            <div class="form-group unstyled_select">
                <label>Student Category</label>
                <?php echo $model->form->dropdownListFor("category_id", $model->categories); ?>
            </div>
           <!-- <div class="form-group">
                <label>Phone</label>
                <?php /*echo $model->form->editorFor("phone"); */?>
            </div>
            <div class="form-group">
                <label>Comments</label>
                <?php /*echo $model->form->editorFor("comments"); */?>
            </div>
            <div class="form-group">
                <label>Address</label>
                <?php /*echo $model->form->editorFor("address"); */?>
            </div>
            <div class="form-group">
                <label>City</label>
                <?php /*echo $model->form->editorFor("city"); */?>
            </div>
            <div class="form-group">
                <label>State</label>
                <?php /*echo $model->form->editorFor("state"); */?>
            </div>
            <div class="form-group">
                <label>Zip</label>
                <?php /*echo $model->form->editorFor("zip"); */?>
            </div>-->
          <input type="hidden" name="id" value="<?php echo $model->student->id;?>" />
          <input type="hidden" name="role" value="student" />
          <input type="hidden" name="account_id" value="<?php echo $model->user->account_id;?>" />
          <input type=hidden name="token" value="<?php echo get_token(); ?>" />
          <button type="submit" class=" button">Save</button>
          
        </form>
    </section>
</section>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
});
</script>