<section class='students_page account_page filter_hide'>



    <section class='filters_holder'>
        <div class='filters'>
            <div class='no_filters'>
                <i class="fas fa-times"></i>
            </div>
            <form> 
                <div class="custom-select">
                  <select name="ucategory">
                        <option value="allyears">All Years</option>
                        <? foreach($model->ucategories AS $ucat) { 
                            // var_dump($ucat);
                            // exit; ?>
                            <option value="<?=$ucat->id?>" <?= isset($_GET['ucategory']) && $_GET['ucategory'] == $ucat->id?'selected':'' ?>>
                                <?=$ucat->name?>
                            </option>
                        <? } ?>
                        <!-- <option>1st Year</option>
                        <option>2nd Year</option>
                        <option>3rd Year</option>
                        <option>4th Year</option>
                        <option>5th Year</option> -->
                    </select>
                </div>
            </form>
            <form id="src" class='search'>
                <input id="search_input" type="text" name="search" placeholder='Search by email or name'>
                <input class='submit' type="submit" value='search'>
            </form>

            <div style='margin-left: 50px;'>
                <a style='height: 52px !important;' href="<?= SITE_URL ?>students/import"><button class='button'>UPLOAD</button></a>
            </div>
            <div style='margin-left: 50px;'>
                <a  href="<?= SITE_URL ?>students/update/0"><button style='height: 52px !important; min-width: 180px;' class='button reverse'>Add a new Student</button></a>
            </div>

        </div>
        <div class='filters mobile_filter'>
            <a style='margin-left: 50px; height: 52px !important;' href="<?= SITE_URL ?>students/import"><button class='button'>UPLOAD</button></a>
            <img src="<?= FRONT_IMG ?>../img/setting.png">
        </div>
    </section>


    <section class='table_holder'>
        <table class='general_table head_table'>
            <thead>
                <tr>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Class/Category</p></td>
                    <td><p>Edit</p></td>
                    <td><p>Delete</p></td>
                </tr>
            </thead>
        </table>

        <table id="data-list" class='general_table'>
            <tbody>
                <?php
                if(count($model->students)>0) { ?>
                    <? foreach($model->students as $obj){ ?>
                    <tr class="originalProducts">
                        <td><p><?php echo $obj->first_name; ?></p></td>
                        <td><p><?php echo $obj->last_name; ?></p></td>
                        <td><p><?php echo $obj->email; ?></p></td>
                        <td><p><?= $obj->category()?$obj->category()->name:'N/A'; ?></p></td>
                        <td>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>students/update/<?php echo $obj->id; ?>">
                                <i class="fas icon-pencil fa-pencil-alt"></i>

                            </a>
                        </td>
                        <td>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>students/update/<?php echo $obj->id; ?>">
                                <i class="fas fa-times-circle"></i>
                            </a>
                        </td>
                    </tr>
                    <? } ?>
                <? } else { ?>
                <? }?>
            </tbody>
        </table>
        
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </section>
    <?php echo footer(); ?>
    <script type="text/javascript">
        $('.mobile_filter img').click(function(){
            $('.filters').slideDown();
            $('.mobile_filter').hide();
            $('.no_filters').fadeIn();
        });

        $('.no_filters').click(function(){
            $('.filters').slideUp();
            $('.mobile_filter').fadeIn();
            $('.no_filters').hide();
        });

        var site_url = '<?= SITE_URL.'students';?>';
        var total_pages = <?= $model->pagination->total_pages;?>;
        var page = <?= $model->pagination->current_page_index;?>;

        $('form :input').change(function(e){
            $(this).parents('form').submit();
        });
    </script>


    <script type="text/javascript">
        $(function(){
            $("form#src").bind("submit",function() {
                    // alert("Searching....");
                    var keywords = $('#search_input').val();
                    // alert(keywords)
                    // var url = "<?php echo SITE_URL; ?>students/search";
                    console.log(keywords);

                    if (keywords.length > 0) {
                        // alert("In if");
                    $.post('students/search', {keywords: keywords}, function(data) {
                        
                        console.log("Inside post")
                        $("#data-list tbody tr").not('.originalProducts').remove();
                        $('.paginationContent').hide();
                        $('.originalProducts').hide();

                        var list = JSON.parse(data);
                        console.log(data);
                        for (key in list) {
                            var tr = $('<tr />');
                            
                            $('<td width="25%" />').appendTo(tr).html(list[key].first_name);
                            $('<td width="25%" />').appendTo(tr).html(list[key].last_name);
                            $('<td width="25%" />').appendTo(tr).html(list[key].email);
                            $('<td width="25%" />').appendTo(tr).html(list[key].class_category);

                            var editTd = $('<td />').addClass('text-center').appendTo(tr);
                            var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= SITE_URL;?>students/update/' + list[key].id);
                            var editIcon = $('<i />').appendTo(editLink).addClass('fas icon-pencil fa-pencil-alt');
                            var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                            var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= SITE_URL;?>students/delete/' + list[key].id);
                            deleteLink.click(function () {
                                return confirm('Are You Sure?');
                            });
                            var deleteIcon = $('<i />').appendTo(deleteLink).addClass('fas fa-times-circle');

                            tr.appendTo($("#data-list tbody"));
                        }

                    });
                } else {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').show();
                    $('.originalProducts').show();
                }
            })
        })
    </script>

</section>

