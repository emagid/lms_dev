<? if(!isset($model->review) || $model->review != true) {?>
<section class='csv_page'>

    <section class='fixed_background' style='background-image: url(../../content/frontend/img/login_hero.jpg)'></section>

    <section class='forms_hero'>
        <div class='form_holder'>
            <h2>CSV UPLOAD</h2>
            <form action="<?= $this->emagid->uri?>" method="POST" enctype="multipart/form-data">
                <p>Upload a CSV file with a list of your enrolled students</p>

                <input type="file" name="file" id="uploadFile" placeholder="Upload CSV" />
                <div class='upload_button half' hidden>
                    <div class="fileUpload button">
                        <span>Upload</span>
                        <input id="uploadBtn" type="file" class="upload" />
                    </div>
                </div>

                <!--  <input type="file" name="students" /> -->
                <h4>How would you like us to handle this file?</h4>
                <label>
                    Remove any students currently in our system not also in this csv.
                    <input type="checkbox" name="remove" value=1 />
                </label>

                <p style='margin-top: 30px;'>When updating your current student records, how should we determine that a record is a match? </p>
                <label>
                    Same Email
                    <input class="matchby min1 simple" type="checkbox" name="match[]" value='email'/>
                </label>
                <label>
                    First Name
                    <input class="matchby min1 simple" type="checkbox" name="match[]" value='first_name'/>
                </label>
                <label>
                    Last Name
                    <input class="matchby min1 simple" type="checkbox" name="match[]" value='last_name'/>
                </label>
                <label>
                    Year Level
                    <input class="matchby simple" type="checkbox" name="match[]" value='year_level'/>
                </label>
                <input class='submit button disabled' type="submit" disabled name="">
            </form>
        </div>
    </section>

</section>
<? } else { ?>
<section class='students_page account_page upload_banner'>

    <section class='filters_holder '>
        <div class='filters'>
            <p style='margin: 0; color: white; margin-bottom: 60px;'>Review your upload</p>
        </div>
    </section>

    <section class='table_holder'>
        <form action="/students/finalize" method="POST" enctype="multipart/form-data">
            <input class='upload_btn button' type="submit" value="FINALIZE" />
            <table class='general_table head_table'>
                <thead>
                <tr>
                    <td><p>Control</p></td>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Year</p></td>
                </tr>
                </thead>
            </table>

            <table class='general_table'>            
                <tbody>
                <? if(count($model->to_remove) > 0) {?>
                    <tr class='removed removed_row'>
                        <th colspan="5">Remove these students?</th>
                    </tr>
                    <? foreach ($model->to_remove as $student) {?>
                        <tr class='removed'>
                            <td>
                                <p>Remove?</p>
                                <input type="checkbox" name="remove[]" checked value="<?=$student->id?>">
                            </td>
                            <td><p><?=$student->last_name?></p></td>
                            <td><p><?=$student->first_name?></p></td>
                            <td><p><?=$student->email?></p></td>
                            <td><p><?=$student->year_level?></p></td>
                        </tr>
                    <? } ?>
                <? } ?>
                <? if(count($model->add) > 0){?>
                    <tr class='added added_row'>
                        <th colspan="5">These students will be added</th>
                    </tr>
                    <? foreach ($model->add as $student) {?>
                        <tr class='added'>
                            <td></td>
                            <td><p><?=$student->last_name?></p></td>
                            <td><p><?=$student->first_name?></p></td>
                            <td><p><?=$student->email?></p></td>
                            <td><p><?=$student->year_level?></p></td>
                        </tr>
                    <? } ?>
                <?}?>
                <? if(count($model->updated) > 0) {?>
                    <tr class = "updated updated_row">
                        <th colspan="5">These students records were updated</th>
                    </tr>
                    <? foreach ($model->updated as $student) {?>
                        <tr class='updated'>
                            <td></td>
                            <td><p><?=$student->last_name?></p></td>
                            <td><p><?=$student->first_name?></p></td>
                            <td><p><?=$student->email?></p></td>
                            <td><p><?=$student->year_level?></p></td>
                        </tr>
                    <? } ?>
                <? } ?>
                <? if(count($model->error) > 0) { ?>
                    <tr class = "error error_row">
                        <th colspan="5">There was an error updating these records</th>
                    </tr>
                    <? foreach ($model->error as $student) {?>
                        <tr class='error'>
                            <td></td>
                            <td><p><?=$student->last_name?>, <?=$student->first_name?></p></td>
                            <td><p><?=$student->email?></p></td>
                            <td colspan='2'><p><?= $student->errors[0]['message'] == 'This Email already exists.'?'This Student is already registered to another school':'Unkown Error' ?></p></td>
                        </tr>
                    <? } ?>
                <? } ?>
                </tbody>
            </table>
        </form>
    </section>

</section>
<? } ?>
<script type="text/javascript">
$(document).ready(function(){
    $('input').on('ifChanged', function (event) {
        $(event.target).trigger('change');
    });
    $('.matchby').change(function(){
       checkValid();
    });
    $('#uploadFile').change(function(){
       checkValid();
    });

});

function checkValid(){
    var valid = true;
    if(!$('#uploadFile').get(0).files.length === 0){
        valid = false;
    }
    if($('.min1:checked').length == 0){
        valid = false;
    }
    if(valid){
        $('.submit').prop('disabled',false);
        $('.submit').removeClass('disabled')
    } else {
        $('.submit').prop('disabled',true);
        $('.submit').addClass('disabled')
    }
}
</script>