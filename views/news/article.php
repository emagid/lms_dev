<div class="banner default" style="background-image:url('/content/frontend/img/banner_news.jpg');">
	<div class="container">
		<div class="inner">
			<h1>News</h1>
		</div>
	</div>
</div>
<!-- ENTRY CONTENT -->
<div class="box box-three box_blogs">
	<div class="container">
		<? require_once(ROOT_DIR.'templates/'.$this->template.'/news_sidebar.php'); ?>
		<!-- Blog Post -->
		<div class="col-md-16 col-md-pull-6 blog_post entry_post">
			<h2><?=$model->article->name?></h2>	
			<div class="img_align">
				<?php 
            	  if($model->article->image != "" && file_exists(UPLOAD_PATH.'articles'.DS.$model->article->image)){
                		$img_path = UPLOAD_URL . 'articles/' . $model->article->image;
                	} else {
                		$img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev default product image
                  	}
                ?>
				 <img src="<?=$img_path?>" alt="" class="img-responsive" />
			</div>
			<div>
				<div class="wrap">					
					<span><?//=$model->article->author?>  <?=$model->article->insert_time?> &nbsp|&nbsp <a href="#">Leave A Comment</a></span>
					<?=$model->article->content?>
 				</div>	
			</div>
		</div>		
	</div>
</div>
<!-- END BLOG CONTENT -->
