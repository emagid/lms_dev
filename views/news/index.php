<div class="banner default" style="background-image:url('content/frontend/img/banner_news.jpg');">
	<div class="container">
		<div class="inner">
			<h1>News</h1>
		</div>
	</div>
</div>


<div class="box box-three box_blogs">
	<div class="container">
		<? require_once(ROOT_DIR.'templates/'.$this->template.'/news_sidebar.php'); ?>
		<div class="container">
			<div class="filters" style="display:none;">

				<ul class="nav nav-pills news-topic" role="tablist">
					<li>
						<a href="#">
							Filter by Topic:
						</a>
					</li>
					<li>
						<a id="filter-brand" href="#" class="active">
							All
						</a>
					</li>
					<li>
						<a id="filter-brand" href="#">
							Watch Reviews
						</a>
					</li>
					<li>
						<a id="filter-brand" href="#">
							Articles
						</a>
					</li>
					<li>
						<a id="filter-brand" href="#">
							Brands
						</a>
					</li>
					<li>
						<a id="filter-brand" href="#">
							Videos
						</a>
					</li>
				</ul>
			</div>

			<div class="col-md-16 col-md-pull-6">
				<h2>Latest News</h2>
				<? foreach($model->articles as $article) { ?>
					<div class="row">
                        <div class="wrap">                  
                        	<?php 
                        		if($article->image != "" && file_exists(UPLOAD_PATH.'articles'.DS.$article->image)){
		                    		$img_path = UPLOAD_URL . 'articles/' . $article->image;
		                    	} else {
		                    		$img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev make default image
		                    	}
		                    ?>
		                    <div class="col-sm-6">
                            	<a href="<?=SITE_URL?>news/article/<?=$article->slug?>"><img src="<?=$img_path?>" alt="" class="img-responsive" /></a>
                            </div>
                            <div class="col-sm-18">
                            	<div class="row">
	                                <a href="<?=SITE_URL?>news/article/<?=$article->slug?>"><?=$article->name?></a>

	                                <p><?=substr($article->content, 0, 100)?>...</p>
		                        </div>
		                        <div class="row">
	                            	<span><?=$article->insert_time?></span>
	                            </div>
                        	</div>
                        </div>
                	</div>
                <? } ?>
                <nav class="text-right"><ul class="pagination"></ul></nav>
			</div>	
		</div>
	</div>
</div>

<? footer(); ?>
<script>
var params = <?php echo (isset($model->params)) ? json_encode($model->params) : json_encode((object)array()); ?>;
if(params instanceof Array) {
	params = {};
}
var site_url = '<?= SITE_URL."news"?>';
$(function() {
	$('ul.pagination').pagination({
		pages: <?=$model->pagination->total_pages?>,
		currentPage: <?=$model->pagination->current_page_index + 1?>,
		cssStyle: 'light-theme',
		onPageClick: function(pageNumber,event) {
			var url_params = params || {};
			url_params.page = parseInt(pageNumber);
			var full_url = site_url;
			build_url(full_url,url_params,true);			
		}
	});
});
function build_url(url,params,redirect) {
	var params_arr = [];
	$.each(params,function(i,e) {
		params_arr.push(i+"="+e);
	});
	if(redirect) {
		window.location.href = url + "?"+params_arr.join("&");
		return false;
	} else {
		return url + "?"+params_arr.join("&");
	}
}
</script>
