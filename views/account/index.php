<section class='account_page accounts_page'>

<!--     <section class='hero account_hero'>
        <h1><?= $model->account->name ?></h1>
    </section> -->


    <section class='filters_holder'>
        <div class='filters'>
            <div class='account_stats'>
                <div>
                    <p class='amount'><?=$model->account->studentCount?></p>
                    <p>STUDENTS</p>
                </div>
                <div>
                    <p class='amount'><?=$model->account->courseCount?></p>
                    <p>COURSES</p>
                </div>
                <div>
                    <p class='amount'><?=$model->account->maxUserCount?></p>
                    <p>MAXIMUM USERS</p>
                </div>
                <div>
                    <p class='amount'><?=$model->account->remainUserCount?></p>
                    <p>REMAINING USERS</p>
                </div>
            </div>
        </div>
    </section>


    <section class='account_info'>
        <h2>ACCOUNT INFO</h2>

        <div class='form_holder'>
            <div class='form register_holder'>
                <form method="POST" action="/account/register">
                    <input class='input half' type="text" name="contact_name" placeholder='Name' value="<?=$model->user->first_name.' '.$model->user->last_name?>">
                    <input class='input half' type="text" name="email" placeholder='Email' value="<?=$model->user->email?>">
                    <input class='input half' type="password" name="password" placeholder='Password'>
                    <input class='input half' type="password" name="confirm_password" placeholder='Confirm Password'>
                    <input class='input half' type="text" name="contact_phone" placeholder='Phone'>
                    <input class='input half' type="text" name="name" placeholder='School Name'>
                    <textarea class='input full' type="textarea" wrap="hard" name="contact_available_hours" placeholder='Hours'></textarea>

                    <div class='curr_images'>
                        <div class='curr_logo'>
                            <p>Current Logo</p>
                            <img src="<?= FRONT_IMG ?>../img/logo.png">
                            <div class='upload_button half'>
                                <input class='upload_field' id="uploadFile" placeholder="Upload Logo" disabled="disabled" />
                                <div class="fileUpload button">
                                    <span>Upload</span>
                                    <input id="uploadBtn" type="file" class="upload" />
                                </div>
                            </div>
                        </div>
                        <div class='curr_logo'>
                            <p>Current Image</p>
                            <img src="<?= FRONT_IMG ?>../img/logo.png">
                            <div class='upload_button half'>
                                <input class='upload_field' id="uploadFile" placeholder="Upload School Image" disabled="disabled" />
                                <div class="fileUpload button">
                                    <span>Upload</span>
                                    <input id="uploadBtn" type="file" class="upload" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class='adding_pics'>
                        <label>Certificate</label>
                        <input class='input half' type="text" name="certificate_signature_title" placeholder='Certificate title'>
                        <input class='input half' type="text" name="certificate_signature_name" placeholder='Certificate Name'>
                        <div class='upload_button'>
                            <input class='upload_field' id="uploadFile" placeholder="Upload School Image" disabled="disabled" />
                            <div class="fileUpload button">
                                <span>Upload</span>
                                <input id="uploadBtn" type="file" class="upload" />
                            </div>
                        </div>
                        <div class='upload_button'>
                            <input class='upload_field' id="uploadFile" placeholder="Upload School Image" disabled="disabled" />
                            <div class="fileUpload button">
                                <span>Upload</span>
                                <input id="uploadBtn" type="file" class="upload" />
                            </div>
                        </div>
                   </div> -->
                    <input class='button' type="submit" value='UPDATE'>
                </form>
            </div>
        </div>


        <div class='billing_info'>
            <h2>BILLING INFORMATION</h2>
            <div class='curr_plan'>
                <img src="<?= FRONT_IMG ?>../img/gold.png">
                <p>You are currently on the Gold Plan. <span><a href="home/plans">CHANGE PLAN</a></span></p>
            </div>
        </div>
    </section>


</section>

<script type="text/javascript">
	
	document.getElementById("uploadBtn").onchange = function () {
        document.getElementById("uploadFile").value = this.value;
    };
</script>
