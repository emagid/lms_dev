<section class='fixed_background' style='background-image: url(../../content/frontend/img/login_hero.jpg)'></section>

<section class='forms_hero'>
    <div class='login_register'>
        <p id='login' class='login active_login'>LOGIN</p>
        <p id='register' class='register inactive_login'>REGISTER</p>
    </div>

    <div class='form_holder'>
        <div class='form login_holder'>
            <form method="POST" action="<?= $this->emagid->uri ?>">
                <input class='input half' type="text" name="email" placeholder='Email'>
                <input class='input half' type="password" name="password" placeholder='Password'>
                <input type="hidden" name="redirect-url" value="/account">
                <!-- <p>Forgot Password?</p> -->
                <input class='button' type="submit" value='LOGIN'>
            </form>
            <p class='forgot_pass'>Forgot Password?</p>
        </div>
        <div class='form register_holder'>
            <form method="POST" action="/account/register">
                <input class='input half' type="text" name="contact_name" placeholder='Name'>
                <input class='input half' type="text" name="email" placeholder='Email'>
                <input class='input half' type="password" name="password" placeholder='Password'>
                <input class='input half' type="password" name="confirm_password" placeholder='Confirm Password'>
                <input class='input half' type="text" name="contact_phone" placeholder='Phone'>
                <input class='input half' type="text" name="name" placeholder='School Name'>
                <textarea class='input full' type="textarea" wrap="hard" name="contact_available_hours" placeholder='Hours'></textarea>
                <input class='input full' type="text" name="subdomain" placeholder='Preferred Subdomain (eg: arizona.sm2elearning.com)'>
                <div class='divide'></div>
                <label class='select_label'>Select Category</label>
                <div class='select full'>
                    <span class="arr"></span>
                    <select name="category_id" class="input">
                        <!-- <option selected>Box</option> -->
                        <?php foreach ($model->acategories as $acat) {
                            $select = ($acat->id) ? " selected='selected'" : "";
                            ?>
                            <option value="<?php echo $acat->id; ?>" <?php echo $select; ?> ><?php echo $acat->name; ?></option>
                        <?php } ?>
                    </select>
                </div>


                <label class='select_label'>Main Color (probably this will be the color of your logo)</label>
                <input class='input full jscolor' type="text" name="main_color">

                <label class='select_label'>Button Color (secondary color)</label>
                <input class='input full jscolor' type="text" name="button_color">

                <div class='upload_button full'>
                    <label class='select_label'>School Logo</label>
                    <p>
                        <small>(Ideal image size is 360px x 70px)</small>
                    </p>
                    <input class='upload_field' id="uploadFile2" placeholder="Upload Logo" disabled="disabled" />
                    <div class="fileUpload button">
                        <span>Upload</span>
                        <input id="uploadBtn2" type="file" class="upload" />
                    </div>
                </div>

                <div class='upload_button half'>
                    <label class='select_label'>Certificate Logo</label>
                    <p>
                        <small>(Ideal image size is 190px x 120px)</small>
                    </p>
                    <input class='upload_field' id="uploadFile1" placeholder="Upload Logo" disabled="disabled" />
                    <div class="fileUpload button">
                        <span>Upload</span>
                        <input id="uploadBtn1" type="file" class="upload" />
                    </div>
                </div>

                <div class='upload_button half'>
                    <label class='select_label'>Signature Image</label>
                    <p>
                        <small>(Ideal image size is 200px x 85px</small>
                    </p>
                    <input class='upload_field' id="uploadFile" placeholder="Upload School Image" disabled="disabled" />
                    <div class="fileUpload button">
                        <span>Upload</span>
                        <input id="uploadBtn" type="file" class="upload" />
                    </div>
                </div>

               <!--  <div class='adding_pics'>
                    <input class='input half' type="text" name="certificate_signature_title" placeholder='Certificate title'>
                    <input class='input half' type="text" name="certificate_signature_name" placeholder='Certificate Name'>
                    <div class='upload_button'>
                        <input class='upload_field' id="uploadFile2" placeholder="Certificate File" disabled="disabled" />
                        <div class="fileUpload button">
                            <span>Upload</span>
                            <input id="uploadBtn2" type="file" class="upload" />
                        </div>
                    </div>
                    <div class='upload_button'>
                        <input class='upload_field' id="uploadFile3" placeholder="Upload School Image" disabled="disabled" />
                        <div class="fileUpload button">
                            <span>Upload</span>
                            <input id="uploadBtn3" type="file" class="upload" />
                        </div>
                    </div>
                </div> -->
                <input class='button' type="submit" value='REGISTER'>
            </form>
        </div>
    </div>
</section>

<section class='success_steps'>
    <div class='off_click'></div>
    <div class='steps_holder'>
        <p class='success_head'>SUCCESS!</p>
        <p>Next Steps</p>
        <p>Right now your account is pending approval. We will send a message to your provided email letting you know when your account is ready.</p>
    </div>
</section>

<div class='forgot_pass_popup'>
    <div class='off_click'></div>
    <div class='forgot_pass_holder'>
        <p class='success_head'>PASSWORD RESET</p>
        <p>An email to reset your password has been sent!</p>
    </div>
</div>
<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<script type="text/javascript">
    // var categories = <?php //echo json_encode($cat_id); ?>;
    $(document).ready(function(){

        if ( $('.alert-success').is(':visible') ) {
            $('.alert-success').hide();
            $('.success_steps').css('display', 'flex');
            $('.success_steps').fadeIn();
        };

        $('.forgot_pass').click(function(){
            $('.forgot_pass_popup').fadeIn();
            $('.forgot_pass_popup').css('display', 'flex');
        });

        $('.off_click').click(function(){
            $('.success_steps').fadeOut();
            $('.forgot_pass_popup').fadeOut();
        });


        // login / register toggles
        $('.login_register p').click(function(){
            if ( !$(this).hasClass('active_login') ) {
                $('.login_register p').removeClass('active_login');
                $('.login_register p').addClass('inactive_login');
                $(this).addClass('active_login');

                var formType = $(this).attr('id') + "_holder";
                $('.form').slideUp();
                $("." + formType).slideDown();
            }
        });

        document.getElementById("uploadBtn").onchange = function () {
            document.getElementById("uploadFile").value = this.value;
        };

        document.getElementById("uploadBtn1").onchange = function () {
            document.getElementById("uploadFile1").value = this.value;
        };

        document.getElementById("uploadBtn2").onchange = function () {
            document.getElementById("uploadFile2").value = this.value;
        };

        /*document.getElementById("uploadBtn3").onchange = function () {
            document.getElementById("uploadFile3").value = this.value;
        };*/

        // $("select.multiselect").each(function(i,e) {
        //     $(e).val('');
        //     var placeholder = $(e).data('placeholder');
        //     $(e).multiselect({
        //         nonSelectedText:placeholder,
        //         includeSelectAllOption: true,
        //         maxHeight: 415,
        //         checkboxName: '',
        //         enableCaseInsensitiveFiltering: true,
        //         buttonWidth: '100%'
        //     });
        // });
        // $("select[name='category_id[]']").val(categories);


    });
</script>