<div class="banner cms">
	<div class="container">
		<div class="inner">
			<h1>Order Summary</h1>
		</div>
	</div>
</div>

<div class="container">
	<div class="order_status">
		<h2>We Received Your Order</h2>	
		<?
			if (is_null($model->order->user_id)){
				$email = $model->order->email;
			} else {
				$user = \Model\User::getItem($model->order->user_id);
				$email = $user->email;
			}
		?>
		<p>Thank you for shopping at <a href="<?= SITE_URL?>">ModernVice.com</a>. You will receive a confirmation email shortly at <?=$email?>.</p>
	</div>

	<div class="order_details">
	<div class="PrintOnly">
   
<br>
	<center><img src="http://modernvice.com/content/frontend/img/logo.png" ><br><?=$model->order->date?></center></div>
		<div class="col-sm-24 col-md-24">
			<h3><span>#<?=$model->order->id?> - <?=$model->order->date?> - <a href="#">Print Receipt</a></span>ORDER DETAILS</h3>
			<? foreach($model->order_products as $order_product) { ?>
				<div class="col-md-24 ordered_products">
					<h4>
						<div class="col-sm-4 col-md-4 order_image">
						<?php 
				            $img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev default product image
				            if(!is_null($order_product->product->featured_image) && $order_product->product->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$order_product->product->featured_image)){
				             	$img_path = UPLOAD_URL . 'products/' . $order_product->product->featured_image;
				            }
						if(!is_null($order_product->details) && $order_product->details != ''){
							$productDetails = json_decode($order_product->details,true);
						} else {
							$productDetails = ['color'=>'','size'=>''];
						}
				        ?>
		        			<a href="<?=SITE_URL.'product/'.$order_product->product->slug?>"><img src="<?php echo $img_path; ?>" /></a>
						</div>
						<a href="<?=SITE_URL.'product/'.$order_product->product->slug?>"><?=$order_product->product->name?></a> 
						<p>Quantity: <?=$order_product->quantity?>	</p>
						<p>Color: <?=$productDetails['color']?></p>
						<p>Size: <?=$productDetails['size']?></p>
					</h4>
				</div>
			<? } ?>
		</div>
		<div class="col-md-24 order_summary table-responsive">
			<h3>Payment Summary</h3>
			<table class="table" >
				<tr>
					<td>SubTotal:</td>
					<td>$<?=number_format($model->order->subtotal,2)?></td>
				</tr>
				<?
					if (!is_null($model->order->coupon_code)){
						if ($model->order->coupon_type == 1){
							$savings = $model->order->coupon_amount;	
						} else if ($model->order->coupon_type == 2){
							$savings = $model->order->subtotal * $model->order->coupon_amount/100;
						}
				?>
						<tr>
							<td>Savings:</td>
							<td>$<?=number_format($savings,2)?></td>
						</tr>
				<? } ?>
				<tr>
					<td>Shipping:</td>
					<td>$<?=number_format($model->order->shipping_cost,2)?></td>
				</tr>
				<tr>
					<td>Tax:</td>
					<td>$<?=number_format($model->order->tax,2)?></td>
				</tr>
				<tr>
					<td><b>Total:</b></td>
					<td>$<?=number_format($model->order->total,2)?></td>
				</tr>
			</table>
		</div>
	</div>
</div>

<? footer() ?>

<style type="text/css">
 .PrintOnly {         display:none; } 
	@media print {
		.top-bar, .site-nav, .banner.cms, footer .container, h2, .order_status, h3  {
			display: none;
		}
		.ordered_products img {
			width: 50px;
			height: 50px;
		}
		.PrintOnly {     display:block; margin:0 auto; width: 199px;
			height: 71px;}
		 
a[href^="/"]:after {content: " ";}
 
	}
</style>

<script>
	$(function(){
		$('h3 a').click(function(e){
			e.preventDefault();
			window.print();
			return false;
		})
	})
</script>

<? 
	if (($_SERVER['SERVER_NAME'] == 'modernvice.com' || $_SERVER['SERVER_NAME'] == 'www.modernvice.com')
		&& isset($_SESSION['purchase_conversion']) && $_SESSION['purchase_conversion']) { 

		unset($_SESSION['purchase_conversion']);
?>
			<!--TODO st-dev google ads for MV-->
<!--		<!-- Google Code for Purchase Conversion Page -->-->
<!--		<script type="text/javascript">-->
<!--			/* <![CDATA[ */-->
<!--			var google_conversion_id = 1010367779;-->
<!--			var google_conversion_language = "en";-->
<!--			var google_conversion_format = "3";-->
<!--			var google_conversion_color = "ffffff";-->
<!--			var google_conversion_label = "QjyTCNqoxl4Qo_rj4QM";-->
<!--			var google_remarketing_only = false;-->
<!--			/* ]]> */-->
<!--		</script>-->
<!---->
<!--		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>-->
<!---->
<!--		<noscript>-->
<!--			<div style="display:inline;">-->
<!--				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1010367779/?label=QjyTCNqoxl4Qo_rj4QM&amp;guid=ON&amp;script=0"/>-->
<!--			</div>-->
<!--		</noscript>-->
<? } ?>