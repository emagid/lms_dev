<style type="text/css">
    .select-selected:after {
        top: 21px;
    }

    .select-selected.select-arrow-active:after {
        top: 14px;
    }
</style>



<section class='students_page account_page filter_hide'>
    <section class='filters_holder'>
        <div class='filters'>
            <div class='no_filters'>
                <i class="fas fa-times"></i>
            </div>
            <div class="custom-select">
                <select>
                    <option>All Years</option>
                    <option>1st Year</option>
                    <option>2nd Year</option>
                    <option>3rd Year</option>
                    <option>4th Year</option>
                    <option>5th Year</option>
                </select>
            </div>

            <form class='search'>
                <input type="text" name="search" placeholder='Search Title'>
                <input class='submit' type="submit" value='search'>
            </form>

            <div style='margin-left: 50px;'> 
                <a style='height: 52px !important;' href="<?= SITE_URL ?>students/import"><button class='button'>UPLOAD</button></a>
            </div>
        </div>

        <div class='filters mobile_filter'>
            <a style='margin-left: 50px; height: 52px !important;' href="<?= SITE_URL ?>students/import"><button class='button'>UPLOAD</button></a>
            <img src="<?= FRONT_IMG ?>../img/setting.png">
        </div>
    </section>


    <section class='table_holder'>
        <table class='general_table head_table'>
            <thead>
                <tr>
                    <td><p>Course Title</p></td>
                    <td><p>Video Title</p></td>
                    <td><p>Video Url</p></td>
                    <td><p>Categories</p></td>
                    <td><p>Remove</p></td>
                </tr>
            </thead>
        </table>

        <table class='general_table'>
            <tbody>
            <? foreach ($model->modules as $obj) {?>
                <tr>
                    <td><p><?=$obj->get_course()->title?></p></td>
                    <td><p><?=$obj->title?></p></td>
                    <td><p><?=$obj->video_url?></p></td>
                    <td>
                        <select multiple class='module_groups' data-module_id='<?=$obj->id?>'>
                        <? foreach($model->categories AS $category) {
                            $selected = \Model\Account_Module_Cat::getItem(null,['where'=>"account_id = {$model->user->account_id} AND ucat_id = $category->id AND module_id = $obj->id"])?'selected':'';
                            ?>
                            <option <?=$selected?> value='<?=$category->id?>'><?=$category->name?></option>
                        <? } ?>
                        </select>
                    </td>
                    <td>
                        <a class="btn-actions remove_module" data-module="<?=$obj->id?>">
                            <i class="fas fa-times-circle"></i>
                        </a>
                    </td>
                </tr>
            <? } ?>

                <!-- <?php
                $model->students = $model->users;
                if(count($model->students)>0) { ?>
                    <? foreach($model->students as $obj){ ?>
                    <tr>
                        <td><p><?php echo $obj->first_name; ?></p></td>
                        <td><p><?php echo $obj->last_name; ?></p></td>
                        <td><p><?php echo $obj->email; ?></p></td>
                        <td><p><?php echo $obj->year_level; ?></p></td>
                        <td>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>students/update/<?php echo $obj->id; ?>">
                                <i class="fas icon-pencil fa-pencil-alt"></i>


                            </a>
                        </td>
                        <td>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>students/update/<?php echo $obj->id; ?>">
                                <i class="fas fa-times-circle"></i>
                            </a>
                        </td>
                    </tr>
                    <? } ?>
                <? } else { ?>
                <? }?> -->
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </section>
    <?php echo footer(); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.min.css" />
    <script type="text/javascript">

        $('.mobile_filter img').click(function(){
            $('.filters').slideDown();
            $('.mobile_filter').hide();
            $('.no_filters').fadeIn();
        });

        $('.no_filters').click(function(){
            $('.filters').slideUp();
            $('.mobile_filter').fadeIn();
            $('.no_filters').hide();
        });


        var site_url = '<?= SITE_URL.'courses/media';?>';
        var total_pages = <?= $model->pagination->total_pages;?>;
        var page = <?= $model->pagination->current_page_index;?>;

        $(document).ready(function () {
            $(".module_groups").chosen({width:'100%'});
            $('form :input').change(function(e){
                $(this).parents('form').submit();
            });
            $(document).on("change",".module_groups",function(){
                var postData = {
                    categories: $(this).val(),
                    account_id: <?=$model->user->account_id?:0?>,
                    module_id: $(this).data('module_id')
                }
                $.post('/courses/update_module_cats',postData,function(data){});

            });
        })
    </script>

</section>

