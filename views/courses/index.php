<style type="text/css">
    .select-selected:after {
        top: 21px;
    }

    .select-selected.select-arrow-active:after {
        top: 14px;
    }
</style>


<section class='account_page courses_page filter_hide'>
    
    <section class='filters_holder'>
        <div class='filters'>
            <form class='dbl_filter' style='display: flex;'>
                <div class='no_filters'>
                    <i class="fas fa-times"></i>
                </div>
                <div class="custom-select">
                    <select name="courses">
                        <option selected>Courses</option>
                        <? foreach ($model->plan_courses as $pc) { 
                            $course = \Model\Course::getItem(null,['where'=>" active = 1 AND id =".$pc->course_id]); ?>
                            <option value="<?=$course->id?>" <?= isset($_GET['courses']) && $_GET['courses'] == $course->id?'selected':'' ?>>
                                <?=$course->title?>
                            </option>
                        <? } ?>
                    </select>
                </div>
                <div class="custom-select" style='margin-left: 50px;'>
                    <select>
                        <option>All Year Levels</option>
                        <option>1st Year Level</option>
                        <option>2nd Year Level</option>
                        <option>3rd Year Level</option>
                        <option>4th Year Level</option>
                        <option>5th Year Level</option>
                    </select>
                </div>
            </form>
            <form id="src" class='search'>
                <input id="search_input" type="text" name="search" placeholder='Search by title'>
                <input class='submit' type="submit" value='search'>
            </form>
        </div>

        <div class='filters mobile_filter'>
            <div class="custom-select">
                <span class="arr"></span>
                <select>
                    <option>Courses</option>
                    <option>Course</option>
                    <option>Course</option>
                    <option>Course</option>
                    <option>Course</option>
                    <option>Course</option>
                </select>
            </div>
            <img src="<?= FRONT_IMG ?>../img/setting.png">
        </div>
    </section>


    <section class='table_holder course_table'>
        <table class='general_table head_table'>
            <thead>
                <tr>
                    <td width="25%"><p>Course</p></td>
                    <td width="25%"><p>Module Title</p></td>
                    <td width="15%"><p>Description</p></td>
                    <td width="15%"><p>Video Url</p></td>
<!--                    <td width="10%"><p>Sugg. Year Level</p></td>-->
                    <td width="10%"><p>Add for Students</p></td>
                </tr>
            </thead>
          </table>

        <table id="data-list" class='general_table'>
            <tbody>
                <? 
                    // foreach($model->modules AS $obj) {
                    foreach ($model->modules as $obj) {
                ?>
                <tr class="originalProducts">
                    <td width="25%"><p><?=$obj->get_course()->title?></p></td>
                    <td width="25%"><p><?=$obj->title?></p></td>
                    <td width="15%">
                        <div class='desc_icon'>
                            <i class="fas fa-align-left"></i>
                        </div>
                        <div class='desc_popup'>
                            <div class='off_click'></div>
                            <div class='desc_holder'>
                            <div id='x'><i class="fas fa-times-circle"></i></div>
                                <p><?=$obj->short_description?></p>
                            </div>
                        </div>
                    </td>
                    <td width="15%">
                        <!-- <div class='desc_icon'>
                            <i class="fas fa-link"></i>
                        </div>
                        <div class='desc_popup'>
                            <div class='off_click'></div>
                            <div class='desc_holder'>
                            <div id='x'><i class="fas fa-times-circle"></i></div>
                                <p><?=$obj->video_url?></p>
                            </div>
                        </div> -->
                        <p style='word-break: break-word;'><a><?=$obj->video_url?></a></p></td>
                    </td>
<!--                    <td width="10%"><p>--><?//=$obj->suggested_year_level?:'N/A'?><!--</p></td>-->
                    <td width="10%">
                        <? $inPlan = \Model\Account_Module_Cat::getCount(['where'=>"module_id = $obj->id AND account_id = {$model->user->account_id}"]);?>
                       <a style="<?= $inPlan != 0 ?'display:none':''?>" class="btn-actions add_module" data-module="<?=$obj->id?>">
                            <i class="fas icon-plus fa-plus"></i>
                       </a>
                       <a style="<?= $inPlan == 0?'display:none':''?>" class="btn-actions remove_module" data-module="<?=$obj->id?>">
                            <i class="fas icon-minus fa-minus"></i>
                       </a>
                    </td>
                </tr>
            <? } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </section>
    <?php echo footer(); ?>
    <script type="text/javascript">
        var site_url = '<?= SITE_URL.'courses';?>';
        var total_pages = <?= $model->pagination->total_pages;?>;
        var page = <?= $model->pagination->current_page_index;?>;

        $('form :input').change(function(e){
            $(this).parents('form').submit();
        });

    </script>
    <script>
        $(document).ready(function () {

            $('.mobile_filter img').click(function(){
                $('.filters').slideDown();
                $('.mobile_filter').hide();
                $('.no_filters').fadeIn();
            });

            $('.no_filters').click(function(){
                $('.filters').slideUp();
                $('.mobile_filter').fadeIn();
                $('.no_filters').hide();
            });

            $('.off_click, #x').click(function(){
                $('.desc_popup').fadeOut(200);
            });

            $('.desc_icon').click(function(){
                $(this).next('.desc_popup').fadeIn(200);
                $(this).next('.desc_popup').css('display', 'flex');
            });

            $('.add_module').click(function(){
                var button = $(this);
                $.post('/courses/add_module',{module_id:button.data('module')},function(data){
                    if(data.status == true){
                        button.hide().parent().find('.remove_module').show();
                    }
                });
            });
            $('.remove_module').click(function(){
                var button = $(this);
                $.post('/courses/remove_module',{module_id:button.data('module')},function(data){
                    if(data.status == true){
                        button.hide().parent().find('.add_module').show();
                    }
                });
            });


            $("form#src").bind("submit",function() {
                // alert("Searching....");
                var keywords = $('#search_input').val();
                alert(keywords)
                var url = "<?php echo SITE_URL; ?>courses/search";
                console.log(keywords);

                if (keywords.length > 2) {
                $.post(url, {keywords: keywords}, function(data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();

                    var list = JSON.parse(jsonEscape(data));

                    for (key in list) {
                        var tr = $('<tr />');
                        
                        $('<td width="25%" />').appendTo(tr).html(list[key].course_title);
                        $('<td width="25%" />').appendTo(tr).html(list[key].module_title);
                        $('<td width="15%" />').appendTo(tr).wrapInner(('<div class="desc_icon" />').html($('<i class="fas fa-align-left"/>'))).prop('href', '<?= ADMIN_URL;?>courses/update/' + list[key].id).html(list[key].description);

                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>courses/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>courses/delete/' + list[key].id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        })
    })
    </script>
</section>