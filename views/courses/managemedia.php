<section class='students_page account_page'>



    <section class='filters_holder'>
        <div class='filters'>
            <div class='select'>
                <span class="arr"></span>
                <select>
                    <option>All Years</option>
                    <option>1st Year</option>
                    <option>2nd Year</option>
                    <option>3rd Year</option>
                    <option>4th Year</option>
                    <option>5th Year</option>
                </select>
            </div>

            <form class='search'>
                <input type="text" name="search" placeholder='Search by email or name'>
                <input class='submit' type="submit" value='search'>
            </form>

            <a style='margin-left: 50px; height: 52px !important;' href="<?= SITE_URL ?>students/import"><button class='button'>UPLOAD</button></a>
        </div>
    </section>


    <section class='table_holder'>
        <table class='general_table head_table'>
            <thead>
                <tr>
                    <td><p>Last Name</p></td>
                    <td><p>First Name</p></td>
                    <td><p>Email</p></td>
                    <td><p>Year</p></td>
                    <td><p>Edit</p></td>
                    <td><p>Delete</p></td>
                </tr>
            </thead>
        </table>

        <table class='general_table'>
            <tbody>
                <?php
                $model->students = $model->users;
                if(count($model->students)>0) { ?>
                    <? foreach($model->students as $obj){ ?>
                    <tr>
                        <td><p><?php echo $obj->first_name; ?></p></td>
                        <td><p><?php echo $obj->last_name; ?></p></td>
                        <td><p><?php echo $obj->email; ?></p></td>
                        <td><p><?php echo $obj->year_level; ?></p></td>
                        <td>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>students/update/<?php echo $obj->id; ?>">
                                <i class="fas icon-pencil fa-pencil-alt"></i>


                            </a>
                        </td>
                        <td>
                            <a class="btn-actions" href="<?php echo SITE_URL; ?>students/update/<?php echo $obj->id; ?>">
                                <i class="fas fa-times-circle"></i>
                            </a>
                        </td>
                    </tr>
                    <? } ?>
                <? } else { ?>
                <? }?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </section>
    <?php echo footer(); ?>
    <script type="text/javascript">
        var site_url = '<?= SITE_URL.'students';?>';
        var total_pages = <?= $model->pagination->total_pages;?>;
        var page = <?= $model->pagination->current_page_index;?>;

        $('form :input').change(function(e){
            $(this).parents('form').submit();
        });
    </script>

</section>

