<section class='update_page'>
    <section class='filters_holder'>
        <h2>MEDIA UPDATE</h2>
    </section>
    <section class='table_holder'>
        
        <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
            
            <div class="form-group">
                <label>Video Title</label>
                <input type="text" name="title">
            </div>
            <div class="form-group">
                <label>Video Url</label>
                <input type="text" name="title">
            </div>
            <div class="form-group">
                <label>Video Year Levels</label>
                <div class='checkbox'>
                    <label>Year 1</label>
                    <input type="checkbox" name="yr1" value='Year 1' checked='checked'>
                    <label>Year 2</label>
                    <input type="checkbox" name="yr2" value='Year 2'>
                    <label>Year 3</label>
                    <input type="checkbox" name="yr3" value='Year 3'>
                    <label>Year 4</label>
                    <input type="checkbox" name="yr4" value='Year 4'>
                    <label>Year 5</label>
                    <input type="checkbox" name="yr5" checked='checked' value='Year 5'>
                </div>
            </div>
            <div class="form-group">
                <label>Video Description</label>
                <textarea></textarea>
            </div>
          <input type="hidden" name="id" value="<?php echo $model->user->id;?>" />
          <input type=hidden name="token" value="<?php echo get_token(); ?>" />
          <button type="submit" class=" button">Save</button>
          
        </form>
    </section>
</section>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
});
</script>