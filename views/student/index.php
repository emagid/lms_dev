<? 
    $user = $model->user->id;
    $cstudent = \Model\User::getItem(null,['where'=>"id = $user"]);
    $studCoursesSql = "SELECT * from courses where id IN (SELECT course_id from course_modules where id IN (SELECT course_module_id from user_course_modules where user_id = $user));";
    $studCourseCount = \Model\Course::getCount(['where'=>"id IN (SELECT course_id from course_modules where id IN (SELECT course_module_id from user_course_modules where user_id = $user))"]);
    $cStudCourses = \Model\Course::getList(['sql'=>$studCoursesSql]);
    $school_color = '#000033';
?>

<style type="text/css">
    html, body {
        background-color: transparent;
    }
</style>

<section class='student_page course_page'>
    <section class='filters_holder' style='background-color: <?= $school_color ?>' >
        <div class='filters'>
            <div class='account_stats threeup'>
                <div>
                    <p class='amount'><?= $studCourseCount; ?></p>
                    <p>COURSES</p>
                </div>
                <div>
                    <p class='amount'>24</p>
                    <p>HOURS LEFT</p>
                </div>
                <div>
                    <p class='amount'>10</p>
                    <p>HOURS COMPLETE</p>
                </div>
            </div>
        </div>
    </section>

    <section class='background' style="background-image: url(' <?= FRONT_IMG ?>../img/account.jpeg');"></section>

    <section class='course_info student_course'>
        <? foreach ($cStudCourses as $course) { 
            $module_count = \Model\Course_Modules::getCount(['where'=>"course_id = $course->id"]);
            $module = \Model\Course_Modules::getItem(null,['where'=>"course_id = $course->id", 'orderBy'=>"id asc"]);
            $user_modules = \Model\User_Course_Modules::getItem(null, ['where'=>"course_module_id = $module->id"]);
        ?>

            <? if ( $course != null ) { ?>
                <div class='container'>   
                    <div class='flex_info'>
                        <div class='video_info'>
                            <a href="/student/video/<?= $course->id?>">
                                <div class='vid_holder'>
                                    <div class='vid_hover'><i class="far fa-play-circle"></i></div>
                                    <img src="<?= FRONT_IMG ?>../img/video_img.png">
                                </div>
                            </a>

                            <div class='progress'>
                                <div style="background-color: <?= $school_color ?>; width: <?= $user_modules->video_progress_in_percent*100 ?>%"><p><?= $user_modules->video_progress_in_percent*100 ?>%</p></div>
                            </div>
                            <p class='time' style="color: <?= $school_color ?>"><?= gmdate("i:s", $module->video_duration_in_seconds);?> Left</p>
                            <div class='moreinfo'>
                                <a href="/student/course/<?=$course->id?>" class='button'>MORE INFO</a>
                            </div>
                            <? if($course->isCompleted($model->user->id)) { ?>
                                <div id="get-certificate" class='moreinfo'>
                                    <!-- <a onclick='get_certificate(<?=$course->id?>)' class='button'>Get Certificate</a> -->
                                    <a href="/certificate/index/<?=$course->id?>" class="button">Get Certificate</a>
                                </div>
                            <? } ?>
                        </div>

                        <div class='info'>
                           <p class='course_title'><?= $course->title ?></p>
                           <p class='module_title'><?= $module->title ?> <span>(1/<?=$module_count?>)</span></p>
                            <p class='desc'><?= $module->short_description ?></p>
                            <p class='readmore'><a href="/student/course/<?=$course->id?>">READ MORE</a></p>
                        </div>

                    </div>

                </div>
            <? } else { ?>
                <!-- If no courses please show this -->
                <div class='container'>   
                    <div class='flex_info'>
                        <p>You currently have no courses</p>
                    </div>
                </div>
            <? } ?>
        <? } ?>

    </section>

</section>

<script type="text/javascript">
    // $(document).ready(function(){
        function get_certificate(course){
            $.post("/certificate",{user: <?=$model->user->id?>, course: course}, function(data){
                console.log(data);
            })
        }
    // })
</script>