<link href="//players.brightcove.net/videojs-overlay/2/videojs-overlay.css" rel="stylesheet">

<style type="text/css">
    #start-quiz {
      margin: auto;
        top: 50%;
        text-align: center;
        transform: translateX(-50%);
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
    }
</style>


<? 
    $user = $model->user->id;
    $ucm = \Model\User_Course_Modules::getItem(null,['where'=>"user_id = ".$user." AND course_module_id = ".$model->course_module->id]);
    $questions = \Model\Question::getList(['where'=>"course_module_id = ".$model->course_module->id, 'orderBy'=>"number asc"]);
    $school_color = '#000033'; 
?>

<section class='video_page video_pg' style="background-image: url(' <?= FRONT_IMG ?>../img/account.jpeg');">
    <section class='filters_holder' style='background-color: <?= $school_color ?>' >
        <p>
            <a href='/student'>
                <i class="fas fa-arrow-left"></i>MY COURSES
            </a> 
            <span class='slash'>/</span> 
            <!-- <span>Course Title</span></p> -->
            <span><?=$model->course->title?></span></p>
    </section>
    <div class='container'>   
        <div class='flex_info'>

            <? if($ucm->can_watch_video()){ ?>
                <video id="myPlayerID"
                   data-video-id="<?=$model->course_module->video_url?>"
                   data-account="1752604059001"
                   data-player="default"
                   data-embed="default"
                   data-application-id
                   class="video-js"
                   controls
                   width="640"
                   height="360"
                    style="margin: auto;"></video>

                <? if($ucm->can_take_quiz()){ ?>
                    <!-- <div id-"take-quiz" style="display: none;"> -->
                        <a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#Quiz" id="start-quiz" style="display: none;">Start Quiz</a>

                    <!-- </div> -->
                <? } ?>
            <? } else { ?>

                    <? if($ucm->can_take_quiz()){ ?>
                        <a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#Quiz" id="start-quiz">Start Quiz</a>
                    <? } ?>    
            <? } ?>
        </div>
    </div>



    
    <!-- Quiz Modal -->
    <div class="modal fade quiz-modal" id="Quiz">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <? foreach ($questions as $i=>$question) { ?>
                <div class='question'>
                    <div class="modal-header">
                        <div class="row">
                            <h4>QUIZ<i class="fa fa-angle-right"></i><span><?=$model->course_module->title?></span></h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div id="quest-content" class="quest">
                                <h4 class="question-title"><?=$question->number?>) <?=$question->title?></h4>
                                <ul class="list-unstyled">
                                    <? foreach ($question->getAnswers() as $answer) { ?>
                                        <li>
                                            <div class="checkbox">
                                                    <input class="cb" type="checkbox" name="answer[]" value="<?=$answer->title?>">
                                                    <?=$answer->title?> 
                                            </div>
                                        </li>
                                    <? } ?> 
                                </ul>
                                <br>
                                <br>
                                <p class='error'>Please select an answer</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p class='quest_num'><?=$question->number?> of <?=count($questions)?></p>
                        <?  if( count($questions) === 1 ) { ?>
                            <a class="btn button btn-lg quiz-btn">Submit Quiz</a>
                        <? } else if ( count($questions) === $i +1 ) { ?>
                            <a class="btn button btn-lg quiz-btn">Submit Quiz</a>
                        <? }  else { ?>
                            <a href="" class=" btn button btn-lg quiz-btn">Next Question</a>
                        <? } ?>
                    </div>
                </div>
                <? } ?>
            </div>
        </div>
    </div>


    <!-- After Quiz Completion - Failed Message -->
    <!-- <div class="modal fade quiz-modal" id="Quiz-Error" hidden>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-8 col-sm-9">
                            <h4><span>QUIZ <i class="fa fa-angle-right"></i></span> <?=$model->course_module->title?></h4>
                        </div>
                        <div class="col-md-4 col-ms-3 text-right">
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 modal-box">
                            <h4 class="question-count">8/10 questions correct</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success"  style="width: 80%;"></div>
                            </div>      
                            <p>You need to have 100% correct to continue</p>
                            <br>
                            <br>
                            <div class="alert alert-danger">
                                <p><strong>We're sorry but you have 2 answers incorrect</strong><br>
                                    Please go back and review and reanswer those questions.</p>
                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="video-player.php" class="btn btn-default btn-lg quiz-btn">Rewatch Video</a>
                    <a href="video-player-end.php" class="btn btn-success btn-lg quiz-btn">Retake Quiz</a>
                </div>
            </div>
        </div>
    </div> -->

</section>

<script src="//players.brightcove.net/5537314717001/default_default/index.min.js"></script>
<!-- script for the overlay plugin -->
<!-- <script src="//players.brightcove.net/videojs-overlay/2/videojs-overlay.min.js"></script> -->

<script type="text/javascript">
    $(document).on('ready', function(){
        var answers = [];
        $('.quiz-btn').click(function(e){
            e.preventDefault()
            var checkbox = $(this).parents('.question').children('.modal-body').children('.row').children('.quest').children('ul').children('li').children('.checkbox').children('.icheckbox_square');
            var btn = $(this).parents('.question').children('.modal-footer').children('.quiz-btn')
            var next = false
            var submit = false
            console.log(checkbox)
            $(checkbox).each(function(){
                if ( $(this).hasClass('checked') && $(btn).html() == 'Next Question' ) {
                    answers.push($(this).children('.cb:checked').val());
                    console.log(answers);
                    next = true
                } 
                else if ($(this).hasClass('checked') && $(btn).html() == 'Submit Quiz') {
                    answers.push($(this).children('.cb:checked').val());
                    submit = true
                }
            });

            if ( next ) {
                e.preventDefault();
                $(this).parents('.question').slideUp(500);
                $(this).parents('.question').next('.question').delay(500).slideDown(500);
            } else if ( submit ) {
                e.preventDefault();
                // var answers = [];
                $.post('/student/submit_quiz',{answers: answers, module: <?=$model->course_module->id?>, ucm: <?=$ucm->id?> }, function(data){
                    var correct = data['correct'];
                    var incorrect = data['incorrect'];

                    if(incorrect){
                        if(incorrect.length > 0){
                            var div = $('.quest')[$('.quest').length - 1];
                            var footer = $('.modal-footer')[$('.modal-footer').length - 1];
                            var progress = (correct.length*100)/(<?=count($questions)?>);
                            div.innerHTML = "<h4 class='question-title'>" + (correct.length)+"/<?=count($questions)?> questions correct <h4>";
                            div.innerHTML += "<div class='progress'><div style='width: "+ progress +"%; max-width: 100%'></div></div>";
                            div.innerHTML += "<p>You need to have 100% correct to continue</p>"
                            div.innerHTML += "<div class='error'><p><strong>We are sorry but you have " + incorrect.length +" answers incorrect</strong><br>Please go back and review and reanswer those questions.</p></div>";
                            footer.innerHTML = "<a id='retake' href='/student/video/<?=$model->course_module->id?>?rewatch=true' class='btn btn-default btn-lg quiz-btn'>Rewatch Video</a><a onclick='retakeQuiz("+incorrect+")' class='btn btn-success btn-lg quiz-btn'>Retake Quiz</a>";
                        } else {
                            var div = $('.quest')[$('.quest').length - 1];
                            var footer = $('.modal-footer')[$('.modal-footer').length - 1];
                            var progress = (correct.length*100)/(<?=count($questions)?>);
                            div.innerHTML = "<h4 class='question-title'> Congratulations!! You Passed this Module.<h4>";
                            div.innerHTML += "<div class='progress'><div style='width:"+ progress +"%;'></div></div>";
                            footer.innerHTML = "<a href='/student/course/<?=$model->course->id?>' class='btn btn-default btn-lg'>Continue</a>";
                        }
                    }

                    // div.appendTo($(".modal-body"));
                });
            
            } else {
                console.log(next)
                $('.quest .error').fadeIn();
            }
        });

        // $(document).on('click', '#retake', function(){
        //     retakeQuiz(incorrect);
        // });




        function retakeQuiz(incorrect) {
            $.each(incorrect, function(key, val) {
                alert('index ' + key + ' points to file ' + val);
            });
            $("#modal-window").addClass("fade").modal("show");
            // <? $incorrect ?> = incorrect;
            // var questions = <? //\Model\Question::getList(['where'=>"id IN ".$incorrect])?>
            // var div = $('.quest')[$('.quest').length - 1];
            // for each(var question in questions){
            //     div.innerHTML = "<h4 class='question-title'>" + question.number +") "+ question.title + "</h4>"
            //     div.innerHtml += "<ul class='list-unstyled'>"
            // }
        } 

        $(document).click(function(e) {
            var target = e.target;

            if ($(target).is('.modal-dialog') && !$(target).parents().is('.modal-content')) {
                $(".modal").fadeOut(300);
                setTimeout(function(){
                    $('.modal').modal("hide");
                }, 500);
            }
          });      

        var myPlayer = videojs('myPlayerID');
        myPlayer.ready(function(){
            var currentTime = 0;
            function onSeeking(){
                if (currentTime < myPlayer.currentTime()) {
                    myPlayer.currentTime(currentTime);
                }
            }

            function onSeeked(){
                if (currentTime < myPlayer.currentTime()) {
                    myPlayer.currentTime(currentTime);
                    trackVideoProgress();
                }
            }

            var goTo = "01.000";
    
            <?php if($ucm->getVideoProgressInSeconds($ucm->video_progress_in_milliseconds) != 0) { ?>
                goTo = <?= $ucm->getVideoProgressInSeconds($ucm->video_progress_in_milliseconds) ?>;
            <? } ?>

            <? if($ucm->status == 'passed') { ?>
                myPlayer.currentTime(goTo);
                myPlayer.play();
                setTimeout(function(){
                  myPlayer.on("seeking", onSeeking);
                  myPlayer.on("seeked", onSeeked);
                }, 1000);
            <? } else { ?>
                myPlayer.on("seeking", onSeeking);
                myPlayer.on("seeked", onSeeked);
            <? } ?>

            myPlayer.currentTime(goTo);
            myPlayer.play();

            myPlayer.on('ended', function() {
                myPlayer.exitFullscreen();
                $.ajax({
                  type: "POST",
                  url: "/student/video_finished",
                  data: { user_course_module_id: "<?=$ucm->id?>" },
                });
                $('#start-quiz').show(1000);
                $("#modal-window").addClass("fade").modal("show");
            });

            setInterval(function() {
                if (!myPlayer.paused()) {
                  currentTime = myPlayer.currentTime();
                  trackVideoProgress();
                }
            }, 1000);

            var playProgressMsCounter = 0;

            function trackVideoProgress() {
                // if ($('#start-quiz').is(":hidden")) {
                    var videoCurrentTime = myPlayer.currentTime();
                    $.ajax({
                        type: "POST",
                        url: "/student/video_progress",
                        data: { user_course_module_id: "<?=$ucm->id?>", video_progress_in_milliseconds: videoCurrentTime.toFixed(3) },
                    });
                    playProgressMsCounter = 0;
                // }
            }

            function onPause() {
                trackVideoProgress();
            }

            myPlayer.on('pause', onPause);
            
        })
    })
</script>