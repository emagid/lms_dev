<? 
    $school_color = '#000033' 
?>

<style type="text/css">
    html, body {
        background-color: transparent;
    }
</style>

<section class='video_page course_page'>
    <section class='filters_holder' style='background-color: <?= $school_color ?>' >
        <p>
            <a href='/student'>
                <i class="fas fa-arrow-left"></i>MY COURSES
            </a> 
            <span class='slash'>/</span> 
            <!-- <span>Course Title</span> -->
            <span><?= $model->course->title ?></span>
        </p>
    </section>

    <section class='background' style="background-image: url(' <?= FRONT_IMG ?>../img/account.jpeg');"></section>

    <section class='course_info'>
        <div class='container'>            
            <div class='flex_info'>
                <img src="<?= FRONT_IMG ?>../img/video_img.png">
                <div class='info'>
                   <!-- <p class='title'>2017 Social Media Foundation Training for Collegiate Athletics</p>  -->
                   <p class='title'><?= $model->course->title ?></p>
                   <!-- <p>This is the 2017 Social Media Education Program for Collegiate Athletics. The program is part of a National Social Media Initiative in Athletics dedicated to establishing a standard of digital excellence by championing foundation social media education and providing best practices, necessary tools, policies and procedures to empower, equip and mitigate risk in the lives of athletic stakeholders and their constituents. The program includes social media standards gleaned from the United States Government and Fortune 500 companies as well as valuable lessons and positive instructions from collegiate and professional sports social media victories and defeats from around the nation. Our passion is to ensure that every user successfully complete their program and get certified for a more compassionate, successful and safer digital future.</p> -->
                   <p><?= $model->course->description ?></p>
                   
                   <div class='contact_info'>
                       <p>CONTACT INFORMATION</p>
                        <p>Your Brand's Assignment Manager</p>
                        <p>(123) 234-3456</p>
                        <p>Assignment.Manager.Email@brand.edu.com</p>
                        <p>8:00 AM - 5:00 PM (PST)</p>
                   </div>
                </div>

            </div>

            <section class='videos'>

                <? 
                    $video_active = false;

                    foreach ($model->modules as $id => $module) { 
                    $ucm = \Model\User_Course_Modules::getItem(null,['where'=>"user_id = ".$model->user->id . " AND course_module_id = ".$module->id]);
                    $input = $ucm->video_progress_in_milliseconds;
                    $video_progress = floor($input/60000).":".floor(($input/1000) % 60);
                    if($id > 0 ){
                        
                        $previous_number = $module->number-1;
                        $previous_module = \Model\Course_Modules::getItem(null,['where'=>"course_id = ".$model->course->id." AND number = ".$previous_number]);
                        $previous_video = \Model\User_Course_Modules::getItem(null,['where'=>"user_id = ".$model->user->id." AND course_module_id = ".$previous_module->id]);
                        
                    }
                ?>
                    <div class='video'>
                        
                            <? if ( $ucm->video_progress_in_percent*100 < 100 ) { 
                    
                                if ( $id == 0 || $previous_video->status == 'passed' ) { ?>
                                    <div class='play'>
                                        <a href="/student/video/<?=$module->id?>">
                                            <i class="far fa-play-circle" style='font-size: 16px;'></i>
                                        </a>
                                    </div>
                                <? } ?>
                                <!-- <p>fjdsp</p> -->

                            <? } else { ?>
                                <div class='play' style='background-color: #65bd42'>
                                    <a href="/student/video/<?=$module->id?>" class="no_link">
                                        <i class="fas fa-check"></i>
                                    </a>
                                </div>
                            <? } ?>
                        <p class='title'><?= $id + 1 ?>. <?= $module->title ?></p>
                        <div>
                            <div class='progress'>
                                <div style="width: <?=$ucm->video_progress_in_percent*100?>%"></div>
                            </div>
                            <p class='time'><?= $video_progress ?><span> / <?= $module->getVideoDuration($module->video_duration_in_seconds); ?></span></p>
                        </div>
                        <div class='details'>
                            <p><?= $module->short_description ?></p>
                        </div>
                    </div>
                <? } ?>

                <!-- <div class='video'>
                    <div class='play'>
                        <i class="far fa-play-circle"></i>
                    </div>
                    <p class='title'>1. Introduction and Overview</p>
                    <div>
                        <div class='progress'>
                            <div></div>
                        </div>
                        <p class='time'>1:00 <span>/ 3:00</span></p>
                    </div>
                    <div class='details'>
                        <p>No matter how you lock down your privacy settings, what you say and do on social media can be accessed, watched, recorded and shared by people you don’t know. We help you better understand digital privacy to make educated decisions about what you want the world to know about you.</p>
                    </div>
                </div> -->
                <!-- <div class='video'>
                    <div class='play'>
                        <i class="far fa-play-circle"></i>
                    </div>
                    <p class='title'>1. Introduction and Overview</p>
                    <p class='status' style='color: <?= $school_color ?>'>COMPLETE</p>
                    <div>
                        <div class='progress'>
                            <div style='width: 100%;'></div>
                        </div>
                        <p class='time'>1:00 <span>/ 3:00</span></p>
                    </div>

                    <div class='details'>
                        <p>No matter how you lock down your privacy settings, what you say and do on social media can be accessed, watched, recorded and shared by people you don’t know. We help you better understand digital privacy to make educated decisions about what you want the world to know about you.</p>
                    </div>
                </div>
                <div class='video'>
                    <div class='play'>
                        <i class="far fa-play-circle"></i>
                    </div>
                    <p class='title'>1. Introduction and Overview</p>
                    <p class='status' style='color: #868686'>NOT STARTED</p>
                    <div>
                        <div class='progress'>
                            <div style='width: 0%;'></div>
                        </div>
                        <p class='time'>1:00 <span>/ 3:00</span></p>
                    </div>
                    <div class='details'>
                        <p>No matter how you lock down your privacy settings, what you say and do on social media can be accessed, watched, recorded and shared by people you don’t know. We help you better understand digital privacy to make educated decisions about what you want the world to know about you.</p>
                    </div>
                </div> -->


            </section>
        </div>

    </section>

</section>


<script type="text/javascript">
    $('.video').click(function(){
        if ( $(this).children('.details').is(':visible') ) {
            $('.details').slideUp();
        }else {
            $('.details').slideUp();
            $(this).children('.details').slideDown();
        }
    });
</script>