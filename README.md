sbe reporting

Full data import steps:

##1. Script to import all admin users
```
php bin/importUser.php
```

##2. Script to import all last year data
```
php bin/lastYearImport.php 'aaaa'
php bin/lastYearImport.php 'aaab'
php bin/lastYearImport.php 'aaac'
php bin/lastYearImport.php 'aaad'
php bin/lastYearImport.php 'aaae'
php bin/lastYearImport.php 'aaaf'
php bin/lastYearImport.php 'aaag'
php bin/lastYearImport.php 'aaah'
php bin/lastYearImport.php 'aaai'
php bin/lastYearImport.php 'aaaj'
```

##3. Script to importing all budget data
```
php bin/restaurantBudgetImport.php 'Restaurants - Bazaar SLS Beverly Hills' 'bazaar beverly hills' 1
php bin/restaurantBudgetImport.php 'Restaurants - Bazaar South Beach' 'bazaar south beach' 1
php bin/restaurantBudgetImport.php 'Restaurants - Cleo HW' 'cleo' 1
php bin/restaurantBudgetImport.php 'Restaurants - Cleo South Beach' 'cleo south beach' 1
php bin/restaurantBudgetImport.php 'Restaurants - Katsuya South Beach' 'katsuya south beach' 1
php bin/restaurantBudgetImport.php 'Restaurants - RMS' 'raleigh miami rms' 2
php bin/restaurantBudgetImport.php 'Restaurants - The Library' 'the library' 1
php bin/restaurantBudgetImport.php 'Restaurants - Tres Beverly Hills' 'tres beverly hills' 1
php bin/restaurantBudgetImport.php 'Hyde Beach Kitchen + Cocktails v3' 'hyde beach kitchen and cocktail' 3
php bin/restaurantBudgetImport.php 'Katsuya Brentwood v3' 'katsuya brentwood' 1
php bin/restaurantBudgetImport.php 'Katsuya Downtown v3' 'katsuya downtown' 1
php bin/restaurantBudgetImport.php 'Katsuya Glendale v3' 'katsuya glendale' 1
php bin/restaurantBudgetImport.php 'Katsuya Hollywood v3' 'katsuya hollywood' 1

php bin/nightlifeBudgetImport.php 'Nightlife - Hyde Bellagio' 'hyde bellagio' 1
php bin/nightlifeBudgetImport.php 'Sayers' 'the sayers club' 2
php bin/nightlifeBudgetImport.php 'Abbey v3' 'the abbey' 2
php bin/nightlifeBudgetImport.php 'Create' 'create' 2
php bin/nightlifeBudgetImport.php 'Greystone' 'greystone manor' 2
php bin/nightlifeBudgetImport.php 'Hyde Sunset v3' 'hyde sunset kitchen + cocktails' 2
php bin/nightlifeBudgetImport.php 'Nightlife - Hyde South AAA' 'hyde miami aaa arena' 1
php bin/nightlifeBudgetImport.php 'Nightlife - Hyde South Beach' 'hyde south beach' 1
php bin/nightlifeBudgetImport.php 'Nightlife - Hyde Staples Center' 'hyde staples' 1
php bin/nightlifeBudgetImport.php 'Hyde LV Arena - Nightlife' 'hyde t-mobile arena' 1
*Double Barrel is the damn nightlife*
php bin/nightlifeBudgetImport.php 'Restaurants - Double Barrel' 'double barrel' 1
php bin/nightlifeBudgetImport.php 'Vetro v3' 'doheny room' 3

php bin/hotelBudgetImport.php 'Hotels - Raleigh' 'the raleigh hotel miami'
php bin/hotelBudgetImport.php 'Hotels - Redbury HW' 'the redbury hw hotel'
php bin/hotelBudgetImport.php 'Hotels - Redbury NY Martha Washington' 'the redbury ny'
php bin/hotelBudgetImport.php 'Hotels - Redbury South Beach' 'the redbury sb hotel'
php bin/hotelBudgetImport.php 'Hotels - SLS South Beach' 'sls sb hotel'
```

```
php bin/restaurantBudgetImport_2017.php 'Katsuya Brentwood' 'katsuya brentwood' 1
php bin/restaurantBudgetImport_2017.php 'Katsuya Downtown' 'katsuya downtown' 1
php bin/restaurantBudgetImport_2017.php 'Katsuya Glendale' 'katsuya glendale' 1
php bin/restaurantBudgetImport_2017.php 'Katsuya Hollywood' 'katsuya hollywood' 1
php bin/restaurantBudgetImport_2017.php 'Cleo DT' 'cleo downtown la' 1

php bin/restaurantBudgetImport_2017.php 'Katsuya Brentwood' 'katsuya brentwood' 1 && php bin/restaurantBudgetImport_2017.php 'Katsuya Downtown' 'katsuya downtown' 1 && php bin/restaurantBudgetImport_2017.php 'Katsuya Glendale' 'katsuya glendale' 1 && php bin/restaurantBudgetImport_2017.php 'Katsuya Hollywood' 'katsuya hollywood' 1 && php bin/restaurantBudgetImport_2017.php 'Cleo DT' 'cleo downtown la' 1
 
php bin/nightlifeBudgetImport_2017.php 'Create' 'create' 1
php bin/nightlifeBudgetImport_2017.php 'Doheny Room' 'doheny room' 1
php bin/nightlifeBudgetImport_2017.php 'Hyde AAA' 'hyde miami aaa arena' 1
php bin/nightlifeBudgetImport_2017.php 'Hyde Beach Kitchen Cocktails' 'hyde beach kitchen + cocktail' 1
php bin/nightlifeBudgetImport_2017.php 'Hyde Bellagio' 'hyde bellagio' 1
php bin/nightlifeBudgetImport_2017.php 'Hyde Staples' 'hyde staples' 1
php bin/nightlifeBudgetImport_2017.php 'Hyde Sunset' 'hyde sunset kitchen + cocktails' 1
php bin/nightlifeBudgetImport_2017.php 'Hyde Tmobile Arena' 'hyde t-mobile arena' 1
php bin/nightlifeBudgetImport_2017.php 'Nightingale' 'greystone manor' 1
php bin/nightlifeBudgetImport_2017.php 'Double Barrel' 'double barrel' 1

php bin/nightlifeBudgetImport_2017.php 'Create' 'create' 1 && php bin/nightlifeBudgetImport_2017.php 'Doheny Room' 'doheny room' 1 && php bin/nightlifeBudgetImport_2017.php 'Hyde AAA' 'hyde miami aaa arena' 1 && php bin/nightlifeBudgetImport_2017.php 'Hyde Beach Kitchen Cocktails' 'hyde beach kitchen + cocktail' 1 && php bin/nightlifeBudgetImport_2017.php 'Hyde Bellagio' 'hyde bellagio' 1 && php bin/nightlifeBudgetImport_2017.php 'Hyde Staples' 'hyde staples' 1 && php bin/nightlifeBudgetImport_2017.php 'Hyde Sunset' 'hyde sunset kitchen + cocktails' 1 && php bin/nightlifeBudgetImport_2017.php 'Hyde Tmobile Arena' 'hyde t-mobile arena' 1 && php bin/nightlifeBudgetImport_2017.php 'Nightingale' 'greystone manor' 1 && php bin/nightlifeBudgetImport_2017.php 'Double Barrel' 'double barrel' 1
```

## 4. Download Daily CSV
```
bash dailyBash.sh
php bin/dailyImport.php
```

## Cronjobs

2:05PM EST, download daily CSV file from averoinc, and import to DB
`5 14 * * * curl http://reporting.sbe.com/pages/test1234`

2.15 PM EST, auto submit all location script
`15 19 * * * /usr/bin/casperjs /var/www/html/bin/autoSubmitAllLocations.js`

2:55PM EST, generate PDF
`55 14 * * * curl http://reporting.sbe.com/pages/test5343`

3.01PM EST, send PDF report
`01 15 * * * curl http://reporting.sbe.com/pages/test6789`

3.02PM EST, send sayersclub report
`02 15 * * * curl http://reporting.sbe.com/pages/testsayersclub`

## Useful queries

```
select report_data.value, report_data.insert_time, report_data.update_time, report_data.date, location.name, field.name from report_data
join location on report_data.location_id = location.id
join field on field.id = report_data.field_id
where report_data.date = '2016-07-19' and (field.name = 'gm_65' or field.name = 'gm_172')
```
```
select report_data.value, report_data.insert_time, report_data.update_time, report_data.date, location.name, field.name
from report_data
join location on report_data.location_id = location.id
join field on field.id = report_data.field_id
where report_data.date = '2016-07-19' and field.name like 'gm_%' order by report_data.insert_time desc
```

##Umami Beta Process
```
bash dailyBash.sh
```

```
php bin/dailyUmamiImport.php
```

```
Run CapserJS
```

```
php bin/umamiGenerateReport.php
```

## Add new venue
1. Update newImportUser.php, `username, password, location, owned, type, GM`
2. Update location $MAP and $PRINT
3. Run `php bin/newImportUser.php`
4. Run sample report


## Data Import to report_data_2016

###Create Table
```CREATE TABLE report_data_2016 AS SELECT * FROM report_data WHERE true = false;```

### Delete FC field, which is not used at all.
```
delete from report_data where id in (select report_data.id from report_data join field on field.id = report_data.field_id where field.name like 'fc_%');
```

1. At Dec 6, Moved all 2016-04-01 to 2016-09-30 GM data, total 270139 records.
```
INSERT INTO report_data_2016
select report_data.*
from report_data 
join field on field.id = report_data.field_id 
where report_data.date <= '2016-09-30' and field.name like 'gm_%';
```

```
delete from report_data 
where id in (select report_data.id
             from report_data 
             join field on field.id = report_data.field_id 
             where report_data.date <= '2016-09-30' and field.name like 'gm_%');
```

report_data table left 1356168 records.


2.

INSERT INTO report_data_2016
select report_data.*
from report_data 
join field on field.id = report_data.field_id 
where report_data.date >= '2017-02-01' and field.name like 'bd_%';

delete from report_data 
where id in (
select report_data.id
from report_data 
join field on field.id = report_data.field_id 
where report_data.date >= '2017-02-01' and field.name like 'bd_%'
)



