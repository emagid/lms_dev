<?php

include("emagid.consts.php");
include("emagid.db.php");

$site_routes = [
    'routes' => [
        [
            'name' => 'login',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/login/{?action}",
            'controller' => 'login',
            'action' => 'login',
        ],
        [
            'name' => 'admin',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/{?controller}/{?action}/{?id}",
            'controller' => 'dashboard',
            'action' => 'index',
            'authorize' => [
                'roles' => ['admin', 'venue'],
                'login_url' => '/admin/login'
            ]
        ]
    ]
];

$emagid_config = array(
    'debug' => true,          // When debug is enabled, Kint debugging plugin is enabled.
    //  you can use d($my_var) , dd($my_var) , Kint::Trace() , etc...
    //  documentation available here : http://raveren.github.io/kint/

    'root' => SITE_URL,

    'template' => 'modernvice',  // template must be found in /templates/<template_name>/<template_name>.php //TODO change template
    // so in this example we will have /templates/default/default.php
    // please open the template file to see how the view is being rendered .

    'connection_string' => array(
        'driver' => DB_DRIVER,
        'db_name' => DB_NAME,
        'username' => DB_USER,
        'password' => DB_PWD,
        'host' => DB_HOST
    ),

    'include_paths' => array(
        'libs/Mandrill',
        'libs/Mailchimp'
    ),

    'email' => array(
        'api_key' => '85UZ0rPwBOR5chLjxQiYaw',
        'from' => array(
            'email' => 'support@sbe.com', //TODO change email || TODO st-dev validate email
            'name' => 'SBE Reporting' //TODO change email || TODO st-dev validate email
        )
    )
); 
