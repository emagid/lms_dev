<div class="blog_sidebar">
	<div class="col-md-6 col-md-push-18 right_sidebar">

		<!-- Search Box -->
		<form role="form" class="form-inline"> 
		  <div class="form-group has-feedback has-feedback-left">
		    <label class="control-label sr-only">Search</label>
		    <div class="inline-feedback">
		      <input type="text" class="form-control" placeholder="Search" />
		      <i class="form-control-feedback glyphicon glyphicon-user"></i>
		    </div>
		  </div>
		</form>

		<div class="blog_banners">
			<a href=""><img src="/content/frontend/img/hi3.jpg" alt="" class="img-responsive" /></a>	
		</div>
		
		<div class="sidebar_blurb recent_tweets">
			<h5>Recent Tweets</h5>
			<ul>
				<li>
					<a href="#">
						<p>
							<img src="/content/frontend/img/recent_tweets.png" alt="" class="img-responsive" />
							The Leonardo Dicaprio designed Jaeger-LeCoultre Master Minute Repeater. Valued at $175,000.
						</p>
						<span>5 hours ago</span>
					</a>
				</li>

				<li>
					<a href="#">
						<p>
							<img src="/content/frontend/img/recent_tweets.png" alt="" class="img-responsive" />
							The Kobe Bryant X Hublot King Power Mamba Chronograph #kobebryant
						</p>
						<span>5 hours ago</span>
					</a>
					
				</li>
			</ul>
		</div>


		<div class="blog_banners">
			<a href=""><img src="/content/frontend/img/hi2.jpg" alt="" class="img-responsive" /></a>	
		</div>	

		<div class="sidebar_blurb recent_tweets instagram_feed">
			<h5>Instagram Feed</h5>
			
			<div class="container">
			    <div class="row">

			        <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-e-a.akamaihd.net/hphotos-ak-xfp1/t51.2885-15/10954386_748672485240980_1881123668_n.jpg" alt="..." />
			            </a>
			        </div>

			          <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-c-a.akamaihd.net/hphotos-ak-xfp1/t51.2885-15/10632021_268674966664306_91727428_n.jpg" alt="..." />
			            </a>
			        </div>

			         <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-a-a.akamaihd.net/hphotos-ak-xap1/t51.2885-15/10523542_329318050563176_345103992_n.jpg" alt="..." />
			            </a>
			        </div>

			    </div>

			    <div class="row">

			        <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-a-a.akamaihd.net/hphotos-ak-xaf1/t51.2885-15/10693584_323114284536832_1141431888_n.jpg" alt="..." />
			            </a>
			        </div>

			          <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-h-a.akamaihd.net/hphotos-ak-xta1/t51.2885-15/10684214_316312558551759_2134755970_n.jpg" alt="..." />
			            </a>
			        </div>

			         <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-e-a.akamaihd.net/hphotos-ak-xtp1/t51.2885-15/10575987_583750381735612_580704075_n.jpg" alt="..." />
			            </a>
			        </div>

			    </div>

			    <div class="row">

			        <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://scontent.cdninstagram.com/hphotos-xfp1/t51.2885-15/10507953_746191665447060_1812365227_n.jpg" alt="..." />
			            </a>
			        </div>

			          <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-b-a.akamaihd.net/hphotos-ak-xpf1/t51.2885-15/10175227_519835234804241_906041523_n.jpg" alt="..." />
			            </a>
			        </div>

			         <div class="col-xs-2 thumb_grid">
			             <a href="#" class="thumbnail">
			            <img src="https://igcdn-photos-b-a.akamaihd.net/hphotos-ak-xpf1/t51.2885-15/1725419_1400718660181769_1544925583_n.jpg" alt="..." />
			            </a>
			        </div>

			    </div>

			</div>


		</div>
	</div>
</div>