<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->


<!--[if lte IE 9]>
<script type="text/javascript" src="<?=FRONT_JS?>js/ie8/html5shiv-printshiv.min.js"></script>
<script "text/javascript" src="<?=FRONT_JS?>js/ie8/respond.min.js"></script>
<![endif]-->

<head>
    <!-- META DATA -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<!--    <title>--><?// if($this->emagid->route['controller'] == "brands"){echo $this->viewData->brand->name; echo " watches |";}?><!-- --><?//= $this->configs['Meta Title']; ?><!--</title>-->
    <title>LMS</title>
    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">
    <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

    <meta property="og:title" content="<?=SITE_NAME?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=SITE_URL?>" />

    <meta name="msapplication-TileColor" content="#b91d47">
    <meta name="msapplication-TileImage" content="<?=SITE_URL?>favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="<?=SITE_URL?>content/frontend/img/favicon.png">
    <link rel = "shortcut icon" href = "<?=SITE_URL?>content/frontend/img/favicon.png">
    <link rel = "stylesheet" href = "<?=SITE_URL?>content/frontend/css/main.css">
    <meta name="theme-color" content="#ffffff">
    <meta name="google-site-verification" content="jjNUcRDPX0H4RcovCMgI763lWcMQCCwLYe8_Img5Ki8" />
    <link href='https://fonts.googleapis.com/css?family=Cabin:400,500,600' rel='stylesheet' type='text/css'>
    <!-- Disables Zoom on Mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- <link rel="stylesheet" href="print.css" type="text/css" media="print"> -->


    <!-- SITE CSS -->

    <?php
    if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='localhost') {
        define('WP_ENV', 'development');
    } else {
        define('WP_ENV', 'production');
    }
    ?>
 <link rel="stylesheet" href="<?=FRONT_CSS?>main.css">
    <script src="<?=FRONT_JS?>jquery.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=FRONT_JS?>vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="<?=FRONT_JS?>jquery.zoom.min.js"></script>

    <? if (WP_ENV == 'development') { ?>
        <script src="<?=FRONT_JS?>main.js"></script>
    <?php } else { ?>
        <script src="<?=FRONT_JS?>main.min.js"></script>
    <?php } ?>

    <script src="<?=FRONT_JS?>script.js"></script>
    <script src="<?=FRONT_JS?>ui_scripts.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


    <script>
        function fbshareCurrentPage(){
            window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(window.location.href)+"&t="+document.title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;
        }
    </script>

</head>
    <body>
    <?display_notification();?>
    <? require_once('header.php'); ?>

    <?php $emagid->controller->renderBody($model); ?>
    <? require_once('footer.php'); ?>
    <?
    function footer()  { ?>

        <?php script('jquery.min.js',ADMIN_JS); ?>
        <?php script('Chart.js',ADMIN_JS); ?>

        <?php script('main.js',ADMIN_JS); ?>
        <?php script('ckeditor/ckeditor.js',ADMIN_JS); ?>

        <!--    <script src="/content/admin/js/plugins/bootstrap-multiselect.js"></script>-->
        <link rel = "stylesheet" type = "text/css" href = "<?=ADMIN_CSS.'jquery.datetimepicker.css'?>">
        <script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>
        <?php
    }
    ?>

