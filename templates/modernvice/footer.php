<?php
/**
 * Created by PhpStorm.
 * User: Foran
 * Date: 2/21/2018
 * Time: 5:55 PM
 */

?>


<script type="text/javascript">(function () {
        if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
        if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
            var h = d[g]('body')[0];h.appendChild(s); }})();
</script>
<script type='text/javascript'>
    function slug_async(in_elem,out_elem) {
        in_elem.on('keyup',function(e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g,'-');
            out_elem.val(val.toLowerCase());
        });
    }

    var params = <?php echo (isset($model->params)) ? json_encode($model->params) : json_encode((object)array()); ?>;
    // in case result is an array, change it to object
    if(params instanceof Array) {
        params = {};
    }

    $(document).ready(function() {


        /**
         * builds a url with params from a params object passed to it
         * @param {type} url: url of page
         * @param {type} params: params object with key as param key name and value as param value
         * @param {type} redirect: true or false if we want to redirect to the url
         * @returns {Boolean|String}
         */

        function build_url(url,params,redirect) {

            var params_arr = [];
            $.each(params,function(i,e) {
                params_arr.push(i+"="+e);
            });
            if(redirect) {
                window.location.href = url + "?"+params_arr.join("&");
                return false;
            } else {
                return url + "?"+params_arr.join("&");
            }
        }


        if (typeof total_pages !== 'undefined' && typeof page !=='undefined') {
            // the variable is defined

            $(function() {
                $('div.paginationContent').pagination({
                    pages: total_pages,
                    currentPage: page,
                    cssStyle: 'light-theme',
                    onPageClick: function(pageNumber,event) {
                        var url_params = params || {};
                        url_params.page = parseInt(pageNumber);
                        var full_url = site_url;
                        build_url(full_url,url_params,true);
                        //window.location.href = full_url+"?page="+page;
                    }
                });
            });
        }
    });
</script>
