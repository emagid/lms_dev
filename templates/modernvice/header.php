
<header>
    <a href="/account"><img class='logo' src="../../content/frontend/img/schl_logo.jpg"></a>
    <? if($model->user != null){
    	if($model->user->role == 'student') { ?>
    
    <!-- Student view -->
		    <nav>
				<div class='more_links'>
			    	<a  class='master_link'>ACCOUNT</a>
		    		<div class='sub_nav'>
		    			<a href="<?= SITE_URL ?>student">COURSES</a>
		    			<a href="<?= SITE_URL ?>account/logout">LOGOUT</a>
		    		</div>
		    	</div>
		    </nav>

	    <? } else { ?>
	    <!-- $this->viewData->user) -->
			    <nav>
			    	<div class='more_links'>
		                <a class='master_link'>USERS</a>
		                <div class='sub_nav'>
		                    <a href="<?= SITE_URL ?>students">MANAGE USERS</a>
		                    <a href="<?= SITE_URL ?>students/import">ADD USERS</a>
		                    <a href="<?= SITE_URL ?>categories">CATEGORIES</a>
		                </div>
		            </div>
			    	<div class='more_links'>
				    	<a class='master_link'>COURSES</a>
			    		<div class='sub_nav'>
			    			<a href="<?= SITE_URL ?>courses/">VIDEO LIBRARY</a>
			    			<a href="<?= SITE_URL ?>courses/media">COURSE ASSIGNMENT</a>
			    			<!-- <a href="<?= SITE_URL ?>courses/managemedia">MANAGE MEDIA</a> -->
			    		</div>
			    	</div>
			    	<!-- <div><a href="<?= SITE_URL ?>account">ACCOUNT</a></div> -->
			    	<div class='more_links'>
				    	<a  class='master_link'>ACCOUNT</a>
			    		<div class='sub_nav'>
			    			<a href="<?= SITE_URL ?>account">SETTINGS</a>
			    			<a href="<?= SITE_URL ?>home/plans">BILLING</a>
			    			<a href="<?= SITE_URL ?>account/logout">LOGOUT</a>
			    			<!-- <a href="<?= SITE_URL ?>courses/managemedia">MANAGE MEDIA</a> -->
			    		</div>
			    	</div>
			    </nav>

			    <div id="hamburger">
				      <div class="bar1 hamburger_line"></div>
				      <div class="bar2 hamburger_line"></div>
				      <div class="bar3 hamburger_line"></div>
				  </div>

				  <div class='hamburger_dropdown'>
				    <p>USERS</p>
				    <a href="<?= SITE_URL ?>students">MANAGE USERS</a>
		            <a href="<?= SITE_URL ?>students/import">ADD USERS</a>
		            <a href="<?= SITE_URL ?>categories">USER CATEGORIES</a>
		            <p>COURSES</p>
		            <a href="<?= SITE_URL ?>courses/">COURSE LIBRARY</a>
					<a href="<?= SITE_URL ?>courses/media">COURSE ASSIGNMENT</a>
					<p>ACCOUNT</p>
					<a href="<?= SITE_URL ?>account">SETTINGS</a>
					<a href="<?= SITE_URL ?>home/plans">BILLING</a>
					<a href="<?= SITE_URL ?>account/logout">LOGOUT</a>
				  </div>
	    <? } 
	} else { ?>
	    <nav>
	    	<a href="<?= SITE_URL ?>home/plans">PAYMENT PACKAGES</a>
	    </nav>
	    <!-- <nav>
	    	<a href="<?= SITE_URL ?>students">STUDENTS</a>
	    	<a href="<?= SITE_URL ?>home/courses">COURSES</a>
	    	<a href="<?= SITE_URL ?>account">ACCOUNT</a>
	        <a href="<?= SITE_URL ?>account/logout">LOGOUT</a>
	    </nav> -->
    <? } ?>
</header>
<?display_notification()?>



<script type="text/javascript">
		// Hamburger
	    $("#hamburger").on('click', function(e){
	        $(".hamburger_dropdown").slideToggle();
	        $(".bar1").toggleClass('change1');
	        $(".bar2").toggleClass('change2');
	        $(".bar3").toggleClass('change3');
	    });


		$('.more_links').mouseover(function(){
			$(this).css('padding-bottom', '40px');
			$(this).children('.master_link').addClass('active_menu');
			$(this).children('.sub_nav').slideDown(100);
		});

		$('.more_links').mouseleave(function(){
			$(this).css('padding-bottom', '0');
			$(this).children('.master_link').removeClass('active_menu');
			$('.sub_nav').slideUp(100);
		});
</script>