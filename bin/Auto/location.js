/**
 * Created by zhou on 3/2/17.
 */
var casper = require('casper').create({
    verbose: true,
    logLevel: "debug"
});

var fs = require('fs');

var today = new Date();
var yesterday = new Date();
yesterday.setDate(today.getDate() - 1);
var dd = yesterday.getDate();
var mm = yesterday.getMonth() + 1; //January is 0!
var yyyy = yesterday.getFullYear();

if (dd < 10) {
    dd = '0' + dd
}

if (mm < 10) {
    mm = '0' + mm
}
today = yyyy + '-' + mm + '-' + dd;

var host = 'https://reporting.sbe.com';

casper.start(host + '/pages/autoLogin?no_login=test5343');

casper.thenOpen(host + '/pages/locations', function(){
    var content = this.getPageContent();
    var data = JSON.parse(content);
    var urls = [];

    for (var i in data) {
        var locationName = data[i]['name'];
        var locationType = data[i]['type'];
        locationName = encodeURIComponent(locationName);
        var reportUrl = host + '/admin/reporting/' + locationType + '?date=' + today + '&location=' + locationName + '&auto_submit=1';

        urls.push(reportUrl);

    }

    fs.write('location.txt', urls.join('\n'), 'w');
});

casper.run();

// COMMAND: /usr/bin/casperjs --ignore-ssl-errors=true --ssl-protocol=any /var/www/html/bin/Auto/location.js