<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/27/16
 * Time: 10:39 AM
 */

require(__DIR__.'/../index.php');
$location = $argv[1];
global $emagid;
$db = $emagid->getDb();
$fields = "('ly_152', 'ly_150', 'ly_153')";

if($location){
    $sql = "select field.name as field, report_data.value as value, location.name as location, report_data.date as date from report_data join field on field.id = report_data.field_id join location on location.id = report_data.location_id where location.name = '$location' AND field.name in $fields ";
} else {
    $sql = "select field.name as field, report_data.value as value, location.name as location, report_data.date as date from report_data join field on field.id = report_data.field_id join location on location.id = report_data.location_id where field.name in $fields";
}
$reportData = $db->getResults($sql);

$locationData = $db->getResults("select id, name, type from location");
$fieldData = $db->getResults("select id, name from field");

$locationMap = [];
$fieldMap = [];

/**
 * Format location map, it use for save data
 */
foreach($locationData as $data){
    $locationMap[$data['name']] = ['type' => $data['type'], 'id' => $data['id']];
}

foreach($fieldData as $data){
    $fieldMap[$data['name']] = $data['id'];
}

/**
 * Data structure: ['locationName' => ['2016-01-01' => ['bd_1' => 12, 'bd2' => 2]], 'location2' => []....]
 */
$summaryData = [];
foreach($reportData as $data){
    $field = $data['field'];
    $location = $data['location'];
    $date = $data['date'];
    $value = $data['value'];

    if(!isset($summaryData[$location])){
        $summaryData[$location] = [];
    }

    if(!isset($summaryData[$location][$date])){
        $summaryData[$location][$date] = [$field => $value];
    } else {
        $summaryData[$location][$date] = array_merge($summaryData[$location][$date], [$field => $value]);
    }
}

foreach($summaryData as $location => $data){
    ksort($summaryData[$location]);
}

function getData($data, $field){
    return isset($data[$field])?floatval($data[$field]):0;
}

function saveQuery($locationId, $fieldId, $value, $date)
{
    return "insert into report_data (location_id, field_id, value, date) values ($locationId, $fieldId, $value, '$date')";

}


function getMTDData($date, $data, $field, $isAverage = false)
{
    if(!isset($data[$date])){
        return 0;
    }
    $count = 0;
    $value = getData($data[$date], $field);
    $end = new Carbon\Carbon($date);
    $start = $end->copy()->startOfMonth();
    /**
     * First date MTD is 0
     */
    if($end->daysInMonth == 1){
        return $value;
    }

    $totalValue = 0;

    for($startDate = $start; $startDate->lte($end); $startDate->addDay()) {
        $date = $startDate->toDateString();

        if(!isset($data[$date])){
            continue;
        }

        $totalValue += getData($data[$date], $field);
        $count++;
    }

    return $isAverage?(round($totalValue/$count, 1)):$totalValue;
}

$saveQueries = [];

/**
 * Save MTD data
 */
foreach($summaryData as $location => $dateData){
    $type = $locationMap[$location]['type'];
    $locationId = $locationMap[$location]['id'];
    foreach($dateData as $date => $data){
        if($type == 'hotel'){
            $summaryData[$location][$date]['ly_151'] = getMTDData($date, $dateData, 'ly_150');
            // save into insert query
            $saveQueries[] = saveQuery($locationId, $fieldMap['ly_151'], $summaryData[$location][$date]['ly_151'], $date);
        }

        if($type == 'nightlife'){
            $summaryData[$location][$date]['ly_174'] = getMTDData($date, $dateData, 'ly_152');
            $saveQueries[] = saveQuery($locationId, $fieldMap['ly_174'], $summaryData[$location][$date]['ly_174'], $date);
        }

        if($type == 'restaurant'){
            $summaryData[$location][$date]['ly_172'] = getMTDData($date, $dateData, 'ly_153');
            $saveQueries[] = saveQuery($locationId, $fieldMap['ly_172'], $summaryData[$location][$date]['ly_172'], $date);
        }
    }
}

foreach($saveQueries as $query){
    echo "Executing $query \n";
    $db->getResults($query);
}







