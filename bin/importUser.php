<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/22/16
 * Time: 3:54 PM
 */

use Model\Location;

require(__DIR__.'/../index.php');

$source = [
    'BarCentroSouthBeach' => ['password' => '697161', 'location' =>'bar centro south beach', 'owned' => 0, 'type' => 'restaurant'],
    'BazaarSouthBeach' => ['password' => '581161', 'location' => 'bazaar south beach', 'owned' => 0, 'type' => 'restaurant'],
    'cleosb' => ['password' => 'abcd1234', 'location' => 'cleo south beach', 'owned' => 0, 'type' => 'restaurant'],
    'HydeBeach' => ['password' => 'HydeBeach', 'location' => 'hyde beach kitchen and cocktail', 'owned' => 0, 'type' => 'restaurant'],
    'HydeMiamiArena' => ['password' => '794466', 'location' => 'hyde miami aaa arena', 'owned' => 0, 'type' => 'nightlife'],
    'HydeSouthBeach' => ['password' => '553927', 'location' => 'hyde south beach', 'owned' => 0, 'type' => 'nightlife'],
    'KatsuyaSouthBeach' => ['password' => '194438', 'location' => 'katsuya south beach', 'owned' => 0, 'type' => 'restaurant'],
    'RaleighMiamiRMS' => ['password' => '690166', 'location' => 'raleigh miami rms', 'owned' => 0, 'type' => 'restaurant'],
    'SLSSBHotel' => ['password' => '958925', 'location' => 'SLS SB Hotel', 'owned' => 0, 'type' => 'hotel'],
    'TheRaleighHotelMiami' => ['password' => '889530', 'location' => 'The Raleigh Hotel Miami', 'owned' => 0, 'type' => 'hotel'],
    'TheRedburySBHotel' => ['password' => '232055', 'location' => 'The Redbury SB Hotel', 'owned' => 0, 'type' => 'hotel'],
    'BazaarBeverlyHills' => ['password' => '782059', 'location' => 'Bazaar Beverly Hills', 'owned' => 0, 'type' => 'restaurant'],
    'Create' => ['password' => '590455', 'location' => 'Create', 'owned' => 0, 'type' => 'nightlife'],
    'GreystoneManor' => ['password' => '612021', 'location' => 'Greystone Manor', 'owned' => 1, 'type' => 'nightlife'],
    'HydeStaples' => ['password' => '231575', 'location' => 'Hyde Staples', 'owned' => 0, 'type' => 'nightlife'],
    'hyde' => ['password' => '792133', 'location' => 'Hyde Sunset Kitchen + Cocktails', 'owned' => 1, 'type' => 'nightlife'],
    'KatsuyaBrentwood' => ['password' => '521317', 'location' => 'Katsuya Brentwood', 'owned' => 1, 'type' => 'restaurant'],
    'KatsuyaDowntown' => ['password' => '759330', 'location' => 'Katsuya Downtown', 'owned' => 1, 'type' => 'restaurant'],
    'KatsuyaGlendale' => ['password' => '353308', 'location' => 'Katsuya Glendale', 'owned' => 1, 'type' => 'restaurant'],
    'KatsuyaHollywood' => ['password' => '304937', 'location' => 'Katsuya Hollywood', 'owned' => 1, 'type' => 'restaurant'],
    'SLSBHHotel' => ['password' => '393695', 'location' => 'SLS BH Hotel', 'owned' => 0, 'type' => 'hotel'],
    'TheAbbey' => ['password' => '800624', 'location' => 'The Abbey', 'owned' => 1, 'type' => 'nightlife'],
    'Cleo' => ['password' => '100691', 'location' => 'Cleo', 'owned' => 0, 'type' => 'restaurant'],
    'TheColony' => ['password' => '188319', 'location' => 'The Colony', 'owned' => 1, 'type' => 'nightlife'],
    'TheEmersonTheatre' => ['password' => '394181', 'location' => 'The Emerson Theatre', 'owned' => 1, 'type' => 'nightlife'],
    'TheLibrary' => ['password' => '393677', 'location' => 'The Library', 'owned' => 0, 'type' => 'restaurant'],
    'TresBeverlyHills' => ['password' => '961319', 'location' => 'Tres Beverly Hills', 'owned' => 0, 'type' => 'restaurant'],
    'TheRedburyHWHotel' => ['password' => '150453', 'location' => 'The Redbury HW Hotel', 'owned' => 0, 'type' => 'hotel'],
    'TheSayersClub' => ['password' => 'Partytime1', 'location' => 'The Sayers Club', 'owned' => 1, 'type' => 'nightlife'],
    'DoubleBarrel' => ['password' => '464885', 'location' => 'Double Barrel', 'owned' => 0, 'type' => 'restaurant'],
    'HydeBellagio' => ['password' => '731912', 'location' => 'Hyde Bellagio', 'owned' => 0, 'type' => 'nightlife'],
    'DarinaMelnikova' => ['password' => 'DarinaMelnikova15!', 'location' => 'The Redbury NY', 'owned' => 0, 'type' => 'hotel'],
    'admin' => ['password' => 'admin', 'location' => '', 'owned' => 0, 'type' => ''],
    'manager' => ['password' => 'manager', 'location' => '', 'owned' => 0, 'type' => ''],
    'dummy' => ['password' => 'dummy', 'location' => '', 'owned' => 0, 'type' => ''],
    'ChadShin' => ['password' => 'Chadsbe#', 'location' => '', 'owned' => 0, 'type' => ''],
    'accountant' => ['password' => 'accountant', 'location' => '', 'owned' => 0, 'type' => ''],
    'lhmarny' => ['password' => 'lhmarny', 'location' => '', 'owned' => 0, 'type' => ''],
    'DohenyRoom' => ['password' => '265265', 'location' => 'Doheny Room', 'owned' => 1, 'type' => 'nightlife'],
    'HydeTMobile' => ['password' => '445445', 'location' => 'Hyde T-Mobile Arena', 'owned' => 1, 'type' => 'nightlife'],
];
$venue = \Model\Role::getItem(null, ['where' => "name = 'venue'"]);
//foreach($source as $username => $data){
//
//    $findLocation = Location::findLocation($data['location']);
//
//    $locationModelId = 0;
//
//    if($locationModel = \Model\Location::getItem(null, ['where' => "name = '$findLocation'"])){
//        $locationModelId = $locationModel->id;
//    }
//
//    $locationModel = new Location();
//    $locationModel->id = $locationModelId;
//    $locationModel->name = $findLocation;
//    $locationModel->own_type = $data['owned'];
//    $locationModel->type = $data['type'];
//    $locationModel->save();
//
//    $admin = new \Model\Admin();
//    $admin->username = $username;
//    $hash = Emagid\Core\Membership::hash($data['password']);
//    $admin->password = $hash['password'];
//    $admin->hash = $hash['salt'];
//    $admin->permissions = 'Reporting,Administrators';
//    $admin->locations = json_encode([$locationModel->id]);
//    $admin->first_name = strtoupper($findLocation);
//
//    if($admin->save()){
//        $adminRole = new \Model\Admin_Roles();
//        $adminRole->role_id = $venue->id;
//        $adminRole->admin_id = $admin->id;
//        $adminRole->save();
//        echo "Processed $username \n";
//    }
//}