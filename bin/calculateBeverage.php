<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/28/16
 * Time: 11:06 AM
 */

require(__DIR__.'/../index.php');

global $emagid;
$db = $emagid->getDb();
$sql = "select field.name as field, report_data.value as value, location.name as location, report_data.date as date from report_data join field on field.id = report_data.field_id join location on location.id = report_data.location_id where field.name in ('bd_176', 'bd_20', 'bd_21', 'bd_119', 'bd_107', 'bd_17')";
$reportData = $db->getResults($sql);

$locationData = $db->getResults("select id, name, type from location");
$fieldData = $db->getResults("select id, name from field");

$locationMap = [];
$fieldMap = [];

/**
 * Format location map, it use for save data
 */
foreach($locationData as $data){
    $locationMap[$data['name']] = ['type' => $data['type'], 'id' => $data['id']];
}

foreach($fieldData as $data){
    $fieldMap[$data['name']] = $data['id'];
}

/**
 * Formules
 * bd_176 = bd_20 + bd_21 // restaurant
 */

/**
 * Data structure: ['locationName' => ['2016-01-01' => ['bd_1' => 12, 'bd2' => 2]], 'location2' => []....]
 */
$summaryData = [];
foreach($reportData as $data){
    $field = $data['field'];
    $location = $data['location'];
    $date = $data['date'];
    $value = $data['value'];

    if(!isset($summaryData[$location])){
        $summaryData[$location] = [];
    }

    if(!isset($summaryData[$location][$date])){
        $summaryData[$location][$date] = [$field => $value];
    } else {
        $summaryData[$location][$date] = array_merge($summaryData[$location][$date], [$field => $value]);
    }
}

foreach($summaryData as $location => $data){
    ksort($summaryData[$location]);
}

function getData($data, $field){
    return isset($data[$field])?floatval($data[$field]):0;
}

function saveQuery($locationId, $fieldId, $value, $date)
{
    return "insert into report_data (location_id, field_id, value, date) values ($locationId, $fieldId, $value, '$date')";
}

$saveQueries = [];
/**
 * Save all daily data
 */
foreach($summaryData as $location => $dateData){
    $type = $locationMap[$location]['type'];
    $locationId = $locationMap[$location]['id'];
    foreach($dateData as $date => $data){

        if($type == 'nightlife'){
            if(!isset($data['bd_176']) || (isset($data['bd_176']) && !$data['bd_176'])){
                /**
                 * Calculate total
                 */
                $summaryData[$location][$date]['bd_176'] = getData($data, 'bd_20') + getData($data, 'bd_21');
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_176'], $summaryData[$location][$date]['bd_176'], $date);
            }
        }
        if($type == 'hotel') {
            $summaryData[$location][$date]['bd_105'] = getData($data, 'bd_17') == 0?0:(round(getData($data, 'bd_107') / getData($data, 'bd_17'), 2) * 100);
            $saveQueries[] = saveQuery($locationId, $fieldMap['bd_105'], $summaryData[$location][$date]['bd_105'], $date);

            $summaryData[$location][$date]['bd_15'] = getData($data, 'bd_107') == 0?0:round(getData($data, 'bd_119') / getData($data, 'bd_107'), 0);
            $saveQueries[] = saveQuery($locationId, $fieldMap['bd_15'], $summaryData[$location][$date]['bd_15'], $date);
        }
    }
}

foreach($saveQueries as $query){
    echo "Executing $query \n";
    $db->getResults($query);
}







