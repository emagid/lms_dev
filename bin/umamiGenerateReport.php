<?php


require_once(__DIR__.'/../index.php');

set_time_limit(-1);
$overwrite = isset($argv[1])?false:true;

$groups = ['restaurant'];
foreach($groups as $type){
    ob_start();

    $date = \Carbon\Carbon::now();
    $date = $date->subDay();
    $dateString = $date->toDateString();
    $fileName = "$dateString-umami-managers-$type-report.pdf";
    $fileFullPath = __DIR__.'/../content/report/'.$fileName;
    require(__DIR__."/../views/admin/reporting/umamiReportPDF.php");

    $html = ob_get_clean();
    if(!$html || empty($html)){
        // do nothing
    } else {
        $domPdf = new \Dompdf\Dompdf();
        $domPdf->loadHtml($html);
        $domPdf->render();
        $output = $domPdf->output();
        file_put_contents($fileFullPath, $output);
    }
}