<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/22/16
 * Time: 3:54 PM
 */

use Model\Location;

require(__DIR__.'/../index.php');

$source = [
    'Cleo3rdSt' => ['password' => 'test5343', 'location' =>'cleo- 3rd st', 'owned' => 1, 'type' => 'restaurant', 'GM' => 'Catherine Yoo'],
];
$venue = \Model\Role::getItem(null, ['where' => "name = 'venue'"]);
foreach($source as $username => $data){

    $findLocation = Location::findLocation($data['location']);

    $locationModelId = 0;

    if($locationModel = \Model\Location::getItem(null, ['where' => "name = '$findLocation'"])){
        $locationModelId = $locationModel->id;
    }

    $locationModel = new Location();
    $locationModel->id = $locationModelId;
    $locationModel->name = $findLocation;
    $locationModel->own_type = $data['owned'];
    $locationModel->type = $data['type'];
    $locationModel->save();

    $GMId = 0;
    if($GM = \Model\GmNote::getItem(null, ['where' => "location_id = '{$locationModel->id}'"])){
        $GMId = $GM->id;
    }

    $GMModel = new \Model\GmNote();
    $GMModel->id = $GMId;
    $GMModel->location_id = $locationModel->id;
    $GMModel->value = $data['GM'];
    $GMModel->save();

    $admin = new \Model\Admin();
    $admin->username = $username;
    $hash = Emagid\Core\Membership::hash($data['password']);
    $admin->password = $hash['password'];
    $admin->hash = $hash['salt'];
    $admin->permissions = 'Reporting,Administrators';
    $admin->locations = json_encode([$locationModel->id]);
    $admin->first_name = strtoupper($findLocation);

    if($admin->save()){
        $adminRole = new \Model\Admin_Roles();
        $adminRole->role_id = $venue->id;
        $adminRole->admin_id = $admin->id;
        $adminRole->save();
        echo "Processed $username \n";
    }
}