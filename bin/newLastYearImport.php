<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 7/1/16
 * Time: 9:27 PM
 */

require(__DIR__.'/../index.php');

$file = $argv[1];
$filePath = __DIR__."/../csv/lastyear/$file.csv";
$handle = fopen($filePath, "r");
$totalData = [];

function importData($data, &$totalData){

    $date = trim($data[0]);
    $date = new \Carbon\Carbon($date);
    if($date->toDateString() == '2015-07-01' || $date->toDateString() == '2015-07-02' || $date->toDateString() == '2015-07-03') {
        echo "Found date \n";
        $location = trim($data[1]);
        $field = trim($data[2]);
        $value = $data[3];
        $saveData = ['date' => $date, 'location' => $location, 'field' => $field, 'value' => $value];
        $totalData[] = $saveData;
        if (!\Model\ReportData::saveData($saveData, ['lastyear' => true])) {
            echo "{$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
        }
    }
}

$arrResult = [];
$x = 1;
if ($handle) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if($x == 1){
            $x++;
            continue;
        }
        echo "Processing line $x \n";
        importData($data, $totalData);
        $x++;
    }
    fclose($handle);
}