<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/18/16
 * Time: 12:46 PM
 */

require_once(__DIR__.'/../index.php');

$base = __DIR__.'/../history_daily/';
$files = scandir($base);
foreach($files as $file) {
    if($file == '.' || $file == '..'){
        continue;
    }
    echo "Start processing file $file";
    $filePath = $base.$file;
    $handle = fopen($filePath, "r");
    $arrResult = [];
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if (!empty($data[0])) {
                $arrResult[] = $data;
            }
        }
        fclose($handle);
    }

    $date = $arrResult[0][0];
    if(!$date){
        echo "No Date for $file";
        die();
    }

    for ($x = 1; $x < count($arrResult); $x++) {
        echo "Processing line $x \n";
        $location = $arrResult[$x][0];
        $field = $arrResult[$x][1];
        $value = $arrResult[$x][2];

        $saveData = ['date' => $date, 'location' => $location, 'field' => $field, 'value' => $value];

        if(!\Model\ReportData::saveData($saveData, ['overwrite' => true])){
            echo "{$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
        }

    }
}