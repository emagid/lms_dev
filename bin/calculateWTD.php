<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/27/16
 * Time: 10:39 AM
 */

require(__DIR__.'/../index.php');
$location = $argv[1];
global $emagid;
$db = $emagid->getDb();
if($location){
    $sql = "select field.name as field, report_data.value as value, location.name as location, report_data.date as date from report_data join field on field.id = report_data.field_id join location on location.id = report_data.location_id where location.name = '$location' AND field.name in ('bd_153', 'bd_152', 'bd_119', 'bd_105', 'bd_15', 'bd_65', 'bd_22', 'bd_99', 'bd_64','bd_20','bd_21','bd_139','bd_51','bd_98', 'bd_107', 'bd_17', 'bd_119', 'bd_172', 'bd_174', 'bd_120', 'bd_106', 'bd_16', 'bd_60', 'bd_29')";
} else {
    $sql = "select field.name as field, report_data.value as value, location.name as location, report_data.date as date from report_data join field on field.id = report_data.field_id join location on location.id = report_data.location_id where field.name in ('bd_153', 'bd_152', 'bd_119', 'bd_105', 'bd_15', 'bd_65', 'bd_22', 'bd_99', 'bd_64','bd_20','bd_21','bd_139','bd_51','bd_98', 'bd_107', 'bd_17', 'bd_119', 'bd_172', 'bd_174', 'bd_120', 'bd_106', 'bd_16', 'bd_60', 'bd_29')";
}
$reportData = $db->getResults($sql);

$locationData = $db->getResults("select id, name, type from location");
$fieldData = $db->getResults("select id, name from field");

$locationMap = [];
$fieldMap = [];

/**
 * Format location map, it use for save data
 */
foreach($locationData as $data){
    $locationMap[$data['name']] = ['type' => $data['type'], 'id' => $data['id']];
}

foreach($fieldData as $data){
    $fieldMap[$data['name']] = $data['id'];
}

/**
 * Data structure: ['locationName' => ['2016-01-01' => ['bd_1' => 12, 'bd2' => 2]], 'location2' => []....]
 */
$summaryData = [];
foreach($reportData as $data){
    $field = $data['field'];
    $location = $data['location'];
    $date = $data['date'];
    $value = $data['value'];

    if(!isset($summaryData[$location])){
        $summaryData[$location] = [];
    }

    if(!isset($summaryData[$location][$date])){
        $summaryData[$location][$date] = [$field => $value];
    } else {
        $summaryData[$location][$date] = array_merge($summaryData[$location][$date], [$field => $value]);
    }
}

foreach($summaryData as $location => $data){
    ksort($summaryData[$location]);
}

function getData($data, $field){
    return isset($data[$field])?floatval($data[$field]):0;
}

function saveQuery($locationId, $fieldId, $value, $date)
{
    return "insert into report_data (location_id, field_id, value, date) values ($locationId, $fieldId, $value, '$date')";
}

function getWTDData($date, $data, $field, $isAverage = false)
{
    if(!isset($data[$date])){
        return 0;
    }
    $count = 0;
    $value = getData($data[$date], $field);
    $end = new Carbon\Carbon($date);
    $start = $end->copy()->startOfWeek();
    /**
     * Monday WTD is itself
     */
    if($end->dayOfWeek == \Carbon\Carbon::MONDAY){
        return $value;
    }

    $totalValue = 0;

    for($startDate = $start; $startDate->lte($end); $startDate->addDay()) {
        $date = $startDate->toDateString();

        if(!isset($data[$date])){
            continue;
        }

        $totalValue += getData($data[$date], $field);
        $count++;
    }

    return $isAverage?(round($totalValue/$count, 1)):$totalValue;
}

$saveQueries = [];

/**
 * Save WTD data
 */
foreach($summaryData as $location => $dateData){
    $type = $locationMap[$location]['type'];
    $locationId = $locationMap[$location]['id'];
    foreach($dateData as $date => $data){
        if($type == 'restaurant'){
            if(!isset($data['bd_171']) || (isset($data['bd_171']) && !$data['bd_171'])){
                $summaryData[$location][$date]['bd_171'] = getWTDData($date, $dateData, 'bd_153');
                // save into insert query
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_171'], $summaryData[$location][$date]['bd_171'], $date);
            }
        }

        if($type == 'nightlife'){
            if(!isset($data['bd_173']) || (isset($data['bd_173']) && !$data['bd_173'])){
                $summaryData[$location][$date]['bd_173'] = getWTDData($date, $dateData, 'bd_152');
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_173'], $summaryData[$location][$date]['bd_173'], $date);
            }
        }
    }
}

foreach($saveQueries as $query){
    echo "Executing $query \n";
    $db->getResults($query);
}







