<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/22/16
 * Time: 11:24 AM
 */

require(__DIR__.'/../index.php');

$file = $argv[1];
$location = $argv[2];
$columnType = $argv[3];
if(!$columnType){
    echo "Type required, either 1 or 2 \n";
    exit();
}

$filePath = __DIR__."/../csv/2017_budget/2017 DBD - $file.csv";
$handle = fopen($filePath, "r");

function importData($data, $location, $columnType){
    // in case empty rows
    $month = $data[2];

    if(!$month){
        return null;
    }

    $day = explode(' ', $data[3])[1];

    if(!$month || !$day){
        return null;
    }

    $date = \Carbon\Carbon::createFromFormat('Y-M-j', "2017-$month-$day");

    if($columnType == 1){ // end up at column K
        $fields = [
            'bd_4' => 4,
            'bd_20' => 5,
            'bd_64' => 6,
            'bd_98' => 7,
            'bd_130' => 8,
            'bd_38'=> 9,
            'bd_60'=> 10,
            'bd_29'=> 11,
            'bd_140'=> 12,
        ];
    }

    $dailyTotal = 0;

    foreach($fields as $field => $columnIndex){
        $value = $data[$columnIndex];

        $saveData = ['date' => $date->toDateString(), 'location' => $location, 'field' => $field, 'value' => $value];

        if(!\Model\ReportData::saveData($saveData)){
            echo "Failed: ------ {$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
        } else {
            echo "{$saveData['location']} at {$saveData['date']}: {$saveData['field']} and Value: {$saveData['value']} \n";
        }

        if(in_array($field, ['bd_64', 'bd_20', 'bd_98', 'bd_130'])){
            $dailyTotal += \Model\Field::convertValue($value);
        }
    }

    $saveDataTotal = ['date' => $date->toDateString(), 'location' => $location, 'field' => 'bd_152', 'value' => $dailyTotal];

    \Model\ReportData::saveData($saveDataTotal);
}
$arrResult = [];
$x = 1;
if ($handle) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if($x !== 1){
            echo "Processing line $x \n";
            importData($data, $location, $columnType);

        }

        $x++;
    }
    fclose($handle);
}