<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/22/16
 * Time: 3:54 PM
 */

use Model\Location;

require(__DIR__.'/../index.php');

$source = [
    'Anaheim' => ['password' => 'test5343', 'location' => 'anaheim', 'owned' => 1, 'type' => 'restaurant'],
    'ArtsDistrict' => ['password' => 'test5343', 'location' => 'arts district', 'owned' => 1, 'type' => 'restaurant'],
    'Broadway' => ['password' => 'test5343', 'location' => 'broadway', 'owned' => 1, 'type' => 'restaurant'],
    'BrookfieldPlace' => ['password' => 'test5343', 'location' => 'brookfield place', 'owned' => 1, 'type' => 'restaurant'],
    'Burbank' => ['password' => 'test5343', 'location' => 'burbank', 'owned' => 1, 'type' => 'restaurant'],
    'CostaMesa' => ['password' => 'test5343', 'location' => 'costa mesa', 'owned' => 1, 'type' => 'restaurant'],
    'GreenwichVillageNY' => ['password' => 'test5343', 'location' => 'greenwich village ny', 'owned' => 1, 'type' => 'restaurant'],
    'Grove' => ['password' => 'test5343', 'location' => 'grove', 'owned' => 1, 'type' => 'restaurant'],
    'Hollywood' => ['password' => 'test5343', 'location' => 'hollywood', 'owned' => 1, 'type' => 'restaurant'],
    'LosFeliz' => ['password' => 'test5343', 'location' => 'los feliz', 'owned' => 1, 'type' => 'restaurant'],
    'MarinaDistSF' => ['password' => 'test5343', 'location' => 'marina dist sf', 'owned' => 1, 'type' => 'restaurant'],
    'Oakland' => ['password' => 'test5343', 'location' => 'oakland', 'owned' => 1, 'type' => 'restaurant'],
    'PaloAlto' => ['password' => 'test5343', 'location' => 'palo alto', 'owned' => 1, 'type' => 'restaurant'],
    'Pasadena' => ['password' => 'test5343', 'location' => 'pasadena', 'owned' => 1, 'type' => 'restaurant'],
    'SantaMonica' => ['password' => 'test5343', 'location' => 'santa monica', 'owned' => 1, 'type' => 'restaurant'],
    'SoMa' => ['password' => 'test5343', 'location' => 'soma', 'owned' => 1, 'type' => 'restaurant'],
    'StudioCity' => ['password' => 'test5343', 'location' => 'studio city', 'owned' => 1, 'type' => 'restaurant'],
    'ThousandOaks' => ['password' => 'test5343', 'location' => 'thousand oaks', 'owned' => 1, 'type' => 'restaurant'],
    'WestLoop' => ['password' => 'test5343', 'location' => 'west loop', 'owned' => 1, 'type' => 'restaurant'],
    'WickerPark' => ['password' => 'test5343', 'location' => 'wicker park', 'owned' => 1, 'type' => 'restaurant'],
    'Williamsburg' => ['password' => 'test5343', 'location' => 'williamsburg', 'owned' => 1, 'type' => 'restaurant'],
    'Hermosa' => ['password' => 'test5343', 'location' => 'hermosa', 'owned' => 1, 'type' => 'restaurant'],
];
$venue = \Model\Role::getItem(null, ['where' => "name = 'venue'"]);
foreach($source as $username => $data){

    $findLocation = Location::findLocation($data['location']);

    $locationModelId = 0;

    if($locationModel = \Model\Location::getItem(null, ['where' => "name = '$findLocation'"])){
        $locationModelId = $locationModel->id;
    }

    $locationModel = new Location();
    $locationModel->id = $locationModelId;
    $locationModel->name = $findLocation;
    $locationModel->own_type = $data['owned'];
    $locationModel->type = $data['type'];
    $locationModel->save();

    $admin = new \Model\Admin();
    $admin->username = $username;
    $hash = Emagid\Core\Membership::hash($data['password']);
    $admin->password = $hash['password'];
    $admin->hash = $hash['salt'];
    $admin->permissions = 'Reporting,Administrators';
    $admin->locations = json_encode([$locationModel->id]);
    $admin->first_name = strtoupper($findLocation);

    if($admin->save()){
        $adminRole = new \Model\Admin_Roles();
        $adminRole->role_id = $venue->id;
        $adminRole->admin_id = $admin->id;
        $adminRole->save();
        echo "Processed $username \n";
    }
}