<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/22/16
 * Time: 11:24 AM
 */

require(__DIR__.'/../index.php');

$file = $argv[1];
$location = $argv[2];
$columnType = $argv[3];
if(!$columnType){
    echo "Type required, either 1 or 2 \n";
    exit();
}

$filePath = __DIR__."/../csv/budget/$file.csv";
$handle = fopen($filePath, "r");

function importData($data, $location, $columnType){
    // in case empty rows
    if(!$data[0] || !$data[1]){
        return null;
    }
    $date = trim($data[1]);
    if($columnType == 1){ // end up at column K
        $fields = [
            'bd_65' => 2,
            'bd_22' => 3,
            'bd_99' => 4,
            'bd_39' => 5,
            'bd_127' => 6,
            'bd_125' => 7,
            'bd_129' => 8,
            'bd_61' => 9,
            'bd_30' => 10,
        ];
    }elseif($columnType == 2){ // end up at column L
        $fields = [
            'bd_65' => 5,
            'bd_22' => 6,
            'bd_99' => 7,
            'bd_39' => 8,
            'bd_127' => 9,
            'bd_125' => 10,
            'bd_129' => 11,
            'bd_61' => 12,
            'bd_30' => 13,
        ];
    } elseif($columnType == 3){ // end up at column O
        $fields = [
            'bd_65' => 2,
            'bd_22' => 3,
            'bd_99' => 4,
            'bd_39' => 5,
            'bd_127' => 6,
            'bd_125' => 7,
            'bd_129' => 8,
            'bd_30' => 9,
            'bd_61' => 10,
        ];
    }

    foreach($fields as $field => $columnIndex){
        $value = $data[$columnIndex];

        $saveData = ['date' => $date, 'location' => $location, 'field' => $field, 'value' => $value];

        if(!\Model\ReportData::saveData($saveData)){
            echo "{$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
        }
    }
}
$arrResult = [];
$x = 1;
if ($handle) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if($x !== 1){
            echo "Processing line $x \n";
            importData($data, $location, $columnType);

        }

        $x++;
    }
    fclose($handle);
}