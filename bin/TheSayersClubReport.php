<?php


require_once(__DIR__.'/../index.php');

set_time_limit(-1);

ob_start();

$date = \Carbon\Carbon::now();
$date = $date->subDay();
$dateString = $date->toDateString();

$location = 'the sayers club';

$type = \Model\Location::getType($location);

$filePrintName = str_replace(' ', '-', $location);
$fileName = "$filePrintName-report.pdf";

$fileFullPath = __DIR__.'/../content/individual_report/'.$fileName;
require(__DIR__."/../views/admin/reporting/locationDetailPDF.php");

$domPdf = new \Dompdf\Dompdf();
$domPdf->loadHtml(ob_get_clean());
$domPdf->render();
$output = $domPdf->output();
file_put_contents($fileFullPath, $output);


$subject = "The Sayers Club Report";
$toEmails = ['zhou19911005@gmail.com', 'john.hong@sbe.com', 'ronnen@emagid.com', 'noah@omninet.com'];
$senderName = 'SBE Reporting System';
$fromEmail = 'report@emagid.com';

foreach($toEmails as $toEmail){
    $mail = new \Email\MailMaster();

    $mail->setTo(['email'=>$toEmail, 'name'=>$toEmail, 'type'=>'to'])
        ->setFromName($senderName)
        ->setFromAddress($fromEmail)
        ->setSubject($subject)
        ->setMergeTags(['TYPE' => $type])
        ->setTemplate('sbe-reporting')
        ->setAttachment('application/pdf', $fileName, $fileFullPath)
        ->send();
}