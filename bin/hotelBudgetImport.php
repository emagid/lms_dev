<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/22/16
 * Time: 11:24 AM
 */

require(__DIR__.'/../index.php');

$file = $argv[1];
$location = $argv[2];

$filePath = __DIR__."/../csv/budget/$file.csv";
$handle = fopen($filePath, "r");

function importData($data, $location){
    // in case empty rows
    if(!$data[0] || !$data[1]){
        return null;
    }
    $date = trim($data[1]);
    $fields = [
        'bd_119' => 2,
        'bd_134' => 3,
        'bd_110' => 4,
        'bd_115' => 5,
        'bd_96' => 6,
        'bd_17' => 7,
        'bd_107' => 8
    ];

    foreach($fields as $field => $columnIndex){
        $value = $data[$columnIndex];

        $saveData = ['date' => $date, 'location' => $location, 'field' => $field, 'value' => $value];

        if(!\Model\ReportData::saveData($saveData)){
            echo "{$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
        }
    }
}
$arrResult = [];
$x = 1;
if ($handle) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if($x !== 1){
            echo "Processing line $x \n";
            importData($data, $location);

        }

        $x++;
    }
    fclose($handle);
}