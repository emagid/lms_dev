<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/28/16
 * Time: 4:51 PM
 */

require_once(__DIR__.'/../index.php');

$groups = \Model\Grouping::getList(['where' => "name != 'Umami'"]); //TODO UMUMI: this need to add restrict
$date = \Carbon\Carbon::now();
$date = $date->subDay();
$dateString = $date->toDateString();

foreach($groups as $group){
    $type = strtolower($group->version);
    $fileName = "$dateString-nightly-managers-$type-report.pdf";
    $fileFullPath = __DIR__.'/../content/report/'.$fileName;
    if(file_exists($fileFullPath)){
        reportingController::setGroupEmail($group->name, $fileName);
    } else {
        echo "Use php bin/generateReport.php generate report first \n";
    }

}
