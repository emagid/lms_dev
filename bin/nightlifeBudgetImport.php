<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/22/16
 * Time: 11:24 AM
 */

require(__DIR__.'/../index.php');

$file = $argv[1];
$location = $argv[2];
$columnType = $argv[3];
if(!$columnType){
    echo "Type required, either 1 or 2 \n";
    exit();
}

$filePath = __DIR__."/../csv/budget/$file.csv";
$handle = fopen($filePath, "r");

function importData($data, $location, $columnType){
    // in case empty rows
    if(!$data[0] || !$data[1]){
        return null;
    }
    $date = trim($data[1]);
    if($columnType == 1){ // end up at column K
        $fields = [
            'bd_64' => 2,
            'bd_20' => 3,
            'bd_130' => 4,
            'bd_98' => 5,
            'bd_38' => 6,
            'bd_60' => 7,
            'bd_29' => 8,
            'bd_48' => 9,
            'bd_178' => 10,
        ];
    }elseif($columnType == 2){ // end up at column L
        $fields = [
            'bd_64' => 2,
            'bd_20' => 3,
            'bd_124' => 4,
            'bd_126' => 5,
            'bd_98' => 6,
            'bd_38' => 7,
            'bd_60' => 8,
            'bd_29' => 9,
            'bd_48' => 10,
            'bd_178' => 11,
        ];
    } elseif($columnType == 3){ // end up at column L
        $fields = [
            'bd_64' => 2,
            'bd_20' => 3,
            'bd_98' => 4,
            'bd_38' => 5,
            'bd_124' => 6,
            'bd_126' => 7,
            'bd_128' => 8,
            'bd_60' => 9,
            'bd_29' => 10
        ];
    }



    foreach($fields as $field => $columnIndex){
        $value = $data[$columnIndex];

        $saveData = ['date' => $date, 'location' => $location, 'field' => $field, 'value' => $value];

        if(!\Model\ReportData::saveData($saveData)){
            echo "{$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
        }
    }
}
$arrResult = [];
$x = 1;
if ($handle) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if($x !== 1){
            echo "Processing line $x \n";
            importData($data, $location, $columnType);

        }

        $x++;
    }
    fclose($handle);
}