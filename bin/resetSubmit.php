<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 4/24/16
 * Time: 4:46 PM
 */

/**
 * Reset submit to 0, runs everyday at 8am
 */

require_once(__DIR__.'/../index.php');

$locations = \Model\Location::getList();
foreach($locations as $location){
    $location->submit = 0;
    $location->save();
}