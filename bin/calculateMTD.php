<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/27/16
 * Time: 10:39 AM
 */

require(__DIR__.'/../index.php');
$location = $argv[1];
global $emagid;
$db = $emagid->getDb();
if($location){
    $sql = "select field.name as field, report_data.value as value, location.name as location, report_data.date as date from report_data join field on field.id = report_data.field_id join location on location.id = report_data.location_id where location.name = '$location' AND field.name in ('bd_153', 'bd_152', 'bd_119', 'bd_105', 'bd_15', 'bd_65', 'bd_22', 'bd_99', 'bd_64','bd_20','bd_21','bd_139','bd_51','bd_98', 'bd_107', 'bd_17', 'bd_119', 'bd_172', 'bd_174', 'bd_120', 'bd_106', 'bd_16', 'bd_60', 'bd_29')";
} else {
    $sql = "select field.name as field, report_data.value as value, location.name as location, report_data.date as date from report_data join field on field.id = report_data.field_id join location on location.id = report_data.location_id where field.name in ('bd_153', 'bd_152', 'bd_119', 'bd_105', 'bd_15', 'bd_65', 'bd_22', 'bd_99', 'bd_64','bd_20','bd_21','bd_139','bd_51','bd_98', 'bd_107', 'bd_17', 'bd_119', 'bd_172', 'bd_174', 'bd_120', 'bd_106', 'bd_16', 'bd_60', 'bd_29')";
}
$reportData = $db->getResults($sql);

$locationData = $db->getResults("select id, name, type from location");
$fieldData = $db->getResults("select id, name from field");

$locationMap = [];
$fieldMap = [];

/**
 * Format location map, it use for save data
 */
foreach($locationData as $data){
    $locationMap[$data['name']] = ['type' => $data['type'], 'id' => $data['id']];
}

foreach($fieldData as $data){
    $fieldMap[$data['name']] = $data['id'];
}

/**
 * Formules
 * bd_153 = bd_65 + bd_22 + bd_99 // restaurant
 * bd_152 = bd_64 + bd_20 + bd_21 + bd_139 + bd_51 + bd_98 // nightlife
 * bd_105 = bd_107 / bd_17 // hotel
 * bd_15 = bd_119 / bd_107 // hotel
 * bd_58 = bd_60/bd_152
 * bd_27 = bd_29/bd_152
 */

/**
 * Data structure: ['locationName' => ['2016-01-01' => ['bd_1' => 12, 'bd2' => 2]], 'location2' => []....]
 */
$summaryData = [];
foreach($reportData as $data){
    $field = $data['field'];
    $location = $data['location'];
    $date = $data['date'];
    $value = $data['value'];

    if(!isset($summaryData[$location])){
        $summaryData[$location] = [];
    }

    if(!isset($summaryData[$location][$date])){
        $summaryData[$location][$date] = [$field => $value];
    } else {
        $summaryData[$location][$date] = array_merge($summaryData[$location][$date], [$field => $value]);
    }
}

foreach($summaryData as $location => $data){
    ksort($summaryData[$location]);
}

function getData($data, $field){
    return isset($data[$field])?floatval($data[$field]):0;
}

function saveQuery($locationId, $fieldId, $value, $date)
{
    return "insert into report_data (location_id, field_id, value, date) values ($locationId, $fieldId, $value, '$date')";
}

function getMTDData($date, $data, $field, $isAverage = false)
{
    if(!isset($data[$date])){
        return 0;
    }
    $count = 0;
    $value = getData($data[$date], $field);
    $end = new Carbon\Carbon($date);
    $start = $end->copy()->startOfMonth();
    /**
     * First date MTD is 0
     */
    if($end->daysInMonth == 1){
        return $value;
    }

    $totalValue = 0;

    for($startDate = $start; $startDate->lte($end); $startDate->addDay()) {
        $date = $startDate->toDateString();

        if(!isset($data[$date])){
            continue;
        }

        $totalValue += getData($data[$date], $field);
        $count++;
    }

    return $isAverage?(round($totalValue/$count, 1)):$totalValue;
}

$saveQueries = [];
/**
 * Save all daily data
 */
foreach($summaryData as $location => $dateData){
    $type = $locationMap[$location]['type'];
    $locationId = $locationMap[$location]['id'];
    foreach($dateData as $date => $data){
        if($type == 'restaurant'){
            if(!isset($data['bd_153']) || (isset($data['bd_153']) && !$data['bd_153'])){
                $summaryData[$location][$date]['bd_153'] = getData($data, 'bd_65') + getData($data, 'bd_22') + getData($data, 'bd_99');
                // save into insert query
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_153'], $summaryData[$location][$date]['bd_153'], $date);
            }
        }

        if($type == 'nightlife'){
            if(!isset($data['bd_152']) || (isset($data['bd_152']) && !$data['bd_152'])){
                /**
                 * Calculate total
                 */
                $summaryData[$location][$date]['bd_152'] = getData($data, 'bd_64') + getData($data, 'bd_20') + getData($data, 'bd_21') + getData($data, 'bd_139') + getData($data, 'bd_51') + getData($data, 'bd_98');
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_152'], $summaryData[$location][$date]['bd_152'], $date);

                /**
                 * Calculate FOH % / BOH %
                 */
                if($summaryData[$location][$date]['bd_152'] != 0){
                    $saveQueries[] = saveQuery($locationId, $fieldMap['bd_58'], round($summaryData[$location][$date]['bd_60']/$summaryData[$location][$date]['bd_152'], 0)*100, $date);
                    $saveQueries[] = saveQuery($locationId, $fieldMap['bd_27'], round($summaryData[$location][$date]['bd_29']/$summaryData[$location][$date]['bd_152'], 0)*100, $date);
                }
            }
        }

        if($type == 'hotel'){
            if(!isset($data['bd_105']) || (isset($data['bd_105']) && !$data['bd_105'])){
                $summaryData[$location][$date]['bd_105'] = getData($data, 'bd_17') == 0?0:round(getData($data, 'bd_107') / getData($data, 'bd_17'), 0) * 100;
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_105'], $summaryData[$location][$date]['bd_105'], $date);
            }

            if(!isset($data['bd_15']) || (isset($data['bd_15']) && !$data['bd_15'])){
                $summaryData[$location][$date]['bd_15'] = getData($data, 'bd_107') == 0?0:round(getData($data, 'bd_119') / getData($data, 'bd_107'), 0);
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_15'], $summaryData[$location][$date]['bd_15'], $date);
            }
        }
    }
}

/**
 * Save MTD data
 */
foreach($summaryData as $location => $dateData){
    $type = $locationMap[$location]['type'];
    $locationId = $locationMap[$location]['id'];
    foreach($dateData as $date => $data){
        if($type == 'restaurant'){
            if(!isset($data['bd_172']) || (isset($data['bd_172']) && !$data['bd_172'])){
                $summaryData[$location][$date]['bd_172'] = getMTDData($date, $dateData, 'bd_153');
                // save into insert query
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_172'], $summaryData[$location][$date]['bd_172'], $date);
            }
        }

        if($type == 'nightlife'){
            if(!isset($data['bd_174']) || (isset($data['bd_174']) && !$data['bd_174'])){
                $summaryData[$location][$date]['bd_174'] = getMTDData($date, $dateData, 'bd_152');
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_174'], $summaryData[$location][$date]['bd_174'], $date);
            }
        }

        if($type == 'hotel'){
            if(!isset($data['bd_120']) || (isset($data['bd_120']) && !$data['bd_120'])){
                $summaryData[$location][$date]['bd_120'] = getMTDData($date, $dateData, 'bd_119');
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_120'], $summaryData[$location][$date]['bd_120'], $date);
            }

            if(!isset($data['bd_106']) || (isset($data['bd_106']) && !$data['bd_106'])){
                $summaryData[$location][$date]['bd_106'] = getMTDData($date, $dateData, 'bd_105', true);
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_106'], $summaryData[$location][$date]['bd_106'], $date);
            }

            if(!isset($data['bd_16']) || (isset($data['bd_16']) && !$data['bd_16'])){
                $summaryData[$location][$date]['bd_16'] = getMTDData($date, $dateData, 'bd_15', true);
                $saveQueries[] = saveQuery($locationId, $fieldMap['bd_16'], $summaryData[$location][$date]['bd_16'], $date);
            }
        }
    }
}

foreach($saveQueries as $query){
    echo "Executing $query \n";
    $db->getResults($query);
}







