-- Feb 1

ALTER TABLE users ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE accounts ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE account_courses ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE answers ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE audits ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE categories ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE courses ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE course_modules ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE questions ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE responses ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE response_sets ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE schema_integration ADD COLUMN active SMALLINT DEFAULT 1;
ALTER TABLE user_course_modules ADD COLUMN active SMALLINT DEFAULT 1;

ALTER TABLE users ADD COLUMN permissions VARCHAR(255);

-- Feb 2
ALTER TABLE certificates ADD COLUMN active SMALLINT DEFAULT 1;

-- Feb 21

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `config` (`id`, `name`, `value`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Meta Title', 'LMS', '2018-02-21 18:13:43', '2018-02-21 18:13:43', 1),
(2, 'Meta Description', 'LMS', '2018-02-21 18:13:43', '2018-02-21 18:13:43', 1),
(3, 'Meta Keywords', 'LMS', '2018-02-21 18:14:03', '2018-02-21 18:14:03', 1);
ALTER TABLE `config` ADD PRIMARY KEY (`id`);
ALTER TABLE `config` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

-- Feb 22

ALTER TABLE `users` ADD COLUMN year_level INTEGER DEFAULT 0;

-- Feb 28

DROP INDEX index_users_on_email ON users;

-- Mar 20

ALTER TABLE `course_modules` ADD COLUMN suggested_year_level INTEGER DEFAULT 0;

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` smallint(6) DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` smallint(6) DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

DROP TABLE IF EXISTS `plan_tiers`;
CREATE TABLE `plan_tiers` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` smallint(6) DEFAULT '1',
  `plan_id` int(11) NOT NULL,
  `min_students` int(11) DEFAULT NULL,
  `max_students` int(11) DEFAULT NULL,
  `price` FLOAT COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `plan_tiers`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `plan_tiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

DROP TABLE IF EXISTS `account_module_years`;
CREATE TABLE `account_module_years` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` smallint(6) DEFAULT '1',
  `account_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `year_level` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `account_module_years`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `account_module_years`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Mar 22

RENAME TABLE `account_module_years` TO account_module_cats;
ALTER TABLE `account_module_cats` CHANGE COLUMN `year_level` `ucat_id` int(11);

DROP TABLE IF EXISTS `ucategory`;
CREATE TABLE `ucategory` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` smallint(6) DEFAULT '1',
  `account_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `ucategory`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ucategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users` ADD COLUMN user_category_id INT(11);

-- MAR 23
ALTER TABLE `users` DROP INDEX `index_users_on_email`;;

-- APR 19
ALTER TABLE `plan` DROP `title`, DROP `status`, DROP `description`;
ALTER TABLE `plan` ADD COLUMN `student_limit` INT(11) NOT NULL,
                   ADD COLUMN `icon` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                   ADD COLUMN `features` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL;
ALTER TABLE `plan` ADD `display` BOOLEAN NOT NULL DEFAULT FALSE;

DROP TABLE IF EXISTS `plan_courses`;
CREATE TABLE `plan_courses` (
  `id` INT(11) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `active` SMALLINT(6) DEFAULT '1',
  `plan_id` INT(11) NOT NULL,
  `course_id` INT(11) NOT NULL,
  `modules` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `plan_courses`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `plan_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `plan_tiers` CHANGE `min_students` `min_users` INT(11) NOT NULL;
ALTER TABLE `plan_tiers` CHANGE `max_students` `max_users` INT(11) NOT NULL;

-- April 23, 2018

ALTER TABLE accounts ADD COLUMN plan VARCHAR(255);

-- April 26, 2018

ALTER TABLE `accounts` CHANGE COLUMN `plan` `plan_id` INT(11);

-- May 1, 2018
ALTER TABLE plan ADD COLUMN category_id INT(11);

ALTER TABLE accounts ADD COLUMN category_id INT(11);

-- May 4, 2018  
ALTER TABLE users ADD COLUMN phone VARCHAR(255);
ALTER TABLE users ADD COLUMN address VARCHAR(255); 
ALTER TABLE users ADD COLUMN city VARCHAR(255); 
ALTER TABLE users ADD COLUMN state VARCHAR(255);
ALTER TABLE users ADD COLUMN zip VARCHAR(255);

-- May 16, 2018

CREATE TABLE `account_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255),
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `account_category`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `account_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE plan CHANGE category_id account_category_id INT(11);
ALTER TABLE accounts CHANGE category_id account_category_id INT(11);
ALTER TABLE categories ADD COLUMN slug VARCHAR(255);

--June 2, 2018
CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(255),
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `brand` ADD PRIMARY KEY(`id`);
ALTER TABLE `brand` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `brand` (`name`) VALUES ('sm2');
INSERT INTO `brand` (`name`) VALUES ('socially-ready');

UPDATE `accounts` SET `brand` = 1 WHERE `brand` = 'sm2';
UPDATE `accounts` SET `brand` = 2 WHERE `brand` = 'socially-ready';

ALTER TABLE `accounts` CHANGE `brand` `brand_id` INT(11) NULL DEFAULT '1';

-- June 3
CREATE TABLE `files` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `file` varchar(255),
  `accessible` tinyint(1),
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `files` CHANGE `accessible` `accessible` INT(11) NULL DEFAULT '0';
ALTER TABLE `files` CHANGE `accessible` `default_file` SMALLINT NULL DEFAULT '0';

--June 4, 2018
ALTER TABLE `files` ADD COLUMN name VARCHAR(255);

--June 7, 2018
CREATE TABLE `account_files` (
  `id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `active` SMALLINT(6) DEFAULT '1',
  `file_id` INT(11) NOT NULL,
  `account_id` INT(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;