<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/21/16
 * Time: 11:22 AM
 */

require(__DIR__.'/../index.php');

$file = $argv[1];
$filePath = __DIR__."/../csv/lastyear/$file.csv";
$handle = fopen($filePath, "r");

function importData($data){
    if(strtolower($data[4]) != 'gm - draft'){
        return null;
    }
    $date = new \Carbon\Carbon(trim($data[1]));
    $location = trim($data[2]);
    $field = trim($data[5]);
    $value = $data[6];
    $saveData = ['date' => $date, 'location' => $location, 'field' => $field, 'value' => $value];
    if(!\Model\ReportData::saveData($saveData, ['lastyear' => true])){
        echo "{$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
    }
}
$arrResult = [];
$x = 1;
if ($handle) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        echo "Processing line $x \n";
        importData($data);
        $x++;
    }
    fclose($handle);
}