<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/18/16
 * Time: 12:46 PM
 */

require_once(__DIR__.'/../index.php');

$filePath = __DIR__.'/../csv/averoinc/umami.csv';
$handle = fopen($filePath, "r");
$arrResult = [];
if ($handle) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if (!empty($data[0])) {
            $arrResult[] = $data;
        }
    }
    fclose($handle);
}

$date = $arrResult[0][0];

if($date){
    try{
        $dateObj = new \Carbon\Carbon($date);
    } catch(Exception $e) {
        reportingController::sendAveroErrorEmail(['error' => "Umami file has wrong date format"], false);
        die();
    }
    /**
     * If date doesn't match yesterday date, in case they don't have file at all
     */
    if($dateObj->toDateString() !== \Carbon\Carbon::now()->subDay()->toDateString()){
        reportingController::sendAveroErrorEmail(['error' => "Umami file date isn't yesterday"], false);
        die();
    }
}

if(!$date || !$arrResult){
    reportingController::sendAveroErrorEmail(['error' => "Umami file doesn't have any data"], false);
    die();
}

echo "$date \n";

for ($x = 1; $x < count($arrResult); $x++) {
    echo "Processing line $x \n";
    $location = $arrResult[$x][0];
    $field = $arrResult[$x][1];
    $value = $arrResult[$x][2];

    $findLocation = \Model\Location::findLocation($location);

    $saveData = ['date' => $date, 'location' => $location, 'field' => $field, 'value' => $value];

    $option = ['overwrite' => true];

    if(!\Model\ReportData::saveData($saveData, $option)){
        echo "{$saveData['location']} at {$saveData['date']} failed for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
    } else {
        echo "Saved {$saveData['location']} at {$saveData['date']} for Field: {$saveData['field']} and Value: {$saveData['value']} \n";
    }

}