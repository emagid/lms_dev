<?php

class categoriesController extends manageController {

    function __construct()
    {
        parent::__construct('Category','categories');
    }

	public function index(Array $params = []){
        $params['queryOptions'] = [];
        if (!Empty($_GET['how_many']))
        {
            $params['limit'] = $_GET['how_many'];
        } else {
            $params['limit'] = 10;
        }
        $account = $this->viewData->user->account_id;
        if(!$this->viewData->user->account_id){
            $account = 1;
        }
        $params['queryOptions']=['where'=>" account_id = {$account}"];

        $this->viewData->hasCreateBtn = true;
        parent::index($params);
	}

}