<?php

class userController extends siteController {

	function __construct(){
		parent::__construct();

		if (is_null($this->viewData->user)){
			redirect(SITE_URL);
		}
	}

	public function index(Array $params = []){
		$this->viewData->form = new \Emagid\Html\Form($this->viewData->user);
		$this->loadView($this->viewData);
	}

	public function update_post(){
        if($_POST['password'] != ""){
        	$hash = \Emagid\Core\Membership::hash($_POST['password']);
	        $this->viewData->user->password = $hash['password'];
	        $this->viewData->user->hash = $hash['salt'];
        }

      	$this->viewData->user->email = $_POST['email'];
      	$this->viewData->user->first_name = $_POST['first_name'];
      	$this->viewData->user->last_name = $_POST['last_name'];
      
      	if ($this->viewData->user->save()){
      		$n = new \Notification\MessageHandler('User information saved.');
           	$_SESSION["notification"] = serialize($n);
      	} else {
      		$n = new \Notification\ErrorHandler($this->viewData->user->errors);
           	$_SESSION["notification"] = serialize($n);
      	};
      	redirect(SITE_URL.'user');
	}

	public function payment_methods(Array $params = []){
		$this->viewData->payment_methods = \Model\Payment_Profile::getList(['where'=>'user_id = '.$this->viewData->user->id]);
		foreach ($this->viewData->payment_methods as $method){
			$method->cc_number = '****'.substr($method->cc_number, -4);
		}
		$this->loadView($this->viewData);
	}

	public function pm_update(Array $params = []){
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
	    $payment_profile = null;
	    
	    if((int)$id>0) {
			$payment_profile = \Model\Payment_Profile::getItem(null, ['where'=>'id = '.$id.' and user_id = '.$this->viewData->user->id]);
			if($payment_profile==null) {redirect(SITE_URL.'user');}
			$payment_profile->cc_number = '****'.substr($payment_profile->cc_number, -4);
	    } else {
			$payment_profile = new \Model\Payment_Profile();
			$payment_profile->country = "United States";
	    }
	    
	    $this->viewData->payment_profile = $payment_profile;
	    $this->viewData->form = new \Emagid\Html\Form($payment_profile);
	    
	    $this->loadView($this->viewData);
	}

	public function pm_update_post(){
		$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);

		$old = new \Model\Payment_Profile();
		if ($id > 0){
			$old = \Model\Payment_Profile::getItem($id);
		}

	    $payment_profile = \Model\Payment_Profile::loadFromPost();
	    $payment_profile->user_id = $this->viewData->user->id;

	    if ($payment_profile->cc_number == '****'.substr($old->cc_number, -4)){
	    	$payment_profile->cc_number = $old->cc_number;
	    }

	    if ($payment_profile->save()){
      		$n = new \Notification\MessageHandler('Payment Method saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(SITE_URL.'user/payment_methods');
      	} else {
      		$n = new \Notification\ErrorHandler($payment_profile->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(SITE_URL.'user/pm_update/'.$id);
      	};
	}

	function pm_delete($params) {
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
	    $payment = \Model\Payment_Profile::getItem(null, ['where'=>'id = '.$id.' and user_id = '.$this->viewData->user->id]);
	    \Model\Payment_Profile::delete($payment->id);
	    $n = new \Notification\MessageHandler('Payment Method deleted.');
        $_SESSION["notification"] = serialize($n);
	    redirect(SITE_URL.'user/payment_methods');
    }

	public function addresses(Array $params = []){
		$this->viewData->addresses = \Model\Address::getList(['where'=>'user_id = '.$this->viewData->user->id]);
		$this->loadView($this->viewData);
	}

	public function ad_update($params){
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
	    if((int)$id>0) {
	      	$address = \Model\Address::getItem(null, ['where'=>'id = '.$id.' and user_id = '.$this->viewData->user->id]);
	      	if($address==null) {redirect(SITE_URL.'user');}
	    } else {
	      	$address = new \Model\Address();
	      	$address->country = "United States";
	    }
	    
	    $this->viewData->address = $address;
	    $this->viewData->form = new \Emagid\Html\Form($address);
	    
	    $this->loadView($this->viewData);
	}

	public function ad_update_post(){
		$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);

		$address = \Model\Address::loadFromPost();
		$address->user_id = $this->viewData->user->id;

		if ($address->save()){
      		$n = new \Notification\MessageHandler('Address saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(SITE_URL.'user/addresses');
      	} else {
      		$n = new \Notification\ErrorHandler($address->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'user/ad_update/'.$id);
      	};
	}

	public function ad_delete($params) {
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
	    $address = \Model\Address::getItem(null, ['where'=>'id = '.$id.' and user_id = '.$this->viewData->user->id]);
	    \Model\Address::delete($address->id);
	    $n = new \Notification\MessageHandler('Address deleted.');
        $_SESSION["notification"] = serialize($n);
	    redirect(SITE_URL.'user/addresses');
    }

	public function wishlist(){
		$this->viewData->products = \Model\Wishlist::getProductsByUserId($this->viewData->user->id);
		$this->loadView($this->viewData);	
	}

	public function wishlist_add($params){
		$product = \Model\Product::getItem((isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0);
	    if(!is_null($product)) {
	      	$wishlist = new \Model\Wishlist();
	      	$wishlist->product_id = $product->id;
	      	$wishlist->user_id = $this->viewData->user->id;
	      	$wishlist->save();
	    }
	}

	public function wishlist_remove($params){
		if ((isset($params['id']) && is_numeric($params['id']) && $params['id']>0)){
			$wishlist = \Model\Wishlist::getItem(null, ['where'=>'product_id = '.$params['id'].' and user_id = '.$this->viewData->user->id]);
			if (!is_null($wishlist)){
				\Model\Wishlist::delete($wishlist->id);
			}
		    $n = new \Notification\MessageHandler('Item removed.');
	        $_SESSION["notification"] = serialize($n);
		}
	    redirect(SITE_URL.'user/wishlist');
	}

}























