<?php

class cartController extends siteController {

	public function index(Array $params = []){
		if (count($this->viewData->cart->products) > 0){
			$arr = [];
			foreach($this->viewData->cart->products as $key=>$product){
				$prod = \Model\Product::getItem($product);
				$prod->details = $this->viewData->cart->productDetails[$key];
				$arr[$key] = $prod;
			}
			$this->viewData->products = $arr;
		} else {
			$this->viewData->products = [];
		}

		$order_products = [];
		$subtotal = 0;
		foreach($this->viewData->cart->products as $productId){
			$product = \Model\Product::getItem($productId);
			if(isset($_SESSION['price'])){
				$code= $_SESSION['code'];
				$new_price = \Model\Price_Alert::getItem(null,['where'=>" code =  '$code'"]);
				if($product->id == $new_price->product_id){
					$subtotal += $new_price->price;
				}else{
					$subtotal += $product->price;
				}
			}
			else{
				$subtotal += $product->price;
			}

			
			if (!in_array($productId, $order_products)){
				$order_products[] = $productId;
			}
		}
		$this->viewData->subtotal = $subtotal;

		if (isset($_SESSION['coupon'])){
			$coupon = \Model\Coupon::getItem(null, ['where'=>"code = '".$_SESSION['coupon']."'"]);
			if ((!is_null($coupon) 
					&& time() >= $coupon->start_time && time() < $coupon->end_time
					&& $this->viewData->subtotal >= $coupon->min_amount
					&& \Model\Order::getCount(['where'=>"coupon_code = '".$coupon->code."'"]) < $coupon->num_uses_all)){
				if ($coupon->discount_type == 1){
					$subtotal = $subtotal - $coupon->discount_amount;
				} else if ($coupon->discount_type == 2) {
					$subtotal = $subtotal * (1 - ($coupon->discount_amount/100));
				}
			}
		}

		$this->viewData->shipping_methods = [];
		$shipping_methods = \Model\Shipping_Method::getList(['orderBy'=>'id asc']);
		foreach ($shipping_methods as $shipping_method){
			$costs = json_decode($shipping_method->cost);
			$shipping_method->cost = 0;
			$shipping_method->cart_subtotal_range_min = json_decode($shipping_method->cart_subtotal_range_min);
			foreach($shipping_method->cart_subtotal_range_min as $key=>$range){
				if (is_null($range) || trim($range) == ''){
					$range = 0;
				}
				if ($subtotal >= $range && isset($costs[$key])){
					$shipping_method->cost = $costs[$key];
				}
			}
			$this->viewData->shipping_methods[] = $shipping_method;
		}

		if (isset($_COOKIE['shippingMethod'])){
			$this->viewData->shipping_method = $_COOKIE['shippingMethod'];
		} else {
			$this->viewData->shipping_method = \Model\Shipping_Method::getItem(null, ['orderBy'=>'is_default desc']);
			$this->viewData->shipping_method = $this->viewData->shipping_method->id;
			setcookie('shippingMethod', $this->viewData->shipping_method, time()+60*60*24*100, "/");
		}

		parent::index($params);
	}

	public function add($params)
	{ //cart page
		$check = \Model\Product::getItem($params['id']);
		$token = \Model\Product::generateToken();
		if ($check->availability !== 0) {
			$this->viewData->cart->products[$token] = $params['id'];

			$qtyAdd = $this->viewData->cart->productDetails;
			$_POST['colorId'] = isset($_POST['colorId']) ? $_POST['colorId'] : $qtyAdd->color;
			$_POST['sizeId'] = isset($_POST['sizeId']) ? $_POST['sizeId'] : $qtyAdd->size;

			$this->viewData->cart->productDetails[$token] = ['product'=>$params['id'],'color'=>$_POST['colorId'], 'size'=>$_POST['sizeId']];

			$this->updateCookies();
			echo json_encode(['status'=>"success"]);
		} else {
			echo json_encode(['status'=>"fail", 'message'=>"This item is not available for purchase"]);
		}
	}

	public function remove_once($params){
		foreach ($this->viewData->cart->products as $key=>$product){
			if ($product == $params['id']){
				unset($this->viewData->cart->products[$key]);
				break;
			}
		}
		$this->updateCookies();
		redirect(SITE_URL.'cart');
	}

	public function remove_product($params){
		unset($this->viewData->cart->products[$params['id']]);
		unset($this->viewData->cart->productDetails[$params['id']]);
		$this->updateCookies();
		redirect(SITE_URL.'cart');
	}

	public function remove_all(){
		$this->viewData->cart->products = [];
		$this->viewData->cart->productDetails = [];
		$this->updateCookies();
		redirect(SITE_URL.'cart');
	}

	//This function is also executed in the ordersController after a new order is inserted
	private function updateCookies(){
		setcookie('cartProducts', json_encode($this->viewData->cart->products), time()+60*60*24*100, "/");
		if(isset($this->viewData->cart->productDetails)){
			setcookie('cartProductDetails',json_encode($this->viewData->cart->productDetails), time()+60*60*24*100, "/");
		}

	}
	
	public function updateShipping(Array $params = []){
		if(isset($_GET['id']) && is_numeric($_GET['id']) && !is_null(\Model\Shipping_Method::getItem($_GET['id']))){
			setcookie('shippingMethod', $_GET['id'], time()+60*60*24*100, "/");
		}
	}

	public function applyCoupon(Array $params = []){
		if (isset($_GET['code'])){
			$coupon = \Model\Coupon::getItem(null, ['where'=>" code = '".$_GET['code']."'"]);
			if (is_null($coupon)){
				$_SESSION['coupon'] = null;
				echo json_encode(['error'=>['code'=>1,'message'=>'Invalid coupon code.'], 'coupon'=>null]);
			} else if (time() >= $coupon->start_time && time() < $coupon->end_time) {
				$cartTotal = (float) str_replace(',', '', $this->viewData->cart->total);
				if ($cartTotal >= $coupon->min_amount){
					$already_used = \Model\Order::getCount(['where'=>"coupon_code = '".$coupon->code."'"]);
					if (is_null($already_used)){
						$already_used = 0;
					}
					
					if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all){
						$_SESSION['coupon'] = $coupon->code;
						echo json_encode(['error'=>['code'=>0,'message'=>''], 'coupon'=>$coupon]);
					} else {
						$_SESSION['coupon'] = null;
						echo json_encode(['error'=>['code'=>4,'message'=>'Coupon sold out.'], 'coupon'=>null]);
					}
				} else {
					$_SESSION['coupon'] = null;
					echo json_encode(['error'=>['code'=>2,'message'=>'Coupon requires a minimum amount of '.$coupon->min_amount.'.'], 'coupon'=>null]);		
				}
			} else {
				$_SESSION['coupon'] = null;
				echo json_encode(['error'=>['code'=>3,'message'=>'Coupon currently unavailable.'], 'coupon'=>null]);
			}
		} else {
			$_SESSION['coupon'] = null;
			echo json_encode(['error'=>['code'=>1,'message'=>'Invalid coupon code.'], 'coupon'=>null]);
		}
	}

}




























