<?php

class topicsController extends adminController{

    function __construct()
    {
        parent::__construct('Topic');
    }

    public function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['orderBy'] = 'name asc';
        parent::index($params);
    }

}