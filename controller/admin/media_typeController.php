<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/7/16
 * Time: 11:34 AM
 */

class media_typeController extends adminController {

    function __construct(){
        parent::__construct("MediaType", 'media_type');
    }

    function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }
}
