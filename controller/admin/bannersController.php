<?php

class bannersController extends adminController {
	
	function __construct(){
		parent::__construct("Banner");
	}

	function update(Array $params = []){}
	function update_post(Array $params = []){}

	function afterImageUpload($image){
		if($this->emagid->route['action'] == 'featured_update'){
			if ($_POST['featured_id'] == 1 || $_POST['featured_id'] == 4 || $_POST['featured_id'] == 5){
				$image->resize('370_310'.$image->fileName, false, 370, 310);
			} else if ($_POST['featured_id'] == 2 || $_POST['featured_id'] == 3 || $_POST['featured_id'] == 6) {
				$image->resize('370_220'.$image->fileName, false, 370, 220);
			}
		}
	}

	function main(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		$params['overrideView'] = 'main';
		$params['queryOptions'] = ['where'=>'is_deal = false and (featured_id = 0 or featured_id is null)'];
		parent::index($params);
	}

	function main_update(Array $params = []){
		$params['overrideView'] = 'main_update';
		parent::update($params);
	}

	function main_update_post(){
		$obj = new $this->_model($_POST); 

    	if ($obj->save()){
    		$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
    		if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
    			$image = new \Emagid\Image();
    			$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
    			$image->resize('1200_540'.$image->fileName, true, 1200);
    			$obj->$imageType = $image->fileName;
    			$obj->save();
    		}
            $n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/main');
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/main_update/'.$obj->id);
    	}
	}

	function main_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/main';
		parent::delete($params);
	}

	function main_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem($params['id']);

		if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
			\Emagid\Image::delete('banners/'.$banner->image);
		}

		$banner->image = null;
		$banner->is_deal = 0;
		$banner->save();

		redirect(ADMIN_URL.'banners/main_update/'.$banner->id);
	}

	function featured(){
		$this->loadView($this->_viewData);	
	}

	function featured_update(Array $params = []){
		if ($params['id'] > 0 && $params['id'] < 7){
			$banner = \Model\Banner::getItem(null,['where'=>'is_deal = false and featured_id = '.$params['id']]);

			if (is_null($banner)){
				$banner = new \Model\Banner();
				$banner->featured_id = $params['id'];
				$banner->save();
			}

			$this->_viewData->banner = $banner;

			$this->loadView($this->_viewData);	
		} else {
			redirect(ADMIN_URL.'banners/featured');
		}
	}

	function featured_update_post(){
		$obj = new $this->_model($_POST); 

    	if ($obj->save()){
    		$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
    		if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
    			$image = new \Emagid\Image();
    			$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
    			$this->afterImageUpload($image);
    			$obj->$imageType = $image->fileName;
    			$obj->save();
    		}
            $n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/featured');
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/featured_update/'.$obj->featured_id);
    	}
	}

	function featured_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem(null, ['where'=>'featured_id = '.$params['id']]);

		if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
			\Emagid\Image::delete('banners/'.$banner->image);
		}

		$banner->image = null;
		$banner->is_deal = 0;
		$banner->save();

		redirect(ADMIN_URL.'banners/featured_update/'.$banner->featured_id);
	}

	function deal_of_the_week(){
		redirect(ADMIN_URL.'banners/deal_of_the_week_update');
	}

	function deal_of_the_week_update(){
		$banner = \Model\Banner::getItem(null,['where'=>'is_deal = true and (featured_id = 0 or featured_id is null)']);

		if (is_null($banner)){
			$banner = new \Model\Banner();
			$banner->is_deal = true;
			$banner->save();
		}

		$this->_viewData->banner = $banner;

		$this->loadView($this->_viewData);
	}

	function deal_of_the_week_update_post(){
		$banner = \Model\Banner::getItem(null,['where'=>'is_deal = 1 and featured_id = 0']);

		if (is_null($banner)){
			$banner = new \Model\Banner();
			$banner->is_deal = true;
		}
		$banner->link = $_POST['link'];
		if ($banner->save()){
			$image = new \Emagid\Image();
			$image->upload($_FILES['image'], UPLOAD_PATH.'banners'.DS);
			$image->resize('deal_of_the_week.'.$image->fileType, true, 1200);
			$n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
		} else {
			$n = new \Notification\ErrorHandler($banner->errors);
           	$_SESSION["notification"] = serialize($n);
		};
		redirect(ADMIN_URL.'banners/deal_of_the_week');
	}

	function deal_of_the_week_delete(){
		$img_path = '';
		if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.jpg')){
			$img_path = 'banners/deal_of_the_week.jpg';
		} else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.jpeg')){
			$img_path = 'banners/deal_of_the_week.jpeg';
		} else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.png')){
			$img_path = 'banners/deal_of_the_week.png';
		} else if (file_exists(UPLOAD_PATH.'banners/deal_of_the_week.gif')){
			$img_path = 'banners/deal_of_the_week.gif';
		}
		if ($img_path != ''){
			\Emagid\Image::delete($img_path);
		}
		redirect(ADMIN_URL.'banners/deal_of_the_week');
	}

}






















































