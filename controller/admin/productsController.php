<?php

use Emagid\Html\Form;

class productsController extends adminController {
	
	function __construct(){
		parent::__construct("Product");
	}

	function index(Array $params = []){
		if (!Empty($_GET['how_many']))
		 {	
		 	 
		 	$params['limit'] = $_GET['how_many'];

		 }
		 else{
		 	$params['limit'] = 10;
		 }

			if (!Empty($_GET['how_many']) && !Empty($_GET['status_show'])){
				$st=str_replace ('%20',' ',$_GET['status_show']);
		 		$params['queryOptions']['where'] = " brand = '$st'";
		 		$params['limit'] = $_GET['how_many'];
			}

		 if (!Empty($_GET['status_show']))
		 {	
		 	$st=str_replace ('%20',' ',$_GET['status_show']);
		 	$params['queryOptions']['where'] = " brand = '$st'";

		 }
		 /*else{
		 	$params['queryOptions']['orderBy'] = "mpn ASC";
		 } */
		$this->_viewData->hasCreateBtn = true; 
		$this->_viewData->brands = \Model\Brand::getList();
		 
		parent::index($params); 
	
	}

	public function import(Array $params = [])
	{
		set_time_limit(0);
		$handle = fopen($_FILES['csv']['tmp_name'], "r");
		$headSkip = true;
		$id1 = '';
		$id2 = '';
		$id3 = '';
		$allArr = [];
		if ($handle) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if (empty($data[0])) {
				} else {
					if($headSkip){
						$headSkip = false;
					} else {
						if(!array_key_exists($data[0],$allArr)){
							$id1 = strtolower($data[7]);$id2 = strtolower($data[9]);$id3 = strtolower($data[11]);
							$allArr[$data[0]] = ['slug'=>$data[0],'name'=>$data[1],'description'=>$data[2],'tags'=>$data[5],'price'=>$data[19],'msrp'=>$data[20],'meta_title'=>isset($data[31])?$data[31]:'', 'meta_description'=>isset($data[32])?$data[32]:'',
												'options'=>[$id1=>[$data[8]],$id2=>[$data[10]],$id3=>[$data[12]]], 'images'=>[$data[24]]];
						} else {
							if(!in_array($data[8],$allArr[$data[0]]['options'][$id1])){
								$allArr[$data[0]]['options'][$id1][] = $data[8];
							}
							if(!in_array($data[10],$allArr[$data[0]]['options'][$id2])){
								$allArr[$data[0]]['options'][$id2][] = $data[10];
							}
							if(!in_array($data[12],$allArr[$data[0]]['options'][$id3])){
								$allArr[$data[0]]['options'][$id3][] = $data[12];
							}
							if(isset($data[24]) && $data[24] != ''){
								$allArr[$data[0]]['images'][] = $data[24];
							}
						}
					}
				}
			}
			fclose($handle);
		}
		foreach($allArr as $arr){
			$product = new \Model\Product();
			$product->slug = $arr['slug'];
			$product->name = $arr['name'];
			$product->description = $arr['description'];
			$product->tags = $arr['tags'];
			$product->price = $arr['price'];
			$product->msrp = $arr['msrp'] != ''?$arr['msrp']:$arr['price'];
			$product->meta_title = $arr['meta_title'];
			$product->meta_description= $arr['meta_description'];
			$product->options = json_encode($arr['options']);
			if(array_key_exists('',$arr['options'])){
				unset($arr['options'][""]);
			}
			if(isset($arr['options']['size'])){
				$product->size = json_encode($arr['options']['size']);
			}
			$arrColor = [];
			if(isset($arr['options']['color'])){
				foreach($arr['options']['color'] as $color){
					if($c = \Model\Color::getItem(null,['where'=>"name = '{$color}'"])){
						$arrColor[] = $c->id;
					} else {
						$strReplace = str_replace(',','-',trim($color));
						$strReplace = str_replace('/','-',trim($strReplace));
						$strReplace = str_replace('&','-',trim($strReplace));
						$strReplace = str_replace(' ','-',trim($strReplace));
						$c = new \Model\Color();
						$c->name = $color;
						$c->slug = $strReplace;
						$c->save();
						$arrColor[] = $c->id;
					}
				}
				$product->color = json_encode($arrColor);
			}
			$product->save();
			foreach($arr['images'] as $image){
				$prod_image = new \Model\Product_Image();
				$uniqueId = uniqid($product->slug);
				file_put_contents(__DIR__ . '/../../content/uploads/products/' . $uniqueId . '.jpg', file_get_contents($image));
				$prod_image->product_id = $product->id;
				$prod_image->image = $uniqueId . '.jpg';
				$prod_image->save();
			}
		}


		$n = new \Notification\MessageHandler("Import success!");
		$_SESSION["notification"] = serialize($n);
		redirect(SITE_URL . 'admin/products');


	}
	function export(Array $params = []){
		$this->template = false;
		$br = $_GET['brand'];
		$this->_viewData->hasCreateBtn = true;
		$this->_viewData->product = \Model\Product::getList(['where'=>' brand = '.$br]);
		$brand = \Model\Brand::getItem($br);
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$brand->name.'.csv');
		$output = fopen('php://output', 'w');
		$t=array("Name","Mpn","Sell price","MSRP", "Availability", 'Image File', 'Mpn 2', 'Mpn 3');
		fputcsv($output, $t);
		$row ='';
		foreach($this->_viewData->product as $b) {
		$row = array($b->name,$b->mpn,$b->price,$b->msrp, $b->getAvailability(), $b->featured_image,$b->mpn2,$b->mpn3);
		fputcsv($output, $row);  
		}   
		parent::index($params);
	}

/*	function import(Array $params = []){
		$this->template = false;
		$br = $_GET['brand'];
		$this->_viewData->hasCreateBtn = true;
		$this->_viewData->product = \Model\Product::getList(['where'=>' brand = '.$br]);


				foreach($files['csv'] as $key=>$val) {
	
			$temp_file_name = $files['tmp_name'][$key];
		
	
				$img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size'][$key]);
				if($img_path===false) {
					move_uploaded_file($temp_file_name, $upload_path . $file_name);
				}
				//move_uploaded_file($temp_file_name, $upload_path . $file_name);
				 
				if(count($sizes)>0) {
					foreach($sizes as $key=>$val) {
						if(is_array($val) && count($val)==2) {
							resize_image_by_minlen($upload_path,
							$file_name, min($val), $filetype);
	
							$path = images_thumb($file_name,min($val),
									$val[0],$val[1], file_format($file_name));
							$image_size_str = implode('_',$val);
							copy($path, $upload_path.$image_size_str.$file_name);
						}
					}
				}
	
				$product_image->image = $file_name;
				$product_image->product_id = $obj_id;
				$product_image->display_order = $counter;
				$product_image->save();
			}
			$counter++;
		}*/



		/*$fd = fopen("test.csv", "r"); 
		while (($arr = fgetcsv($fd, 1024, ";")) !== FALSE) 
		{ $sql = “insert into my_group ("Name","Mpn","Price") values(‘$arr[0]’, ‘$arr[1]’, ‘$arr[2]’)”; } 
		fclose($fd);*/

 
		/* }   
		parent::index($params);
	}*/



	public function update(Array $arr = []) {
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);

		$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
//		$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
//		$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
//		$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
//		foreach($this->_viewData->product_notes as $note){
//			$note->admin = \Model\Admin::getItem($note->admin_id);
//			$note->insert_time = new DateTime($note->insert_time);
//			$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
//		}
		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_images = [];
		$this->_viewData->brands = \Model\Brand::getList(['orderBy'=>'name']);
		$this->_viewData->categories = \Model\Category::getList(['orderBy'=>'name']);
		$this->_viewData->colors = \Model\Color::getList(['orderBy'=>'name']);
//		$this->_viewData->collections = \Model\Collection::getList(['orderBy'=>'name']);
//		$this->_viewData->materials = \Model\Material::getList(['orderBy'=>'name']);

		parent::update($arr);
	}


	public function update_post()
	{

		if($hottestDeal = \Model\Hottest_Deal::getItem(null,['where'=>['product_id'=>$_POST['id']]])){
			$hottestDeal->original_price = $_POST['price'];
			$hottestDeal->save();
		}
		$arr = $_POST['product_category'];
		if(isset($_POST['product_category']) && count($_POST['product_category']) > 0){
			foreach($_POST['product_category'] as $pCatId){
				$pCat = \Model\Category::getItem($pCatId);
				$parentId = $pCat->parent_category;
				if($parentId != 0) {
					do {
						$parent = \Model\Category::getItem($parentId);
						if (!in_array($parent->id, $arr)) {
							$arr[] = (string)$parent->id;
						}
						$parentId = $parent->parent_category;
					} while ($parent->parent_category > 0);
				}
			}
		}
		if(count($_POST['color']) > 0){
			$_POST['color'] = json_encode($_POST['color']);
		}
		if(count($_POST['size']) > 0){
			$_POST['size'] = json_encode($_POST['size']);
		}
		$_POST['product_category'] = $arr;
		$arr = ['length','width','height', 'quantity'];
		foreach($arr as $ar){
			if(isset($_POST[$ar]) && $_POST[$ar] == null){
				$_POST[$ar] = 0;
			}
		}
		parent::update_post();
	}


	public function multi_update(Array $arr = []) {
		$id_array = $_GET;
		
		$q = implode("," , $id_array);

		if (count($_GET) <= 1){
			$n = new \Notification\ErrorHandler('You must select at least one product to Multi Edit.');
           	$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'products');
		}

		$this->_viewData->q=$q;
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
		$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
		$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_notes as $note){
			$note->admin = \Model\Admin::getItem($note->admin_id);
			$note->insert_time = new DateTime($note->insert_time);
			$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_images = [];
		$this->_viewData->brands = \Model\Brand::getList();
		$this->_viewData->categories = \Model\Category::getList();
		$this->_viewData->colors = \Model\Color::getList();
		$this->_viewData->collections = \Model\Collection::getList();
		$this->_viewData->materials = \Model\Material::getList();

		parent::update($arr);
	}
	
	public function multi_update_post(){

		$q = explode(",", $_POST['id']);
		$a = $_POST;
		
		foreach($q as $productId){
			if (is_numeric($productId)){
				$product = Model\Product::getItem($productId);
				
				if ((isset($a['product_category'])) && (count($a['product_category'])>0)  )  {
					for ($x = 0; $x < count($a['product_category']); $x++){
						$category = \Model\Product_Category::getItem(null, ['where' => "product_id = '" . $productId . "' and category_id = ".$a['product_category'][$x]]);
						if (!isset($category))	{
							$category = new Model\Product_Category();
							$category->product_id = $productId;
							$category->category_id = $a['product_category'][$x];
							$category->save();
						}
					}
				}

				if ((isset($a['product_collection'])) && (count($a['product_collection'])>0)  )  {
					for ($x = 0; $x < count($a['product_collection']); $x++){
						$collection = \Model\Product_Collection::getItem(null, ['where' => "product_id = '" . $productId . "' and collection_id = ".$a['product_collection'][$x]]);
						if (!isset($collection))	{
							$collection = new Model\Product_Collection();
							$collection->product_id = $productId;
							$collection->collection_id = $a['product_collection'][$x];
							$collection->save();
						}
					}
				} 

				if ((isset($a['product_material'])) && (count($a['product_material'])>0)  )  {
					for ($x = 0; $x < count($a['product_material']); $x++){
						$material = \Model\Product_Material::getItem(null, ['where' => "product_id = '" . $productId . "' and material_id = ".$a['product_material'][$x]]);
						if (!isset($material))	{
							$material = new Model\Product_Material();
							$material->product_id = $productId;
							$material->material_id = $a['product_material'][$x];
							$material->save();
						}
					}
				}

				foreach($a as $key1 => $value1)	{
					if (!empty($value1) && $key1 != "id"){
						$product->$key1 = $value1;
						$product->save();
					}
				}
			}
		}
		redirect(ADMIN_URL.'products');
	}

	 

	protected function afterObjSave($obj){
		if (isset($_POST['note'])){
			$note = new \Model\Note();
			$note->name = $_POST['note']['name'];
			$note->description = $_POST['note']['description'];
			$note->admin_id = \Emagid\Core\Membership::userId();
			$note->product_id = $obj->id;
			$note->save();
		}
	}
	
	function upload_images($params) {
		header("Content-type:application/json");
		$data = [];
		$data['success'] = false;
		$data['redirect'] = false;
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
		if((int)$id>0) {
			$product = \Model\Product::getItem($id);
			if($product==null) {$data['redirect']=true;}
			$this->save_upload_images($_FILES['file'], $product->id, UPLOAD_PATH.'products'.DS,[]);
			$data['success'] = true;
		}
	
		//print_r($_FILES);
		echo json_encode($data);exit();
	}
	
	public function save_upload_images($files,$obj_id,$upload_path,$sizes=[]) {
		$counter = 1;
		// get highest display order counter if any images exist for specified listing
		$image_high = \Model\Product_Image::getItem(null,['where'=>'product_id='.$obj_id,'orderBy'=>'display_order','sort'=>'DESC']);
		if($image_high!=null) {
			$counter = $image_high->display_order+1;
		}
		foreach($files['name'] as $key=>$val) {
			$product_image = new \Model\Product_Image();
			$temp_file_name = $files['tmp_name'][$key];
			$file_name = uniqid() . "_" . basename($files['name'][$key]);
			$file_name = str_replace(' ', '_', $file_name);
			$file_name = str_replace('-', '_', $file_name);
	
			$allow_format = array('jpg', 'png', 'gif','JPG');
			$filetype = explode("/", $files['type'][$key]);
			$filetype = strtolower(array_pop($filetype));
			//$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
			//$filetype = array_pop($filetype);
			if($filetype == 'jpeg' || $filetype=="JPG"){$filetype = 'jpg';}
	
			if(in_array($filetype, $allow_format)){
	
				$img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size'][$key]);
				if($img_path===false) {
					move_uploaded_file($temp_file_name, $upload_path . $file_name);
				}
				 //move_uploaded_file($temp_file_name, $upload_path . $file_name);
				 
				if(count($sizes)>0) {
					foreach($sizes as $key=>$val) {
						if(is_array($val) && count($val)==2) {
							resize_image_by_minlen($upload_path,
							$file_name, min($val), $filetype);
	
							$path = images_thumb($file_name,min($val),
									$val[0],$val[1], file_format($file_name));
							$image_size_str = implode('_',$val);
							copy($path, $upload_path.$image_size_str.$file_name);
						}
					}
				}
	
				$product_image->image = $file_name;
				$product_image->product_id = $obj_id;
				$product_image->display_order = $counter;
				$product_image->save();
			}
			$counter++;
		}
	}

	public function search(){
		$products = \Model\Product::search($_GET['keywords']);

		echo '[';
		foreach($products as $key=>$product){
			echo '{ "id": "'.$product->id.'", "name": "'.$product->name.'", "featured_image": "'.$product->featured_image.'", "price":"'.$product->price.'", "msrp":"'.$product->msrp.'", "mpn":"'.$product->mpn.'" }';
			if ($key < (count($products)-1)){
				echo ",";
			}
		}
		echo ']';
	}   

	public function check_slug(){
		$slug = $_POST['slug'];
		$id = $_POST['id'];
		if($id>0){
			$list = \Model\Product::getItem(null,['where'=>"slug = '".$slug."' and id = '".$id."'"]);
				 if (isset($list) && count($list) == 1){
				 	echo "0";
				 }else{
					 	$list = \Model\Product::getList(['where'=>"slug = '".$slug."' "]);
					 if (isset($list) && count($list) > 0){
					 	echo "1";
					 }else{
					 	echo "0";
					 }
				 }
		}else{
			$list = \Model\Product::getList(['where'=>"slug = '".$slug."' "]);
				 if (isset($list) && count($list) > 0){
				 	echo "1";
				 }else{
				 	echo "0";
				 }
		}
		
	}   

	public function google_product_feed(){
        if(isset($_SESSION["google_feed_generated"]) && $_SESSION["google_feed_generated"] == 1){
            $this->_viewData->generated = true;
            unset($_SESSION["google_feed_generated"]);
        }
        $this->loadView($this->_viewData);
    }

    public function google_product_feed_post(){

    	$genders = [ 1 => 'unisex', 2 => 'Male', 3 => 'Female' ];
    	$type = "Apparel & Accessories > Jewelry > Watches";
    	$condition = "new";
        $availability = "in stock"; // "out of stock"
        $gpc = 201;
        $identifier_exists = "TRUE";
        $age_group = "Adult";

        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","w"); fclose($f); //clear the file
        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","a"); //open with append write mode

        $header = "id\ttitle\tdescription\tgoogle product category\tlink\timage link\tcondition\tavailability\tprice\tbrand\tmpn\tidentifier exists\tcolor\tage group\tgender\tproduct type";
        fwrite($f, $header);

        $sql = "SELECT product.id as id, product.name as name, product.description as description, product.slug as slug, ";
        $sql .= "product.featured_image as featured_image, product.price as price, product.mpn as mpn, brand.name as brand_name, ";
        $sql .= "color.name as color_name, product.gender as gender ";
        $sql .= "FROM product ";
        $sql .= "INNER JOIN brand ON (product.brand = brand.id) ";
        $sql .= "INNER JOIN color ON (product.color = color.id) ";
        $sql .= "WHERE product.active = 1 ";

        $products = $this->emagid->db->getResults($sql);
        $line_items = "";
        foreach($products as $product){
            $id = $product['id'];
            $title = $product['name'];

			if($product['description'] != '') {
				$description = str_replace('\\', '\\\\', $product['description']);
				$description = str_replace('<p>', ' ', $description);
				$description = str_replace('</p>', ' ', $description);
				$description = str_replace('<br>', ' ', $description);
				$description = str_replace('<br />', ' ', $description);
				$description = str_replace('<br/>', ' ', $description);
				$description = str_replace("\n", " ", $description);
				$description = str_replace('\n', ' ', $description);
				$description = str_replace("\r", " ", $description);
				$description = str_replace('\r', ' ', $description);
				$description = str_replace("\t", " ", $description);
				$description = str_replace('\t', ' ', $description);
				$description = str_replace(PHP_EOL, ' ', $description);
			} else {
				$description = $product['name'];
			}
            $link = "http://www.modernvice.com/product/".$product['slug']; //TODO st-dev check if valid url

			$image_link = $product['featured_image'];
			if (is_null($image_link) || trim($image_link) == '' || !file_exists(UPLOAD_PATH.'products'.DS.$image_link)){
//				$image_link = "http://www.modernvice.com".ADMIN_IMG.'modernvice_shoe.png';
				continue;
			} else {
				$image_link = "http://www.modernvice.com".UPLOAD_URL . 'products/' . $image_link; //TODO st-dev check if valid url
			}

            $price = $product['price'];
            $brand = $product['brand_name'];
            $mpn = $product['mpn'];
            //$color = $product['color_name'];
            $color = "Multicolor";
            $gender = isset($genders[$product['gender']])?$genders[$product['gender']]:$genders[1];

            $line_item = "\n".$id."\t".$title."\t".$description."\t".$gpc."\t".$link."\t".$image_link."\t".$condition."\t".$availability."\t".$price."\t".$brand."\t".$mpn."\t".$identifier_exists."\t".$color."\t".$age_group."\t".$gender."\t".$type;
            fwrite($f, $line_item);
        }
        fclose($f); 
        $_SESSION["google_feed_generated"] = 1;
        redirect(ADMIN_URL.'products/google_product_feed');
    }
     
}






















