<?php

use Emagid\Html\Form;
use Emagid\Core\Membership;

require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class ordersController extends adminController
{

    function __construct()
    {
        parent::__construct("Order");
    }

    public function index(Array $params = [])
    {
        //$order_by=$_GET['order'];
        //$limit=$_GET['limit'];
        if (Empty($_GET['sort']) || Empty($_GET['sort_param'])) {
            $params['queryOptions']['orderBy'] = "insert_time DESC";
        } else {
            $sort_by = $_GET['sort'];
            $sort_param = $_GET['sort_param'];
            $params['queryOptions']['orderBy'] = $sort_by . ' ' . $sort_param;
        }
        if (!Empty($_GET['status_show'])) {
            $st = explode('%2c', $_GET['status_show']);
            $where = "status in (";
            $it = 0;
            foreach($st as $queryString) {
                $where .= "'".$queryString."'";
                if(++$it<count($st)){
                    $where .= ',';
                }
            }
            $where .= ")";
            $params['queryOptions']['where'] = $where;
        }
        if (!Empty($_GET['how_many']))
        {
            $params['queryOptions']['page_size'] = $_GET['how_many'];
        }
        else{
            $params['queryOptions']['page_size'] = 10;
        }

        $this->_viewData->brands = \Model\Brand::getList(['orderBy'=>"name ASC"]);

        if(isset($_GET['brand']) && $_GET['brand'] != "") {
            $products = \Model\Product::getList(['where'=>"brand = '".$_GET['brand']."'"]);
            $sql = "SELECT DISTINCT ON (order_id)* FROM order_products WHERE product_id IN(";
            $iterator = 0;
            foreach($products as $prod){
                $sql .= "$prod->id";
                $sql .= ++$iterator < count($products) ? ',': ')';
            }
            $sql .= ';';

            $order = [];
            $product = [];
            $op = \Model\Order_Product::getList(['sql'=>$sql]);
            if($op){
                foreach($op as $oop) {
                    if(\Model\Order::getList(['where' => "id = $oop->order_id"])) {
                        $order[] = \Model\Order::getItem($oop->order_id);
                    }
                    foreach($order_prod = \Model\Order_Product::getList(['where'=>"order_id = $oop->order_id"]) as $orderProd) {
                        $pro = \Model\Product::getItem($orderProd->product_id);
                        $pro->brand_name = \Model\Brand::getItem($pro->brand)->name;
                        $product[$oop->order_id][] = $pro;
                    }
                }
            }
            $this->_viewData->orders = $order;
            $this->_viewData->products = $product;
            $this->loadView($this->_viewData);
        } else {
            parent::index($params);
        }
    }

    public function beforeLoadIndexView()
    {
        $this->_viewData->products = [];
        foreach ($this->_viewData->orders as $order) {
            $order->insert_time = new DateTime($order->insert_time);
            $order->insert_time = $order->insert_time->format('m-d-Y H:i:s');
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);
            $this->_viewData->products[$order->id] = [];
            foreach ($order_products as $order_product) {
                $product = \Model\Product::getItem($order_product->product_id);
                $product->brand_name = \Model\Brand::getItem($product->brand)->name;
                $this->_viewData->products[$order->id][] = $product;
            }
        }
    }

    public function beforeLoadUpdateView()
    {
        if (is_null($this->_viewData->order->viewed) || !$this->_viewData->order->viewed) {
            $this->_viewData->order->viewed = true;
            $this->_viewData->order->save();
        }

        $this->_viewData->order_status = \Model\Order::$status;

        $shipping_methods = \Model\Shipping_Method::getList(['orderBy' => 'shipping_method.id']);
        $this->_viewData->shipping_methods = [];
        foreach ($shipping_methods as $shipping_method) {
            $this->_viewData->shipping_methods[$shipping_method->id] = $shipping_method->name;
        }

        $this->_viewData->order_products = \Model\Order_Product::getList(['where' => "order_id=" . $this->_viewData->order->id]);
        foreach ($this->_viewData->order_products as $order_product) {
            $order_product->product = \Model\Product::getItem($order_product->product_id);
        }

        if (is_null($this->_viewData->order->user_id)) {
            $this->_viewData->order->user = null;
        } else {
            $this->_viewData->order->user = \Model\User::getItem($this->_viewData->order->user_id);
        }

        if (is_null($this->_viewData->order->coupon_code)) {
            $this->_viewData->coupon = null;
            $this->_viewData->savings = '0';
        } else {
            $this->_viewData->coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $this->_viewData->order->coupon_code . "'"]);
            if ($this->_viewData->order->coupon_type == 1) {//$
                $this->_viewData->savings = $this->_viewData->order->coupon_amount;
            } else if ($this->_viewData->order->coupon_type == 2) {//%
                $this->_viewData->savings = $this->_viewData->order->subtotal * $this->_viewData->order->coupon_amount / 100;
            }
        }

        if (!is_null($this->_viewData->order->cc_number)) {
            $this->_viewData->order->cc_number = '****' . substr($this->_viewData->order->cc_number, -4);
        }
    }

    public function mail(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        $mail = \Model\Mail_Log::getItem($params['id']);
        echo $mail->text;
        $this->template = FALSE;


    }

    public function print_packing_slip(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        $this->slip_pack($id);
        $this->template = FALSE;


    }


     public function upd_fraud(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';
 
         $post_array = array(
                                'ApiLogin' => 'YwZzFUKX66lx',
                                'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                                'Action' => 'getOrderStatus',
                                'OrderNumber' => $id
                            );

// Convert post array into a query string
                            $post_query = http_build_query($post_array);

// Do the POST
                            $ch = curl_init('https://www.eye4fraud.com/api/');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($ch);
                            curl_close($ch);
                            preg_match_all('~<value>(.*)</value>~', $response, $out_status);
                             
                                $order = \Model\Order::getItem($id);
                                $order->fraud_status =  $out_status[0][2];
                                $order->save();
                                redirect($_SERVER["HTTP_REFERER"]);
        $this->template = FALSE;


    }

    public function update_post()
    {
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {

            $order = \Model\Order::getItem($_POST['id']);

            /*if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
                $this->sendShippedEmail($order, $_POST['tracking_number']);
            }*/

            if (isset($_POST['status'])) {
                $order->status = $_POST['status'];
                if ($_POST['status'] !== $_POST['old_status']) {
                    $log_status = new \Model\Log_Status();
                    $log_status->order_id = $_POST['id'];
                    $log_status->status = $_POST['status'];
                    $log_status->admin_id = \Emagid\Core\Membership::userId();
                    $log_status->save();
                }


            }

            $order->tracking_number = $_POST['tracking_number'];
            if (isset($_POST['cc_number'])) {
                if ('****' . substr($order->cc_number, -4) != $_POST['cc_number']) {
                    $order->cc_number = $_POST['cc_number'];
                }
            }
            if (isset($_POST['cc_expiration_month'])) {
                $order->cc_expiration_month = $_POST['cc_expiration_month'];
            }
            if (isset($_POST['cc_expiration_year'])) {
                $order->cc_expiration_year = $_POST['cc_expiration_year'];
            }

            if (isset($_POST['cc_ccv'])) {
                $order->cc_ccv = $_POST['cc_ccv'];
            }


            if (isset($_POST['payment_method'])) {
                $order->payment_method = $_POST['payment_method'];
            }

            $order->bill_first_name = $_POST['bill_first_name'];
            $order->bill_last_name = $_POST['bill_last_name'];
            $order->bill_address = $_POST['bill_address'];
            $order->bill_address2 = $_POST['bill_address2'];


            if (isset($_POST['bill_state'])) {
                $order->bill_state = $_POST['bill_state'];
            }
            if (isset($_POST['bill_country'])) {
                $order->bill_country = $_POST['bill_country'];
            }


            $order->bill_city = $_POST['bill_city'];


            $order->bill_zip = $_POST['bill_zip'];

            $order->ship_first_name = $_POST['ship_first_name'];
            $order->ship_last_name = $_POST['ship_last_name'];
            $order->ship_address = $_POST['ship_address'];
            $order->ship_address2 = $_POST['ship_address2'];


            if (isset($_POST['ship_state'])) {
                $order->ship_state = $_POST['ship_state'];
            }
            if (isset($_POST['ship_country'])) {
                $order->ship_country = $_POST['ship_country'];
            }


            $order->ship_zip = $_POST['ship_zip'];
            $order->note = $_POST['note'];

            $order->phone = $_POST['phone'];
            $subtotal = $order->subtotal;
            if (!is_null($order->coupon_code)) {
                if ($order->coupon_type == 1) {
                    $subtotal = $subtotal - $order->coupon_amount;
                } else if ($order->coupon_type == 2) {
                    $subtotal = $subtotal * (1 - ($order->coupon_amount / 100));
                }
            }

            /*$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());  
            $orderchange = new \Model\Orderchange();  
            $orderchange->order_id = $_POST['id'];
            $orderchange->user_id =  $logged_admin->id;
            $orderchange->save() ; */


            if (!empty($_POST['mail'])) {
                $email = new \Emagid\Email();
                global $emagid;
                $emagid->email->from->email = 'orders@modernvice.com'; //TODO st-dev get email
                $mail_log = new \Model\Mail_Log();
                $mail_log->from_ = "orders@modernvice.com"; //TODO st-dev get email

                if (is_null($order->user_id)) {
                    $email->addTo($order->email);
                    $mail_log->to_ = $order->email;
                } else {
                    $email->addTo(\Model\User::getItem($order->user_id)->email);
                    $mail_log->to_ = \Model\User::getItem($order->user_id)->email;
                }
                $email->addBcc("orders@modernvice.com"); //TODO st-dev get email
                if ($_POST['status'] !== $_POST['old_status']) {
                    $subject_array = Array();
                    foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
                        $product = \Model\Product::getItem($order_product->product_id);
                        $brand = \Model\Brand::getItem($product->brand);
                        $subject_array[] = $brand->name;
                        $subject_array[] = $product->mpn;

                        //$subject_array[] = $order_product->id;
                    }
                    if ($_POST['status'] == "Shipped") {

                        $email->subject('Your order has shipped! ' . implode(" ", $subject_array) . " ModernVice Order #" . $order->id);
                        $mail_log->subject = 'Your order has shipped! ' . implode(" ", $subject_array) . "ModernVice Order #" . $order->id;
                    } else {
                        $email->subject('Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " ModernVice Order #" . $order->id);
                        $mail_log->subject = 'Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " ModernVice Order #" . $order->id;
                    }
                }
                $status = $_POST['status'];
                $text = $_POST['mail'];
                $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="http://modernvice.com/content/frontend/img/logo.png" alt="Luggage"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
    <tbody>
        <tr>
            <td style="padding:0in 0in 0in 0in">
                <p class=mm "MsoNormal" ><a href="http://modernvice.com/brands" target="_blank"><span  style="text-decoration:none;color:black;">BRANDS </span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/new-arrivals" target="_blank"><span style="text-decoration:none;color:black;">NEW ARRIVALS</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/men-s" target="_blank"><span style="text-decoration:none;color:black;">MEN`S</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/women-s" target="_blank"><span style="text-decoration:none;color:black;">WOMEN`S</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/accessories" target="_blank"><span style="text-decoration:none;color:black;">ACCESSORIES</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/pre-owned-clearance" target="_blank"><span style="text-decoration:none;color:black;">PRE-OWNED &amp; CLEARANCE</span></a>&nbsp;</p>
            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Order ' . $status . '</span></b> </p>
                        </td>
                        <td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</a></span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Client:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:green">' . $order->ship_first_name . ' ' . $order->ship_last_name . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';


                $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%">
                <tbody>
                     <p align="justify">' . $text . '</p>
                </tbody>
                </table>';

                $mail_log->text = $email->body;

                $mail_log->order_id = $order->id;
                $mail_log->save();
                // echo $email->body;
                // echo $email->subject;
                $email->send();

            }

            if (isset($_POST['shipping_method'])) {
                if ($order->shipping_method != $_POST['shipping_method']) {
                    $order->shipping_method = \Model\Shipping_Method::getItem($_POST['shipping_method']);
                    $costs = json_decode($order->shipping_method->cost);
                    $order->shipping_method->cost = 0;
                    $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
                    foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                        if (is_null($range) || trim($range) == '') {
                            $range = 0;
                        }
                        if ($subtotal >= $range && isset($costs[$key])) {
                            $order->shipping_cost = $costs[$key];
                        }
                    }
                    $order->shipping_method = $order->shipping_method->id;
                }
            }

            if (isset($_POST['ship_state'])) {
                if ($order->ship_state == 'NY') {
                    $order->tax_rate = 8.875;
                    $order->tax = $subtotal * ($order->tax_rate / 100);
                } else {
                    $order->tax_rate = null;
                    $order->tax = null;
                }
            }

            $order->total = $subtotal + $order->shipping_cost + $order->tax;

            if ($order->save()) {
                $n = new \Notification\MessageHandler('Order saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . $this->_content . '/update/' . $order->id);
            }

        }
        redirect(ADMIN_URL . $_POST['redirectTo']);
    }




    /*  public function update_post_status() {   
            $id = $_POST['id'];
            $status = $_POST['status'];
            $order = \Model\Order::getItem($_POST['id']);
            $order->status = $status;
            $order->save();
        }*/


    /*     private function sendShippedEmail($order, $trackingNumber){
            $email = new \Emagid\Email();
            if (is_null($order->user_id)){
                $email->addTo($order->email);
            } else {
                $email->addTo(\Model\User::getItem($order->user_id)->email);
            }
            $email->bccTo("orders@modernvice.com");
            $email->subject('Order shipped');
            $email->body = '<p><a href="www.modernvice.com"><img src="http://modernvice.com/content/frontend/img/logo.png" /></a></p>'
                          .'<p>Your modernvice order was shipped!</p>'
                          .'<p>The tracking number is: <b>'.$trackingNumber.'</b></p>'
                          .'<p>Regards,<br />modernvice Team</p>'
                          .'<p>Find us on <a href="https://www.facebook.com/pages/modernvicecom/202088860134">Facebook</a> and <a href="https://twitter.com/modernvice">Twitter</a>.</p>';
            $email->send();
        } */

    public function getCcNumber()
    {
        if (isset($_POST['password']) && $_POST['password'] == 'test5343') {
            echo json_encode(\Model\Order::getItem($_POST['order_id'])->cc_number);
        } else {
            echo json_encode("false");
        }
    }

    public function unlock()
    {
        if (isset($_POST['pwd']) && $_POST['pwd'] == 'test5343') {
            echo json_encode("ok");
        } else {
            echo json_encode("false");
        }
    }

    public function search()
    {
        $orders = \Model\Order::Search($_GET['keywords']);

        echo '[';
        foreach ($orders as $key => $order) {
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);

            echo '{ "id": "' . $order->id . '", 
            "status": "' . $order->status . '", 
            "insert_time": "' . $order->insert_time . '", 
            "tracking_number": "' . $order->tracking_number . '", 
            "status": "' . $order->status . '",
            "bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
            "products": "'; ?><? foreach ($order_products as $op) {
                $product = \Model\Product::getItem($op->product_id);
                echo $product->name;
                echo "<br>";
            } ?><? echo '",' ?><? echo '"brands": "'; ?><? foreach ($order_products as $op) {
                $product = \Model\Product::getItem($op->product_id);
                $brand = \Model\Brand::getItem($product->brand);
                echo $brand->name;
                echo "<br>";
            } ?><? echo '","total": "' . number_format($order->total, 2) . '"}';
            if ($key < (count($orders) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }


    public function search_by_mpn()
    {
        $mpn = trim($_GET['keywords']);
        $mpn = str_replace('%20', ' ', $mpn);
        $mpn = str_replace('+', ' ', $mpn);
        $mpn = str_replace('%2F', '/', $mpn);
        $mpn = urldecode($mpn);

        $product = \Model\Product::getList(['where' => "mpn = '$mpn'"]);
        $a = Array();

        echo '[';
        foreach ($product as $pr) {
            $order_product = \Model\Order_Product::getList(['where' => 'product_id = ' . $pr->id]);


            foreach ($order_product as $op) {
                $orders = \Model\Order::getList(['where' => 'id = ' . $op->order_id]);


                foreach ($orders as $order) {

                    $a[] = '{ "id": "' . $order->id . '", 
            "insert_time": "' . $order->insert_time . '", 
            "tracking_number": "' . $order->tracking_number . '",
            "status": "' . $order->status . '",
            "bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
            "products": "' . $pr->name . '",
            "total": "' . $order->total . '"}';


                }
            }
        }

        echo implode(',', $a);
        echo "]";

    }


    public function pay(Array $params = [])
    {
        if (isset($params['id']) && is_numeric($params['id'])) {

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)) {

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = ' . $order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])) {
                    $refTransId = $localTransaction[0]['ref_trans_id'];

                    $transaction = new AuthorizeNetAIM;
                    $response = $transaction->priorAuthCapture($refTransId);

                    $n = new \Notification\MessageHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                }

                redirect(ADMIN_URL . 'orders/update/' . $order->id);

            }

        }

        redirect(ADMIN_URL . 'orders');
    }

    private function slip_pack($order)
    {
        global $emagid;
        $emagid->email->from->email = 'orders@modernvice.com';

        $email = new \Emagid\Email();

        $order = \Model\Order::getItem($order);


        $email->subject('Order packing slip!');///wtf?
        $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="http://modernvice.com/content/frontend/img/logo.png" alt="Luggage"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
    <tbody>
        <tr>
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal" ><a href="http://modernvice.com/brands" target="_blank"><span  style="text-decoration:none;color:black;">BRANDS </span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/new-arrivals" target="_blank"><span style="text-decoration:none;color:black;">NEW ARRIVALS</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/men-s" target="_blank"><span style="text-decoration:none;color:black;">MEN`S</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/women-s" target="_blank"><span style="text-decoration:none;color:black;">WOMEN`S</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/accessories" target="_blank"><span style="text-decoration:none;color:black;">ACCESSORIES</span></a>&nbsp;</p>
            </td> 
            <td style="padding:0in 0in 0in 0in">
                <p class="MsoNormal"><a href="http://modernvice.com/category/pre-owned-clearance" target="_blank"><span style="text-decoration:none;color:black;">PRE-OWNED &amp; CLEARANCE</span></a>&nbsp;</p>
            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Packing slip</span></b> </p>
                        </td>
                        <td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Number:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Date:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->insert_time . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';

        /* if ($order->payment_method == 1) {
            $email->body .= 'We have received your order and we are currently processing it.<br>';
            $email->body .= 'Your credit card will not be charged until the item’s availability has been established, at which time you will receive a confirmation email.';
            $email->body .= '<p>Then, as soon as your item ships you will receive another email with the tracking information.</p>';
            $email->body .= '<p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } else if ($order->payment_method == 2) {
            $email->body .= '<p>We will contact you shortly for more information. If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } */


        $shp = array($order->ship_first_name, $order->ship_last_name, $order->ship_address, $order->ship_address2,
            $order->ship_country, $order->ship_city, $order->ship_state, $order->ship_zip);


        $blng = array($order->bill_first_name, $order->bill_last_name, $order->bill_address, $order->bill_address2,
            $order->bill_country, $order->bill_city, $order->bill_state, $order->bill_zip);


        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:9.75pt">
                        <td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:9.75pt"></td>
                    </tr>
                    <tr style="height:41.25pt">
                        <td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 15.0pt 15.0pt;height:41.25pt">
                            <p><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Dear ' . $order->bill_first_name . ' ' . $order->bill_last_name . ',</span></b></p>
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Thank you for your order. Please retain this email confirmation as your official purchase receipt.</span></p>
                        </td>
                    </tr>
                    <tr style="height:20.25pt">
                        <td width="50%" style="width:50.0%;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping to:</span></b> </p>
                        </td>
                        <td width="50%" style="width:50.0%;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Billing to:</span></b> </p>
                        </td>
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>
                    <tr>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">
                            ' . $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . ' <br>
                            ' . $order->ship_zip . ' ' . $order->ship_country . '                       
                            </span></p>
                        </td>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">';
        if (count(array_diff($shp, $blng)) > 0) {
            $email->body .= $order->bill_first_name . ' ' . $order->bill_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->bill_address . ' ' . $order->bill_address2 . '<br>
                            ' . $order->bill_city . ' ' . $order->bill_state . '<br>
                            ' . $order->bill_zip . ' ' . $order->bill_country;
        } else {
            $email->body .= $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . '<br>
                            ' . $order->ship_zip . ' ' . $order->ship_country;
        }
        $email->body .= '</p>
                        </td>
                    </tr>
                </tbody>
            </table>';


        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:20.25pt">
                        <td width="255" colspan="2" style="width:191.25pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Items</span></b> </p>
                        </td>
                        <td width="146" style="width:109.5pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping Method</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Qty</span></b> </p>
                        </td>
                         
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="6" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>';

        foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {


            $final_sale = $order_product->final_sale;
            If ($final_sale > 0) {
                $fin = 'This is final sale!';
            } else {
                $fin = '';
            }
            $email->body .= '<tr style="height:75.0pt">'
                . '<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal"><img  width="100px!important;" src="http://modernvice.com/content/uploads/products/' . \Model\Product::getItem($order_product->product_id)->featured_image . '"></p></td>'
                . '<td width="170" valign="top" style="width:127.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 3.75pt 0in;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042"> ' . \Model\Product::getItem($order_product->product_id)->name . ' ' . $fin . '</span></p>
                        </td>'
                . '<td width="140" valign="top" style="width:105.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 3.75pt 7.5pt;height:75.0pt">
                            <p><strong><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . \Model\Shipping_Method::getItem($order->shipping_method)->name . '</span></strong><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042"></span></p></td>'

                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order_product->quantity . '</span></b></p>
                        </td> </tr>     
                ';
        }
        $email->body .= '</tbody>
            </table>
            <p class="MsoNormal">&nbsp;</p>';
        if (!is_null($order->coupon_code)) {
            $savings = ($order->coupon_type == 1) ? '$' : '';
            $savings .= number_format($order->coupon_amount, 2);
            $savings .= ($order->coupon_type == 2) ? '%' : '';
        } else {
            $savings = 0;
        }

        $show_saving = 'display:none;';

        $a = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt;padding-left: 604px;">
                <tbody>
                    <tr>
                         
                        <td style="padding:0in 0in 0in 0in">
                            <table border="0" cellspacing="0" cellpadding="0" width="200" style="width:150.0pt">
                                <tbody>
                                    <tr style="height:20.25pt">
                                        <td width="200" colspan="2" style="width:150.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Order Summary</span></b></p>
                                        </td>
                                    </tr><tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sub Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->subtotal, 2) . '</span></p>
                                        </td>
                                    </tr>
                                     
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->shipping_cost, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sales Tax:</span></b> </p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->tax, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->total, 2) . '</span></b></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>';


        /*  $email->addTo($order->email);
          $email->addBcc('info@modernvice.com');
          $email->addBcc('orders@modernvice.com');
          $mail_log = new \Model\Mail_Log();
          $mail_log->to_ = $order->email;
          $mail_log->from_ = "orders@modernvice.com";
          $mail_log->subject = "Order packing slip";
          $mail_log->order_id = $order->id;
          $mail_log->text = $email->body;
          $mail_log->save();*/
        echo $email->body; //sends to the customer


    }

}
