<?php

class achieveController extends adminController{

    function __construct()
    {
        parent::__construct('Admin');
    }

    public function index(array $params = [])
    {
        $this->_viewData->files = glob('content/report/*.pdf');
        parent::index($params);
    }



}