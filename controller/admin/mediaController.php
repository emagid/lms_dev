<?php

use Emagid\Html\Form;

class mediaController extends adminController {

  	function __construct(){
  		  parent::__construct("Media", 'media');
  	}
  	
    function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    public function upload_post()
    {
        $mediaModel = new Emagid\Media();
        if(count($_FILES) == 1){
            $media = new \Model\Media();
            foreach($_FILES as $fileType=>$file){
                if ($file['error'] == 0){
                    $mediaModel->upload($file, UPLOAD_PATH.'media'.DS);
                    $media->name = $mediaModel->fileName;
                    $media->file = $mediaModel->fileName;
                    $media->save();
                }
            }
        }

        if(isset($_POST['listing']) && $_POST['listing']){
            $listingMedia = new \Model\Listing_Media();
            $listingMedia->listing_id = $_POST['listing'];
            $listingMedia->media_id = $media->id;
            $listingMedia->save();
        }
    }

    public function update_post()
    {

    }
}
