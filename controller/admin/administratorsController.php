<?php

use Emagid\Core\Membership,
	Model\Admin,
	Emagid\Html\Form;

class administratorsController extends adminController {
	
	function __construct(){
		parent::__construct("User");
	}

	function index(Array $params = [])
	{
		$this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['where'] = "role = 'superadmin'";
        $this->_viewData->admins = \Model\User::getAdmins();
        parent::index($params);

	}
	
	public function update(Array $arr = []) {
		$adm = new \Model\User(isset($arr['id'])?$arr['id']:null);
		if($adm->id != null && $adm->role != 'superadmin'){
            $n = new \Notification\ErrorHandler('User is not an administrator');
            $_SESSION["notification"] = serialize($n);

		    if($adm->role == 'student'){
                redirect("/admin/students/update/$adm->id");
            } else {
		        redirect("/admin/administrators/");
            }
        }
		$this->_viewData->all_admin_sections = Admin::$admin_sections_nested;
		$this->_viewData->admin_permissions = explode(",", $adm->permissions);
		parent::update($arr);
	}
	
	public function update_post() {
		$hash_password = true;
		$password = "";

		$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);

		$id = $_POST['id'];
		$admin = \Model\User::getItem($id);

		if($_POST['password'] == ""){
			$hash_password = false;
		}

		if(isset($_POST['admin_section'])) {
			$permissions = implode(",", $_POST['admin_section']);
		} else {
			$permissions = "";
		}
		$admin->permissions = trim($permissions, ',');

		if($hash_password) {
            $password = $_POST['password'];
            $hash = \Emagid\Core\Membership::hash($password);
            $salt = $hash['salt'];

            $ePassword = crypt($password,$salt);
            $admin->encrypted_password = $ePassword;
		}

		if ($admin->save()){
		 	$n = new \Notification\MessageHandler('Administrator saved.');
           	$_SESSION["notification"] = serialize($n);
		} else {
			$n = new \Notification\ErrorHandler($admin->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'administrators/update/'.$id);
		};
		
		redirect(ADMIN_URL.'administrators');
	}

	public function delete(Array $arr = []) {
      	$class = new $this->_model();
      	$class::delete($arr['id']);

      	$content = str_replace("\Model\\", "", $this->_model);
        $n = new \Notification\MessageHandler('Administrator deleted.');
       	$_SESSION["notification"] = serialize($n);

   		redirect(ADMIN_URL.'administrators');
    }


}
















































