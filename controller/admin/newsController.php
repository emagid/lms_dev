<?php

class newsController extends adminController {
	
	function __construct(){
		parent::__construct("Article");
	}
  	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

		$params['queryOptions'] = ['orderBy'=>'article.insert_time desc'];
		parent::index($params);
	}

	function delete_image($params) {
		$_POST['redirectTo'] = ADMIN_URL.'news/update/'.$params['id'];
		parent::delete_image($params);
	}

	function update_post() {
		if ($_POST['insert_time'] == ""){
			$_POST['insert_time'] = null;
		}
		parent::update_post();
	}
  	
}