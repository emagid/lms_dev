<?php

class opportunitiesController extends adminController{

    function __construct()
    {
        parent::__construct('Opportunities', 'opportunities');
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        parent::index($params); // TODO: Change the autogenerated stub
    }

    function update(Array $arr = [])
    {
        $this->_viewData->amenities = \Model\Amenity::getList();
        if(isset($arr['id'])) {
            $this->_viewData->contact = \Model\Opportunities::getItem($arr['id'])->getContact();
            $this->_viewData->appointments = \Model\Appointment::getList(['where' => "opportunities_id = {$arr['id']}"]);
            $this->_viewData->communications = \Model\Communication::getList(['where'=>"opportunity_id = {$arr['id']}"]);
        }
        parent::update($arr); // TODO: Change the autogenerated stub
    }

    function update_post()
    {
        $postContact = ['first_name','last_name','email','phone','comments','address','city','state','zip'];
        $contact = new \Model\Contact();
        foreach($postContact as $pc){
            if(isset($_POST[$pc]) && $_POST[$pc] != '') {
                $contact->$pc = $_POST[$pc];
            }
        }
        $contact->save();
        $_POST['contact_id'] = $contact->id;
        $amenities = array_map(function($amenity){return intval($amenity);},$_POST['amenities']);
        $_POST['amenities'] = json_encode($amenities);
        parent::update_post(); // TODO: Change the autogenerated stub
    }

    function sendEmail(){
        /*
         * to=>contact_id, from=>admin_id
         * */
        $to = $_POST['to'];
        $from = $_POST['from'];
        $toEmail = $_POST['to_email'];
        $fromEmail = $_POST['from_email'];
        $subject = $_POST['subject'];
        $body = $_POST['body'];
        $opportunity_id = $_POST['opportunity_id'];

        $comm = new \Model\Communication();
        $comm->opportunity_id = $opportunity_id;
        $comm->to_ = $to;
        $comm->from_ = $from;
        $comm->subject = $subject;
        $comm->body = $body;

        $sendToName = \Model\Contact::getItem($to)->fullName();
        $senderName = \Model\Admin::getItem($from)->full_name();

        $email = new Email\MailMaster();
        $mergeFields = [
            'CONTENT' => $body
        ];
        if($email->setTo(['email'=>$toEmail, 'name'=>$sendToName, 'type'=>'to'])->setFromName($senderName)->setFromAddress($fromEmail)->setSubject($subject)->setMergeTags($mergeFields)->setTemplate('pd-communication-email')->send()){
            $comm->status = 1;
            echo json_encode(['status'=>"success", 'message'=>"Email sent"]);
        } else {
            $comm->status = 0;
            echo json_encode(['status'=>"fail", 'message'=>"Failed to send"]);
        }
        $comm->save();
    }
}