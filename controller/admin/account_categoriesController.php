<?php

class account_categoriesController extends adminController{

    function __construct()
    {
        parent::__construct('Account_Category', 'account_categories');
    }

    public function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    public function update(Array $arr = [])
    {
        parent::update($arr);
    }

    public function update_post()
    {   

        parent::update_post(); // TODO: Change the autogenerated stub
    }    
}