<?php

use Emagid\Core\Membership,
Emagid\Html\Form,
Model\Admin,
Model\Banner; 

class dashboardController extends adminController {
	
	function __construct(){
		parent::__construct('Dashboard');
	}

	public function index(Array $params = []){
		$this->_viewData->new_orders = \Model\Order::getCount(['where'=>"viewed is null or viewed = false"]);

		$this->_viewData->recent_accounts = \Model\Account::getList(['orderBy'=>'created_at DESC', 'limit'=>10]);
		$this->_viewData->recent_students = \Model\User::getList(['where'=>"role = 'student'", 'orderBy'=>'created_at DESC', 'limit'=>10]);
		$this->_viewData->monthly_new_accounts = \Model\Account::getList(['where'=>"EXTRACT(MONTH FROM created_at) = ".date('m')." and EXTRACT(YEAR FROM created_at) = ".date('Y')]);
		
		$this->_viewData->total_monthly_new_accounts = 0;

		if(count($this->_viewData->monthly_new_accounts) > 0){
			$this->_viewData->total_monthly_new_accounts = count($this->_viewData->monthly_new_accounts);
		}
		else{
			$this->_viewData->total_monthly_new_accounts = 0;
		}

		$this->_viewData->new_questions = [];//\Model\Question::getList(['where'=>"status = '".\Model\Question::$status[0]."'"]);
		$this->_viewData->active_accounts = \Model\Account::getCount(['where'=>"status = 'active'"]);

		$this->_viewData->monthly_sales = \Model\Order::getList(['where'=>"status = '".\Model\Order::$status[4]."' and EXTRACT(MONTH FROM insert_time)::INTEGER = ".date('m')." and EXTRACT(YEAR FROM insert_time)::INTEGER = ".date('Y')]);

		$this->_viewData->monthly_sum = 0;
		foreach($this->_viewData->monthly_sales as $sale){
			$this->_viewData->monthly_sum += $sale->total;
		}

		
		// foreach($this->_viewData->recent_clients as $client){
		// 	$client->insert_time = new DateTime($client->insert_time);
		// 	$client->insert_time = $client->insert_time->format('m-d-Y H:i:s');
		// }

		$this->_viewData->page_title = 'Dashboard';
		$this->setGraphsData();		
		$this->loadView($this->_viewData);
	}

	private function setGraphsData(){
		$dataset = [
			'accounts'=>[
				date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")))=>(object)[],
				date("Y-m")=>(object)[]
			],
			'students'=>[
				date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")))=>(object)[],
				date("Y-m")=>(object)[]
			]
		];
		// var_dump($dataset['accounts']);
		// var_dump($dataset['students']);
		// exit;

		foreach($dataset as $period=>$set){
			// var_dump($period === 'accounts');
			if($period === 'accounts'){
				foreach($set as $date=>$data){
					$year = explode('-', $date)[0];
					$month = explode('-', $date)[1];
					// var_dump($year);
					// var_dump($month);
					// var_dump($period);
					// var_dump($date);
					// exit;
					$dataset[$period][$date] = \Model\Account::getAccountDashBoardData($year, $month);
				}
			}else{
				foreach($set as $date=>$data){
					$year = explode('-', $date)[0];
					$month = explode('-', $date)[1];
					// var_dump($year);
					// var_dump($month);
					// var_dump($period);
					// var_dump($date);
					// exit;
					$dataset[$period][$date] = \Model\Account::getStudentDashBoardData($year, $month);
				}
			}
		}
		// exit;
		$this->_viewData->graphsData = $dataset;
	}
}










































