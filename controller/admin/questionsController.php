<?php

class questionsController extends adminController {
    
    function __construct(){
        parent::__construct("Question","questions");
    }
    
    function index(Array $params = []){
        
        parent::index($params);
    }

    function update(Array $arr = []){
    	$qid = $arr['id'];
    	
        // $q = new $this->_model(isset($arr['id'])?$arr['id']:null);
        $this->_viewData->answers = \Model\Answers::getList(['where'=>"question_id = $qid"]);
        parent::update($arr);
    }

    function update_post(){
        $question = \Model\Question::loadFromPost();
        if($question->save()){
            $course_module = \Model\Course_Modules::getItem($question->course_module_id);
            redirect(ADMIN_URL.'courses/update/'.$course_module->course_id);
        }
        // parent::update_post(); // TODO: Change the autogenerated stub
    }

    function getCourseModules(){
        $course_id = $_POST['course_id'];
        $course_modules = \Model\Course_Modules::getList(['where'=>"course_id = $course_id"]);
        foreach ($course_modules as $obj) {
            echo "<option value='$obj->id'>". $obj->title . "</option>"; 
        }
    }

    function update_answer_post(){
        $answer = \Model\Answers::loadFromPost();
        $qanswers = \Model\Answers::getList(['where'=>"question_id = $answer->question_id "]);
        if($qanswers){
            if($answer->correct == '1'){
                foreach ($qanswers as $qa) {
                    if($qa->correct == '1'){
                        $qa->correct = '0';
                        $qa->save();
                    }
                }
            }
        }
        if($answer->correct == ''){
            $answer->correct = 0;
        }
        if($answer->save()){
            redirect(ADMIN_URL.'questions/update/'.$answer->question_id);
        }
        
    }

    function delete_answer(){
        \Model\Answers::delete($_POST['id']);
        $response = ['status'=>true,'msg'=>'Answer Deleted'];
        $this->toJson($response);
    }

}





















// class questionsController extends adminController {
	
// 	function __construct(){
// 		parent::__construct("Question");
// 	}

//     public function beforeLoadIndexView(){
//     	$this->_viewData->hasCreateBtn = true;
//      	foreach($this->_viewData->questions as $question){
// 		 	$question->product = \Model\Product::getItem($question->product_id);
// 		 	$question->insert_time = new DateTime($question->insert_time);
// 		 	$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');

// 		}
			
//     }

//     public function beforeLoadUpdateView(){
//     	$this->_viewData->question_product = \Model\Product::getItem($this->_viewData->question->product_id);
//     	$this->_viewData->answers = \Model\Answer::getList();
//     	if (is_null($this->_viewData->question->viewed) || !$this->_viewData->question->viewed){
//     		$this->_viewData->question->viewed = true;
//     		$this->_viewData->question->save();
//     	}
// 	}
// 	 public function multi_delete(){
//     	 if(!$_POST){
//     	 		$n = new \Notification\ErrorHandler("You have to select as minimum 1 question");
// 	           	$_SESSION["notification"] = serialize($n);
// 	           	redirect(ADMIN_URL.'questions/');
//     	 }else{
//     	 	foreach ($_POST as $key => $value) {
//     			$this->emagid->getDb()->execute("DELETE from question  where  id = '" . $value. "' ");
//     	 	}
//     	 		$n = new \Notification\MessageHandler('Questions removed!');
// 	           	$_SESSION["notification"] = serialize($n);
// 	           	redirect(ADMIN_URL.'questions/');
//     	 }
// 	}

// 	function update(Array $arr = []){
//         // $this->_viewData->courses = \Model\Course::getList(['where'=>"active = 1"]);
//         // $q = new $this->_model(isset($arr['id'])?$arr['id']:null);
//         // $this->_viewData->answers = \Model\Answers::getList(['where'=>"question_id = {$q->id}"]);
//         // $this->_viewData->c_answers = [];
//         // foreach ($this->_viewData->answers as $answer){
//         //     $this->_viewData->c_answers[$answer->id] = $answer->text;
//         // }

//         parent::update($arr);
//     }
// 	public function update_post(Array $params = []){
		
// 		// redirect(ADMIN_URL.'questions');
// 	}
  	
// }