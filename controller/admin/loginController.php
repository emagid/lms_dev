<?php

use Emagid\Core\Membership;

/**
 * Base class to controll login
 */

class loginController extends \Emagid\Mvc\Controller {
	
	public function login(){

        $logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());
        if(\Emagid\Core\Membership::isAuthenticated() && \Emagid\Core\Membership::isInRoles(['admin', 'venue'])){
            redirect('/admin/dashboard/index');
        }

        $model = new \stdClass();
        $model->page_title = 'Login';
        $model->logged_admin = null;
        $model->admin_sections = [];
		$this->loadView($model);
	}
	
	public function login_post()
	{	
		if (\Model\Admin::login($_POST['email'],$_POST['password'])){
			// save user login
			$loggedAdmin = \Model\User::getItem(\Emagid\Core\Membership::userId());
			$now = \Carbon\Carbon::now();
			$loggedAdmin->last_sign_in_at = $now;
			$loggedAdmin->zlast_sign_in_ip = $_SERVER['HTTP_CLIENT_IP'];
			$loggedAdmin->last_sign_in_at = $now;
			$loggedAdmin->zlast_sign_in_ip = $_SERVER['HTTP_CLIENT_IP'];
			$loggedAdmin->save();
			redirect(ADMIN_URL . 'dashboard/index');
		} 
 
		$model = new \stdClass();
		$model->errors = ['Invalid username or password'];
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}

	public function logout(){
		Membership::destroyAuthenticationSession();
		redirect(ADMIN_URL . 'login');
	}

}