<?php

use Emagid\Html\Form,
    Emagid\Pagination,
    Emagid\Core\Membership;

/**
 * V1.8
 * Base class to be extended for admin controllers
 */

class manageController extends siteController {

    protected $_model = "";
    protected $_content = "";
    
    protected $_pageSize = 10;

    function __construct($model = null, $content = null){
        parent::__construct();

        $this->_model = "\Model\\".$model;
        $this->_content = is_null($content)?strtolower($model).'s':$content;
        if ($this->emagid->route['action'] == 'update'){
            $this->viewData->page_title = 'Manage '.ucwords(str_replace('_', ' ', $model));
        } else {
            $this->viewData->page_title = 'Manage '.ucwords(str_replace('_', ' ', $this->_content));
        }
    }
    
    public function index(Array $params = []){
        if (isset($params['limit'])){
            $options = ['where'=>'active = 1', 'page_size'=>$params['limit']];
        }else{
            $options = ['where'=>'active = 1', 'page_size'=>$this->_pageSize];
        }
        
        if (isset($params['queryOptions'])){
            $options = array_merge($options, $params['queryOptions']);
        }

        $page = new Pagination($this->_model, $options);
        $list = $page->getList();
        $page->current_page_index += 1;
        $this->viewData->pagination = $page;
        
        $content = $this->_content;
        $this->viewData->$content = $list;

        $this->beforeLoadIndexView();
        if (isset($params['overrideView'])){
            $this->loadView($params['overrideView'], $this->viewData);
        } else {
            $this->loadView($this->viewData);
        }
    }
    
    public function update(Array $arr = []){
        $obj = new $this->_model(isset($arr['id'])?$arr['id']:null);
        $content = str_replace("\model\\", "", strtolower($this->_model));
        if($content == 'user'){
            $content = 'student';
        }
        $this->viewData->$content = $obj;
        $this->viewData->form = new Form($obj);

        $this->beforeLoadUpdateView();

        if (isset($arr['overrideView'])){
            $this->loadView($arr['overrideView'], $this->viewData);
        } else {
            $this->loadView($this->viewData);
        }
    }
    
    public function update_post() {
        $obj = new $this->_model($_POST); 

        if ($obj->save()){
            foreach($_FILES as $fileType=>$file){
                if ($file['error'] == 0){
                    if ($fileType == 'image' || $fileType == 'featured_image' || $fileType == 'banner'){
                        $image = new \Emagid\Image();
                        $image->upload($_FILES[$fileType], UPLOAD_PATH.$this->_content.DS);
                        $this->afterImageUpload($image);
                        $obj->$fileType = $image->fileName;
                        $obj->save();
                    }
                }
            }
            $this->update_relationships($obj);
            $this->afterObjSave($obj);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' saved.');
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL.$this->_content.'/update/'.$obj->id);
        }

        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(SITE_URL.$this->_content);
        }
    }

    protected function update_relationships($obj) {
        $obj_class = new \ReflectionClass($obj); 
        $props = $obj_class->getStaticProperties();
        foreach ($props['relationships'] as $relationship) {
            $rel_class = new $relationship['class_name']();
            $rel_props = new \ReflectionClass($rel_class);
            $rel_props = $rel_props->getStaticProperties();
            if (isset($_POST[$relationship['name']])){
                $ids = $_POST[$relationship['name']];
                $existing = $rel_class::getList(['sql'=>'select * from '.$rel_props['tablename'].' where '.$relationship['remote'].' = '.$obj->id]);
                foreach($existing as $rel_obj){
                    $i = array_search($rel_obj->$relationship['remote_related'], $ids);
                    if ($i === false){
                        //deactivate record existent but not selected
                        $rel_class::delete($rel_obj->id);
                    } else {
                        //activate record existent and selected
                        if ($rel_obj->active != 1){
                            $this->emagid->getDb()->execute('update '.$rel_props['tablename'].' set active = 1 where id = '.$rel_obj->id);
                        }
                        unset($ids[$i]);                        
                    }
                }
                //insert new records
                foreach($ids as $newId){
                    $this->emagid->getDb()->execute('insert into '.$rel_props['tablename'].' ('.$relationship['remote'].', '.$relationship['remote_related'].') values ('.$obj->id.', '.$newId.')');
                }
            } else {
                //deactivate all records if none selected
                $this->emagid->getDb()->execute('update '.$rel_props['tablename'].' set active = 0 where '.$relationship['remote'].' = '.$obj->id);
            }
        }
    }
    
    public function delete(Array $arr = []) {
        $class = new $this->_model();
        $class::delete($arr['id']);

        $content = str_replace("\Model\\", "", $this->_model);
        $content = str_replace('_', ' ', $content);
        $n = new \Notification\MessageHandler(ucwords($content).' deleted.');
        $_SESSION["notification"] = serialize($n);

        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } elseif(isset($_GET['redirect']) && $_GET['redirect']){
            redirect($_GET['redirect']);
        } else {
            redirect(SITE_URL.$this->_content);
        }
    }
    
    function delete_image($params) {
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
        if($id>0) {
            $class = new $this->_model();
            $obj = $class::getItem($id);
            if($obj==null) {
                // record does not exist, redirect to index page
                redirect(SITE_URL.$this->_content);
            } else {
                // successful delete of image, redirect to item page
                if (isset($_GET['featured_image']) && $_GET['featured_image'] == 1){
                    $imgField = 'featured_image';
                } else if (isset($_GET['banner']) && $_GET['banner'] == 1){
                    $imgField = 'banner';
                } else {
                    $imgField = 'image';
                }
                if($obj->$imgField!="") {
                    \Emagid\Image::delete($this->_content.DS.$obj->$imgField);
                    $obj->$imgField = "";
                    $obj->save();
                    if (isset($_POST['redirectTo'])){
                        redirect($_POST['redirectTo']);
                    } else {
                        redirect(SITE_URL.$this->_content.'/update/'.$obj->id);
                    }
                }
            }
        }
        // invalid id for item or invalid file, redirect to index page
        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(SITE_URL.$this->_content);
        }
    }


     function delete_prod_image($params) {
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
        $product = \Model\Product_Image::getItem($id);
       $this->emagid->getDb()->execute('Delete  from  product_images where id='.$id);
        // invalid id for item or invalid file, redirect to index page
        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(SITE_URL.'products/update/'.$product->product_id);
        }
    }

    public function generateSlugs(){
        $model = '\Model\Brand';
        $list = $model::getList();

        foreach($list as $obj){
            if (is_null($obj->slug) || trim($obj->slug) == ""){
                $slug = $obj->name;
                $slug = preg_replace ( "/[^\w-]/" , '-' , $slug );
                $slug = preg_replace ( "/[-]+/" , '-' , $slug );
                $slug = strtolower($slug);
                $obj->slug = self::uniqueSlug($model, $slug);
                $obj->save();
            }
        }

//        dd($model::getList());
    }

    private static function uniqueSlug($model, $slug, $count = 0){
        $list = $model::getList(['where'=>"slug = '".$slug."' "]);
        if (isset($list) && count($list) > 0){
            return self::uniqueSlug($model, $slug.'-'.++$count, $count);
        } else {
            return $slug;
        }
    }

    public function download(Array $params = []){
        header('Content-Disposition: attachment; filename="'.$params['id'].'"');
        readfile(UPLOAD_PATH.$params['id']);
    }

    protected function afterImageUpload($image){}
    protected function afterObjSave($obj){}
    protected function beforeLoadIndexView(){}
    protected function beforeLoadUpdateView(){}

}





































