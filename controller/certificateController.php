<?php

class certificateController extends siteController
{

    public function index(Array $params = [])
    {
    	$course = $params['id'];
    	$this->viewData->course = \Model\Course::getItem($course);
        $this->loadView($this->viewData);
    }

    public function sendEmail(){
    	// var_dump($_POST);
    	// exit;
    	$image = $_POST['certificate'];
    	$user_id = $_POST['user'];
    	$course = $_POST['course'];
    	$user = \Model\User::getItem($user_id);
    	$student_email = "andrew@emagid.com";
    	$email = new \Email\MailMaster();
    	$mergeFields = [
            'NAME'=>"ABC",
            'COURSE_NAME'=>"XYZ",
            'LINK'=>$image
        ];

    	// $mergeTags = [
     //        'CONTENT'=>"<a href='".$image."'>Certificate</a>"
     //    ];
        $email->setTo(['email' => $student_email, 'type' => 'to'])->setSubject('SM2 Course Completion Certificate')->setTemplate('lms-course-certificate')->setMergeTags($mergeFields)->send();
        // $email->setTo(['email' => $student_email, 'type' => 'to'])->setSubject('SM2 Course Completion Certificate')->setTemplate('webairkiosk')->setMergeTags($mergeTags)->send();
        redirect("/student");
    }
}