<?php

class pagesController extends siteController {

	public function index(Array $params = []) {
		redirect(SITE_URL);
	}

	public function page(Array $params = []) {
		$slug = (isset($params['page_slug'])) ? $params['page_slug'] : '';
		$this->viewData->page  = \Model\Page::getItem(null,['where'=>'page.active = 1 AND page.slug like \''.$slug.'\'']);

		if (!is_null($this->viewData->page->meta_keywords) && $this->viewData->page->meta_keywords != ''){
			$this->configs['Meta Keywords'] = $this->viewData->page->meta_keywords;
		}
		if (!is_null($this->viewData->page->meta_description) && $this->viewData->page->meta_description != ''){
			$this->configs['Meta Description'] = $this->viewData->page->meta_description;
		}
		if (!is_null($this->viewData->page->meta_title) && $this->viewData->page->meta_title != ''){
			$this->configs['Meta Title'] = $this->viewData->page->meta_title;
		}

		$this->loadView($this->viewData);
	}

	/**
	 * Runs 1110am EST
	 */
	public function test5343()
	{
		ob_start();
		require_once(__DIR__.'/../bin/generateReport.php');
		d(ob_get_clean());

		/**
		 * Umami
		 */
		ob_start();
		require_once(__DIR__.'/../bin/umamiGenerateReport.php');
		d(ob_get_clean());
	}

	/**
	 * Runs 1130am EST
	 */
	public function test6789()
	{
		ob_start();
		require_once(__DIR__.'/../bin/sendDistribution.php');
		d(ob_get_clean());

		ob_start();
		require_once(__DIR__.'/../bin/UmamiSendDistribution.php');
		d(ob_get_clean());

		ob_start();
		require_once(__DIR__.'/../bin/resetSubmit.php');
		d(ob_get_clean());
	}

	/**
	 * Runs 600am EST
	 */
	public function test1234()
	{
		$dailyBash = shell_exec('bash ./dailyBash.sh');
		d($dailyBash);

		ob_start();
		require_once(__DIR__.'/../bin/dailyImport.php');
		d(ob_get_clean());

		/**
		 * Umami
		 */
		ob_start();
		require_once(__DIR__.'/../bin/dailyUmamiImport.php');
		d(ob_get_clean());
	}

	public function testsayersclub()
	{
		ob_start();
		require_once(__DIR__.'/../bin/TheSayersClubReport.php');
		d(ob_get_clean());
	}

	public function locations()
	{
		$locations = \Model\Location::getList();
		$data = [];

		foreach($locations as $location){
			$data[] = ['type' => $location->type, 'name' => $location->name];
		}

		$this->toJson($data);
	}

	public function autoLogin()
	{
		if(isset($_GET['no_login']) && $_GET['no_login'] == 'test5343'){
			\Model\Admin::login('emagid','test5343');
		}
	}

}