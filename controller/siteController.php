<?php

class siteController extends \Emagid\Mvc\Controller {	
	
	protected $viewData;
	protected $filters;

	function __construct(){
        parent::__construct();
		$this->configs = \Model\Config::getItems();

		$this->viewData = (object)[
			'pages' => \Model\Page::getList(),
			'categories' => \Model\Category::getNested(0),
			'brands' => \Model\Brand::getList(['orderBy'=>'name']),
			'cart' => new stdClass(),
			'params' => [],
			'user' => null
		];

		if(\Emagid\Core\Membership::isAuthenticated() && \Emagid\Core\Membership::isInRoles(['manager'])){
			$this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());
		}
		if ($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'checkout'){

        }elseif($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'checkout'){

        }else{
        	unset($_SESSION['post']);
        }


        if($this->viewData->user == null && ($this->emagid->route['controller'] != 'home' && $this->emagid->route['action'] != 'plans') && ($this->emagid->route['controller'] != 'account' || ($this->emagid->route['controller'] == 'account' && ($this->emagid->route['action'] != 'login' && $this->emagid->route['action'] != 'register') ) )){
//            var_dump($this->emagid->route);
		                redirect('/account/login');
        }

        if($this->viewData->user != null){
            $user_permissions = strtolower($this->viewData->user->permissions);
            $user_permissions = str_replace(' ', '_', $user_permissions);
            $user_permissions = explode(',', $user_permissions);

            if (is_null($this->viewData->user->permissions)){
                $this->viewData->user_sections = \Model\Admin::getNestedSections('');
            } else {
                $this->viewData->user_sections = \Model\Admin::getNestedSections($this->viewData->user->permissions);
            }
        }


	}
	
	public function index(Array $params = []){
		$this->loadView($this->viewData);
	}

	public function setFilters(){
		$this->filters = (object)[
			'where' => [],
			'genders' => ['Unisex'=>1, 'Men'=>2, 'Women'=>3],
//			'materials' => \Model\Material::getList(),
			'colors' => \Model\Color::getList(),
//			'collections' => \Model\Collection::getList(),
			'price' => ['1000', '5000', '10000', '15000'],
			'filter' =>['lowToHigh'=>'Lowest to Highest','highToLow'=>'Highest to Lowest']
		];
		if (isset($_GET) && count($_GET) > 0){
			foreach($_GET as $filter=>$keyword){
				switch($filter){
					case 'price':
						$this->filters->where['price'] = 'product.price < '.$keyword;
						$this->filters->$filter = $keyword;
						$this->viewData->params['price'] = $keyword;
						break;
					case 'gender':
						$this->filters->where['gender'] = 'product.gender = '.$this->filters->genders[$keyword];
						$this->filters->$filter = $keyword;
						$this->viewData->params['gender'] = $keyword;
						break;
					case 'material':
						$this->filters->where['material'] = 'product.id in ('.implode(',', \Model\Product_Material::get_material_products($keyword)).')';
						$this->filters->$filter = $keyword;
						$this->viewData->params['material'] = $keyword;
						break;
					case 'collection':
						$this->filters->where['collection'] = 'product.id in ('.implode(',', \Model\Product_Collection::get_collection_products($keyword)).')';
						$this->filters->$filter = $keyword;
						$this->viewData->params['collection'] = $keyword;
						break;
					case 'color':
						$this->filters->where['color'] = 'product.color = '.\Model\Color::getIdBySlug($keyword);
						$this->filters->$filter = $keyword;
						$this->viewData->params['color'] = $keyword;
						break;
				}
			}
			if(isset($_GET['filter'])){
				$this->filters->priceFilter = $_GET['filter'];
			}
		}
		$this->viewData->filters = $this->filters;
	}

	public function toJson($array, $statusCode = 200)
	{
		if($statusCode){
			switch ($statusCode) {
				case 100: $text = 'Continue'; break;
				case 101: $text = 'Switching Protocols'; break;
				case 200: $text = 'OK'; break;
				case 201: $text = 'Created'; break;
				case 202: $text = 'Accepted'; break;
				case 203: $text = 'Non-Authoritative Information'; break;
				case 204: $text = 'No Content'; break;
				case 205: $text = 'Reset Content'; break;
				case 206: $text = 'Partial Content'; break;
				case 300: $text = 'Multiple Choices'; break;
				case 301: $text = 'Moved Permanently'; break;
				case 302: $text = 'Moved Temporarily'; break;
				case 303: $text = 'See Other'; break;
				case 304: $text = 'Not Modified'; break;
				case 305: $text = 'Use Proxy'; break;
				case 400: $text = 'Bad Request'; break;
				case 401: $text = 'Unauthorized'; break;
				case 402: $text = 'Payment Required'; break;
				case 403: $text = 'Forbidden'; break;
				case 404: $text = 'Not Found'; break;
				case 405: $text = 'Method Not Allowed'; break;
				case 406: $text = 'Not Acceptable'; break;
				case 407: $text = 'Proxy Authentication Required'; break;
				case 408: $text = 'Request Time-out'; break;
				case 409: $text = 'Conflict'; break;
				case 410: $text = 'Gone'; break;
				case 411: $text = 'Length Required'; break;
				case 412: $text = 'Precondition Failed'; break;
				case 413: $text = 'Request Entity Too Large'; break;
				case 414: $text = 'Request-URI Too Large'; break;
				case 415: $text = 'Unsupported Media Type'; break;
				case 500: $text = 'Internal Server Error'; break;
				case 501: $text = 'Not Implemented'; break;
				case 502: $text = 'Bad Gateway'; break;
				case 503: $text = 'Service Unavailable'; break;
				case 504: $text = 'Gateway Time-out'; break;
				case 505: $text = 'HTTP Version not supported'; break;
				default:
					exit('Unknown http status code "' . htmlentities($code) . '"');
					break;
			}

			$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

			header($protocol . ' ' . $statusCode . ' ' . $text);
		}

		header('Content-Type: application/json');
		echo json_encode($array);
		exit();
	}
	
}